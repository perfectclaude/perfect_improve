﻿using UnityEngine;
using System.Collections;

public class ni_regulardisplay : ni_showdatafields
{
	tk2dTextMesh pph1;
	tk2dTextMesh pph2;
	tk2dTextMesh pph3;
	/*
	bool col1 = false;
	bool col2 = false;
	bool col3 = false;
*/
	Texture2D [] innertextures = new Texture2D[10];
	int ninnertextures = 0;

	void Awake()
	{
		_Awake ();
	}

	void OnDestroy()
	{
		for (int i=0;i<ninnertextures;i++)
		{
			Destroy (innertextures[i]);
		}
	}
		
	void LoadSetImage(GameObject ret,string itemfile)
	{
		if (ret)
		{
			ret.GetComponent<Renderer>().material = new Material (Shader.Find("Unlit/Transparent"));
			
			string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "/" + itemfile;
			if (System.IO.File.Exists(pathname))
			{
				byte[] imageData = System.IO.File.ReadAllBytes(pathname);
				if (imageData != null)
				{
					mytexture = new Texture2D(0,0,TextureFormat.RGBA32,false);
					mytexture.LoadImage(imageData);
					ret.GetComponent<Renderer>().material.mainTexture = mytexture;
					innertextures[ninnertextures] = mytexture;
					ninnertextures++;
					imageData = null;
				}
			}
		}
	}
	
	void SetModuleName(int id,string itemname,string infoname,string gfxname)
	{
		GameObject obj = module_2[0].FindInChildren("module_item"+(id+1));
		tk2dTextMesh tm = (tk2dTextMesh) obj.FindInChildren("item_name").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,itemname);
		tm = (tk2dTextMesh) obj.FindInChildren("info_name").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,infoname);
		
		obj = obj.FindInChildren("gfx_item");
		if (obj) 
		{
			if (gfxname == "")
				obj.SetActive(false);
			else
			{
				LoadSetImage(obj,gfxname);
			}
		}
		
		obj = module_2[1].FindInChildren("module_item"+(id+1));
		tm = (tk2dTextMesh) obj.FindInChildren("item_name").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,itemname);
		obj = obj.FindInChildren("gfx_item");
		if (obj) obj.SetActive(false);
	}

	void OnEnable()
	{
		int machine = ni_pointofinterest_button.MachineID;

		for (int i=0;i<ninnertextures;i++)
		{
			Destroy (innertextures[i]);
		}
		ninnertextures = 0;

		_OnEnable ();
		module_2[0].SetActiveRecursively (true);
		module_2[1].SetActiveRecursively (true);

		ni_button_ged ged = (ni_button_ged)gameObject.FindInChildren ("button_ged").GetComponent<ni_button_ged> ();
//		ged.GedLink = "http://10.1.1.200/lizaigne";
		ged.GedLink = "GED/1_Fiches_instructions/RPC_INS_03_C_Sertisseuse_a_chapeaux_DRAMECA.pdf";

		_ModuleAlert(false,false,false);
		switch (ni_regularmainloop.nitemnames[machine])
		{
		case 1 :
			_ModuleSetRows_2(true,false,false,true,false,false);
			SetModuleName(0,ni_regularmainloop.itemtitles[machine,0],ni_regularmainloop.itemdescriptions[machine,0],"item_icon_" + machine + "_1.png");
			break;
		case 2 :
			_ModuleSetRows_2(true,true,false,true,true,false);
			SetModuleName(0,ni_regularmainloop.itemtitles[machine,0],ni_regularmainloop.itemdescriptions[machine,0],"item_icon_" + machine + "_1.png");
			SetModuleName(1,ni_regularmainloop.itemtitles[machine,1],ni_regularmainloop.itemdescriptions[machine,1],"item_icon_" + machine + "_2.png");
			break;
		case 3 :
			_ModuleSetRows_2(true,true,true,true,true,true);
			SetModuleName(0,ni_regularmainloop.itemtitles[machine,0],ni_regularmainloop.itemdescriptions[machine,0],"item_icon_" + machine + "_1.png");
			SetModuleName(1,ni_regularmainloop.itemtitles[machine,1],ni_regularmainloop.itemdescriptions[machine,1],"item_icon_" + machine + "_2.png");
			SetModuleName(2,ni_regularmainloop.itemtitles[machine,2],ni_regularmainloop.itemdescriptions[machine,2],"item_icon_" + machine + "_3.png");
			break;
		}

		tk2dSprite sprt = (tk2dSprite)module_2[0].FindInChildren("alert_icon").GetComponent<tk2dSprite>();
		sprt.SetSprite(sprt.GetSpriteIdByName("icon_agenda"));
		sprt = (tk2dSprite)module_2[1].FindInChildren("alert_icon").GetComponent<tk2dSprite>();
		sprt.SetSprite(sprt.GetSpriteIdByName("icon_chrono"));

		// variable text in front of the description ...
		pph2 = (tk2dTextMesh)module_2[0].FindInChildren("module_item2").FindInChildren("info_head").GetComponent<tk2dTextMesh>();
		settext.ForceText(pph2,"");
		pph3 = (tk2dTextMesh)module_2[0].FindInChildren("module_item3").FindInChildren("info_head").GetComponent<tk2dTextMesh>();
		settext.ForceText(pph3,"");
		pph1 = (tk2dTextMesh)module_2[0].FindInChildren("module_item1").FindInChildren("info_head").GetComponent<tk2dTextMesh>();
		settext.ForceText(pph1,"");
	}

	void Start()
	{
	}

	int updatesequence = 0;
	void Update()
	{
		int machine = ni_pointofinterest_button.MachineID;

        tk2dSprite sprite = ni_preparedatafields.pointofinterest[machine].GetComponent<tk2dSprite>();
        if (sprite.spriteId == sprite.GetSpriteIdByName("picto_green"))
            _ModuleAlert(false,false,false);
        else
            _ModuleAlert(true,true,true);

		switch (updatesequence)
		{
		case 0 :
			settext.ForceText (base.subtitle, ni_regularmainloop.fathervalues[machine].ToString() + ni_regularmainloop.fathervariablesdesc[machine,0]);
			updatesequence ++;

			tk2dTextMesh tm = (tk2dTextMesh)module_2[0].FindInChildren("title").GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,"La journée depuis " + bufferme.GetString ("StartDateTime"));
			tm = (tk2dTextMesh)module_2[1].FindInChildren("title").GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,"La dernière heure depuis " + bufferme.GetString ("LastHourSince"));
			break;
		case 1 :
			int setval;
			switch (ni_regularmainloop.nitemnames[machine])
			{
			case 1 :
				setval = (int)ni_regularmainloop.formula[machine,0];
				_ModuleSetValue(0,0,setval.ToString(),ni_regularmainloop.formula_valid[machine,0],Color.green);

				setval = (int)ni_regularmainloop.formulahour[machine,0];
				_ModuleSetValue(1,0,setval.ToString(),ni_regularmainloop.formulahour_valid[machine,0],Color.green);

				if (ni_regularmainloop.seconditemnames[machine,0] == "")
					settext.ForceText(pph1,"");
				else
				{
					setval = (int)ni_regularmainloop.secondformula[machine,0];
					settext.ForceText(pph1,setval.ToString());
				}
				break;
			case 2 :
				setval = (int)ni_regularmainloop.formula[machine,0];
				_ModuleSetValue(0,0,setval.ToString(),ni_regularmainloop.formula_valid[machine,0],Color.green);
				setval = (int)ni_regularmainloop.formulahour[machine,0];
				_ModuleSetValue(1,0,setval.ToString(),ni_regularmainloop.formulahour_valid[machine,0],Color.green);
				setval = (int)ni_regularmainloop.formula[machine,1];
				_ModuleSetValue(0,1,setval.ToString(),ni_regularmainloop.formula_valid[machine,1],Color.green);
				setval = (int)ni_regularmainloop.formulahour[machine,1];
				_ModuleSetValue(1,1,setval.ToString(),ni_regularmainloop.formulahour_valid[machine,1],Color.green);

				if (ni_regularmainloop.seconditemnames[machine,0] == "")
					settext.ForceText(pph1,"");
				else
				{
					setval = (int)ni_regularmainloop.secondformula[machine,0];
					settext.ForceText(pph1,setval.ToString());
				}
				if (ni_regularmainloop.seconditemnames[machine,1] == "")
					settext.ForceText(pph2,"");
				else
				{
					setval = (int)ni_regularmainloop.secondformula[machine,1];
					settext.ForceText(pph2,setval.ToString());
				}
				break;
			case 3 :
				setval = (int)ni_regularmainloop.formula[machine,0];
				_ModuleSetValue(0,0,setval.ToString(),ni_regularmainloop.formula_valid[machine,0],Color.green);
				setval = (int)ni_regularmainloop.formulahour[machine,0];
				_ModuleSetValue(1,0,setval.ToString(),ni_regularmainloop.formulahour_valid[machine,0],Color.green);
				setval = (int)ni_regularmainloop.formula[machine,1];
				_ModuleSetValue(0,1,setval.ToString(),ni_regularmainloop.formula_valid[machine,1],Color.green);
				setval = (int)ni_regularmainloop.formulahour[machine,1];
				_ModuleSetValue(1,1,setval.ToString(),ni_regularmainloop.formulahour_valid[machine,1],Color.green);
				setval = (int)ni_regularmainloop.formula[machine,2];
				_ModuleSetValue(0,2,setval.ToString(),ni_regularmainloop.formula_valid[machine,2],Color.green);
				setval = (int)ni_regularmainloop.formulahour[machine,2];
				_ModuleSetValue(1,2,setval.ToString(),ni_regularmainloop.formulahour_valid[machine,2],Color.green);

				if (ni_regularmainloop.seconditemnames[machine,0] == "")
					settext.ForceText(pph1,"");
				else
				{
					setval = (int)ni_regularmainloop.secondformula[machine,0];
					settext.ForceText(pph1,setval.ToString());
				}
				if (ni_regularmainloop.seconditemnames[machine,1] == "")
					settext.ForceText(pph2,"");
				else
				{
					setval = (int)ni_regularmainloop.secondformula[machine,1];
					settext.ForceText(pph2,setval.ToString());
				}
				if (ni_regularmainloop.seconditemnames[machine,2] == "")
					settext.ForceText(pph3,"");
				else
				{
					setval = (int)ni_regularmainloop.secondformula[machine,2];
					settext.ForceText(pph3,setval.ToString());
				}
				break;
			}
			updatesequence ++;
			break;
		case 2 :
			/*
			col1 = false;
			col2 = false;

			if ((ni_saintlizaigne_mainloop.cirmeca_cadence < ni_saintlizaigne_mainloop.cirmeca_mincadence) || (ni_saintlizaigne_mainloop.cirmeca_cadence > ni_saintlizaigne_mainloop.cirmeca_maxcadence))
			{
				_ModuleSetValue(0,0,ni_saintlizaigne_mainloop.cirmeca_cadence.ToString(),false,Color.red);
				col1 = true;
			}
			else
				_ModuleSetValue(0,0,ni_saintlizaigne_mainloop.cirmeca_cadence.ToString(),true,Color.green);

			if ((ni_saintlizaigne_mainloop.cirmeca_cadence_hour < ni_saintlizaigne_mainloop.cirmeca_mincadence) || (ni_saintlizaigne_mainloop.cirmeca_cadence_hour > ni_saintlizaigne_mainloop.cirmeca_maxcadence))
			{
				col2 = true;
				_ModuleSetValue(1,0,ni_saintlizaigne_mainloop.cirmeca_cadence_hour.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(1,0,ni_saintlizaigne_mainloop.cirmeca_cadence_hour.ToString(),true,Color.green);

			if ((ni_saintlizaigne_mainloop.cirmeca_rendement < ni_saintlizaigne_mainloop.cirmeca_minrendement) || (ni_saintlizaigne_mainloop.cirmeca_rendement > ni_saintlizaigne_mainloop.cirmeca_maxrendement))
			{
				col1 = true;
				_ModuleSetValue(0,1,ni_saintlizaigne_mainloop.cirmeca_rendement.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(0,1,ni_saintlizaigne_mainloop.cirmeca_rendement.ToString(),true,Color.green);
			
			if ((ni_saintlizaigne_mainloop.cirmeca_rendement_hour < ni_saintlizaigne_mainloop.cirmeca_minrendement) || (ni_saintlizaigne_mainloop.cirmeca_rendement_hour > ni_saintlizaigne_mainloop.cirmeca_maxrendement))
			{
				col2 = true;
				_ModuleSetValue(1,1,ni_saintlizaigne_mainloop.cirmeca_rendement_hour.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(1,1,ni_saintlizaigne_mainloop.cirmeca_rendement_hour.ToString(),true,Color.green);
			_ModuleAlert(col1,col2,false);

			_ModuleAlertSetText_2(ni_saintlizaigne_mainloop.cirmeca_alert1,ni_saintlizaigne_mainloop.cirmeca_alert2);
*/
			updatesequence = 0;
			break;
		}

	}
}
