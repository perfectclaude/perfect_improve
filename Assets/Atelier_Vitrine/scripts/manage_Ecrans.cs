﻿using UnityEngine;
using System.Collections;

public class manage_Ecrans : MonoBehaviour
{
	static int MaxStack = 10;
	public static GameObject 			father;
	public static ScreensManager		screensManager;

	static ScreenAbstract [] returnscreenstack = new ScreenAbstract[MaxStack];
	static int nreturnscreenstack = 0;


	public static 	bool				AdminMode = false;
	public static ScreenAdminMenu		MenuAdminScreen;
	public static RemoteListener		remoteListener;


	public static ScreenAbstract ReturnScreen()
	{
		if (nreturnscreenstack == 0)
						return (null);
		nreturnscreenstack --;
		return (returnscreenstack [nreturnscreenstack]);
	}

	public static void CallFromScreen(ScreenAbstract screen)
	{
		returnscreenstack [nreturnscreenstack] = screen;
		nreturnscreenstack++;
	}

	void Awake()
	{
		GameObject go = GameObject.Find ("interface_atelier");

		screensManager = go.FindInChildren ("Ecrans").GetComponent<ScreensManager>();
		father = go.FindInChildren ("Scene_atelier");
		nreturnscreenstack = 0;

		MenuAdminScreen = go.FindInChildren ("AdminScreenMenu").GetComponent<ScreenAdminMenu>();
		remoteListener = GameObject.FindObjectOfType (typeof(RemoteListener)) as RemoteListener;
	}
}
