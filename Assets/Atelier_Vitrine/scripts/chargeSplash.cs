﻿using UnityEngine;
using System.Collections;

public class chargeSplash : MonoBehaviour
{
	GameObject plane;
	tk2dSprite sprite;
//	GameObject fadein;

	float alpha = 0;
	
	void Awake()
	{
		Application.targetFrameRate = 60;
		plane = GameObject.Find("Plane");
		sprite = plane.GetComponent<tk2dSprite>();
		sprite.color = new Color(1,1,1,0);
	}
	
	void Start () 
	{
		StartCoroutine("JustWait");
	}

	IEnumerator JustWait()
	{
		while (alpha < 1.0f)
		{
			alpha += 0.05f;
			if (alpha > 1.0f)		alpha = 1.0f;
			sprite.color = new Color(1,1,1,alpha);
			yield return new WaitForSeconds (0.03f);
		}
		
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevelAsync(1); //"ScenePC_Workshop2");
	}
}
