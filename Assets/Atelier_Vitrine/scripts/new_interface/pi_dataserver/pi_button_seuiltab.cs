﻿using UnityEngine;
using System.Collections;

public class pi_button_seuiltab : buttonswap
{
	GameObject fatherofitems;
	pi_machinetabs	tabcenter = null;
	pi_planactiontabs	tabcenter2 = null;

	void Start()
	{
		fatherofitems = gameObject.transform.parent.parent.gameObject;
		fatherofitems = fatherofitems.FindInChildren("items_pa");
		if (fatherofitems != null)
		{
			tabcenter2 = (pi_planactiontabs)fatherofitems.GetComponent<pi_planactiontabs>();
		}

		fatherofitems = gameObject.transform.parent.gameObject;
		fatherofitems = fatherofitems.FindInChildren("items");
		if (fatherofitems != null)
		{
			tabcenter = (pi_machinetabs)fatherofitems.GetComponent<pi_machinetabs>();
		}
	}


	public override void ButtonPressed ()
	{
		Vector3 vec = new Vector3(0.0f,0.0f,0.0f);

		base.ButtonPressed();

		string myname = gameObject.name.Replace("tabname","");

		if (tabcenter != null)
		{
			switch (myname)
			{
			case "0" :
				vec.x = 3.4f;
				tabcenter.selectedtabobject = 0;
				break;
			case "1" :
				vec.x = 0.0f;
				tabcenter.selectedtabobject = 1;
				break;
			case "2" :
				vec.x = -3.4f;
				tabcenter.selectedtabobject = 2;
				break;
			case "3" :
				vec.x = -3.4f*2;
				tabcenter.selectedtabobject = 3;
				break;
			case "4" :
				vec.x = -3.4f*3;
				tabcenter.selectedtabobject = 4;
				break;
			}
			fatherofitems.transform.localPosition = vec;
		}
		if (tabcenter2 != null)
		{
			int.TryParse(myname, out tabcenter2.selectedtabobject);

			if (tabcenter2.selectedtabobject == tabcenter2.ntabobjects-1)
			{
				tabcenter2.selectedtabobject = 1000;

				bufferme.SetInt("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+tabcenter2.ntabobjects+"_n",0);
			}
			else
				ni_plandaction_listactions.CloneLines();

//			Debug.Log ("Selected item : " + tabcenter2.selectedtabobject);
		}

	}

}
