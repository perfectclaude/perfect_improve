﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class pi_button_calclasthour : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (gameObject.name == "button_calclasthour")
			StartCoroutine("FetchJson");
		else
			StartCoroutine("FetchHourJson");
	}

	IEnumerator FetchHourJson()
	{
		DateTime enddate = DateTime.Now;
		DateTime startdate = new DateTime (enddate.Year, enddate.Month, enddate.Day, enddate.Hour, 0, 0, 0); //.ToLocalTime ();
		startdate = startdate.AddHours(-1); //-2);
		long stime = (long)network.GetMiliseconds(startdate);
		long etime = (long)network.GetMiliseconds(startdate.AddHours(1));

		network.GetJson("h"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),3);
		while (network.waitingnetwork)
		{
			yield return new WaitForSeconds(0.2f);
		}
		Debug.Log ("Hour found:" + network.retreivejson);
	}


	IEnumerator FetchJson()
	{
		DateTime enddate = DateTime.Now;
		DateTime startdate = new DateTime (enddate.Year, enddate.Month, enddate.Day, enddate.Hour, 0, 0, 0); //.ToLocalTime ();
//		startdate = startdate.AddHours(-1);
		Debug.Log (startdate.ToShortTimeString() + " - " + enddate.ToShortTimeString());

		DateTime sampledate = startdate.AddMinutes(30);
		enddate = enddate.AddSeconds(1.0f);
		long savetime = (long)network.GetMiliseconds(sampledate);
		long stime = (long)network.GetMiliseconds(startdate);
		long etime = (long)network.GetMiliseconds(enddate);
		network.GetIntermediateJson(bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,false,"ni_calclasthour");
		while (network.waitingnetworkintermediate)
		{
			yield return new WaitForSeconds(0.2f);
		}
		if (network.retreiveintermediatejson.Length > 0)
		{
			ni_preparedatafields.CalcAverageHour(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson);
		}

	}
}
