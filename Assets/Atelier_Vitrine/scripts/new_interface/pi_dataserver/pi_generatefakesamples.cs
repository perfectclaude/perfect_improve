﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;

public class pi_generatefakesamples : MonoBehaviour
{
	public static DateTime startdate;
	public static DateTime enddate;
	static ni_getsetdatetimefromdropdowns starttime;
	static ni_getsetdatetimefromdropdowns endtime = null;
	static ni_dropdownmenu sampling = null;

	void Awake()
	{
		starttime = (ni_getsetdatetimefromdropdowns)gameObject.FindInChildren("starttime").GetComponent<ni_getsetdatetimefromdropdowns>();
		endtime = (ni_getsetdatetimefromdropdowns)gameObject.FindInChildren("endtime").GetComponent<ni_getsetdatetimefromdropdowns>();
		sampling = (ni_dropdownmenu)gameObject.FindInChildren("button_sampling").GetComponent<ni_dropdownmenu>();
	}

	public static void GenerateFakeNow()
	{
		startdate = starttime.GetDateTime();
		enddate = endtime.GetDateTime();
		int int_sampling = sampling.GetSelected();
		double sampleperiod = PrepareSamplingAndDates(startdate,enddate,int_sampling);
		//		scan_startdatereturn
		//		scan_enddatereturn
		// sampleperiod
		ni_preparedatafields.ResetAllParameters();

		while (DateTime.Compare(scan_startdatereturn, scan_enddatereturn.AddSeconds(1)) <  0)
		{
			scan_startdatereturn = scan_startdatereturn.AddMilliseconds(sampleperiod);
			ni_preparedatafields.Generate1FakeData();
			// use scan_startdatereturn as date + use current data to SAVE or PHP to server
			if (ni_preparedatafields.ONLINESTORAGE)
			{
			}
			else
			{
// save 1 json
			}
			ni_preparedatafields._elapsedtime += TimeSpan.FromMilliseconds(sampleperiod);
		}

	}

	/*
	IEnumerator Start()
	{
		Debug.Log ("Wait before clean");

		while (network.waitingnetwork)
			yield return null;
		Debug.Log ("Wait clean");
		network.CleanDbase(bufferme.GetClientId());
		Debug.Log ("Wait while before clean");
		while (network.waitingnetwork)
			yield return null;

		Debug.Log ("Wait create");
		network.CreateDbase(bufferme.GetClientId());
		while (network.waitingnetwork)
			yield return null;
	}
	*/





	static DateTime scan_startdatereturn;
	static DateTime scan_enddatereturn;
	
	static double PrepareSamplingAndDates(DateTime start,DateTime end,int samplingvar)
	{
		double periodmilisec = 0.0f;
		int tmp = 0;
		
		scan_startdatereturn = DateTime.Now;
		scan_enddatereturn = DateTime.Now;
		
		// get period + Clean DateTime
		switch (samplingvar)
		{
		case 0 :		// 1 min
			periodmilisec = 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour, start.Minute, 0, 0); //.ToLocalTime ();
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour, end.Minute, 0, 0); //.ToLocalTime ();
			break;
		case 1 :		// 5 min
			tmp = start.Minute % 5;
			periodmilisec = 5.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour, start.Minute - tmp, 0, 0); //.ToLocalTime ();
			tmp = end.Minute % 5;
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour, end.Minute - tmp, 0, 0); //.ToLocalTime ();
			break;
		case 2 :		// 15 min
			tmp = start.Minute % 15;
			periodmilisec = 15.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour, start.Minute - tmp, 0, 0); //.ToLocalTime ();
			tmp = end.Minute % 15;
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour, end.Minute - tmp, 0, 0); //.ToLocalTime ();
			break;
		case 3 :		// 30 min
			tmp = start.Minute % 30;
			periodmilisec = 30.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour, start.Minute - tmp, 0, 0); //.ToLocalTime ();
			tmp = end.Minute % 30;
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour, end.Minute - tmp, 0, 0); //.ToLocalTime ();
			break;
		case 4 :		// 1h
			periodmilisec = 60.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour, 0, 0, 0); //.ToLocalTime ();
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour, 0, 0, 0); //.ToLocalTime ();
			break;
		case 5 :		// 3h
			tmp = start.Hour % 3;
			periodmilisec = 3.0f * 60.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour - tmp,0, 0, 0); //.ToLocalTime ();
			tmp = end.Hour % 3;
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour - tmp,0, 0, 0); //.ToLocalTime ();
			break;
		case 6 :		// 6h
			tmp = start.Hour % 6;
			periodmilisec = 6.0f * 60.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour - tmp,0, 0, 0); //.ToLocalTime ();
			tmp = end.Hour % 6;
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour - tmp,0, 0, 0); //.ToLocalTime ();
			break;
		case 7 :		// 12h
			tmp = start.Hour % 12;
			periodmilisec = 12.0f * 60.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, start.Hour - tmp,0, 0, 0); //.ToLocalTime ();
			tmp = end.Hour % 12;
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, end.Hour - tmp,0, 0, 0); //.ToLocalTime ();
			break;
		case 8 :		// 1day
			periodmilisec = 24.0f * 60.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day, 0,0, 0, 0); //.ToLocalTime ();
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day, 0,0, 0, 0); //.ToLocalTime ();
			break;
		case 9 :		// 7days
			tmp = start.Day % 7;
			periodmilisec = 7.0f * 24.0f * 60.0f * 60.0f * 1000.0f;
			scan_startdatereturn = new DateTime (start.Year, start.Month, start.Day - tmp, 0,0, 0, 0); //.ToLocalTime ();
			tmp = end.Day % 7;
			scan_enddatereturn = new DateTime (end.Year, end.Month, end.Day - tmp, 0,0, 0, 0); //.ToLocalTime ();
			break;
		}
//		scan_startdatereturn = scan_startdatereturn.AddHours (-1);
//		scan_enddatereturn = scan_enddatereturn.AddHours (-1);
		
		return(periodmilisec);
	}



	// Create and Save fake data samples !!!
	IEnumerator CreateFakeData()
	{
		ni_endlessturning.waiting.SetActiveRecursively (true);
		//		ni_preparedatafields.SaveFakeDate(startdate, enddate);
		if (bufferme.GetInt ("ButtonState", 0) == 1)
			yield break;
		
		GameObject obj = gameObject.FindInChildren ("place_button_return");
		obj = obj.FindInChildren ("text");		tk2dTextMesh tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh> ();
		string mytext;
		DateTime start = startdate;
		DateTime end = enddate;
		TimeSpan halfaminute = new TimeSpan(0,0,(int)ni_preparedatafields.datacollectorperiod);
		
		ni_preparedatafields.ResetAllParameters ();
		ni_preparedatafields._elapsedtime = new TimeSpan(0,0,0,0,0);
		
		double stmil = ni_preparedatafields.GetMilliseconds (start);
		double edmil = ni_preparedatafields.GetMilliseconds (end);
		edmil = (edmil - stmil)/1000.0f;
		edmil = ni_preparedatafields.datacollectorperiod * 100.0f / edmil;
		stmil = 0.0f;
		
		while (DateTime.Compare(start, end) <  0)
		{
			ni_preparedatafields.Generate1FakeData();
			ni_preparedatafields.Save1SampleJsonFile(start);
			yield return new WaitForSeconds (0.03f);
			stmil = stmil + edmil;
			int res = (int)stmil;
			if (res > 100) res = 100;
			mytext = "Load "+res+"%";
			settext.ForceText(tm,mytext);
			
			start = start.AddSeconds(ni_preparedatafields.datacollectorperiod); // 
			ni_preparedatafields._elapsedtime = ni_preparedatafields._elapsedtime.Add(halfaminute);
		}
		mytext = "Load 100%";
		settext.ForceText(tm,mytext);
		
		ni_endlessturning.waiting.SetActiveRecursively (false);
	}

}
