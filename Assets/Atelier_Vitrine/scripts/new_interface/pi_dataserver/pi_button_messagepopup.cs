﻿using UnityEngine;
using System.Collections;

public class pi_button_messagepopup : buttonswap
{
	GameObject	slide1;
	GameObject	slide2;

	void Awake()
	{
		_Awake ();
		slide1 = gameObject.transform.parent.gameObject.FindInChildren("slideball1");
		slide2 = gameObject.transform.parent.gameObject.FindInChildren("slideball2");
	}

	void OnEnable()
	{
		_OnEnable();
		pi_message_popup.myself.SetActive(false);
	}

	void Update()
	{
		GameObject	one;
		GameObject	two;
		Vector3 vec = gameObject.transform.localPosition;

		if (slide1.transform.localPosition.x > slide2.transform.localPosition.x)
		{
			one = slide2;
			two = slide1;
		}
		else
		{
			one = slide1;
			two = slide2;
		}

		switch (gameObject.name)
		{
		default :
			vec.x = (0.955f + two.transform.localPosition.x) / 2.0f;
			break;
		case "message_bad" :
			vec.x = (-0.955f + one.transform.localPosition.x) / 2.0f;
			break;
		case "message_good" :
			vec.x = (two.transform.localPosition.x + one.transform.localPosition.x) / 2.0f;
			break;
		}
		gameObject.transform.localPosition = vec;

		_Update ();
	}

	public override void ButtonPressed ()
	{
		GameObject myfather = gameObject.transform.parent.parent.gameObject;

		base.ButtonPressed();
		pi_message_popup.showmessage = true;

		pi_message_popup.variablename = myfather.name;
		switch (gameObject.name)
		{
			default :
			pi_message_popup.postfix = "_message_toogood";
			break;
			case "message_bad" :
			pi_message_popup.postfix = "_message_bad";
			break;
			case "message_good" :
			pi_message_popup.postfix = "_message_good";
			break;
		}
		pi_message_popup.myself.SetActive(true);

	}
}
