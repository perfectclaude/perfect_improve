﻿using UnityEngine;
using System.Collections;

public class pi_button_forcemachineonoff : buttonswap
{
	public static bool machineon = true;
	tk2dTextMesh ton;
	tk2dTextMesh toff;

	void Awake ()
	{
		_Awake ();
		ton = (tk2dTextMesh)on.GetComponent<tk2dTextMesh>();
		toff = (tk2dTextMesh)off.GetComponent<tk2dTextMesh>();
	}

	void Update()
	{
		_Update();
		if (machineon)
		{
			settext.ForceText(ton,"ON");
			settext.ForceText(toff,"ON");
		}
		else
		{
			settext.ForceText(ton,"OFF");
			settext.ForceText(toff,"OFF");
		}
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (machineon)
			machineon = false;
		else
			machineon = true;
	}

}
