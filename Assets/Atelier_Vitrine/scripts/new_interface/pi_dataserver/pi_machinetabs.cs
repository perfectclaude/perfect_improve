﻿using UnityEngine;
using System.Collections;

public class pi_machinetabs : MonoBehaviour
{
	GameObject [] tabobjects = new GameObject[10];
	public int selectedtabobject = 0;
	public float tabposy = 1.49f; //0.736f;
	public string useassaving = "";

	int ntabobjects = 0;
	GameObject tabtoclone;
	int nrfields;

	void Awake ()
	{
		tabtoclone = gameObject.transform.parent.gameObject.FindInChildren("tabtoclone");
		tabtoclone.SetActive (false);

	}
	
	void Update ()
	{
		for (int n=0;n<ntabobjects;n++)
		{
			tk2dSlicedSprite ssprite = (tk2dSlicedSprite)tabobjects[n].GetComponent<tk2dSlicedSprite>();
			if (n == selectedtabobject)
			{
				ssprite.color = new Color(0.782f,0.782f,0.782f);
			}
			else
			{
				ssprite.color = Color.grey;
			}
			if (useassaving != "")
			{
				if (selectedtabobject != bufferme.GetInt(useassaving))
				{
					bufferme.SetInt(useassaving,selectedtabobject);
					if (useassaving == "planaction_machine")
						ni_plandaction_editplandaction.RestartPage();
				}
			}
		}
	}

	void OnEnable ()
	{
		ReSetDisplay ();
		tabtoclone.SetActive (false);
	}


	public void ReSetDisplay()
	{
		//		ni_preparedatafields.poinofinteresttitle[]
		Vector3 vec;
		float ypos2 = 0.25f;
		float ypos = 0;
		float xpos = -3.4f * 2.0f;
		string fathername = "";
		Vector3 tabpos = new Vector3(-0.9f,tabposy,10.0f);
		int tabindex = 0;

		if (bufferme.HasKey("Curve_displays"))
		{
			nrfields = bufferme.GetInt("Curve_displays");
			for (int n=0;n<nrfields;n++)
			{
				bool fillsection = true;
				
				if (fathername != bufferme.GetString("Curve_displays_father"+n))
				{
					fathername = bufferme.GetString("Curve_displays_father"+n);
					ypos = 0.0f;
					ypos2 = 0.3f;
					xpos += 3.4f;
					// Create tab clone
					GameObject tabobj = gameObject.transform.parent.gameObject.FindInChildren("tabname"+tabindex);
					
					if (tabobj == null)
					{
						tabobj = (GameObject)GameObject.Instantiate(tabtoclone);
						tabobj.transform.parent = gameObject.transform.parent;
						tabobj.transform.localPosition = tabpos;
						tabpos.x += 0.8f;
						tabobj.name = "tabname"+tabindex;
						
						tabobjects[tabindex] = tabobj;
						
						tk2dTextMesh innertm = (tk2dTextMesh)tabobj.FindInChildren("name").GetComponent<tk2dTextMesh>();
						settext.ForceText(innertm,fathername);
						tabobj.SetActive(true);
					}
					tk2dSlicedSprite ssprite = (tk2dSlicedSprite)tabobj.GetComponent<tk2dSlicedSprite>();
					if (tabindex == 0)
					{
						ssprite.color = new Color(0.78125f,0.78125f,0.78125f);
					}
					else
					{
						ssprite.color = Color.grey;
					}
					tabindex ++;
				}
				/*
				string datanameid = bufferme.GetString("Curve_displays_variable"+n);
				GameObject obj = gameObject.FindInChildren(datanameid);
				if (obj == null)
				{
					obj = (GameObject)GameObject.Instantiate(toclone);
					obj.name = datanameid;
					obj.transform.parent = gameObject.transform;
				}
				obj.SetActiveRecursively(true);
				myobject[n] = obj;
				
				vec = new Vector3(xpos,ypos2,0);
				obj.transform.localPosition = vec;
				
				tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("name").GetComponent<tk2dTextMesh>();
				string additional = "";
				if (bufferme.HasKey(datanameid+"_form"))
					additional = " [FORM]";
				settext.ForceText(tm,datanameid+additional);
				*/

				ypos2 -= 0.48f;
				// 
			}
		}
//		toclone.SetActive (false);
		
//		Vector3 setvector = new Vector3(3.4f,0,0);
//		gameObject.transform.localPosition = setvector;

		if (useassaving != "")
		{
			selectedtabobject = bufferme.GetInt(useassaving);
		}
		else
			selectedtabobject = 0;

		ntabobjects = tabindex;
	}

}
