﻿using UnityEngine;
using System;
using MiniJSON;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

public class pi_server_trackintermediate : MonoBehaviour
{
	DateTime	lastverification_h;
	DateTime	lastverification_d;
	DateTime	lastverification_m;
	DateTime	lastverification_y;

	DateTime	nextverification_h;
	DateTime	nextverification_d;
	DateTime	nextverification_m;
	DateTime	nextverification_y;

	long		nextverification_h_ms;
	long		nextverification_d_ms;
	long		nextverification_m_ms;
	long		nextverification_y_ms;

	int lastminute = -1;
	float		verificationtime;

	bool initialized = false;
	bool firstinit = true;
	// Use this for initialization
	void Awake ()
	{
		/*
		bufferme.DeleteKey("server_lasttime"+"lastverification_h");
		bufferme.DeleteKey("server_lasttime"+"lastverification_d");
		bufferme.DeleteKey("server_lasttime"+"lastverification_m");
		bufferme.DeleteKey("server_lasttime"+"lastverification_y");
		*/
		OnStartApplication();
	}


	void SaveValue(DateTime val,string name)
	{
		long myval = (long)network.GetMiliseconds(val);
		bufferme.SetString("server_lasttime"+name,myval.ToString());
	}

	DateTime LoadValue(string name)
	{
		DateTime ret;
		if (bufferme.HasKey("server_lasttime"+name))
		{
			string lastval = bufferme.GetString("server_lasttime"+name);
			long myval;
			long.TryParse(lastval,out myval);
			myval = myval / 1000;			// miliseconds to seconds
			ret = new DateTime(1970, 1, 1,0,0,0,0); //.ToLocalTime();
			ret = ret.AddSeconds(myval);

			TimeSpan span = DateTime.Now - ret;
			if (span.TotalDays > 30)			// 1 months out... reset !!!
			{
				ret = DateTime.Now;
			}
		}
		else
			ret = DateTime.Now;
		return (ret);
	}

	void OnApplicationQuit ()
	{
		if (initialized)
		{
			SaveValue(lastverification_h,"lastverification_h");
			SaveValue(lastverification_d,"lastverification_d");
			SaveValue(lastverification_m,"lastverification_m");
			SaveValue(lastverification_y,"lastverification_y");
		}
	}

	void GetMyMiliseconds()
	{
		nextverification_h_ms = (long)network.GetMiliseconds(nextverification_h);
		nextverification_d_ms = (long)network.GetMiliseconds(nextverification_d);
		nextverification_m_ms = (long)network.GetMiliseconds(nextverification_m);
		nextverification_y_ms = (long)network.GetMiliseconds(nextverification_y);
	}

	void OnStartApplication()
	{
//		ni_preparedatafields.ResetAllParameters();

		lastverification_h = LoadValue("lastverification_h");
		lastverification_h = new DateTime(lastverification_h.Year,lastverification_h.Month,lastverification_h.Day,lastverification_h.Hour,0,0);
		lastverification_d = LoadValue("lastverification_d");
		lastverification_d = new DateTime(lastverification_d.Year,lastverification_d.Month,lastverification_d.Day,0,0,0);
		lastverification_m = LoadValue("lastverification_m");
		lastverification_m = new DateTime(lastverification_m.Year,lastverification_m.Month,1,0,0,0);
		lastverification_y = LoadValue("lastverification_y");
		lastverification_y = new DateTime(lastverification_y.Year,1,1,0,0,0);


// special debug
//		lastverification_h = lastverification_h.AddDays(-1);
//		lastverification_d = lastverification_d.AddDays(-1);
//		lastverification_m = lastverification_m.AddDays(-1);
//		lastverification_y = lastverification_y.AddDays(-1);

		nextverification_h = lastverification_h.AddHours(1);
		nextverification_d = lastverification_d.AddDays(1);
		nextverification_m = lastverification_m.AddMonths(1);
		nextverification_y = lastverification_y.AddYears(1);

		GetMyMiliseconds();

		Debug.Log ("NextHour backed:" + nextverification_h.ToShortDateString() + " " + nextverification_h.ToShortTimeString());
		Debug.Log ("NextDay backed:" + nextverification_d.ToShortDateString() + " " + nextverification_d.ToShortTimeString());
		Debug.Log ("NextMonth backed:" + nextverification_m.ToShortDateString() + " " + nextverification_m.ToShortTimeString());
		Debug.Log ("NextYear backed:" + nextverification_y.ToShortDateString() + " " + nextverification_y.ToShortTimeString());

		StopAllCoroutines();
		verificationtime = Time.time + 60.0f * 3.0f;			// 3 minutes to check if this routine is alive
		initialized = true;
		StartCoroutine("EndlessLoop");
	}

	void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
			OnApplicationQuit ();
		else
		{
#if UNITY_IOS || UNITY_ANDROID
			if (firstinit)
				firstinit = false;
			else
				OnStartApplication();
#endif
		}
	}

	void Update()
	{
		if (verificationtime < Time.time)
		{
			verificationtime = Time.time + 60.0f * 3.0f;			// 3 minutes to check if this routine is alive
			StartCoroutine ("EndlessLoop");
		}
	}

	IEnumerator EndlessLoop()
	{
		long currentms;

		while (true)
		{
			verificationtime = Time.time + 60.0f * 3.0f;			// 3 minutes to check if this routine is alive
			yield return new WaitForSeconds(20.0f);
//			Debug.Log ("Endless loop step...");

			currentms = (long)network.GetMiliseconds(DateTime.Now);

			while (currentms >= nextverification_h_ms)		// first make sure all Hours have been created
			{
				Debug.Log ("Try and save last hour !!!" + nextverification_h_ms);
				DateTime enddate = nextverification_h.AddSeconds(1.0f);

				DateTime startdate = lastverification_h;
				long stime = (long)network.GetMiliseconds(startdate);
				long etime = (long)network.GetMiliseconds(enddate);

				DateTime sampledate = startdate.AddMinutes(30);
				long savetime = (long)network.GetMiliseconds(sampledate);

				network.GetIntermediateJson(bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,false,"pi_server_trackintermediate hour");
				while (network.waitingnetworkintermediate)
				{
					yield return new WaitForSeconds(0.2f);
				}
				if (network.retreiveintermediatejson.Length > 0)
				{
					Debug.Log ("HOUR test :"+network.retreiveintermediatejson);
					ni_preparedatafields.CalcAverageHour(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson);
				}
				lastverification_h = lastverification_h.AddHours(1);
				nextverification_h = nextverification_h.AddHours(1);
				GetMyMiliseconds();
				yield return new WaitForSeconds(0.1f);
			}

			while (currentms >= nextverification_d_ms)		// first make sure all days have been created
			{
				Debug.Log ("Try and save last day !!!");
				DateTime enddate = nextverification_d.AddSeconds(-10);
				DateTime startdate = lastverification_d;
				long stime = (long)network.GetMiliseconds(startdate);
				long etime = (long)network.GetMiliseconds(enddate);
				
				DateTime sampledate = startdate.AddHours(12);
				long savetime = (long)network.GetMiliseconds(sampledate);
				
				network.GetIntermediateJson("h"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),26,true,"pi_server_trackintermediate day");
				while (network.waitingnetworkintermediate)
				{
					yield return new WaitForSeconds(0.2f);
				}
				if (network.retreiveintermediatejson.Length > 0)
				{
					ni_preparedatafields.CalcAverageOnBulk(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson,"d");
				}

				lastverification_d = lastverification_d.AddDays(1);
				nextverification_d = nextverification_d.AddDays(1);
				GetMyMiliseconds();

				ni_preparedatafields.ResetAllParameters();

				yield return new WaitForSeconds(0.1f);
			}
			while (currentms >= nextverification_m_ms)		// first make sure all months have been created
			{
				//				Debug.Log ("Try and save last month !!!");
				DateTime enddate = nextverification_m.AddSeconds(-10);
				DateTime startdate = lastverification_m;
				long stime = (long)network.GetMiliseconds(startdate);
				long etime = (long)network.GetMiliseconds(enddate);
				
				DateTime sampledate = startdate.AddDays(15);
				long savetime = (long)network.GetMiliseconds(sampledate);
				
				network.GetIntermediateJson("d"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),34,true,"pi_server_trackintermediate month");
				while (network.waitingnetworkintermediate)
				{
					yield return new WaitForSeconds(0.2f);
				}
				if (network.retreiveintermediatejson.Length > 0)
				{
					ni_preparedatafields.CalcAverageOnBulk(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson,"m");
				}
				// here you must get all days between 2 dates
				lastverification_m = lastverification_m.AddMonths(1);
				nextverification_m = nextverification_m.AddMonths(1);
				GetMyMiliseconds();
				yield return new WaitForSeconds(0.1f);
			}
			while (currentms >= nextverification_y_ms)
			{
				//				Debug.Log ("Try and save last year !!!");
				DateTime enddate = nextverification_y.AddSeconds(-10);
				DateTime startdate = lastverification_y;
				long stime = (long)network.GetMiliseconds(startdate);
				long etime = (long)network.GetMiliseconds(enddate);
				
				DateTime sampledate = startdate.AddMonths(6);
				long savetime = (long)network.GetMiliseconds(sampledate);
				
				network.GetIntermediateJson("m"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),14,true,"pi_server_trackintermediate year");
				while (network.waitingnetworkintermediate)
				{
					yield return new WaitForSeconds(0.2f);
				}
				if (network.retreiveintermediatejson.Length > 0)
				{
					ni_preparedatafields.CalcAverageOnBulk(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson,"y");
				}
				// here you must get all months between 2 dates
				lastverification_y = lastverification_y.AddYears(1);
				nextverification_y = nextverification_y.AddYears(1);
				GetMyMiliseconds();
				yield return new WaitForSeconds(0.1f);
			}
		}
	}
	

}
