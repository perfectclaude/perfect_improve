﻿using UnityEngine;
using System.Collections;

public class pi_button_scheduletab :  buttonswap
{
	GameObject fatherofitems;
	pi_machinetabs	tabcenter;
	
	void Awake()
	{
		_Awake ();
		fatherofitems = gameObject.transform.parent.gameObject.FindInChildren("items");
		tabcenter = (pi_machinetabs)fatherofitems.GetComponent<pi_machinetabs>();
	}
	
	
	public override void ButtonPressed ()
	{
		Vector3 vec = new Vector3(0.0f,0.0f,0.0f);
		
		base.ButtonPressed();
		string myname = gameObject.name.Replace("tabname","");
		
		switch (myname)
		{
		case "0" :
			vec.x = 3.4f;
			tabcenter.selectedtabobject = 0;
			break;
		case "1" :
			vec.x = 0.0f;
			tabcenter.selectedtabobject = 1;
			break;
		case "2" :
			vec.x = -3.4f;
			tabcenter.selectedtabobject = 2;
			break;
		case "3" :
			vec.x = -3.4f*2;
			tabcenter.selectedtabobject = 3;
			break;
		case "4" :
			vec.x = -3.4f*3;
			tabcenter.selectedtabobject = 4;
			break;
		}
//		fatherofitems.transform.localPosition = vec;
		
	}
	
}
