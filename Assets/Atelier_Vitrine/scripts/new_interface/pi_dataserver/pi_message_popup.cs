﻿using UnityEngine;
using System.Collections;

public class pi_message_popup : MonoBehaviour
{
	public static bool showmessage = false;
	public static GameObject myself;
	public static string variablename = "";
	public static string postfix = "";
	GameObject pu_value;
	GameObject pu_name;

	void Awake ()
	{
		pu_value = gameObject.FindInChildren("value");
		pu_name = gameObject.FindInChildren("name");
		myself = gameObject;

	}

	void Start()
	{
		myself.SetActive(false);

	}

	void OnDisable()
	{
		if ((variablename != "") && (postfix != ""))
		{
			tk2dTextMesh tm = (tk2dTextMesh)pu_value.GetComponent<tk2dTextMesh>();
			bufferme.SetString(variablename+postfix,tm.text);
		}
	}

	void OnEnable()
	{
		tk2dTextMesh tm = (tk2dTextMesh)pu_value.GetComponent<tk2dTextMesh>();
		if (bufferme.HasKey(variablename+postfix))
		{
			settext.ForceText(tm,bufferme.GetString(variablename+postfix));
		}
		tm = (tk2dTextMesh)pu_name.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,variablename + " " + postfix);

	}

	void Update ()
	{
	
	}
}
