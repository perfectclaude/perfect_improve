﻿using UnityEngine;
using System.Collections;

public class ni_server_inputcommandtext : MonoBehaviour
{
	ni_edittext myedittext;
	bool		toggle = false;

	void Awake ()
	{
		bufferme.SetString("inputcommandtext","");
		myedittext = (ni_edittext)gameObject.GetComponent<ni_edittext>();
	}
	
	void Update ()
	{
		if (toggle == false)
		{
			if (myedittext.inputbusy)
				toggle = true;
		}
		else
		{
			if (!myedittext.inputbusy)
			{
				Debug.Log ("Input not longer busy");
				tk2dTextMesh tm = (tk2dTextMesh)gameObject.GetComponent<tk2dTextMesh>();
				bufferme.SetString("inputcommandtext",tm.text);
				toggle = false;
				ni_preparedatafields.SaveClientData ();
			}
		}
	}
}
