﻿using UnityEngine;
using System.Collections;

public class pi_planactiontabs : MonoBehaviour
{
	static int MaxItems = 30;	
	GameObject [] tabobjects = new GameObject[MaxItems];
	public int selectedtabobject = 0;
	public float tabposy = 1.49f; //0.736f;
	public string useassaving = "";
	
	public int ntabobjects = 0;
	GameObject tabtoclone;
	GameObject title_show;
	GameObject action_root = null;
	GameObject none_action_root = null;

	tk2dTextMesh title;
	int nrfields;
	GameObject	fatherofall = null;

	void Awake ()
	{
		tabtoclone = gameObject.transform.parent.gameObject.FindInChildren("tabtoclone");
		title_show = gameObject.transform.parent.gameObject.FindInChildren("title");
		action_root = gameObject.transform.parent.gameObject.FindInChildren("action_root");
		none_action_root = gameObject.transform.parent.gameObject.FindInChildren("none_action_root");

		title = (tk2dTextMesh)title_show.GetComponent<tk2dTextMesh>();
		tabtoclone.SetActive (false);
	}



	void Update ()
	{
		if (selectedtabobject == 1000)
		{
			selectedtabobject = ntabobjects-1;

			bufferme.SetInt ("vocabularytype",0);		// 0 is ISSUE dico
			ni_plandaction_editplandaction.vocabulaire.savetype = "issue";					// change this if other item to vocabulate
			
			ni_plandaction_editplandaction.root_vocabulary.SetActive(true);
			ni_plandaction_editplandaction.root_listing.SetActive(false);
			ni_plandaction_editplandaction.root_master.SetActive(false);
		}

		for (int n=0;n<ntabobjects;n++)
		{
			tk2dSlicedSprite ssprite = (tk2dSlicedSprite)tabobjects[n].GetComponent<tk2dSlicedSprite>();
			if (n == selectedtabobject)
			{
				ssprite.color = new Color(0.782f,0.782f,0.782f);
			}
			else
			{
				ssprite.color = Color.grey;
			}

			if (selectedtabobject != bufferme.GetInt(useassaving))
			{
				bufferme.SetInt(useassaving,selectedtabobject);
				if (useassaving != "")
				{
					{
						int selectedmachine = bufferme.GetInt("planaction_machine");
						int selecteditem = ni_plandaction_listactions.selecteditem;
						
						settext.ForceText(title,"Qualification problème : " + bufferme.GetString("planaction"+selectedmachine+"_"+selecteditem+"_issue_"+selectedtabobject));
					}
				}
			}
		}
	}
	
	void OnEnable ()
	{
		ReSetDisplay ();
		tabtoclone.SetActive (false);
	}
	
	
	public void ReSetDisplay()
	{
		Vector3 vec;
		float ypos2 = 0.25f;
		float ypos = 0;
		float xpos = -3.4f * 2.0f;
		string fathername = "";
		Vector3 tabpos = new Vector3(-0.9f,tabposy,10.0f);
		int tabindex = 0;

		if (fatherofall != null)
		{
			Destroy(fatherofall);
		}

		fatherofall = new GameObject();
		fatherofall.transform.parent = gameObject.transform.parent;
		fatherofall.name = "tabsfather";
		fatherofall.transform.localPosition = Vector3.zero;
		fatherofall.transform.localScale = Vector3.one;

// check all current Plan d'action items
		int selectedmachine = bufferme.GetInt("planaction_machine");
		int selecteditem = ni_plandaction_listactions.selecteditem;

		int nroflines = bufferme.GetInt("planaction"+selectedmachine+"_"+selecteditem+"_n");

		tk2dSlicedSprite slsprite = (tk2dSlicedSprite)tabtoclone.GetComponent<tk2dSlicedSprite>();
		float floatscreen = 1.42714f+1.2280f;
		float charsize = floatscreen / 90.0f;

		floatscreen = floatscreen / (nroflines + 2.0f);
		float tabwidth = 2048.0f / (nroflines + 2.0f);

		charsize = floatscreen / charsize;
		int intcharsize = (int)charsize;

		tabpos.x = -1.2280f + (floatscreen / 2.0f);

		Destroy(tabtoclone.GetComponent<Collider>());
		slsprite.dimensions = new Vector2((tabwidth * 80.0f)/100.0f,slsprite.dimensions.y);
		tabtoclone.AddComponent<BoxCollider>();

		slsprite.dimensions = new Vector2(tabwidth,slsprite.dimensions.y);

		for (tabindex=0;tabindex<=nroflines;tabindex++)
		{
			ypos = 0.0f;
			ypos2 = 0.3f;
			xpos += 3.4f;
			// Create tab clone
			GameObject tabobj;

			if (tabindex == nroflines)
				fathername = "+";
			else
			{
				fathername = bufferme.GetString("planaction"+selectedmachine+"_"+selecteditem+"_issue_"+tabindex);
				if (fathername.Length > intcharsize)
				{
					fathername = fathername.Substring(0,intcharsize)+"...";
				}
			}

			tabobj = (GameObject)GameObject.Instantiate(tabtoclone);
			tabobj.transform.parent = fatherofall.transform;
			tabobj.transform.localPosition = tabpos;
			tabobj.name = "tabname"+tabindex;
			tabpos.x += floatscreen; //0.6f;
			tabobjects[tabindex] = tabobj;
			
			tk2dTextMesh innertm = (tk2dTextMesh)tabobj.FindInChildren("name").GetComponent<tk2dTextMesh>();
			settext.ForceText(innertm,fathername);
			tabobj.SetActive(true);

			tk2dSlicedSprite ssprite = (tk2dSlicedSprite)tabobj.GetComponent<tk2dSlicedSprite>();
			if (tabindex == 0)
			{
				ssprite.color = new Color(0.78125f,0.78125f,0.78125f);
			}
			else
			{
				ssprite.color = Color.grey;
			}

		}

		if (useassaving != "")
		{
			selectedtabobject = bufferme.GetInt(useassaving);
		}
		else
			selectedtabobject = 0;

		settext.ForceText(title,"Qualification problème : " + bufferme.GetString("planaction"+selectedmachine+"_"+selecteditem+"_issue_"+selectedtabobject));

		ntabobjects = tabindex;
		if (ntabobjects > 1)
		{
			none_action_root.SetActive(false);
			action_root.SetActive(true);
			ni_plandaction_listactions.CloneLines();
		}
		else
		{
			none_action_root.SetActive(true);
			action_root.SetActive(false);
			ni_plandaction_listactions.CloneLines();
		}
	}
	
}
