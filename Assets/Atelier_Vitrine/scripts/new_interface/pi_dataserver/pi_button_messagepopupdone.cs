﻿using UnityEngine;
using System.Collections;

public class pi_button_messagepopupdone : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		pi_message_popup.showmessage = false;
		pi_message_popup.myself.SetActive(false);
	}
}
