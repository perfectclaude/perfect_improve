﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ni_scroll_content_text : MonoBehaviour {

    public string longText = "";
    public Color longTextColor = Color.black;
    public Font longTextFont = null;
    public float PadX = 0;
    public float PadY = 0;


    private string longTextSav = "";
    private List<GameObject> lstGo = null;


	// Use this for initialization
	void Start ()
    {
        lstGo = new List<GameObject>();

        MajTxtContent();
	
	}
	
	// Update is called once per frame
	void Update ()
    {

        //StartCoroutine( MajTxtContent() );
        MajTxtContent();
	
	}

    //IEnumerator MajTxtContent()
    void MajTxtContent()
    {
        if ((longText != longTextSav) && (longTextFont != null) && (lstGo != null))
        {
            string[] rows = longText.Replace("\r\n", "\n").Split('\n');
            int no = 0;
            GameObject go = null;
            float y = 0;
            float width;
            float height;
            float max_width = 0;
            float max_height = 0;

            foreach (GameObject igo in lstGo)
            {
                Destroy(igo);
            }
            lstGo.Clear();

            foreach (string row in rows)
            {
                //Debug.Log("ni_scroll_text: " + row);

                go = new GameObject();
                if (go)
                {
                    go.name = "Txt" + no++;
                    go.transform.parent = transform;

                    Text txtGo = go.AddComponent<Text>();

                    txtGo.font = longTextFont;
                    txtGo.color = longTextColor;
                    txtGo.horizontalOverflow = HorizontalWrapMode.Overflow;
                    txtGo.verticalOverflow = VerticalWrapMode.Truncate;
                    txtGo.text = row;

                    RectTransform rtGo = go.GetComponent<RectTransform>();

                    width = txtGo.preferredWidth;
                    height = txtGo.preferredHeight;

                    rtGo.sizeDelta = new Vector2( width, height );

                    rtGo.anchorMin = new Vector2( 0, 1 );
                    rtGo.anchorMax = new Vector2( 0, 1 );
                    rtGo.pivot = new Vector2(0, 0);
                    rtGo.localPosition = new Vector3(PadX, y-height-PadY);

                    y -= height;
                    max_height += height;

                    if (max_width < width)
                        max_width = width;

                    lstGo.Add(go);

                    //yield return null;
                }


                RectTransform rt = this.GetComponent<RectTransform>();

                rt.sizeDelta = new Vector2( max_width + 2*PadX, max_height + 2*PadY );
                rt.localPosition = new Vector3(0, 0);


            }


            longTextSav = longText;
        }  
    }
}
