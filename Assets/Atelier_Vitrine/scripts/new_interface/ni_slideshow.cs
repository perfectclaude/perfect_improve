﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_slideshow : MonoBehaviour
{
	string SlideShowText = "";
	IDictionary SlideShowJson = null;
	int SlideShowIndex;
	Texture2D slidetexture;
	string bitmaptoload = "";
	long timetowait = 0;
	public static bool SlideShowShowImage = false;
	public static float SlideShowTimer;
	bool simpletoggle = false;

	void Awake()
	{
		slidetexture = new Texture2D(1,1);
	}

	public void SlideShowReset()
	{
		SlideShowIndex = 0;
		SlideShowTimer = Time.time;
		SlideShowShowImage = false;
		SlideShowTimer = Time.time+60.0f;		// one minute
	}


	void Update()
	{
		if (ni_edittext.locked)		return;

		if (bufferme.GetString("inputcommandtext") != "")
		{
			if (Input.GetMouseButtonDown(0))
				SlideShowTimer = Time.time+60.0f;		// one minute

			if (SlideShowTimer < Time.time)
			{
				// 
				SlideShowTimer = Time.time+60.0f;		// one minute
				if (simpletoggle)
				{
					simpletoggle = false;
					ni_manage_screens.SetNextScreen("info_showslide");
				}
				else
				{
					simpletoggle = true;
					// go back to root screen
					for (int i=0;i<10;i++)
					{
						ni_manage_screens.SetPreviousScreen();
					}
				}
			}
		}
	}

	/*
		if (SlideShowJson != null)
		{
			if (SlideShowTimer < Time.time)
			{
				int counter = 0;
				string keytouse = "";
				foreach (var key in SlideShowJson.Keys)
				{
					if (counter == SlideShowIndex)
					{
						keytouse = key.ToString();
						break;
					}
					counter++;
				}
				keytouse = keytouse.ToLower();
				if (keytouse != "")
					timetowait = (long)SlideShowJson[keytouse];
				if (keytouse.Contains(".jpg") || keytouse.Contains(".png"))
				{
					// load bitmap from web !!
					bitmaptoload = keytouse;
					SlideShowTimer = Time.time + timetowait;
					StartCoroutine("LoadBitmap");
					return;
				}
				else
				{
					if (keytouse == "overlay_menu")		// root !!
					{
						for (int i=0;i<20;i++)
							ni_manage_screens.SetPreviousScreen();
					}
					else
					{
						// load selected screen !!!
						for (int i=0;i<20;i++)
							ni_manage_screens.SetPreviousScreen();
						ni_manage_screens.SetNextScreen(keytouse);

					}
					// show existing screen
					SlideShowShowImage = false;
				}

				SlideShowTimer = Time.time + timetowait;
				SlideShowIndex++;
				if (SlideShowIndex == SlideShowJson.Keys.Count)		SlideShowIndex = 0;
			}
		}
		*/
	/*
	IEnumerator LoadBitmap()
	{
		string url = network.WSUrl + "clients/" + bitmaptoload;
		var r = new HTTP.Request ("GET", url);
		yield return r.Send ();
			
		if (r.exception == null)
		{
			slidetexture.LoadImage (r.response.Bytes);
			SlideShowShowImage = true;
			SlideShowTimer = Time.time + timetowait;
			SlideShowIndex++;
			if (SlideShowIndex == SlideShowJson.Keys.Count)		SlideShowIndex = 0;
		}
		else
		{
			SlideShowReset();
		}

	}

	void OnGUI()
	{
		if ((SlideShowJson != null) && (SlideShowShowImage))
		{
			GUI.DrawTexture(new Rect(-1,-1,Screen.width+1,Screen.height+1),slidetexture);
		}
	}
	*/

	public void SlideShowInit(string jsontext)
	{
		/*
		if (SlideShowText != jsontext)
		{
			// different slideshow -> use it !!!
			SlideShowText = jsontext;
			if (SlideShowText.Length > 10)
			{
				try
				{
					SlideShowJson = (IDictionary)Json.Deserialize(SlideShowText);
				}
				catch
				{
					SlideShowJson = null;
				}
			}
			else
			{
				SlideShowJson = null;
			}
			SlideShowReset();
		}
		*/
	}

}
