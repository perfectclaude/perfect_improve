﻿using UnityEngine;
using System.Collections;

public class ni_company_panel : MonoBehaviour
{
	tk2dSprite sprite;
	float	nexttest = 0.0f;

	void Start ()
	{
		sprite = (tk2dSprite)gameObject.GetComponent<tk2dSprite>();
		nexttest = Time.time + 2.0f;
	}

	void Update()
	{
		if (Time.time > nexttest)
		{
			nexttest = Time.time + 2.0f;
//			if (true)
			{
				if (Application.loadedLevelName == "Demo_usine")
				{
					sprite.color = Color.green;
				}
				else
				{
					bool globalcolor = true;
					for (int i=0;i<ni_regularmainloop.nrmachine;i++)
					{
						tk2dSprite sprite2 = ni_preparedatafields.pointofinterest[i].GetComponent<tk2dSprite>();
						if (sprite2.spriteId != sprite2.GetSpriteIdByName("picto_green"))
							globalcolor = false;
					}
					if (globalcolor)
						sprite.color = Color.green;
					else
						sprite.color = Color.red;
					/*
					if (bufferme.GetInt ("ButtonState", 0) == 1)
						sprite.color = Color.green;
					else
						sprite.color = Color.red;
						*/
				}
			}
		}
	}
}
