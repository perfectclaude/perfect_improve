﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Globalization;
using System.Text;
using MiniJSON;
using System.Collections.Generic;


public class stats_param 
{
    public string value = "";
    public int max_size = 0;
}


public class ni_cvdebug : MonoBehaviour {

	//private System.DateTime dateDeb;
	//private System.DateTime dateFin;

	private InputField goDateDeb = null;
	private InputField goDateFin = null;
	private InputField goEnregMaxi = null;
    private ni_scroll_content_text goJson = null;

	// Use this for initialization
	void Start () 
	{
        //InputField[] tabIF = this.FindObjectsOfType<InputField> ();
        InputField[] tabIF = this.GetComponentsInChildren<InputField>(true);
        ni_scroll_content_text[] tabSCRTXT = this.GetComponentsInChildren<ni_scroll_content_text>(true);

		foreach (InputField goIF in tabIF) 
		{
			switch (goIF.name) 
			{
				case "IFStartDate":
					goDateDeb = goIF;
					break;

				case "IFEndDate":
					goDateFin = goIF;
					break;

				case "IFEnregMaxi":
					goEnregMaxi= goIF;
					break;

				default:
					break;
			}
		}

        foreach (ni_scroll_content_text goTXT in tabSCRTXT) 
		{
			switch (goTXT.name) 
			{
				case "TXTJson":
					goJson = goTXT;
					break;

                default:
					break;
			}
		}
            
		InitParams();

	}


	
	// Update is called once per frame
	void Update () 
	{
	
	}


	void OnEnable() 
	{
		InitParams();
	}

	void OnDisable() 
	{
		StopCoroutine("FindDataHttpRequest");
	}

	private void InitParams()
	{
		SetValue (goDateDeb, DateTime.Now.ToString ("dd/MM/yyyy 00:00:00"));
		SetValue (goDateFin, DateTime.Now.ToString ("dd/MM/yyyy HH:mm:ss"));
		SetValue (goEnregMaxi, "500");
		SetValue (goJson, "");
	}

	private void SetValue( InputField go, string val, bool bErr )
	{
		if (go != null)
		{
			go.text = val;

			if( bErr )
				go.textComponent.color = Color.red;
			else
				go.textComponent.color = Color.blue;
				
		} 
	}

	private void SetValue( InputField go, string val )
	{
		SetValue (go, val, false);
	}

	private void SetValue( Text go, string val, bool bErr )
	{
		if (go != null)
		{
			go.text = val;

			if( bErr )
				go.color = Color.red;
			else
				go.color = Color.blue;
		} 
	}

	private void SetValue( Text go, string val )
	{
		SetValue (go, val, false);
	}

    private void SetValue( ni_scroll_content_text go, string val, bool bErr )
    {
        if (go != null)
        {
            go.longText = val;

            if (bErr)
                go.longTextColor = Color.red;
            else
                go.longTextColor = Color.blue;
        }
    }

    private void SetValue( ni_scroll_content_text go, string val )
    {
        SetValue (go, val, false);
    }

	private string GetValue( InputField go )
	{
		string sRet = "";

		if (go != null)
		{
			sRet = go.text;
		} 

		return sRet;
	}

	//3232FFFF

	private long UnixTimestampFromDateTime(DateTime date)
	{
		long unixTimestamp = date.Ticks - new DateTime(1970, 1, 1).Ticks;
		unixTimestamp /= TimeSpan.TicksPerSecond;
		return unixTimestamp;
	}

	public void FindData()
	{
		StartCoroutine("FindDataHttpRequest");
	}

	IEnumerator FindDataHttpRequest()
	{
		string dateDeb;
		string dateFin;
		string enregMaxi;

		string timestampDeb = "";
		string timestampFin = "";
		int maxRec = 0;
		DateTime dt;
		long ts;


		//Debug.Log( "FindDataHttpRequest" );

		dateDeb = GetValue (goDateDeb);
		dateFin = GetValue (goDateFin);
		enregMaxi = GetValue (goEnregMaxi);

		try
		{
			dt = DateTime.ParseExact (dateDeb, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			//timestampDeb = UnixTimestampFromDateTime( dt ).ToString();
			ts = (long)network.GetMiliseconds(dt);
			timestampDeb = ts.ToString();
		}
		catch ( Exception ex )
		{
			Debug.LogError ("Error Convert DateTime:" + dateDeb + "(" + ex.Message + ")" );
			timestampDeb = "";
			//yield break;
		}

		try
		{
			dt = DateTime.ParseExact (dateFin, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			//timestampFin = UnixTimestampFromDateTime( dt ).ToString();
			ts = (long)network.GetMiliseconds(dt);
			timestampFin = ts.ToString();

		}
		catch ( Exception ex )
		{
			Debug.LogError ("Error Convert DateTime:" + dateDeb + "(" + ex.Message + ")" );
			timestampFin = "";
			//yield break;
		}

		if (!int.TryParse (enregMaxi, out maxRec)) 
		{
			Debug.LogError ("Error Convert Integer:" + enregMaxi );
			maxRec = 0;
			//yield break;
		}

		//Debug.Log( "FindDataHttpRequest:" + timestampDeb + " " + timestampFin + " " + maxRec.ToString() );


		if ((timestampDeb.Length > 0) && (timestampFin.Length > 0) && (maxRec > 0)) {

			SetValue (goJson, "Recherche en-cours ...");
		
			while(!network.GetIntermediateJson(bufferme.GetClientId(),timestampDeb,timestampFin,maxRec,true,"ni_statlegend"))
			{
				yield return new WaitForSeconds(0.2f);
			}

			while (network.waitingnetworkintermediate)
			{
				yield return new WaitForSeconds(0.2f);
			}

			//string realjson = bufferme.Decrypt(machin string);

			if (network.retreiveintermediatejson.Length > 50) 
            {

				//SetValue (goJson, network.retreivejson);

				StringBuilder sData = new StringBuilder ();
                IDictionary<string, object> dicSeries = (IDictionary<string, object>)Json.Deserialize(network.retreiveintermediatejson);
                Dictionary<string, stats_param> dicParams = new Dictionary<string, stats_param>();

            
                // On excuse 2 fois le Traitement :
                // La premieres fois c'est pour obtenir le nombres de parametre different
                // et la taille de la colonne maximum (le plus long text)
                // et la 2eme pour contruire le tableau
                for (int i = 0; i < 2; i++)
                {

                    if (i > 0)
                    {
                        int maxCar = 0;

                        foreach (KeyValuePair<string, stats_param> kvp2 in dicParams)
                        {
                            sData.Append( kvp2.Key.PadRight( kvp2.Value.max_size ) + " | " );
                            maxCar += kvp2.Value.max_size + 3;

                            dicParams[kvp2.Key].value = "";
                        }

                        sData.Append("\r\n".PadRight( maxCar + 1, '-') + "\r\n");

                    }

                    foreach (KeyValuePair<string, object> kvp in dicSeries)
                    {
                        //Debug.Log("Key:" + kvp.Key);

                        if (kvp.Key.Substring(0, 4) == "time")
                        {
                            string stime = (string)kvp.Value;

                            ts = 0;
                            long.TryParse(stime, out ts);

                            dt = network.GetDateTime((double)ts);
                        
                            stime = dt.ToString( "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                            if (!dicParams.ContainsKey("time"))
                                dicParams["time"] = new stats_param();

                            dicParams["time"].value = stime;
                        }
                        else if (kvp.Key.Substring(0, 6) == "sample")
                        {
                            IDictionary<string, object> dictVal = (IDictionary<string, object>)kvp.Value;

                            foreach (KeyValuePair<string, object> kvp2 in dictVal)
                            {
                                if (!dicParams.ContainsKey(kvp2.Key))
                                    dicParams[kvp2.Key] = new stats_param();
                                    
                                dicParams[kvp2.Key].value = (string)kvp2.Value;
                            }

                            foreach (KeyValuePair<string, stats_param> kvp2 in dicParams)
                            {
                                if (i == 0)
                                {
                                    if (kvp2.Value.max_size < kvp2.Key.Length)
                                        kvp2.Value.max_size = kvp2.Key.Length;

                                    if (kvp2.Value.max_size < kvp2.Value.value.Length)
                                        kvp2.Value.max_size = kvp2.Value.value.Length;
                                }
                                else
                                {
                                    sData.Append( kvp2.Value.value.PadLeft( kvp2.Value.max_size ) + " | " );
                                }
                            }

                            if( i > 0 )
                                sData.Append("\r\n");
                        }
                    }
                }

				//sData.Append (network.retreiveintermediatejson.Replace ("{", "{\r\n").Replace ("}", "\r\n}").Replace (",", ",\r\n").Trim());

				SetValue (goJson, sData.ToString());


			} else {
				SetValue (goJson, "Pas de données pour ces critères de sélection !");
			}
		} 
		else 
		{
			SetValue (goJson, "Critères de sélection incorrectes !", true);
			
		}
			
	}

}
