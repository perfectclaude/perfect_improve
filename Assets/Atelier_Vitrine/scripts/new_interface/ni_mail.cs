﻿using UnityEngine;
using System.Collections;
using CielaSpike;
using System.Threading;
using System;
using System.Text;
using BestHTTP;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Asn1.X509;
using System.Net;
using System.Collections.Specialized;


public class ni_mail : MonoBehaviour, ICertificateVerifyer {

    private static volatile ni_mail _instance;
    private static object syncRoot = new System.Object();
    private Uri mailURL = null;

    private ni_mail()
    {
        mailURL = new Uri(network.WSUrl + "mail.php");
        //mailURL = new Uri("https://www.google.fr");
    }

    public static ni_mail Instance
    {
        get 
        {
            if (_instance == null) 
            {
                lock (syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<ni_mail>();

                        singleton.name = "ni_mail";

                        DontDestroyOnLoad(singleton);
                    }
                }
            }

            return _instance;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool IsValid(X509CertificateStructure[] certs)
    {
        // TODO: Return false, if validation fails
        return true;
    }
   
    void OnRequestFinishedMail(HTTPRequest request, HTTPResponse response)
    {
        switch (request.State)
        {
            case HTTPRequestStates.Finished:
                if (response.IsSuccess)
                {
                    //Debug.Log("MAIL OK"+response.DataAsText);
                    ni_logger.LogDebug( "ni_mail", "OnRequestFinishedMail", "MAIL OK" );
                }
                else
                {
                    ni_logger.LogError( "ni_mail", "OnRequestFinishedMail","MAIL HS Status:"+response.StatusCode.ToString());
                }
                break;
            case HTTPRequestStates.Error:
                ni_logger.LogError( "ni_mail", "OnRequestFinishedMail","MAIL ERROR");
                break;
            case HTTPRequestStates.Aborted:
                ni_logger.LogError( "ni_mail", "OnRequestFinishedMail","MAIL ABORTED");
                break;
            case HTTPRequestStates.ConnectionTimedOut:
                ni_logger.LogError( "ni_mail", "OnRequestFinishedMail","MAIL CNXTIMEOUT");
                break;
            case HTTPRequestStates.TimedOut:
                ni_logger.LogError( "ni_mail", "OnRequestFinishedMail","MAIL TIMEOUT");
                break;
            default:
                ni_logger.LogError( "ni_mail", "OnRequestFinishedMail","MAIL HS");
                break;
        }    
    }


    IEnumerator CRTNinja_Send( string login, string mdp, string mess, HTTPProxy proxy )
    {
        yield return Ninja.JumpToUnity;

        HTTPRequest req = new HTTPRequest(mailURL,HTTPMethods.Post, OnRequestFinishedMail);
        req.Proxy = proxy;
        req.Proxy = null;
        req.CustomCertificateVerifyer = this;
        req.UseAlternateSSL = true; // A Mettre a true pour forcer les Certificats comme valide
      
        req.SetHeader ("Content-Type", "application/x-www-form-urlencoded"); // superflux (a titre informatif) car par defaut est deja dans ce type

        req.AddField("login", login);
        req.AddField("mdp", mdp);
        req.AddField("action", "Envoyer");
        req.AddField("message", mess);

        req.Send();


        //yield return Ninja.JumpBack;

        //yield return new WaitForSeconds(3.0f);
        yield break;
    }
        

    IEnumerator CRTNinja_Send_WWW( string login, string mdp, string mess, HTTPProxy proxy )
    {
        yield return Ninja.JumpToUnity;

        WWWForm req = new WWWForm();

        req.AddField("login", login);
        req.AddField("mdp", mdp);
        req.AddField("action", "Envoyer");
        req.AddField("message", mess);

        //WWW www = new WWW(network.WSUrl+"mail.php", req);
        WWW www = new WWW("https://www.piserver.fr/perfectimprove/mail.php", req);
       
        yield return www;

        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
        } else {
            Debug.Log("WWW Error: "+ www.error);
        }  

        //yield return Ninja.JumpToUnity;

        //yield return Ninja.JumpBack;

        //yield return new WaitForSeconds(3.0f);
        yield break;
    }

    private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true;
    } 

    IEnumerator CRTNinja_Send_Web( string login, string mdp, string mess, HTTPProxy proxy )
    {
        //System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
        System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

        using (var wb = new WebClient())
        {
            var data = new NameValueCollection();
            data["login"] = login;
            data["mdp"] = mdp;
            data["action"] = "Envoyer";
            data["message"] = mess;

            var response = wb.UploadValues(network.WSUrl+"mail.php", "POST", data);

            int i = 0;
        }

     
       

        //yield return Ninja.JumpToUnity;

        //yield return Ninja.JumpBack;

        //yield return new WaitForSeconds(3.0f);
        yield break;
    }


    IEnumerator CRT_Send(string mess, bool waitTask)
    {
        Task task;

        this.StartCoroutineAsync(CRTNinja_Send(bufferme.clientid,bufferme.clientkey,mess,bufferme.WhichProxy()), out task);

        if( waitTask )
            yield return StartCoroutine(task.Wait());

        //Debug.Log("State:" + task.State);

        yield break;
    }


    public bool Send( string className, string methodeName, string mess )
    {
        StringBuilder message = new StringBuilder();

        message.Append(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        message.Append( " - "+className+"."+methodeName+"\r\n" );
        message.Append( mess );

        StartCoroutine(CRT_Send(message.ToString(), false));

        return true;
    }


    public static bool MailMessage( string className, string methodeName, string mess )
    {
        ni_mail mailManager = ni_mail.Instance;

        return mailManager.Send(className, methodeName, mess);
    }
        

}
