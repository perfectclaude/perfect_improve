﻿using UnityEngine;
using System.Collections;

public class ni_admin_edit_messages : MonoBehaviour
{
	static int MaxDataFields = 100;
	GameObject toclone;
	GameObject [] myobject = new GameObject[MaxDataFields];
	int nrfields;
	public Camera 		m_ViewCamera;
	public static bool wasIpressed = false;
	Vector3 startpos;
	Vector3 dragstartpos;
	float ypos = 0.0f;
	float timerwait;

	void Awake()
	{
		Debug.Log ("ni_admin_edit_messages awake");
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		toclone = gameObject.FindInChildren ("toclone");
		toclone.SetActive (false);
		timerwait = Time.time + 3.0f;
		Debug.Log ("ni_admin_edit_messages awake end");
	}
	
	void OnDisable ()
	{
		GameObject obj;
		tk2dTextMesh tm;

		StopAllCoroutines ();
// create new Json
		if (timerwait < Time.time)
		{
			if (bufferme.HasKey("Curve_displays"))
			{
				int nrfields = bufferme.GetInt("Curve_displays");
				int n;
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("Curve_displays_variable"+n);
					string objectname = datanameid+"_message_good";
					if (bufferme.HasKey(objectname))
					{
						obj = gameObject.FindInChildren(objectname);
						if (obj)
						{
							obj = obj.FindInChildren("fillthis");
							tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
							bufferme.SetString(objectname,tm.text);
						}
					}
					objectname = datanameid+"_message_bad";
					if (bufferme.HasKey(objectname))
					{
						obj = gameObject.FindInChildren(objectname);
						if (obj)
						{
							obj = obj.FindInChildren("fillthis");
							tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
							bufferme.SetString(objectname,tm.text);
						}
					}
					objectname = datanameid+"_message_toogood";
					if (bufferme.HasKey(objectname))
					{
						obj = gameObject.FindInChildren(objectname);
						if (obj)
						{
							obj = obj.FindInChildren("fillthis");
							tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
							bufferme.SetString(objectname,tm.text);
						}
					}
				}
			}
			ni_preparedatafields.SaveClientData();
		}
	}
	
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			wasIpressed = true;
			startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
		}
		if (wasIpressed)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			startpos = vec;
			vec = gameObject.transform.position;
			vec.y += diffy;
			gameObject.transform.position = vec;
		}
		
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}
	}

	int objcounter;

	void CloneObject(string prefix,string follow)
	{
		Vector3 vec;

		string objectname = prefix + follow;
		if (bufferme.HasKey(objectname))
		{

			GameObject obj = gameObject.FindInChildren(objectname);
			if (obj == null)
			{
				obj = (GameObject)GameObject.Instantiate(toclone);
				obj.name = objectname;
				obj.transform.parent = gameObject.transform;
			}
			obj.SetActiveRecursively(true);
			myobject[objcounter] = obj;
			vec = new Vector3(0,ypos,0);
			ypos = ypos - 0.6f;
			obj.transform.localPosition = vec;
			objcounter++;

			tk2dTextMesh tmquest = (tk2dTextMesh)obj.FindInChildren("question").GetComponent<tk2dTextMesh>();
			settext.ForceText(tmquest,prefix+follow);
			tk2dTextMesh tmfill = (tk2dTextMesh)obj.FindInChildren("fillthis").GetComponent<tk2dTextMesh>();
			settext.ForceText(tmfill,bufferme.GetString(objectname));
		}
	}

	public void ReSetDisplay()
	{
		Vector3 vec;

		ypos = 0.0f;

		objcounter = 0;
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				bool fillsection = true;
				string datanameid = bufferme.GetString("DataField_"+n);

				CloneObject(datanameid,"_message_good");
				CloneObject(datanameid,"_message_bad");
				CloneObject(datanameid,"_message_toogood");
			}
		}
		toclone.SetActive (false);		
	}
	
	void OnEnable ()
	{
		ReSetDisplay ();
	}
	
}
