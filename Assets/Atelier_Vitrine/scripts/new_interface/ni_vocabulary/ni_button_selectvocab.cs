﻿using UnityEngine;
using System.Collections;

public class ni_button_selectvocab : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_vocabulary.popup.active)		return;

		if (gameObject.name == "PLUS")
		{
			ni_vocabulary.popup.SetActive(true);

		}
		else
		{
			ni_vocabulary.nextlevel = gameObject.name;
			ni_vocabulary.n_dictionarylevels ++;
			tk2dSlicedSprite sprite = (tk2dSlicedSprite)gameObject.GetComponent<tk2dSlicedSprite>();
			sprite.color = Color.green;
		}
	}
}
