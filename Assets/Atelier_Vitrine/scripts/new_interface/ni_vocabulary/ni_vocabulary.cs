﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_vocabulary : MonoBehaviour
{
	public static bool saveme = false;
	public static string nextlevel;
	public static int n_dictionarylevels;
	public string savetype = "issue";
	public int selectedmachine = 0;
	public int selecteditem = 0;
	public static GameObject popup = null;

	static int maxdepth = 5;

	public static IDictionary [] dictionarylevels = new IDictionary [maxdepth];
	static string emptytext;
	string [] dictionarylevels_string = new string[maxdepth];
	static int lastn_dictionarylevels;
	GameObject clone;
	GameObject phrasehelptext;
	tk2dTextMesh phrase;
	bool loadingvocabulary = false;
	public static bool ForceIt = false;

	GameObject textarrow;
	GameObject textarrowpanel;
	tk2dTextMesh textsectiontype;
	string [] textarrowlayertexts = new string[maxdepth];

	public static void AddNewVocabulary(string newtext)
	{
		IDictionary newdict = (IDictionary)Json.Deserialize(emptytext);

		dictionarylevels[n_dictionarylevels].Add(newtext,newdict);
// redraw page here
		lastn_dictionarylevels = n_dictionarylevels-1;
	}

	void Awake ()
	{
		popup = gameObject.transform.parent.gameObject.FindInChildren("popup");
		textarrow = gameObject.transform.parent.gameObject.FindInChildren("textarrow");
		textarrowpanel = textarrow.FindInChildren("textarrowpanel");
		textsectiontype = (tk2dTextMesh)textarrowpanel.FindInChildren("textsectiontype").GetComponent<tk2dTextMesh>();

		phrasehelptext = gameObject.transform.parent.gameObject.FindInChildren("phrasehelptext");
		clone = gameObject.FindInChildren("clone");
		clone.SetActive(false);
		phrase = (tk2dTextMesh)gameObject.FindInChildren("phrase").GetComponent<tk2dTextMesh>();
		for (int i=0;i<maxdepth;i++)		dictionarylevels_string[i] = "";

		#if UNITY_ANDROID && (!UNITY_EDITOR)
		WWW newwww = new WWW(Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "vocab.txt");
		while (!newwww.isDone)
		{
		}
		emptytext = newwww.text;
		#else
		emptytext = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "vocab.txt");
		#endif

		dictionarylevels[0] = (IDictionary)Json.Deserialize(emptytext);
		n_dictionarylevels = 0;
		lastn_dictionarylevels = n_dictionarylevels-1;
	}

	void OnEnable()
	{
// try and load the vocabulary before starting ... use MACHINE + ITEM to define which one to load
		loadingvocabulary = true;
		StartCoroutine("LoadVocabulary");

		for (int i=0;i<maxdepth;i++)		dictionarylevels_string[i] = "";
		n_dictionarylevels = 0;
		lastn_dictionarylevels = n_dictionarylevels-1;

		if (popup == null)	return;
		settext.ForceText(phrase,"");

		popup.SetActive(false);
	}

	public static void ForceOnEnable()
	{
		ForceIt = true;
	}

	void _ForceOnEnable()
	{
		StopAllCoroutines();
		n_dictionarylevels = 0;
		lastn_dictionarylevels = n_dictionarylevels-1;
		ForceIt = false;

		loadingvocabulary = true;

		StartCoroutine("LoadVocabulary");
		
		for (int i=0;i<maxdepth;i++)		dictionarylevels_string[i] = "";
		n_dictionarylevels = 0;
		lastn_dictionarylevels = n_dictionarylevels-1;
		
		if (popup == null)	return;
		settext.ForceText(phrase,"");
		
		popup.SetActive(false);
	}

	string local_retreivejson = "";
	void GetJsonBeforeOverwritten()
	{
		local_retreivejson = network.retreivejson;
	}

	IEnumerator LoadVocabulary()
	{
		clone.SetActive(false);
		phrasehelptext.SetActive(true);

		for (int alldepths=0;alldepths<maxdepth;alldepths++)
		{
			GameObject obj = gameObject.FindInChildren ("vocabulary_"+alldepths);
			if (obj != null)				Destroy (obj);
			textarrowlayertexts[alldepths] = "";
		}

		int planaction_machine = bufferme.GetInt("planaction_machine");
		int planaction_currentitem = bufferme.GetInt("planaction_currentitem");


		if (bufferme.GetInt ("vocabularytype")==0)
		{
			ni_plandaction_listactions.Vocabulary_title("Qualification description problème");
			if (bufferme.GetInt("planaction_currentitem") == 1)
			{
				textarrowlayertexts[0] = "Section name 1";
				textarrowlayertexts[1] = "Section name 2";
				textarrowlayertexts[2] = "Section name 3";
			}
		}
		if (bufferme.GetInt ("vocabularytype")==1)
			ni_plandaction_listactions.Vocabulary_title("Description de l'action à entreprendre");
		if (bufferme.GetInt ("vocabularytype")==2)
		{
			planaction_machine = 0;
			planaction_currentitem = 0;
			ni_plandaction_listactions.Vocabulary_title("Choisissez le nom de la personne");
		}

		if (bufferme.GetInt ("vocabularytype")==3)
		{
			ni_plandaction_listactions.Vocabulary_title("Qualification du problème");
			textarrowlayertexts[0] = "Problème";
		}

		int keytosave = 1+(planaction_machine * 1000 + planaction_currentitem) * 20 + bufferme.GetInt ("vocabularytype");

		string usestring = "";

		if (network.FORCENWOFF)
		{
// load vocab now
			string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId () +"/v" + bufferme.GetClientId() + keytosave.ToString()+".txt";
			if (System.IO.File.Exists(pathname))
			{
				#if UNITY_ANDROID && (!UNITY_EDITOR)
				WWW newwww = new WWW(pathname);
				while (!newwww.isDone)
				{
				}
				usestring = newwww.text;
				#else
				usestring = System.IO.File.ReadAllText(pathname);
				#endif
			}
			else
				usestring = "";
		}
		else
		{
			while (!network.GetJson("v"+bufferme.GetClientId(),keytosave.ToString(),(keytosave+1).ToString(),3,GetJsonBeforeOverwritten))
			{
				yield return new WaitForSeconds (0.03f);
			}
			while (network.waitingnetwork)
			{
				yield return new WaitForSeconds (0.03f);
			}
			usestring = local_retreivejson;
		}

		if ((usestring != "") && (usestring.Length > 10))
		{
			dictionarylevels[0] = (IDictionary)Json.Deserialize(usestring);
			n_dictionarylevels = 0;
			lastn_dictionarylevels = n_dictionarylevels-1;
			for (int cnt=0;cnt<maxdepth;cnt++)
				dictionarylevels_string[cnt] = "";
			
			//			Debug.Log(local_retreivejson);
		}
		else
			dictionarylevels[0] = (IDictionary)Json.Deserialize(emptytext);

		yield return new WaitForSeconds(0.03f);
		loadingvocabulary = false;
	}

	float EndOfText(GameObject obj)
	{
		BoxCollider collide = (BoxCollider)obj.GetComponent<BoxCollider>();
		if (collide != null)
			Destroy (obj.GetComponent<Collider>());
		// generate new collider
		obj.AddComponent<BoxCollider>();
		
		collide = (BoxCollider)obj.GetComponent<BoxCollider>();
		
		Bounds bound = collide.bounds;
		Vector3 size = bound.size;
		return (size.x);
	}

	void Update()
	{
		if (ForceIt)		_ForceOnEnable();
		if (loadingvocabulary)		return;

		GameObject obj;
		GameObject myobject;
		Vector3 vec = Vector3.zero;
		vec.y = 0.05f;
		Vector3 scale = Vector3.one;
		Vector3 myposition = Vector3.zero;
		bool validchangedic = true;
		tk2dTextMesh tm;

		myposition.y = -0.105f;
		myposition.x = -0.06f + EndOfText(phrase.gameObject);
		textarrow.transform.localPosition = myposition;

		clone.SetActive(false);
		if (lastn_dictionarylevels != n_dictionarylevels)
		{
			if (n_dictionarylevels > 0)				phrasehelptext.SetActive(false);
			else 
				phrasehelptext.SetActive(true);

			if (n_dictionarylevels >= maxdepth)
			{
				n_dictionarylevels = maxdepth-1;
				dictionarylevels_string[n_dictionarylevels] = nextlevel;
			}
			else
			{
				myposition = Vector3.zero;
				
				//			Debug.Log ("Currentlevel : "+n_dictionarylevels+ " " + nextlevel);
				if ((n_dictionarylevels != 0) && (lastn_dictionarylevels < n_dictionarylevels))
				{
					object jsonobj = dictionarylevels[n_dictionarylevels-1][nextlevel];
					//				Debug.Log (jsonobj.GetType().ToString());
					if (!jsonobj.GetType().ToString().Contains(".Dictionary"))
						validchangedic = false;
				}
				
				if (!validchangedic)
				{
					for (int alldepths=0;alldepths<maxdepth;alldepths++)
					{
						obj = gameObject.FindInChildren ("vocabulary_"+alldepths);
						if (obj != null)				Destroy (obj);
					}

					lastn_dictionarylevels = n_dictionarylevels;
					dictionarylevels_string[n_dictionarylevels] = nextlevel;
				}
				else
				{
					if (lastn_dictionarylevels > n_dictionarylevels)
					{
						dictionarylevels_string[lastn_dictionarylevels] = nextlevel;
						dictionarylevels_string[n_dictionarylevels] = nextlevel;
					}
					else
					{
						if (n_dictionarylevels != 0)
						{
							dictionarylevels[n_dictionarylevels] = (IDictionary)dictionarylevels[n_dictionarylevels-1][nextlevel];
							dictionarylevels_string[n_dictionarylevels-1] = nextlevel;
						}
					}
					
					for (int alldepths=0;alldepths<maxdepth;alldepths++)
					{
						obj = gameObject.FindInChildren ("vocabulary_"+alldepths);
						if (obj != null)				Destroy (obj);
					}

					obj = new GameObject();
					obj.name = "vocabulary_"+n_dictionarylevels;
					obj.transform.parent = gameObject.transform;
					obj.transform.localPosition = vec;
					obj.transform.localScale = scale;
					
					foreach(var key in dictionarylevels[n_dictionarylevels].Keys)
					{
						myobject = (GameObject)GameObject.Instantiate(clone);
						myobject.name = key.ToString();
						myobject.transform.parent = obj.transform;
						myobject.transform.localPosition = myposition;
						myobject.SetActive(true);
						tm = (tk2dTextMesh)myobject.FindInChildren("on").GetComponent<tk2dTextMesh>();
						settext.ForceText(tm,key.ToString());
						tm = (tk2dTextMesh)myobject.FindInChildren("off").GetComponent<tk2dTextMesh>();
						settext.ForceText(tm,key.ToString());
						myposition.y -= 0.3f;
						if (myposition.y <= -1.2f)
						{
							myposition.x += 0.55f;
							myposition.y = 0.0f;
						}
					}
					// add + icon here
					myobject = (GameObject)GameObject.Instantiate(clone);
					myobject.name = "PLUS";
					myobject.transform.parent = obj.transform;
					myobject.transform.localPosition = myposition;
					myobject.SetActive(true);
					tm = (tk2dTextMesh)myobject.FindInChildren("on").GetComponent<tk2dTextMesh>();
					settext.ForceText(tm,"+\nAjoutez un élément");
					tm = (tk2dTextMesh)myobject.FindInChildren("off").GetComponent<tk2dTextMesh>();
					settext.ForceText(tm,"+\nAjoutez un élément");
					myposition.y -= 0.5f;
					if (myposition.y < -1.4f)
					{
						myposition.x += 0.5f;
						myposition.y = 0.0f;
					}
					
					lastn_dictionarylevels = n_dictionarylevels;

					if (textarrowlayertexts[n_dictionarylevels] == "")
						textarrowpanel.SetActive(false);
					else
						textarrowpanel.SetActive(true);
					settext.ForceText(textsectiontype,textarrowlayertexts[n_dictionarylevels]);
				}
			}
			string fulltext = "";
			for (int cnt=0;cnt<maxdepth;cnt++)
				fulltext = fulltext + dictionarylevels_string[cnt] + " ";
//			if (fulltext == "     ")
//				fulltext = fulltext + "...";
			settext.ForceText(phrase,fulltext);
		}
	}
}
