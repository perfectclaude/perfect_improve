﻿using UnityEngine;
using System.Collections;

public class ni_button_backvocab : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_vocabulary.n_dictionarylevels > 0)
		{
			ni_vocabulary.nextlevel = "";
			ni_vocabulary.n_dictionarylevels --;
		}
	}
}
