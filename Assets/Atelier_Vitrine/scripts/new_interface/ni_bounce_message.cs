﻿using UnityEngine;
using System.Collections;

public class ni_bounce_message : MonoBehaviour
{
	Vector3 startvector;
	float	vector = 0.0f;

	void Awake ()
	{
		startvector = gameObject.transform.localPosition;
		vector = 0.009f;
	}
	
	void Update ()
	{
		Vector3 myvector = gameObject.transform.localPosition;
		myvector.y += vector;
		vector -= 0.0005f;
		if (myvector.y < startvector.y)
		{
			vector = -vector * 0.7f;
			if (vector < 0.003f)		vector = 0.009f;		// jump again
			myvector.y = startvector.y;
		}
		gameObject.transform.localPosition = myvector;

	}
}
