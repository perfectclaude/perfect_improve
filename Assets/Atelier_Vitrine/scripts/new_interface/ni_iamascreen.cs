﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;

public class ni_iamascreen : MonoBehaviour
{
	GameObject showdatetime;
	GameObject ANCHORPOINT2D;

	void Awake ()
	{
		if (Application.loadedLevelName.Contains("ScenePC_Workshop_VR"))
		{
			ANCHORPOINT2D = GameObject.Find("ANCHORPOINT2D");
		}
		showdatetime = gameObject.FindInChildren ("showdatetime");

		if (Application.loadedLevelName.Contains("ScenePC_Workshop_VR"))
		{
			gameObject.transform.position = ANCHORPOINT2D.transform.position;
			Vector3 rotme = ANCHORPOINT2D.transform.eulerAngles;
//			rotme.y = rotme.y + 180.0f;
			gameObject.transform.eulerAngles = rotme;
		}
		else
		{
			gameObject.transform.localPosition = new Vector3 (0, 0, 0);
		}
	}

	void Update ()
	{
		if (Application.loadedLevelName.Contains("ScenePC_Workshop_VR"))
		{
			gameObject.transform.position = ANCHORPOINT2D.transform.position;
			Vector3 rotme = ANCHORPOINT2D.transform.eulerAngles;
//			rotme.y = rotme.y + 180.0f;
			gameObject.transform.eulerAngles = rotme;
		}

		if (showdatetime)
		{
			tk2dTextMesh tm = (tk2dTextMesh)showdatetime.GetComponent<tk2dTextMesh>();
			string timetext = DateTime.Now.ToShortTimeString();
			settext.ForceText(tm,timetext);
		}
	}
}
