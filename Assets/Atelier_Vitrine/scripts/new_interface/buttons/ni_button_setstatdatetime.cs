﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class ni_button_setstatdatetime : buttonswap
{
	string [] MonthName = { "Jan", "Feb", "Mar", "Apr", "May", "Juin", "Juil", "Aout", "Sep", "Oct", "Nov", "Dec" };

	ni_popselectmenu popselectmenu = null;

	ni_getsetdatetimefromdropdowns starttime;
	ni_getsetdatetimefromdropdowns endtime = null;
	GameObject informationdisplay;
	bool myonenable = false;
	tk2dTextMesh tm_informationdisplay;

	void Awake()
	{
		_Awake ();
		GameObject father = gameObject.transform.parent.gameObject;
		ni_statlegend stsc = (ni_statlegend)father.GetComponent<ni_statlegend>();
		if (gameObject.name == "button_recalc")
		{
			while (stsc == null)
			{
				father = father.transform.parent.gameObject;
				stsc = (ni_statlegend)father.GetComponent<ni_statlegend>();
			}
		}

		father = GameObject.Find ("statistics_view");
		starttime = (ni_getsetdatetimefromdropdowns)father.FindInChildren("starttime").GetComponent<ni_getsetdatetimefromdropdowns>();
		endtime = (ni_getsetdatetimefromdropdowns)father.FindInChildren("endtime").GetComponent<ni_getsetdatetimefromdropdowns>();
		informationdisplay = GameObject.Find ("statistics_view").FindInChildren ("informationtext");
		tm_informationdisplay = (tk2dTextMesh)informationdisplay.GetComponent<tk2dTextMesh>();

		popselectmenu = gameObject.FindpopselectmenuInChildren(0);

		if (gameObject.name.Contains ("button_recalc"))
		{
			/*
			DateTime edt = DateTime.Now;		// fake calcs
			DateTime sdt = edt.AddHours(-1);
			
			ni_statlegend.startdate = sdt;	// fake
			ni_statlegend.enddate = edt;	// fake
			ni_statlegend.savexls = false;
			
			tk2dTextMesh tm = (tk2dTextMesh)informationdisplay.GetComponent<tk2dTextMesh>();
			settext.ForceText(tm_informationdisplay,"Year 2014");
			ni_statlegend.FAKESTATS = true;

			ni_statlegend.shownewstats = true;
			*/

			ni_statlegend.button_toggle_index = 2;
			settext.ForceText(tm_informationdisplay,"2015");		// "Aujourd'hui"

			DateTime dt = DateTime.Now;
//			Debug.Log(gameObject.name + " " + dt.ToShortDateString());

			DateTime epoch = new DateTime (dt.Year, dt.Month, dt.Day);
			starttime.SetDateTime(epoch); //.AddHours (-1));
			endtime.SetDateTime(epoch.AddHours (24));

			ni_statlegend.startdate = starttime.GetDateTime();
			ni_statlegend.enddate = endtime.GetDateTime();

			ni_statlegend.FAKESTATS = false;
			ni_statlegend.savexls = false;
			ni_statlegend.savecsv = false;
			ni_statlegend.shownewstats = true;

			return;
		}

//		if (popselectmenu != null)	Debug.Log ("Popupfound !!!");
	}

	void OnEnable()
	{
		_OnEnable();
		myonenable = true;
		if (popselectmenu != null)
		{
			if (popselectmenu.submenu != null)
				popselectmenu.submenu.SetActive(false);
		}
	}

	void Update()
	{
		_Update();
		if (myonenable)
		{
			myonenable = false;
			/*
			if (gameObject.name.Contains ("settoday"))
			{
//				ni_statlegend.statlegendtitle = "Today / Hour";
				DateTime dt = DateTime.Now;
				DateTime epoch = new DateTime (dt.Year, dt.Month, dt.Day, 0, 0, 0, 0); //.ToLocalTime ();
				starttime.SetDateTime(epoch); //.AddHours (-1));
				endtime.SetDateTime(epoch.AddHours (24));
				ni_statlegend.button_toggle_index = 0;
				int mm = epoch.AddHours(8).Month;
				while (mm < 0)		mm+=12;
				while (mm > 11)		mm-=12;
				settext.ForceText(tm_informationdisplay,epoch.AddHours(8).Day + " " + MonthName[mm] + " " +epoch.Year);
			}
			*/
		}

/*
		if (myonenable)
		{
			myonenable = false;
			if (gameObject.name.Contains ("year"))
			{
				if (endtime != null)
				{
//					ni_statlegend.statlegendtitle = "Year / Month";
					DateTime dt = DateTime.Now;
					DateTime epoch = new DateTime (dt.Year, 1, 1, 0, 0, 0, 0); //.ToLocalTime ();
//					epoch = epoch.AddHours(-1);
					endtime.SetDateTime(epoch.AddYears (1));
					starttime.SetDateTime(epoch);
					ni_statlegend.button_toggle_index = 2;
				}
			}
		}
*/
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		ni_statlegend.drawstatorder = 0;

		if (gameObject.name.Contains ("demo"))
		{
			DateTime edt = DateTime.Now;		// fake calcs
			DateTime sdt = edt.AddHours(-1);
			
			ni_statlegend.startdate = sdt;	// fake
			ni_statlegend.enddate = edt;	// fake
			ni_statlegend.savexls = false;
			
			tk2dTextMesh tm = (tk2dTextMesh)informationdisplay.GetComponent<tk2dTextMesh>();
			settext.ForceText(tm_informationdisplay,"Année 2014");
			ni_statlegend.FAKESTATS = true;
			ni_statlegend.shownewstats = true;
			return;
		}

		if (gameObject.name.Contains ("createsheet"))
		{
			ni_statlegend.savexls = true;
			ni_statlegend.savecsv = true;
			return;
		}

		if (popselectmenu != null)
			popselectmenu.TogglePop();

		if (gameObject.name.Contains ("recalc"))
		{
			//			ni_statlegend.statlegendtitle = "Today / Hour";
			ni_statlegend.button_toggle_index = 0;
			ni_statlegend.startdate = starttime.GetDateTime();
			ni_statlegend.enddate = endtime.GetDateTime();

//			settext.ForceText(tm_informationdisplay,"Période " + ni_statlegend.startdate.ToShortDateString() + " - " + ni_statlegend.enddate.ToShortDateString());
			settext.ForceText(tm_informationdisplay,"Période " + ni_statlegend.startdate.ToString("dd-MM-yyyy") + " - " + ni_statlegend.enddate.ToString("dd-MM-yyyy"));

			ni_statlegend.FAKESTATS = false;
			ni_statlegend.savexls = false;
			ni_statlegend.savecsv = false;
			ni_statlegend.shownewstats = true;
		}

		if (gameObject.name.Contains ("week"))
		{
			DateTime dt = DateTime.Now;
			DateTime epoch = new DateTime (dt.Year, 1, 1, 0, 0, 0, 0); //.ToLocalTime ();
			epoch = epoch.AddDays((bufferme.GetWeekNr(dt) - 1) * 7);
//			epoch = epoch.AddHours(-1);
			endtime.SetDateTime(epoch.AddDays (7));
			starttime.SetDateTime(epoch);
			ni_statlegend.button_toggle_index = 1;
			settext.ForceText(tm_informationdisplay,"Semaine "+bufferme.GetWeekNr(dt)+", "+epoch.Year);
		}
		if (gameObject.name.Contains ("year"))
		{
//			ni_statlegend.statlegendtitle = "Year / Month";
			DateTime dt = DateTime.Now;
			DateTime epoch = new DateTime (dt.Year, 1, 1, 0, 0, 0, 0); //.ToLocalTime ();
//			epoch = epoch.AddHours(-1);
			endtime.SetDateTime(epoch.AddYears (1));
			starttime.SetDateTime(epoch);
			ni_statlegend.button_toggle_index = 2;
			settext.ForceText(tm_informationdisplay,epoch.AddHours(8).Year.ToString());
		}

		if (gameObject.name.Contains ("settoday"))
		{
//			ni_statlegend.statlegendtitle = "Today / Hour";
			DateTime dt = DateTime.Now;
			DateTime epoch = new DateTime (dt.Year, dt.Month, dt.Day, 0, 0, 0, 0); //.ToLocalTime ();
			starttime.SetDateTime(epoch); //.AddHours (-1));
			endtime.SetDateTime(DateTime.Now);
			ni_statlegend.button_toggle_index = 0;
			int mm = epoch.AddHours(8).Month;
			while (mm < 0)		mm+=12;
			while (mm > 11)		mm-=12;

			int mm2 = DateTime.Today.AddHours(8).Month;
			while (mm2 < 0)		mm2+=12;
			while (mm2 > 11)		mm2-=12;
			string local_displayperiod = epoch.AddHours(8).Day + " " + MonthName[mm] + " " +epoch.Year;
			if (local_displayperiod == DateTime.Today.AddHours(8).Day + " " + MonthName[mm2] + " " +DateTime.Today.Year)
			    local_displayperiod = "Aujourd'hui";
//			settext.ForceText(tm_informationdisplay,local_displayperiod);
		}

		/*
		if (gameObject.name.Contains ("showstats"))
		{
			DateTime sdt = starttime.GetDateTime();
			DateTime edt = endtime.GetDateTime();

			ni_statlegend.startdate = sdt;
			ni_statlegend.enddate = edt;
			ni_statlegend.savexls = false;

			tk2dTextMesh tm = (tk2dTextMesh)informationdisplay.GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,"Period: " + sdt.ToShortDateString()+ " "+sdt.ToLongTimeString() + " - "+edt.ToShortDateString()+ " "+edt.ToLongTimeString());
			ni_manage_screens.SetNextScreen("statistics_view");
		}
		*/

	}
}
