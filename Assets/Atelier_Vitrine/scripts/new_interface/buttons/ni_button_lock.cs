﻿using UnityEngine;
using System.Collections;

public class ni_button_lock : buttonswap
{
	tk2dSprite sprite;

	void Awake()
	{
		_Awake ();
		sprite = (tk2dSprite)gameObject.GetComponent<tk2dSprite>();
		if (ni_edittext.locked)
			sprite.SetSprite(sprite.GetSpriteIdByName("lock_closed"));
		else
			sprite.SetSprite(sprite.GetSpriteIdByName("lock_opened"));
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_edittext.locked)
			ni_edittext.locked = false;
		else
			ni_edittext.locked = true;
		if (ni_edittext.locked)
			sprite.SetSprite(sprite.GetSpriteIdByName("lock_closed"));
		else
			sprite.SetSprite(sprite.GetSpriteIdByName("lock_opened"));
	}
}
