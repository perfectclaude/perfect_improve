﻿using UnityEngine;
using System.Collections;

public class ni_button_colorchange : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		
		tk2dSlicedSprite sprite = (tk2dSlicedSprite)gameObject.GetComponent<tk2dSlicedSprite> ();
		if (sprite.color == Color.black)
		{
			sprite.color = Color.red;
		}
		else
		{
			if (sprite.color == Color.red)
			{
				sprite.color = Color.green;
			}
			else
			{
				if (sprite.color == Color.green)
				{
					sprite.color = Color.white;
				}
				else
				{
					sprite.color = Color.black;
				}
			}
		}
	}
}
