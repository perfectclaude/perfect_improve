﻿using UnityEngine;
using System.Collections;

public class ni_button_panelup : buttonswap
{
	GameObject mypanel;
	Vector3		myvec;
	Vector3		myvec_up;
	Vector3 	scale;
	public static bool pageup = false;

	GameObject button_createsheet;
	GameObject button_recalc;
	GameObject email;


	void Awake()
	{
		_Awake ();

		mypanel = GameObject.Find("statistics_view").FindInChildren("exportpanel");
		button_createsheet = mypanel.FindInChildren("button_createsheet");
		button_recalc = mypanel.FindInChildren("button_recalc");
		email = mypanel.FindInChildren("email");

		myvec = mypanel.transform.localPosition;
		myvec_up = myvec;
		myvec_up.y += 1.40f;
	}

	void Update()
	{
		_Update ();
		if (pageup)
			ScrollPageUp();
		else
			ScrollPageDown();
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (gameObject.name.Contains("quit"))
		{
			if (pageup)
				pageup = false;
		}
		else
		{
			Debug.Log ("Try ");
			button_createsheet.SetActive(false);
			button_recalc.SetActive(true);
			email.SetActive(false);
			pageup = true;
		}
	}

	void ScrollPageUp()
	{
		mypanel.transform.localPosition = myvec_up;
	}

	void ScrollPageDown()
	{
		mypanel.transform.localPosition = myvec;
	}
}
