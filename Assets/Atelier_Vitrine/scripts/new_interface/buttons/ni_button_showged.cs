﻿using UnityEngine;
using System.Collections;
using IndieYP;

public class ni_button_showged : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		string GedLink = "http://10.1.1.200/lizaigne";
//		string GedLink = "GED/1_Fiches_instructions/mainmenu.htm";
		string finallink;

		if (GedLink.Contains ("http"))
		{
#if UNITY_EDITOR
			Application.OpenURL(GedLink);
#else
#if UNITY_IPHONE

#else
#if UNITY_ANDROID
			
#else
			Application.OpenURL(GedLink);
#endif
#endif
#endif
		}
		else
		{
			finallink = PDFReader.AppDataPath + "/" + bufferme.GetClientId() + "/" + GedLink;

#if UNITY_EDITOR
			finallink = "file://"+Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "/" + GedLink;
			Debug.Log (finallink);

			Application.OpenURL(finallink);
			#else
			#if UNITY_IPHONE
			if (finallink.Contains(".htm"))
				PDFReader.OpenHTMLLocal(finallink,"GED");
			else
				PDFReader.OpenDocInWebViewLocal(finallink, "GED");
			#else
			#if UNITY_ANDROID
			StartCoroutine(PDFReader.OpenDocLocal(finallink));
			#else
			finallink = "file://"+Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "/" + GedLink;
			Debug.Log (finallink);
			Application.OpenURL(finallink);
			#endif
			#endif
			#endif
		}
	}

}
