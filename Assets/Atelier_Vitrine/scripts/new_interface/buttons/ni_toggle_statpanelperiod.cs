﻿using UnityEngine;
using System.Collections;

public class ni_toggle_statpanelperiod : buttonswap
{
	public static int selectedperiod;
	tk2dSlicedSprite sprite;
	tk2dTextMesh tm;
	int myid;
	Color mygrey = new Color(0.3f,0.3f,0.3f);
	Color mygreylight = new Color(0.85f,0.85f,0.85f);

	void Awake ()
	{
		_Awake();
		selectedperiod = -1;
		sprite = (tk2dSlicedSprite)gameObject.GetComponent<tk2dSlicedSprite>();
		tm = (tk2dTextMesh)gameObject.FindInChildren("text").GetComponent<tk2dTextMesh>();
		string myidname = gameObject.name.Replace("item","");
		int.TryParse(myidname,out myid);
	}

	void Update()
	{
		_Update ();
		if (myid == selectedperiod)
		{
			sprite.color = mygreylight;
			tm.color = Color.black;
		}
		else
		{
			sprite.color = Color.grey;
			tm.color = mygrey;
		}
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		selectedperiod = myid;
	}
}
