﻿using UnityEngine;
using System.Collections;

public class ni_datafieldopenclose : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		GameObject mygrandfather = gameObject.transform.parent.parent.gameObject;
		ni_admin_edit_datafields clas = (ni_admin_edit_datafields)mygrandfather.transform.parent.gameObject.GetComponent<ni_admin_edit_datafields> ();
		if (gameObject.name.Contains("open"))
		{
			bufferme.SetInt(mygrandfather.name + "_state",0);
		}
		else
		{
			bufferme.SetInt(mygrandfather.name + "_state",1);
		}
		clas.ReSetDisplay ();
	}
}
