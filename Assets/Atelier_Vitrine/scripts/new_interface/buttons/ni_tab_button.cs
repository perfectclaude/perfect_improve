﻿using UnityEngine;
using System.Collections;

public class ni_tab_button : buttonswap
{
	public int tabnr = 0;
	public GameObject father;
	static int MaxTabs = 10;
	//tab1node
	GameObject [] allpages = new GameObject[MaxTabs];
	GameObject [] alltabs = new GameObject[MaxTabs];
	bool	myonenable = false;

	void Awake()
	{
		_Awake ();
		for (int n=0;n<MaxTabs;n++)
		{
			allpages[n] = null;
			alltabs[n] = null;
		}
		father = gameObject.transform.parent.gameObject;
		ni_iamascreen scrn = (ni_iamascreen)father.GetComponent<ni_iamascreen>();
		while (scrn == null)
		{
			father = father.transform.parent.gameObject;
			scrn = (ni_iamascreen)father.GetComponent<ni_iamascreen>();
		}
		GameObject obj = father.FindInChildren("tab1node");
		int i = 0;
		while (obj != null)
		{
			allpages[i] = obj;
			i++;
			obj = father.FindInChildren("tab"+(i+1)+"node");
		}
		obj = father.FindInChildren("tab1");
		i = 0;
		while (obj != null)
		{
			alltabs[i] = obj;
			i++;
			obj = father.FindInChildren("tab"+(i+1));
		}
		if (tabnr == 0)
		{
			tk2dSlicedSprite ssp = (tk2dSlicedSprite)gameObject.GetComponent<tk2dSlicedSprite>();
			ssp.color = Color.grey;
		}
	}

	void OnEnable()
	{
		_OnEnable ();
		myonenable = true;
	}

	void OnDisable()
	{
		_OnDisable();
		myonenable = false;
	}

	void Update()
	{
		_Update ();
		if (myonenable)
		{
			myonenable = false;
			for (int n=0;n<MaxTabs;n++)
			{
				if (n!=0)
				{
					if (allpages[n] != null)
						allpages[n].SetActive(false);
				}
			}
		}
	}

	void Start()
	{
		for (int n=0;n<MaxTabs;n++)
		{
			if (n!=0)
			{
				if (allpages[n] != null)
					allpages[n].SetActive(false);
			}
		}
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		tk2dSlicedSprite ssp;
		for (int n=0;n<MaxTabs;n++)
		{
			if (allpages[n] == null) 		break;
			if (n!=tabnr)
				allpages[n].SetActive(false);
			else
				allpages[n].SetActive(true);
			ssp = (tk2dSlicedSprite)alltabs[n].GetComponent<tk2dSlicedSprite>();
			if (n==tabnr)
				ssp.color = Color.grey;
			else
				ssp.color = Color.white;
		}
	}
}
