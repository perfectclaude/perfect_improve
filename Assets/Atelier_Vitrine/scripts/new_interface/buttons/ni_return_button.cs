﻿using UnityEngine;
using System.Collections;

public class ni_return_button : buttonswap
{
	void Update()
	{
		_Update();

		// a changer pour vr
		//if (Input.GetMouseButtonDown(0))
		//	ButtonPressed();

		if ((Application.loadedLevelName != "generation_server") && ((Application.loadedLevelName != "ScenePC_Workshop2"))
			&& ((Application.loadedLevelName != "Scene_WorkshopDEMO")))
		{
			if (Input.GetMouseButtonDown(0))
				ButtonPressed();
		}

	}

	public override void ButtonPressed ()
	{

//		if (ni_edittext.locked)		return;		// return doesn't work !!!
		base.ButtonPressed();

		GameObject father = gameObject.transform.parent.gameObject;
		ni_iamascreen test = (ni_iamascreen)father.GetComponent<ni_iamascreen> ();
		while (test == null)
		{
			father = father.transform.parent.gameObject;
			test = (ni_iamascreen)father.GetComponent<ni_iamascreen> ();
		}
		if (father.name == "info_showdatafields")
		{
			ni_regularmainloop.blockpopup = false;
			ni_regularmainloop.blockslide = false;

			for (int i=0;i<ni_preparedatafields.poinofinterestscript.Length;i++)
				Destroy (father.GetComponent(ni_preparedatafields.poinofinterestscript[i]));
		}
		ni_manage_screens.SetPreviousScreen();
//		ni_endlessturning.waiting.SetActiveRecursively (false);

	}
	
}