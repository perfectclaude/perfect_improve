﻿using UnityEngine;
using System.Collections;

public class ni_forward_button : buttonswap
{
	public string WhereToGoTo;

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if ((network.SCHEDULETYPE != 0) && (WhereToGoTo == "user_admin_schedule"))		return;


		// Ouvre un Popup type Canvas UI
		//if (WhereToGoTo == "CVDebug") 
		if( true )
		{
			GameObject screen = GameObject.Find(WhereToGoTo);
			GameObject piMenu = GameObject.Find ("pi_menu");

			if( (screen == null) && (piMenu != null))
			{
				screen = piMenu.FindInChildren (WhereToGoTo); 
			}

			if (screen != null) 
			{
				//Debug.Log ("Trouve");

				// Screen de type Popup sont caché en cas de changement de Screen
				if (screen.GetComponent<ni_iamapopup > () != null) 
				{
					//screen.SetVisible (true);
					screen.SetActive (true);
					return;
				}
			} 
			else
			{
				//Debug.Log ("Non Trouve");
				
			}
		}

		ni_manage_screens.SetNextScreen(WhereToGoTo);
	}
	
}