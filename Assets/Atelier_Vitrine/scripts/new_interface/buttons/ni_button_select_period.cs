﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_button_select_period : buttonswap
{
	string myname;
	int	   i_myname;
	string displayname = "";
	ni_popselectmenu popselectmenu = null;
	GameObject informationdisplay;
	tk2dTextMesh tm_informationdisplay;

	void Awake()
	{
		_Awake ();
		informationdisplay = GameObject.Find ("statistics_view").FindInChildren ("informationtext");
		
		tm_informationdisplay = (tk2dTextMesh)informationdisplay.GetComponent<tk2dTextMesh>();

		if (gameObject.transform.parent != null)
		{
			GameObject obj = gameObject.transform.parent.gameObject;
			popselectmenu = (ni_popselectmenu)obj.GetComponent<ni_popselectmenu>();
		}
//		displayname = ni_preparedatafields.GetMachineName(i_myname);
	}
	
	void OnEnable()
	{
		_OnEnable();
		myname = gameObject.name.Replace("statisticsline","");
		int.TryParse(myname,out i_myname);
		if (gameObject.transform.parent != null)
		{
			GameObject obj = gameObject.transform.parent.gameObject;
			popselectmenu = (ni_popselectmenu)obj.GetComponent<ni_popselectmenu>();
		}

		string displayname = "";
//		displayname = ni_preparedatafields.GetMachineName(i_myname);
	}
	
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (popselectmenu != null)
			popselectmenu.submenu.SetActive(false);

//		Debug.Log ("Selected item :"+i_myname);
		DateTime dt = DateTime.Now;
		DateTime epoch = DateTime.Now;

		switch (popselectmenu.ItemsList)
		{
		case ni_popselectmenu.ItemsListEnum.Days :
			epoch = new DateTime (dt.Year, dt.Month, dt.Day, 0, 0, 0, 0); //.ToLocalTime ();
//			epoch = epoch.AddHours(-1);
			epoch = epoch.AddDays(-i_myname);
			break;
		case ni_popselectmenu.ItemsListEnum.Weeks :
			epoch = bufferme.GetYearWeekStart(dt);
			int myweek = bufferme.GetWeekNr(dt);
			int currentweek = (myweek - i_myname)-1;
			epoch = epoch.AddDays(currentweek * 7);
			break;
		case ni_popselectmenu.ItemsListEnum.Years :
			epoch = new DateTime (dt.Year, 1, 1, 0, 0, 0, 0); //.ToLocalTime ();
//			epoch = epoch.AddHours(-1);
			epoch = epoch.AddYears(-i_myname);
			break;
		}
//		Debug.Log ("Display starttime :" + epoch.ToShortDateString());
		ni_statlegend.starttime.SetDateTime(epoch);
		switch (popselectmenu.ItemsList)
		{
		case ni_popselectmenu.ItemsListEnum.Days :
			ni_statlegend.endtime.SetDateTime(epoch.AddDays(1));
			if (ni_statlegend.FORCEFAKESTATS)
				ni_statlegend.button_toggle_index = 0;
			break;
		case ni_popselectmenu.ItemsListEnum.Weeks :
			ni_statlegend.endtime.SetDateTime(epoch.AddDays(7));
			if (ni_statlegend.FORCEFAKESTATS)
				ni_statlegend.button_toggle_index = 1;
			break;
		case ni_popselectmenu.ItemsListEnum.Years :
			ni_statlegend.endtime.SetDateTime(epoch.AddYears(1));
			if (ni_statlegend.FORCEFAKESTATS)
				ni_statlegend.button_toggle_index = 2;
			break;
		}

		int mm;
		epoch = epoch.AddHours(8);		// advance a bit inside your period
		switch (popselectmenu.ItemsList)
		{
		case ni_popselectmenu.ItemsListEnum.Days :
			mm = epoch.Month-1;
			while (mm < 0)		mm+=12;
			while (mm > 11)		mm-=12;

			int mm2 = DateTime.Today.AddHours(8).Month -1;
			while (mm2 < 0)		mm2+=12;
			while (mm2 > 11)		mm2-=12;

			string local_displayperiod = epoch.Day + " " + ni_popselectmenu.MonthName[mm]+" "+epoch.Year;
			string local_compare = DateTime.Today.AddHours(8).Day + " " + ni_popselectmenu.MonthName[mm2] + " " +DateTime.Today.Year;

			if (local_displayperiod == local_compare)
				local_displayperiod = "Aujourd'hui";

			settext.ForceText(tm_informationdisplay,local_displayperiod);
			break;
		case ni_popselectmenu.ItemsListEnum.Weeks :
			settext.ForceText(tm_informationdisplay,"Semaine "+bufferme.GetWeekNr(epoch.AddDays(3))+", "+epoch.Year);
			break;
		case ni_popselectmenu.ItemsListEnum.Years :
			settext.ForceText(tm_informationdisplay,epoch.Year.ToString());
			break;
		}
// NOW RECALC THOSE CURVES !!!

		if (!ni_statlegend.FORCEFAKESTATS)
				ni_statlegend.button_toggle_index = 0;

		ni_statlegend.startdate = ni_statlegend.starttime.GetDateTime();
		ni_statlegend.enddate = ni_statlegend.endtime.GetDateTime();

		ni_statlegend.FAKESTATS = false;
		ni_statlegend.savexls = false;
		ni_statlegend.savecsv = false;
		ni_statlegend.shownewstats = true;
	}
}

