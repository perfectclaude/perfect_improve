﻿using UnityEngine;
using System.Collections;

public class ni_button_statisticsmainpage : buttonswap
{
//	statistics_main
	ni_popselectmenu popselectmenu = null;

	void Awake()
	{
		_Awake();
		popselectmenu = gameObject.FindpopselectmenuInChildren(0);
	}

	void OnEnable()
	{
		_OnEnable();

		if (popselectmenu != null)
		{
			if (popselectmenu.submenu != null)
				popselectmenu.submenu.SetActive(false);
		}

	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (popselectmenu != null)
			popselectmenu.TogglePop();
	}
}
