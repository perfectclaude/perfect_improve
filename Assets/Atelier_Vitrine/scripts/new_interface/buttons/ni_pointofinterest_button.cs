﻿using UnityEngine;
using System.Collections;
using IndieYP;

public class ni_pointofinterest_button : buttonswap
{
	public string WhereToGoTo;
	tk2dSprite sprite;
	Color col1 = new Color(1,1,1,0.3f);
	GameObject informationdisplay;
	GameObject informationtitle;

	public static int MachineID;

	public override void ButtonCamera()
	{
		ButtonSetCamera("3D Camera");
	}

	void Awake()
	{
		_Awake ();
		informationtitle = GameObject.Find ("statistics_view").FindInChildren("topborder").FindInChildren ("title");
		informationdisplay = GameObject.Find ("statistics_view").FindInChildren("topborder").FindInChildren ("informationtext");

		sprite = (tk2dSprite)gameObject.GetComponent<tk2dSprite>();
	}

	void Update()
	{
		_Update ();
		sprite.color = Color.white;
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (WhereToGoTo.Contains("showpdf:"))
		{
			string GedLink = WhereToGoTo.Replace("showpdf:","");
			string finallink;

			if (GedLink.Contains ("http"))
			{
			}
			else
			{
				finallink = PDFReader.AppDataPath + "/" + bufferme.GetClientId() + "/" + GedLink;
				#if UNITY_IPHONE
				if (finallink.Contains(".htm"))
					PDFReader.OpenHTMLLocal(finallink,"GED");
				else
					PDFReader.OpenDocInWebViewLocal(finallink, "GED");
				#endif
				#if UNITY_ANDROID
				StartCoroutine(PDFReader.OpenDocLocal(finallink));
				#endif
			}
		}
		else
		{

			ni_manage_screens.SetNextScreen(WhereToGoTo);
			if (gameObject.name.Contains("pointofinterest"))		// get poi title
			{
				GameObject objfather = GameObject.Find (WhereToGoTo);
				GameObject obj = objfather.FindInChildren ("topborder");
				obj = obj.FindInChildren ("title");
				tk2dTextMesh textmesh = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
				string itemnr = gameObject.name.Replace("pointofinterest","");
				int myval;
				int.TryParse(itemnr,out myval);
				settext.ForceText(textmesh,ni_preparedatafields.poinofinteresttitle[myval]);

				MachineID = myval;

				switch (ni_preparedatafields.poinofinterestscript[myval])
				{
				case "ni_regulardisplay" :
					objfather.AddComponent<ni_regulardisplay>();
					break;
				case "ni_regularmainloop" :
					objfather.AddComponent<ni_regularmainloop>();
					break;
				case "ni_regularverticaldisplay" :
					objfather.AddComponent<ni_regularverticaldisplay>();
					break;
				case "ni_saintlizaigne_cirmeca" :
					objfather.AddComponent<ni_saintlizaigne_cirmeca>();
					break;
				case "ni_saintlizaigne_robot" :
					objfather.AddComponent<ni_saintlizaigne_robot>();
					break;
				case "ni_saintlizaigne_stock" :
					objfather.AddComponent<ni_saintlizaigne_stock>();
					break;
				default:
					break;
				}

				bufferme.SetInt("Curve_display_item",ni_preparedatafields.poinofinterestnumber[myval]);		// now shows the "machine"
				tk2dTextMesh tm = (tk2dTextMesh)informationdisplay.GetComponent<tk2dTextMesh>();
//				settext.ForceText(tm,"Period: ");
				
				tm = (tk2dTextMesh)informationtitle.GetComponent<tk2dTextMesh>();
				settext.ForceText(tm,ni_preparedatafields.poinofinteresttitle[myval]);
			}

		}
//		changeCam.change ();
	}
}
