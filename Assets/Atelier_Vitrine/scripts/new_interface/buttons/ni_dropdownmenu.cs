﻿using UnityEngine;
using System.Collections;

public class ni_dropdownmenu : MonoBehaviour
{
	public string [] dropdownelements;
	GameObject [] dropdownobj = new GameObject[100];

	AudioSource audioSource;
	AudioClip soundbutton1;
	AudioClip soundbutton2;
	AudioClip soundbutton3;

	int selected = 0;

	public Camera 		m_ViewCamera;
	Vector3		MyScaler;
	Vector3		MyScalerTarget;
	float		Mymultiplier;
	bool		wasIpressed = false;
	float 		ysize;
	GameObject	scrollme;
	Vector3		mousepos;

	public int GetSelected()
	{
		return (selected);
	}

	public void SetSelected(int id)
	{
		if (id > dropdownelements.Length)	id = 0;
		selected = id;
		Vector3 pos = scrollme.transform.localPosition;
		pos.y = ysize * selected;
		scrollme.transform.localPosition = pos;
		for (int i=0; i<dropdownelements.Length; i++)
		{
			if (i == selected)
				dropdownobj [i].SetActive (true);
			else
				dropdownobj [i].SetActive (false);
		}
	}

	void Awake ()
	{
		for (int i=0;i<100;i++)
		{
			dropdownobj[i] = null;
		}

		audioSource = gameObject.AddComponent<AudioSource>();
		soundbutton1 =  Resources.Load("sfx_menu_clic_01") as AudioClip;
		soundbutton2 =  Resources.Load("sfx_menu_clic_02") as AudioClip;
		soundbutton3 =  Resources.Load("sfx_menu_clic_03") as AudioClip;
		scrollme = gameObject.FindInChildren ("scrollme");
		MyScaler = gameObject.transform.localScale;
		ButtonSetCamera("Main Camera");
		BoxCollider col = GetComponent<BoxCollider>();
		if (col == null)
			gameObject.AddComponent<BoxCollider>();
		col = GetComponent<BoxCollider>();
		ysize = col.size.y;

		ReCreateObjects();
	}


	public void ReCreateObjects()
	{
		float y = 0;
		gameObject.SetActiveRecursively (true);
		for (int i=dropdownelements.Length;i<100;i++)
		{
			if (dropdownobj[i] != null)
			{
				Destroy(dropdownobj[i]);
				dropdownobj[i] = null;
			}
		}
		for (int i=0;i<dropdownelements.Length;i++)
		{
			GameObject obj = gameObject.FindInChildren ("dropel"+i);
			if (obj == null)
			{
				if (i == 0)
				{
					dropdownobj [i] = gameObject.FindInChildren ("clonetext");
					y = dropdownobj [i].transform.localPosition.y;
				}
				else
					dropdownobj [i] = (GameObject)GameObject.Instantiate(dropdownobj[0]) as GameObject;
			}
			else
				dropdownobj [i] = obj;

			tk2dTextMesh tm = (tk2dTextMesh)dropdownobj [i].GetComponent<tk2dTextMesh>();
			dropdownobj [i].transform.parent = scrollme.transform;
			dropdownobj [i].name = "dropel"+i;
			Vector3 vec = dropdownobj [0].transform.localPosition;
			vec.y = y - ysize*i;
			dropdownobj [i].transform.localPosition = vec;
			settext.ForceText(tm,dropdownelements[i]);
		}
		for (int i=0; i<dropdownelements.Length; i++)
		{
			if (i == selected)
				dropdownobj [i].SetActive (true);
			else
				dropdownobj [i].SetActive (false);
		}
		Vector3 pos = scrollme.transform.localPosition;
		pos.y = ysize * selected;
		scrollme.transform.localPosition = pos;
	}

	void OnEnable()
	{
		ReCreateObjects();
		OnDisable();
	}

	void Update()
	{
		_Update();
	}

	public void _Update ()
	{
		RaycastHit hitInfo;
		Ray ray;
		
		ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
		if (Input.GetMouseButtonDown(0))
		{
			if (gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				mousepos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
				StartCoroutine("Scaleback");
				if (audioSource != null)
				{
					switch (Random.Range(0,3))
					{
					case 0 :							audioSource.clip = soundbutton1;									break;
					case 1 :							audioSource.clip = soundbutton2;									break;
					case 2 :							audioSource.clip = soundbutton3;									break;
					}
					audioSource.Play();	
				}
			}
		}
		if (wasIpressed)
		{
			Vector3 newpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float dify = newpos.y - mousepos.y;
			mousepos = newpos;
			newpos = scrollme.transform.position;
			newpos.y = newpos.y + dify*2.5f;
			scrollme.transform.position = newpos;
		}
		if (Input.GetMouseButtonUp(0))
		{
			Vector3 pos = scrollme.transform.localPosition;
			float mysel = (pos.y / ysize) + 0.5f;
			selected = (int) mysel;
			if (selected < 0)		selected = 0;
			if (selected > dropdownelements.Length-1)		selected = dropdownelements.Length-1;
			pos.y = ysize * selected;
			scrollme.transform.localPosition = pos;

			wasIpressed = false;
			for (int i=0; i<dropdownelements.Length; i++)
			{
				if (i == selected)
					dropdownobj [i].SetActive (true);
				else
					dropdownobj [i].SetActive (false);
			}
		}
	}
	
	IEnumerator Scaleback()
	{
		while (Mymultiplier > 1.0f)
		{
			yield return new WaitForSeconds(0.03f);
			Mymultiplier -= 0.04f;
			if (Mymultiplier < 1.0f)			Mymultiplier = 1.0f;
			MyScalerTarget.x = MyScaler.x * Mymultiplier;
			MyScalerTarget.y = MyScaler.y * Mymultiplier;
			MyScalerTarget.z = MyScaler.z * Mymultiplier;
			gameObject.transform.localScale = MyScalerTarget;
		}
		wasIpressed = true;
		for (int i=0; i<dropdownelements.Length; i++)
		{
			dropdownobj [i].SetActive (true);
		}

	}
	
	public void ButtonSetCamera(string cameraname)
	{
		m_ViewCamera = GameObject.Find(cameraname).GetComponent<Camera>();
	}
	
	void OnDisable()
	{
		Mymultiplier = 1.0f;
		MyScalerTarget.x = MyScaler.x * Mymultiplier;
		MyScalerTarget.y = MyScaler.y * Mymultiplier;
		MyScalerTarget.z = MyScaler.z * Mymultiplier;
		gameObject.transform.localScale = MyScalerTarget;
		if (audioSource != null)
			audioSource.Stop ();	
	}
	
}

