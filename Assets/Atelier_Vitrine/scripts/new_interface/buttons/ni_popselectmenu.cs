﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_popselectmenu : MonoBehaviour
{
	public enum ItemsListEnum {Machines, Days, Weeks,Months, Years};
	public static string [] MonthName = { "Jan", "Feb", "Mar", "Apr", "May", "Juin", "Juil", "Aout", "Sep", "Oct", "Nov", "Dec" };
	/*
// mofify the masterbutten as follows :
	ni_popselectmenu popselectmenu = null;
	void Awake()
	{
		_Awake();
		popselectmenu = gameObject.FindpopselectmenuInChildren(0);
		if (popselectmenu != null)	Debug.Log ("Popupfound !!!");
	}
	void OnEnable()
	{
		_OnEnable();
		if (popselectmenu != null)
		{
			if (popselectmenu.submenu != null)
				popselectmenu.submenu.SetActive(false);
		}
		
	}
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (popselectmenu != null)
			popselectmenu.TogglePop();
	}
*/

	public string script = "";
	public GameObject submenu = null;
	public ItemsListEnum ItemsList;

	tk2dSlicedSprite panel;
	GameObject item = null;
	Camera 		m_ViewCamera;

	int GetNrOfItems()
	{
		int i = 0;
		switch (ItemsList)
		{
		case ItemsListEnum.Machines :
			while (ni_preparedatafields.GetMachineName(i) != "")
				i++;
			break;
		case ItemsListEnum.Days :
			i = 10;
			break;
		case ItemsListEnum.Weeks :
			i = 11;
			break;
		case ItemsListEnum.Months :
			i = 12;
			break;
		case ItemsListEnum.Years :
			i = 4;
			break;
		}
		return i;
	}

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

		submenu = gameObject;
		panel = (tk2dSlicedSprite)submenu.FindInChildren("panel").GetComponent<tk2dSlicedSprite>();
		item = submenu.FindInChildren("item");

		switch (script) {
		default :
			break;
		case "ni_button_select_curvetype":
			item.AddComponent<ni_button_select_curvetype>();
			break;
		case "ni_button_select_period":
			item.AddComponent<ni_button_select_period>();
			break;
		}


			
		if (submenu)
		{
			Vector2 vec2 = panel.dimensions;

			if (ItemsList == ItemsListEnum.Machines)
				vec2.y = 100 + GetNrOfItems() * 100;
			else
				vec2.y = 80 + GetNrOfItems() * 80;

			panel.dimensions = vec2;
			submenu.SetActive(false);
			if (item != null)
			{
				Vector3 vec3;
				if (ItemsList == ItemsListEnum.Machines)
					vec3 = new Vector3(0,-0.340f,-0.9f);
				else
					vec3 = new Vector3(0,-0.30f,-0.9f);
				// create items once !!!
				for (int n=0;n<GetNrOfItems();n++)
				{
					if (n==0)
					{
					}
					else
					{
						item = GameObject.Instantiate(item) as GameObject;
						item.transform.parent = submenu.transform;		// link to father
					}
					tk2dSprite sprite = (tk2dSprite)item.GetComponent<tk2dSprite>();
					if ((n % 2) == 0)
						sprite.color = new Color(0.9f,0.9f,0.9f);
					else
						sprite.color = new Color(0.7f,0.7f,0.7f);
					item.transform.localPosition = vec3;
					item.transform.localScale = Vector3.one;
					if (ItemsList == ItemsListEnum.Machines)
						vec3.y += -0.130f;
					else
						vec3.y += -0.100f;
					item.name = "statisticsline"+n;
					item.SetActive(true);
				}
				item = null;
			}
		}
		OnEnable();

		BoxCollider col = panel.GetComponent<BoxCollider>();
		if (col == null)
			panel.gameObject.AddComponent<BoxCollider>();

		submenu.SetActive(false);
	}


	void OnEnable()
	{
		DateTime today = DateTime.Now;
		int mm;

		if (submenu != null)			//trace trough all objects and doit
		{
			for (int n=0;n<GetNrOfItems();n++)
			{
				GameObject obj = submenu.FindInChildren("statisticsline"+n);
				if (obj != null)
				{
					tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("text").GetComponent<tk2dTextMesh>();
					switch (ItemsList)
					{
					case ItemsListEnum.Machines :
						settext.ForceText(tm,ni_preparedatafields.GetMachineName(n));
						break;
					case ItemsListEnum.Days :
						mm = today.AddDays(-n).Month-1;
						while (mm < 0)		mm+=12;
						while (mm > 11)		mm-=12;
						string local_displayperiod = today.AddDays(-n).Day + " " + MonthName[mm]+" "+today.AddDays(-n).Year;
						settext.ForceText(tm,local_displayperiod);
						break;
					case ItemsListEnum.Weeks :
						settext.ForceText(tm,"Semaine "+bufferme.GetWeekNr(today.AddDays(-n*7))+", "+today.AddDays(-n*7).Year);
						break;
					case ItemsListEnum.Months :
						mm = today.AddMonths(-n).Month-1;
						while (mm < 0)		mm+=12;
						while (mm > 11)		mm-=12;
						settext.ForceText(tm,MonthName[mm]+" "+today.AddMonths(-n).Year);
						break;
					case ItemsListEnum.Years :
						settext.ForceText(tm,today.AddYears(-n).Year.ToString());
						break;
					}
				}
			}
		}

	}

	public void TogglePop()
	{
		/*
		if (submenu.active)
		{
			submenu.SetActive(false);
		}
		else
		*/
		{
			submenu.SetActive(true);
			if (item != null)
				item.SetActive(false);
		}
	}

	void Update()
	{
		RaycastHit hitInfo;
		Ray ray;

		ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
		if (Input.GetMouseButtonDown(0))
		{
			if (!panel.gameObject.GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				submenu.SetActive(false);
			}
		}
	}

}
