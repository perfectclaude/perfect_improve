﻿using UnityEngine;
using System.Collections;

public class ni_edittext_save : MonoBehaviour
{
	public string playerprefname = "";
	tk2dTextMesh	tm;
	string	lasttext = "";
	ni_edittext	edittext;

	void Awake ()
	{
		tm = (tk2dTextMesh)gameObject.GetComponent<tk2dTextMesh>();
		edittext = (ni_edittext)gameObject.GetComponent<ni_edittext>();

		if (bufferme.GetString(playerprefname) != "")
		{
			settext.ForceText(tm,bufferme.GetString(playerprefname));
			lasttext = tm.text;
		}
	}
	
	void Update ()
	{
		if (tm.text != edittext.initialtext)
		{
			if (tm.text != lasttext)
			{
				bufferme.SetString(playerprefname,tm.text);
				lasttext = tm.text;
			}
		}
	}
}
