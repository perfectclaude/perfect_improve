﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class ni_button_createsheet : buttonswap
{
	ni_getsetdatetimefromdropdowns starttime;
	ni_getsetdatetimefromdropdowns endtime = null;
	GameObject informationdisplay;
	bool myonenable = false;
	
	void Awake()
	{
		_Awake ();
		GameObject father = gameObject.transform.parent.gameObject;
		ni_statlegend stsc = (ni_statlegend)father.GetComponent<ni_statlegend>();
		if (gameObject.name.Contains ("createsheet"))
		{
			while (stsc == null)
			{
				father = father.transform.parent.gameObject;
				stsc = (ni_statlegend)father.GetComponent<ni_statlegend>();
			}
			starttime = (ni_getsetdatetimefromdropdowns)father.FindInChildren("starttime").GetComponent<ni_getsetdatetimefromdropdowns>();
			endtime = (ni_getsetdatetimefromdropdowns)father.FindInChildren("endtime").GetComponent<ni_getsetdatetimefromdropdowns>();
			informationdisplay = GameObject.Find ("statistics_view").FindInChildren ("informationtext");
		}
	}
	
	void OnEnable()
	{
		_OnEnable();
//		myonenable = true;
		
	}
	
	void Update()
	{
		_Update();
		/*
		if (myonenable)
		{
			myonenable = false;
			if (gameObject.name.Contains ("year"))
			{
				if (endtime != null)
				{
//					ni_statlegend.statlegendtitle = "Year / Month";
					DateTime dt = DateTime.Now;
					DateTime epoch = new DateTime (dt.Year, 1, 1, 0, 0, 0, 0); //.ToLocalTime ();
//					epoch = epoch.AddHours(-1);
					endtime.SetDateTime(epoch.AddYears (1));
					starttime.SetDateTime(epoch);
					ni_statlegend.button_toggle_index = 0;
				}
			}
		}
		*/
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		ni_statlegend.drawstatorder = 0;
		ni_statlegend.startdate = starttime.GetDateTime();
		ni_statlegend.enddate = endtime.GetDateTime();

		GameObject obj = gameObject.transform.parent.gameObject;
		obj = obj.FindInChildren("email");
		obj = obj.FindInChildren("emailname");
		tk2dTextMesh tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
		sendemail.myemail = tm.text;
		sendemail.mysubject = "Statistiques:"+ni_statlegend.startdate.ToShortDateString()+"-"+ni_statlegend.enddate.ToShortDateString();
		ni_statlegend.savexls = true;
		ni_statlegend.savecsv = true;
		ni_button_panelup.pageup = false;
	}

}
