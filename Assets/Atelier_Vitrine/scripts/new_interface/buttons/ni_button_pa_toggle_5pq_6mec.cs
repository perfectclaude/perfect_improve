﻿using UnityEngine;
using System.Collections;

public class ni_button_pa_toggle_5pq_6mec : buttonswap
{
	public static int whichtoggle = 0;
	GameObject fivewhy;
	GameObject sixmec;
	tk2dSlicedSprite butfivewhy;
	tk2dSlicedSprite butsixmec;

	void Awake()
	{
		_Awake ();
		GameObject obj;

		obj = gameObject.transform.parent.gameObject.FindInChildren("5questions");
		if (obj != null) fivewhy = obj;
		obj = gameObject.transform.parent.gameObject.FindInChildren("6m");
		if (obj != null) sixmec = obj;

		butfivewhy = (tk2dSlicedSprite)gameObject.transform.parent.gameObject.FindInChildren("button5questions").GetComponent<tk2dSlicedSprite>();
		butsixmec = (tk2dSlicedSprite)gameObject.transform.parent.gameObject.FindInChildren("button6m").GetComponent<tk2dSlicedSprite>();
	}

	void Start()
	{
		DisplayToggle();
	}

	void DisplayToggle()
	{
		fivewhy.SetActive(false);
		sixmec.SetActive(false);
		butfivewhy.color = Color.grey;
		butsixmec.color = Color.grey;
		switch (whichtoggle)
		{
		case 0 :
			fivewhy.SetActive(true);
			butfivewhy.color = Color.white;
			break;
		case 1 :
			sixmec.SetActive(true);
			butsixmec.color = Color.white;
			break;
		}
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (name == "button5questions")
		{
			whichtoggle = 0;
		}
		if (name == "button6m")
		{
			whichtoggle = 1;
		}
		DisplayToggle();
	}
}
