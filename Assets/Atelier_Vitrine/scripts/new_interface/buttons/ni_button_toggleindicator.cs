﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class ni_button_toggleindicator : buttonswap
{
	static int currentselectedmyid = 0;
	int myid;
	int mycurveitem = 0;
	string myindicatorname;
	string myindicatorvariable;
	static int MaxTabs = 10;
	tk2dSlicedSprite [] alltabs = new tk2dSlicedSprite [MaxTabs];
	int nalltabs = 0;

	void Awake()
	{
		GameObject father = gameObject.transform.parent.gameObject;
		int i = 0;
		while (true)
		{
			GameObject obj = father.FindInChildren("button_indicator"+i);
			if (obj == null)	break;
			tk2dSlicedSprite sprite = (tk2dSlicedSprite)obj.GetComponent<tk2dSlicedSprite>();
			alltabs[i] = sprite;
			if (i == currentselectedmyid)
			{
				alltabs[i].color = new Color(0.782f,0.782f,0.782f);
			}
			else
			{
				alltabs[i].color = Color.grey;
			}
			i++;
		}
		nalltabs = i;

		string strmyid = name.Replace("button_indicator","");
		int.TryParse(strmyid,out myid);

		_Awake ();
	}
	

	void OnEnable()
	{
		int counter = 0;
		bool shallidestroy = true;

		for (int n=0;n<bufferme.GetInt("Curve_displays");n++)
		{
			if (bufferme.GetString("Curve_displays_father"+n) == bufferme.GetString("Curve_display_rootname"))
			{
				if (myid == counter)
				{
// Great, found matching indicator, change name and item
					myindicatorname = bufferme.GetString("Curve_displays_name"+n);
					myindicatorvariable = bufferme.GetString("Curve_displays_variable"+n);
					tk2dTextMesh tm = (tk2dTextMesh)gameObject.FindInChildren("on").GetComponent<tk2dTextMesh>();
					settext.ForceText(tm,myindicatorname);
					tm = (tk2dTextMesh)gameObject.FindInChildren("off").GetComponent<tk2dTextMesh>();
					settext.ForceText(tm,myindicatorname);
					shallidestroy = false;
					mycurveitem = n;
					break;
				}
				counter++;
			}
		}
		if (shallidestroy)
		{
			gameObject.SetActive(false);
		}
	}

	void Update()
	{
		_Update();
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
// You must set these to be used when searching the statistics !!!
		bufferme.SetInt("Curve_display_item",mycurveitem);
		currentselectedmyid = myid;

		for (int i=0;i<nalltabs;i++)
		{
			if (i == currentselectedmyid)
			{
				alltabs[i].color = new Color(0.782f,0.782f,0.782f);
			}
			else
			{
				alltabs[i].color = Color.grey;
			}
		}
	}
}
