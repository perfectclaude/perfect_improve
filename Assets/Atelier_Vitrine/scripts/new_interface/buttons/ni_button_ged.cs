﻿using UnityEngine;
using System.Collections;
using IndieYP;

public class ni_button_ged : buttonswap
{
	public string WhereToGoTo;
	public string GedLink;

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		string finallink;
		if (GedLink.Contains ("http"))
		{
		}
		else
		{
			finallink = PDFReader.AppDataPath + "/" + bufferme.GetClientId() + "/" + GedLink;
			#if UNITY_IPHONE
			PDFReader.OpenDocInWebViewLocal(finallink, "GED");
			#endif
			#if UNITY_ANDROID
			StartCoroutine(PDFReader.OpenDocLocal(finallink));
			#endif
		}
	}
	
}