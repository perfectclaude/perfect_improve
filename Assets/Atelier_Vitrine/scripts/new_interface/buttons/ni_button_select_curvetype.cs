﻿using UnityEngine;
using System.Collections;

public class ni_button_select_curvetype : buttonswap
{
	string myname;
	int	   i_myname;
	GameObject informationdisplay;
	GameObject informationtitle;
	string displayname = "";
	
	void Awake()
	{
		GameObject obj;
		obj = GameObject.Find ("informationpanels");
		obj = obj.FindInChildren ("statistics_view");
		obj = obj.FindInChildren("topborder");

		informationtitle = obj.FindInChildren ("title");
		informationdisplay = obj.FindInChildren ("informationtext");

		_Awake ();
		myname = gameObject.name.Replace("statisticsline","");
		int.TryParse(myname,out i_myname);

		displayname = ni_preparedatafields.GetMachineName(i_myname);
	}

	void OnEnable()
	{
		_OnEnable();

//		string displayname = "";
		displayname = ni_preparedatafields.GetMachineName(i_myname);
	}


	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		myname = gameObject.name.Replace("statisticsline","");
		int.TryParse(myname,out i_myname);

		int counter = 0;
		string daddyname = "";

		for (int n=0;n<bufferme.GetInt("Curve_displays");n++)
		{
			if (bufferme.GetString("Curve_displays_father"+n) != daddyname)
			{
				if (i_myname == counter)
				{
					bufferme.SetInt("Curve_display_item",n);			// use machine to calc offset
					break;
				}
				daddyname = bufferme.GetString("Curve_displays_father"+n);
				counter++;
			}
		}

		displayname = ni_preparedatafields.GetMachineName(i_myname);

		tk2dTextMesh tm = (tk2dTextMesh)informationdisplay.GetComponent<tk2dTextMesh>();
//		settext.ForceText(tm,"Period: ");

		tm = (tk2dTextMesh)informationtitle.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,displayname);
		Debug.Log ("MACHINE:"+displayname+" "+i_myname);
		bufferme.SetString("Curve_display_rootname",displayname);		// now shows the "machine"

		ni_manage_screens.SetNextScreen("statistics_view");

	}
}
