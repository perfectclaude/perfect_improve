﻿using UnityEngine;
using System.Collections;

public class ni_button_panelupexport : buttonswap
{
	GameObject mypanel;
	Vector3 	scale;

	GameObject button_createsheet;
	GameObject button_recalc;
	GameObject email;
	
	void Awake()
	{
		_Awake ();
		
		mypanel = GameObject.Find("statistics_view").FindInChildren("exportpanel");
		button_createsheet = mypanel.FindInChildren("button_createsheet");
		button_recalc = mypanel.FindInChildren("button_recalc");
		email = mypanel.FindInChildren("email");
	}
	
	void Update()
	{
		_Update ();
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (gameObject.name.Contains("quit"))
		{
			if (ni_button_panelup.pageup)
				ni_button_panelup.pageup = false;
		}
		else
		{
			button_createsheet.SetActive(true);
			button_recalc.SetActive(false);
			email.SetActive(true);
			ni_button_panelup.pageup = true;
		}
	}
	
}
