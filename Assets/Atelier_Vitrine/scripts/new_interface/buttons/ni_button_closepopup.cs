﻿using UnityEngine;
using System.Collections;

public class ni_button_closepopup : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		GameObject obj = gameObject.transform.parent.parent.gameObject;
		obj.SetActive(false);
	}
}
