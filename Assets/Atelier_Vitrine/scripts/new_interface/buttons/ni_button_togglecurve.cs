﻿using UnityEngine;
using System.Collections;

public class ni_button_togglecurve : buttonswap
{
	public string state1 = "On";
	public string state2 = "Off";
	int whoami;

	void Awake ()
	{
		_Awake();
		string strwho = gameObject.name.Replace("button_toggle_n-","");
		int.TryParse(strwho,out whoami);
		tk2dTextMesh tm = (tk2dTextMesh)on.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,state1);
		tm = (tk2dTextMesh)off.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,state1);
	}

	public override void ButtonPressed ()
	{
		if (ni_button_panelup.pageup)		return;
		
		base.ButtonPressed();
		if (bufferme.HasKey("button_toggle_n"+whoami))
		{
			bufferme.DeleteKey("button_toggle_n"+whoami);
			tk2dTextMesh tm = (tk2dTextMesh)on.GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,state1);
			tm = (tk2dTextMesh)off.GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,state1);
		}
		else
		{
			bufferme.SetInt("button_toggle_n"+whoami,1);
			tk2dTextMesh tm = (tk2dTextMesh)on.GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,state2);
			tm = (tk2dTextMesh)off.GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,state2);
		}
	}

}
