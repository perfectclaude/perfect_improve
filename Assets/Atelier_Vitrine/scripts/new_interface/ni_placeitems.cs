﻿using UnityEngine;
using System.Collections;

public class ni_placeitems : MonoBehaviour
{
	GameObject	serverbutton_seuils;
	GameObject	serverbutton_schedule;
	GameObject	serverbutton_slideshow;

	void Awake ()
	{
		serverbutton_schedule = gameObject.FindInChildren("serverbutton_schedule");
		serverbutton_seuils = gameObject.FindInChildren("serverbutton_seuils");
		serverbutton_slideshow = gameObject.FindInChildren("serverbutton_slideshow");
	}
	
	void Update ()
	{
		if (network.SCHEDULETYPE != 0)
			serverbutton_schedule.SetActive(false);
		serverbutton_slideshow.SetActive(false);

	}
}
