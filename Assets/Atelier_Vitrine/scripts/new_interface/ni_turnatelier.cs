﻿using UnityEngine;
using System.Collections;

public class ni_turnatelier : MonoBehaviour
{
	AudioSource audioSource;
	AudioClip soundbutton1;
	public static bool		turnallowed = false;
	public Camera 		m_ViewCamera;
	bool wasIpressed = false;
	Vector3 startpos;
	float inertie = 0.0f;
	GameObject father;

	void Awake ()
	{
		father = gameObject.transform.parent.gameObject;

		audioSource = gameObject.AddComponent<AudioSource>();
		soundbutton1 =  Resources.Load("woosh") as AudioClip;

		if (GameObject.Find("Main Camera") != null)
			m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		else
			m_ViewCamera = null;

				inertie = 0.0f;
	}
	
	void Update ()
	{
		if (m_ViewCamera == null)		return;
		float rotval;
		if (!turnallowed)		return;
		if (buttonswap.anybuttonpressed)
		{
			wasIpressed =  false;
			return;
		}
		if (Input.GetMouseButtonDown(0))
		{
			wasIpressed = true;
			startpos = Input.mousePosition;
		}
		/*
		if (wasIpressed)
		{
			Vector3 vec = Input.mousePosition;
			if (Mathf.Abs(vec.x - startpos.x) > 12.0f)
			{
				float diffy = (vec.x - startpos.x) / 8.0F;
				startpos = vec;
				vec = gameObject.transform.localEulerAngles;
				vec.y -= diffy;
				inertie = diffy;
				gameObject.transform.localEulerAngles = vec;
			}
			else 
				inertie = 0;

//			Debug.Log ("ANGLE:"+gameObject.transform.localEulerAngles.y);

			vec = Input.mousePosition;
			if (Mathf.Abs(vec.y - startpos.y) > 12.0f)
			{
				float diffy = (vec.y - startpos.y) / 8.0F;
				startpos = vec;

				rotval = gameObject.transform.localEulerAngles.y * Mathf.PI * 2.0f / 360.0f;
				vec = father.transform.localPosition;
				rotval = 0;
				vec.x += Mathf.Sin (rotval) * diffy / 50.0f;
				vec.z += Mathf.Cos (rotval) * diffy / 50.0f;
				father.transform.localPosition = vec;
			}
		}
		else
		{
			if (inertie != 0.0f)
			{
				Vector3 vec = gameObject.transform.localEulerAngles;
				vec.y -= inertie;
				gameObject.transform.localEulerAngles = vec;
				inertie = inertie * 0.8f;
			}
		}
		*/

		if (Input.GetMouseButtonUp (0))
		{
			if (Mathf.Abs (inertie) > 0.1f)
			{
				audioSource.clip = soundbutton1;
				audioSource.Play();
			}

			wasIpressed = false;
		}
	}

	void OnEnable()
	{
		inertie = 0.0f;
		audioSource.Stop ();
	}
}
