﻿using UnityEngine;
using System.Collections;
using System.Text;
using System;
using System.IO;

public class ni_logger : MonoBehaviour {
    private static volatile ni_logger _instance;
    private static object syncRoot = new System.Object();
    private string _ficLog = "";
    private TypeLoggerLog _typLog = TypeLoggerLog.INFO;

    private long _maxLogSize = 2*1024*1024; // 2 Mo
   
    public enum TypeLoggerLog
    {
        EXCEPTION = 0,
        ERROR = 1,
        WARNING = 2,
        INFO = 3,  
        DEBUG = 4
    }
     
    private ni_logger()
    {
        //_ficLog = Application.persistentDataPath + "/" + Application.productName + ".log";
        //_ficLog = Application.dataPath + "/" + Application.productName + ".log";
        _ficLog = Application.streamingAssetsPath + "/" + Application.productName + ".log";

        if( Directory.Exists( "C:\\PerfectImprove" ) )
            _ficLog = "C:\\PerfectImprove\\" + Application.productName + ".log";

        //_typLog = TypeLoggerLog.INFO;
        _typLog = TypeLoggerLog.DEBUG;
        
    }
        
    public static ni_logger Instance
    {
        get 
        {
            if (_instance == null) 
            {
                lock (syncRoot) 
                {
                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<ni_logger>();

                        singleton.name = "ni_logger";

                        DontDestroyOnLoad(singleton);
                    }
                }
            }

            return _instance;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void LogMessage( TypeLoggerLog typLog, string className, string methodeName, string mess )
    {
        StringBuilder message = new StringBuilder();

        message.Append(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

        switch (typLog)
        {
            case TypeLoggerLog.EXCEPTION:
                message.Append(" EXCEPT ");
                break;

            case TypeLoggerLog.ERROR:
                message.Append(" ERROR  ");
                break;

            case TypeLoggerLog.WARNING:
                message.Append(" WARN   ");
                break;

            case TypeLoggerLog.INFO:
                message.Append(" INFO   ");
                break;

            case TypeLoggerLog.DEBUG:
                message.Append(" DEBUG  ");
                break;

            default: 
                message.Append(" INFO   ");
                break;
        }

        message.Append( className+"."+methodeName+" " );
        message.Append( mess );


        long sizeFile = 0;

        try
        {
            sizeFile = new FileInfo(_ficLog).Length;
        }
        catch {}

        if (sizeFile >= _maxLogSize)
        {
            // on garde 1/3 de la taille
            long jump = sizeFile - (_maxLogSize * 2 / 3);

            StreamReader fi = null;
            StreamWriter fo = null;

            try
            {
                fi = new StreamReader(_ficLog);
                fo = new StreamWriter(_ficLog+"o", false);

                fi.BaseStream.Seek( jump, SeekOrigin.Begin );
                fi.ReadLine(); // La premier ligne n'est pas complete

                while( !fi.EndOfStream )
                {
                    string lig = fi.ReadLine();

                    if( lig.Length > 0 )
                        fo.WriteLine( lig );
                }     
            }
            catch (Exception)
            {  
            }
            finally
            {
                try
                {
                    if( fi != null )
                        fi.Close();
                    fi = null;
                }
                catch (Exception)
                {
                }

                try
                {
                    if( fo != null )
                        fo.Close();
                    fo = null;
                }
                catch (Exception)
                {
                }
            }


            try
            {
                if (File.Exists(_ficLog))
                    File.Delete(_ficLog);

                if (File.Exists(_ficLog+"o"))
                    File.Move(_ficLog+"o", _ficLog);
            }
            catch (Exception)
            {
            }



        }


        if (typLog >= _typLog)
        {
            switch (typLog)
            {
                case TypeLoggerLog.EXCEPTION:
                case TypeLoggerLog.ERROR:
                    Debug.LogError(message);
                    break;

                case TypeLoggerLog.WARNING:
                    Debug.LogWarning(message);
                    break;

                default:
                    Debug.Log(message);
                    break;
            }

            StreamWriter fo = null;

            lock (syncRoot)
            {
                try
                {
                    fo = new StreamWriter(_ficLog, true);
                    fo.WriteLine(message);
                }
                catch (Exception)
                {  
                }
                finally
                {
                    try
                    {
                        if( fo != null )
                            fo.Close();
                        fo = null;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

    }

    public static void LogDebug( string className, string methodeName, string mess )
    {
        ni_logger loggerManager = ni_logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.DEBUG, className, methodeName, mess);
    }

    public static void LogInfo( string className, string methodeName, string mess )
    {
        ni_logger loggerManager = ni_logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.INFO, className, methodeName, mess);
    }

    public static void LogWarn( string className, string methodeName, string mess )
    {
        ni_logger loggerManager = ni_logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.WARNING, className, methodeName, mess);
    }

    public static void LogError( string className, string methodeName, string mess )
    {
        ni_logger loggerManager = ni_logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.ERROR, className, methodeName, mess);
    }


    public static void LogException( string className, string methodeName, string mess )
    {
        ni_logger loggerManager = ni_logger.Instance;

        loggerManager.LogMessage(TypeLoggerLog.EXCEPTION, className, methodeName, mess);
    }
}
