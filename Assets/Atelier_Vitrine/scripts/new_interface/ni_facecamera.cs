﻿using UnityEngine;
using System.Collections;

public class ni_facecamera : MonoBehaviour
{
	GameObject	Camera3D;

	void Awake()
	{
		if (Application.loadedLevelName.Contains("ScenePC_Workshop_VR"))
			Camera3D = GameObject.Find ("pi_atelier").FindInChildren ("Cam1");
		else
			Camera3D = GameObject.Find ("pi_atelier").FindInChildren ("3D Camera");
	}

	void Update()
	{
		transform.LookAt(Camera3D.transform);
	}
}