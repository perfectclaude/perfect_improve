﻿using UnityEngine;
using System;
using MiniJSON;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using BestHTTP;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Security;
using System.Threading;


public class ni_datarecovery : MonoBehaviour
{
	public static bool AutomateOn = false;
	public static bool AutomateOff = false;

	public static bool CleanAutomate1 = false;
	static int CleanAutomateType = 0;

	string SERVICErootURL = "http://127.0.0.1:60/";
	//string SERVICErootURL = "http://194.168.1.201:60/";
	long   SERVICErootLoopTime = 10;
	string getxmlcommand = "DATAXML";
	string getrazcommand = "TC";
	string getcommandcommand = "commands";
	private bool bRAZDebug = false;

    private string SvcComPI_ErrorCnx = "";
    private string SvcComPI_ErrorCnxSav = "";
    private string SvcComPI_ErrorBadPoint = "";
    private string SvcComPI_ErrorBadPointSav = "";
    HTTPRequest reqGetXML = null;
    private bool bRequestGetXMLFinished = true;
    private int iRequestGetXMLCount = 0;



	void OnGUI() {

		if (false) {
			
			StringBuilder txtIDEC = new StringBuilder ();
			StringBuilder txtCIRMECA = new StringBuilder ();
			StringBuilder txtSTAUBLI = new StringBuilder ();

			txtIDEC.Append ("Debug IDE" + "\r\n\r\n");
			txtIDEC.Append ("Idec_TempsEnMarche: " + bufferme.GetFloat ("Idec_TempsEnMarche_value") + "\r\n");
			txtIDEC.Append ("Idec_TempsEnDefaut: " + bufferme.GetFloat ("Idec_TempsEnDefaut_value") + "\r\n");
			txtIDEC.Append ("Idec_PiecesBonnes: " + bufferme.GetFloat ("Idec_PiecesBonnes_value") + "\r\n");
			txtIDEC.Append ("Idec_CorpsNC: " + bufferme.GetFloat ("Idec_CorpsNC_value") + "\r\n");
			txtIDEC.Append ("Idec_PiecesMauvaisesDefTech: " + bufferme.GetFloat ("Idec_PiecesMauvaisesDefTech_value") + "\r\n");
			txtIDEC.Append ("Idec_PiecesMauvaisesFuyard: " + bufferme.GetFloat ("Idec_PiecesMauvaisesFuyard_value") + "\r\n");
			txtIDEC.Append ("Idec_PiecesMauvaisesDefRaccord: " + bufferme.GetFloat ("Idec_PiecesMauvaisesDefRaccord_value") + "\r\n");


			txtCIRMECA.Append ("Debug CIRMECA" + "\r\n\r\n");
			txtCIRMECA.Append ("Cirmeca_TempsEnMarche: " + bufferme.GetFloat ("Cirmeca_TempsEnMarche_value") + "\r\n");
			txtCIRMECA.Append ("Cirmeca_TempsEnDefaut: " + bufferme.GetFloat ("Cirmeca_TempsEnDefaut_value") + "\r\n");
			txtCIRMECA.Append ("Cirmeca_PiecesBonnes: " + bufferme.GetFloat ("Cirmeca_PiecesBonnes_value") + "\r\n");
			txtCIRMECA.Append ("Cirmeca_PiecesMauvaises: " + bufferme.GetFloat ("Cirmeca_PiecesMauvaises_value") + "\r\n");

			txtSTAUBLI.Append ("Debug STAUBLI" + "\r\n\r\n");
			txtSTAUBLI.Append ("Robot_TempsEnMarche: " + bufferme.GetFloat ("Robot_TempsEnMarche_value") + "\r\n");
			txtSTAUBLI.Append ("Robot_TempsEnDefaut: " + bufferme.GetFloat ("Robot_TempsEnDefaut_value") + "\r\n");
			txtSTAUBLI.Append ("Robot_PiecesBonnes: " + bufferme.GetFloat ("Robot_PiecesBonnes_value") + "\r\n");
			txtSTAUBLI.Append ("Robot_PiecesMauvaises: " + bufferme.GetFloat ("Robot_PiecesMauvaises_value") + "\r\n");
		



			GUI.Box (new Rect (10, 10, 200, 150), txtIDEC.ToString ());

			GUI.Box (new Rect (210, 10, 200, 150), txtCIRMECA.ToString ());

			GUI.Box (new Rect (410, 10, 200, 150), txtSTAUBLI.ToString ());


			if (GUI.Button (new Rect (10, 10, 150, 100), "RAZ"))
				bRAZDebug = true;
				

		}

	}

	void Awake ()
	{
		if (Application.loadedLevelName == "generation_server")
		{
			StartCoroutine("PollHttpRequest");
		}
	}
	
	void Update ()
	{
		//Debug.Log("Idec_PiecesBonnes: " + bufferme.GetFloat("Idec_PiecesBonnes_value") );


		if (CleanAutomate1 || bRAZDebug)
		{
			CleanAutomate1 = false;
			bRAZDebug = false;
			CleanAutomateType = 0;
			StartCoroutine("RAZHttpRequest");
		}

		if (AutomateOn)
		{
			AutomateOn = false;
		}

		if (AutomateOff)
		{
			AutomateOff = false;
		}
            

        if( (SvcComPI_ErrorCnx+"\r\n"+SvcComPI_ErrorBadPoint) != 
            (SvcComPI_ErrorCnxSav+"\r\n"+SvcComPI_ErrorBadPointSav) )
        {
            ni_mail.MailMessage("ni_datarecovery", "Update", SvcComPI_ErrorCnx+"\r\n"+SvcComPI_ErrorBadPoint );
            SvcComPI_ErrorCnxSav = SvcComPI_ErrorCnx;
            SvcComPI_ErrorBadPointSav = SvcComPI_ErrorBadPoint;
        }
	}


	public static string savedata = "";


	IEnumerator RAZHttpRequest()
	{
		OnRequestFinishedAuthRAZSuccess();
		yield return null;
		/*
		HTTPRequest request = new HTTPRequest(new Uri(SERVICErootURL+"auth"),HTTPMethods.Post, OnRequestFinishedAuthRAZ);
		request.Proxy = null;
		request.AddField("password","r@p@rmony"); //WWW.EscapeURL("r@p@rmony"));
		request.Send();
		*/
	}








	IEnumerator PollHttpRequest()
	{
		while (true)
		{
			yield return new WaitForSeconds(SERVICErootLoopTime); // poll every 60 seconds
			
			if (false)
			{
				#if UNITY_ANDROID && (!UNITY_EDITOR)
				WWW newwww = new WWW(Application.streamingAssetsPath + "/Rapport.xml");
				while (!newwww.isDone)
				{
				}
				string testdata = newwww.text;
				#else
				string testdata = File.ReadAllText(Application.streamingAssetsPath + "/Rapport.xml");
				#endif
				TreatIncomingData(testdata);
			}
			else
			{
                OnRequestFinishedAuthSuccess();

                /*
				HTTPRequest request = new HTTPRequest(new Uri(SERVICErootURL+"auth"),HTTPMethods.Post, OnRequestFinishedAuth);
				request.Proxy = null;
				request.AddField("password","r@p@rmony"); //WWW.EscapeURL("r@p@rmony"));
				request.Send();
				*/
			}
		}
	}
	
	
	void TreatIncomingData_old(string data)
	{
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.LoadXml(data);
		
		int nrfields = 0;
		
		if (!Application.loadedLevelName.Contains("DEMO"))
		{
			if (bufferme.HasKey("DataFields"))
			{
				nrfields = bufferme.GetInt("DataFields");
				int n;
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_"+n);

					//Debug.Log ("DataField_"+n+":" + datanameid);
					
//					if (datanameid.Contains("TempsEnMarche")) // Cedric ??? pourquoi ne pas traiter ce cas specialement plus qu'un autre ?
//					{
//					}
//					else
					{
						// These values MUST be set by the dedicated
						if (pi_button_forcemachineonoff.machineon)
						{
							int nconv = 0;
							string test1;
							string test2;
							float getval = 0.0f;
							foreach (var key in ni_preparedatafields.ConversionValues.Keys)
							{
								if (key.ToString() == datanameid)
								{
									test2 = ni_preparedatafields.ConversionValues[key].ToString();
																		Debug.Log ("Looking for : ["+ ni_preparedatafields.ConversionValues[key]+"]");
									foreach(XmlNode node in xmlDoc)
									{
										test1 = node.Name;
										if (test1 == test2)
										{
											//											Debug.Log ("FOUND:[" + test1 + "]:[" + test2 + "]" + node.InnerText);
											float.TryParse(node.InnerText,out getval);
											if (bufferme.GetFloat(datanameid+"_value") != getval)
												ni_preparedatafields.InputValueChanged();
											bufferme.SetFloat(datanameid+"_value",getval);
                                            bufferme.SetBool(datanameid + "_quality", true );
										}
										foreach(XmlNode n2 in node)
										{
											test1 = node.Name + n2.Name;
											if (test1 == test2)
											{
//																								Debug.Log ("FOUND:[" + test1 + "]:[" + test2 + "]" + n2.InnerText);
												float.TryParse(n2.InnerText,out getval);
												if (bufferme.GetFloat(datanameid+"_value") != getval)
													ni_preparedatafields.InputValueChanged();
												bufferme.SetFloat(datanameid+"_value",getval);
                                                bufferme.SetBool(datanameid + "_quality", true );
											}
											foreach(XmlNode n3 in n2)
											{
												test1 = node.Name + n2.Name + n3.Name;
												if (test1 == test2)
												{													
//																										Debug.Log ("FOUND:[" + test1 + "]:[" + test2 + "]" + n3.InnerText);
													float.TryParse(n3.InnerText,out getval);
													if (bufferme.GetFloat(datanameid+"_value") != getval)
														ni_preparedatafields.InputValueChanged();
													
													//													savedata += "--Using--" + datanameid + "_value : " + getval + "\r\n";
													//													System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
													
													bufferme.SetFloat(datanameid+"_value",getval);
                                                    bufferme.SetBool(datanameid + "_quality", true );
												}
												foreach(XmlNode n4 in n3)
												{
													test1 = node.Name + n2.Name + n3.Name + n4.Name;
													if (test1 == test2)
													{
//														Debug.Log ("FOUND:[" + test1 + "]:[" + test2 + "]" + n4.InnerText);

														float.TryParse(n4.InnerText,out getval);
														if (bufferme.GetFloat(datanameid+"_value") != getval)
															ni_preparedatafields.InputValueChanged();
														bufferme.SetFloat(datanameid+"_value",getval);
                                                        bufferme.SetBool(datanameid + "_quality", true );
													}
												}
											}
										}
									}
								}
								nconv++;
							}
						}
					}
				}
			}
		}
	}


	void TreatIncomingData(string data)
	{
        ni_logger.LogDebug("ni_datarecovery", "TreatIncomingData", "DEBUT");

		XmlDocument xmlDoc = new XmlDocument();

		xmlDoc.LoadXml(data);

		XPathNavigator nav = xmlDoc.CreateNavigator();

		if (!Application.loadedLevelName.Contains("DEMO"))
		{
			// These values MUST be set by the dedicated
			if (pi_button_forcemachineonoff.machineon)
			{				
    			if (bufferme.HasKey("DataFields"))
				{
					int nrfields = bufferme.GetInt("DataFields");
					int n;
                    StringBuilder badLstPoints = new StringBuilder();
					for (n=0;n<nrfields;n++)
					{
						string datanameid = bufferme.GetString("DataField_"+n);

						//Debug.Log ("DataField_"+n+":" + datanameid);
						if( ni_preparedatafields.ConversionValues.Contains(datanameid) )
						{
							float getval = 0.0f;
							string xpath = ni_preparedatafields.ConversionValues[datanameid].ToString();

							//Debug.Log ("Looking for : ["+ ni_preparedatafields.ConversionValues[datanameid]+"]");

							try
							{
								if( false )
								{
									XmlNode node = xmlDoc.SelectSingleNode(xpath);

									if (node != null)
									{
										getval = 0;

										if (float.TryParse (node.InnerText, out getval)) 
										{
											if (bufferme.GetFloat(datanameid+"_value") != getval)
												ni_preparedatafields.InputValueChanged();
											
											bufferme.SetFloat (datanameid + "_value", getval);
                                            bufferme.SetBool(datanameid + "_quality", true );
										} 
                                        else
										{
											Debug.LogError( "/!\\ BAD VALUE : " + node.InnerText + "(" + xpath + ")" );

                                            badLstPoints.Append(" - " + datanameid + " (BAD VALUE " + node.InnerText + " [" + xpath + "])\r\n");

                                            // A voir plus tard si besoin de Flag QUALITY:BAD (pour l'instant on conserve l'ancienne valeur)
                                            //bufferme.SetBool(datanameid + "_quality", false );
										}
									} 
                                    else if( (xpath.IndexOf( "SQL:", StringComparison.InvariantCultureIgnoreCase) != 0) && 
                                             (xpath.IndexOf( "@qualite='NEVER'", StringComparison.InvariantCultureIgnoreCase) < 0) )
									{
										Debug.Log("/!\\ NOT FOUND: " + xpath );

                                        badLstPoints.Append(" - " + datanameid + " (NOT FOUND [" + xpath + "])\r\n");

                                        // A voir plus tard si besoin de Flag QUALITY:BAD (pour l'instant on conserve l'ancienne valeur)
                                        //bufferme.SetBool(datanameid + "_quality", false );
									}

								}
								else
								{
									XPathExpression expr = nav.Compile(xpath);
									object obj = nav.Evaluate(expr);
									XPathNodeIterator nodes = obj as XPathNodeIterator;

									if( nodes != null )
									{
										if( nodes.MoveNext() )
										{
											if (float.TryParse (nodes.Current.Value, out getval)) 
											{
												if (bufferme.GetFloat(datanameid+"_value") != getval)
													ni_preparedatafields.InputValueChanged();
												bufferme.SetFloat (datanameid + "_value", getval);
                                                bufferme.SetBool(datanameid + "_quality", true );
											} 
                                            else
											{
												Debug.LogError( "/!\\ BAD VALUE : " + nodes.Current.Value + "(" + xpath + ")" );

                                                badLstPoints.Append(" - " + datanameid + " (BAD VALUE " + nodes.Current.Value + " [" + xpath + "])\r\n");

                                                // A voir plus tard si besoin de Flag QUALITY:BAD (pour l'instant on conserve l'ancienne valeur)
                                                //bufferme.SetBool(datanameid + "_quality", false );
											}
										}
                                        else if( (xpath.IndexOf( "SQL:", StringComparison.InvariantCultureIgnoreCase) != 0) && 
                                                 (xpath.IndexOf( "@qualite='NEVER'", StringComparison.InvariantCultureIgnoreCase) < 0) )
										{
											Debug.Log("/!\\ NOT FOUND(1): " + xpath );

                                            badLstPoints.Append(" - " + datanameid + " (NOT FOUND [" + xpath + "])\r\n");

                                            // A voir plus tard si besoin de Flag QUALITY:BAD (pour l'instant on conserve l'ancienne valeur)
                                            //bufferme.SetBool(datanameid + "_quality", false );
										}
									}
									else if( obj != null )
									{
										getval =  Convert.ToSingle(obj);

										if( !float.IsNaN(getval) )
                                        {
											if (bufferme.GetFloat(datanameid+"_value") != getval)
												ni_preparedatafields.InputValueChanged();
											bufferme.SetFloat (datanameid + "_value", getval);
                                            bufferme.SetBool(datanameid + "_quality", true );
                                        }
                                        else if( (xpath.IndexOf( "SQL:", StringComparison.InvariantCultureIgnoreCase) != 0) && 
                                                 (xpath.IndexOf( "@qualite='NEVER'", StringComparison.InvariantCultureIgnoreCase) < 0) )
                                        {
											Debug.Log("/!\\ NOT FOUND(2): " + xpath );

                                            badLstPoints.Append(" - " + datanameid + " (NOT FOUND [" + xpath + "])\r\n");

                                            // A voir plus tard si besoin de Flag QUALITY:BAD (pour l'instant on conserve l'ancienne valeur)
                                            //bufferme.SetBool(datanameid + "_quality", false );
                                        }
									}
                                    else if( (xpath.IndexOf( "SQL:", StringComparison.InvariantCultureIgnoreCase) != 0) && 
                                             (xpath.IndexOf( "@qualite='NEVER'", StringComparison.InvariantCultureIgnoreCase) < 0) )
									{
										Debug.Log("/!\\ NOT FOUND(3): " + xpath );

                                        badLstPoints.Append(" - " + datanameid + " (NOT FOUND [" + xpath + "])\r\n");

                                        // A voir plus tard si besoin de Flag QUALITY:BAD (pour l'instant on conserve l'ancienne valeur)
                                        //bufferme.SetBool(datanameid + "_quality", false );
									}
								}
									
							}
							catch( Exception ex ) 
							{
                                ni_logger.LogException("ni_datarecovery", "TreatIncomingData", "/!\\ EXCEPTION : " + ex.Message + " (" + xpath + ")");

                                badLstPoints.Append(" - " + datanameid + " (EXCEPTION: " + ex.Message + " [" + xpath + "])\r\n");
							}
						}
					}


                    if (badLstPoints.Length > 0)
                        SvcComPI_ErrorBadPoint = "* Point(s) non accessible(s) (AUTOMATES HS):" + "\r\n" + badLstPoints.ToString();
                    else if( SvcComPI_ErrorBadPoint.Length > 0 )
                    {
                        SvcComPI_ErrorBadPoint = "* Tous les points sont à présent accessible (AUTOMATES OK)";
                    }
				}
			}
		}

        ni_logger.LogDebug("ni_datarecovery", "TreatIncomingData", "FIN");
	}
		

	void OnRequestFinishedGetData_old(HTTPRequest request, HTTPResponse response)
	{
        ni_logger.LogDebug("ni_datarecovery", "OnRequestFinishedGetData", "DEBUT "+request.State.ToString());

		switch (request.State)
		{
		case HTTPRequestStates.Finished:
			if (response.IsSuccess)
			{
//				savedata += "--------OnRequestFinishedGetData GOOD-------" + response.DataAsText;
//				System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
                    if( SvcComPI_ErrorCnx.Length > 0 )
                        SvcComPI_ErrorCnx = "* Le serveur de communication est de nouveau opérationnel (SvcComPi OK)";
				TreatIncomingData(response.DataAsText);
			}
			else
			{
                SvcComPI_ErrorCnx = "* Le serveur de communication à refusé la connection (SvcComPi BAD SATUS "+response.StatusCode.ToString()+")";
			}
			break;
		case HTTPRequestStates.Error:
//			savedata += "---------OnRequestFinishedGetData-Error-------";
//			System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
                SvcComPI_ErrorCnx = "* Le serveur de communication à refusé la connection (SvcComPi ERROR)";
			break;
        case HTTPRequestStates.Aborted:
                SvcComPI_ErrorCnx = "* Le serveur de communication à refusé la connection (SvcComPi ABORT)";
			break;
        case HTTPRequestStates.ConnectionTimedOut:
//			savedata += "---------OnRequestFinishedGetData-ConnectionTimedOut-------";
//			System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
                SvcComPI_ErrorCnx = "* Le serveur de communication ne réponds plus (SvcComPi CONNECTION TIMEOUT)";
			break;
		case HTTPRequestStates.TimedOut:
//			savedata += "---------OnRequestFinishedGetData-TimedOut-------";
//			System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
                SvcComPI_ErrorCnx = "* Le serveur de communication ne réponds plus (SvcComPi REQUETE TIMEOUT)";
			break;

         default:
                SvcComPI_ErrorCnx = "* Le serveur de communication ne réponds plus (SvcComPi HS STATE "+request.State.ToString()+")";
            break;
		} 

        ni_logger.LogDebug("ni_datarecovery", "OnRequestFinishedGetData", "FIN "+SvcComPI_ErrorCnx);
	}


    void OnRequestFinishedGetData(HTTPRequest request, HTTPResponse response)
    {
        ni_logger.LogDebug("ni_datarecovery", "OnRequestFinishedGetData", (request.launchcounter > 1 ? "RETRY " : "DEBUT ")+request.State.ToString());

        switch (network.RetryHttp(response,request,"OnRequestFinishedGetData"))
        {
            case 0 :        // OK
                if( SvcComPI_ErrorCnx.Length > 0 )
                    SvcComPI_ErrorCnx = "* Le serveur de communication est de nouveau opérationnel (SvcComPi OK)";
                TreatIncomingData(response.DataAsText);
                break;

            case 2 :        // retry
                return;

            default :
                switch (request.State)
                {
                    case HTTPRequestStates.Finished:
                        if ((response != null) && !response.IsSuccess)
                        {
                            SvcComPI_ErrorCnx = "* Le serveur de communication à refusé la connection (SvcComPi BAD SATUS " + response.StatusCode.ToString() + ")";
                        }
                        else
                        {
                            SvcComPI_ErrorCnx = "* Le serveur de communication à refusé la connection (SvcComPi BAD RESPONSE NULL ?)";
                        }
                        break;
                    case HTTPRequestStates.Error:
                        //          savedata += "---------OnRequestFinishedGetData-Error-------";
                        //          System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
                        SvcComPI_ErrorCnx = "* Le serveur de communication à refusé la connection (SvcComPi ERROR)";
                        break;
                    case HTTPRequestStates.Aborted:
                        SvcComPI_ErrorCnx = "* Le serveur de communication à refusé la connection (SvcComPi ABORT)";
                        break;
                    case HTTPRequestStates.ConnectionTimedOut:
                        //          savedata += "---------OnRequestFinishedGetData-ConnectionTimedOut-------";
                        //          System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
                        SvcComPI_ErrorCnx = "* Le serveur de communication ne réponds plus (SvcComPi CONNECTION TIMEOUT)";
                        break;
                    case HTTPRequestStates.TimedOut:
                        //          savedata += "---------OnRequestFinishedGetData-TimedOut-------";
                        //          System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
                        SvcComPI_ErrorCnx = "* Le serveur de communication ne réponds plus (SvcComPi REQUETE TIMEOUT)";
                        break;

                    default:
                        SvcComPI_ErrorCnx = "* Le serveur de communication ne réponds plus (SvcComPi HS STATE " + request.State.ToString() + ")";
                        break;
                }
                break;
            } 

        ni_logger.LogDebug("ni_datarecovery", "OnRequestFinishedGetData", "FIN "+SvcComPI_ErrorCnx);
        bRequestGetXMLFinished = true;
    }
	
	void OnRequestFinishedAuthSuccess()
	{
        if (!bRequestGetXMLFinished ) // Garde Fou en cas de plantage de Script pour eviter le cas ou bRequestFinish ne serait jamais a true
        {
            iRequestGetXMLCount++;

            if( (iRequestGetXMLCount*SERVICErootLoopTime) > (60*1+8) ) // Si toujours pas finit au bout de 1 minutes et 8 secondes c'est que ca du planté
            {
                ni_logger.LogWarn("ni_datarecovery", "OnRequestFinishedAuthSuccess", "WAIT At"+(iRequestGetXMLCount*SERVICErootLoopTime).ToString() );

                if (reqGetXML != null)
                {
                    if ((reqGetXML.State == HTTPRequestStates.Processing) || // Si on est dans un cas de ConnectionTimout cela peux prendre plus de 1 minutes et 8 secondes donc attend toujours
                        (reqGetXML.State == HTTPRequestStates.Queued))
                    {
                        ni_logger.LogError("ni_datarecovery", "OnRequestFinishedAuthSuccess", "ABORT At"+(iRequestGetXMLCount*SERVICErootLoopTime).ToString() );

                        reqGetXML.Abort();

                        // On ajoute qd meme une condition si on est dans l'extreme plantage genre que reqGetXML.State est sur processing et qu'il ne changera jamais
                        if( (iRequestGetXMLCount*SERVICErootLoopTime) > (60*5+8) ) // Si toujours pas finit au bout de 5 minutes et 8 secondes c'est que ca du planté grave
                            bRequestGetXMLFinished = true;    
                    }
                    else
                        bRequestGetXMLFinished = true;
                }
                else
                    bRequestGetXMLFinished = true;
            }
        }

        // On evite d'empiler des requetes tant que la precente n'est pas terminer (cas par exmple ou il y aurait des retry)
        if (bRequestGetXMLFinished)
        {
            bRequestGetXMLFinished = false;
            iRequestGetXMLCount = 0;
          
            ni_logger.LogDebug("ni_datarecovery", "OnRequestFinishedAuthSuccess", "DEBUT" );

            reqGetXML = new HTTPRequest(new Uri(SERVICErootURL + getxmlcommand),HTTPMethods.Get, OnRequestFinishedGetData);
            reqGetXML.Proxy = null;
            reqGetXML.DisableCache = true;
            reqGetXML.Send();

            ni_logger.LogDebug("ni_datarecovery", "OnRequestFinishedAuthSuccess", "FIN" );
        }
        else
            ni_logger.LogDebug("ni_datarecovery", "OnRequestFinishedAuthSuccess", "WAIT No"+iRequestGetXMLCount.ToString() );
	}

	void OnRequestFinishedAuth(HTTPRequest request, HTTPResponse response)
	{
		switch (request.State)
		{
		case HTTPRequestStates.Finished:
//			savedata += "--------OnRequestFinishedAuth GOOD-------" + response.DataAsText;
//			System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
			
			if (response.IsSuccess)
			{
				OnRequestFinishedAuthSuccess();
			}
			else
			{
			}
			break;
		case HTTPRequestStates.Error:
//			savedata += "---------OnRequestFinishedAuth-Error-------";
//			System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
			break;
		case HTTPRequestStates.Aborted:
			break;
		case HTTPRequestStates.ConnectionTimedOut:
//			savedata += "---------OnRequestFinishedAuth-ConnectionTimedOut-------";
//			System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
			break;
		case HTTPRequestStates.TimedOut:
//			savedata += "---------OnRequestFinishedAuth-TimedOut-------";
//			System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",savedata);
			break;
		}
	}




	void OnRequestFinishedAuthRAZSuccess_Old()
	{
		HTTPRequest req = new HTTPRequest(new Uri(SERVICErootURL + getcommandcommand),HTTPMethods.Post, OnRequestFinishedGetData);
		req.Proxy = null;
		req.AddField("enctype","x-www-form-urlencoded"); //WWW.EscapeURL("r@p@rmony"));
		req.AddField("btn_action","raz"); //WWW.EscapeURL("r@p@rmony"));
		//					req.SetHeader("AuthorizationToken", bufferme.GetString("login_token"));
		//					req.SetHeader("Content-Type", "application/json; charset=UTF-8");
		req.Send();
	}

	void OnRequestFinishedAuthRAZSuccess()
	{
		HTTPRequest req = new HTTPRequest(new Uri(SERVICErootURL + getrazcommand),HTTPMethods.Post, OnRequestFinishedRAZ);
		req.Proxy = null;
		req.MaxRedirects = 0; // Pas de redirection sur code 302
        req.DisableCache = true;
	
		req.SetHeader ("Content-Type", "application/x-www-form-urlencoded"); // superflux (a titre informatif) car par defaut est deja dans ce type

		if (ni_preparedatafields.ConversionValues.Contains ("RAZCMD")) 
		{
			IDictionary dicRAZ = (IDictionary)ni_preparedatafields.ConversionValues ["RAZCMD"];

			foreach (var okey in dicRAZ.Keys) 
			{ 
				string key = okey.ToString();
				string val = dicRAZ [key].ToString();

				//Debug.Log (key + "=" + val);

				req.AddField (key.Replace( "/", "_"), val);
			}
		}

		try
		{
			req.Send();
		}
		catch( Exception ex ) 
		{
			Debug.LogError( "/!\\ EXCEPTION : " + ex.Message + "(RAZ Compteurs)" ); 
		}
	}


	void OnRequestFinishedAuthRAZ(HTTPRequest request, HTTPResponse response)
	{
		switch (request.State)
		{
		case HTTPRequestStates.Finished:
			if (response.IsSuccess)
			{
				OnRequestFinishedAuthRAZSuccess();
			}
			else
			{
			}
			break;
		case HTTPRequestStates.Error:
			break;
		case HTTPRequestStates.Aborted:
			break;
		case HTTPRequestStates.ConnectionTimedOut:
			break;
		case HTTPRequestStates.TimedOut:
			break;
		}
	}

	void OnRequestFinishedRAZ(HTTPRequest request, HTTPResponse response)
	{
		switch (request.State)
		{
		case HTTPRequestStates.Finished:
			if (response.IsSuccess || (response.StatusCode == 302))
			{
				//Debug.Log ("OK State:"+ response.StatusCode.ToString());						
			}
			else
			{
				//Debug.Log ("KO State:"+ response.StatusCode.ToString());
			}
			break;
		case HTTPRequestStates.Error:
			break;
		case HTTPRequestStates.Aborted:
			break;
		case HTTPRequestStates.ConnectionTimedOut:
			break;
		case HTTPRequestStates.TimedOut:
			break;
		}
	}



}
