﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class ni_getsetdatetimefromdropdowns : MonoBehaviour
{
	DateTime mydate;
	public bool checkdatepicker = false;
	tk2dTextMesh datetimetext = null;

	void Awake ()
	{
		datetimetext = (tk2dTextMesh)gameObject.FindInChildren("datetimetext").GetComponent<tk2dTextMesh>();
		if (datetimetext == null)
			datetimetext = (tk2dTextMesh)gameObject.FindInChildren("text").GetComponent<tk2dTextMesh>();
	}

	void Update()
	{
		if (checkdatepicker && (SNAILRUSHDATEPICKER.DatePicker == false))
		{
			checkdatepicker = false;
// get datetime here
			if (SNAILRUSHDATEPICKER.selectedrealday != 0)
			{
				if (gameObject.name.Contains("start"))
				{
					mydate = new DateTime (SNAILRUSHDATEPICKER.currentyear, SNAILRUSHDATEPICKER.selectedmonth, SNAILRUSHDATEPICKER.selectedrealday, 0, 0, 0, 0); //.ToLocalTime ();
//					mydate = mydate.AddHours(-1);
				}
				else
				{
					mydate = new DateTime (SNAILRUSHDATEPICKER.currentyear, SNAILRUSHDATEPICKER.selectedmonth, SNAILRUSHDATEPICKER.selectedrealday, 23, 0, 0, 0); //.ToLocalTime ();
					mydate = mydate.AddHours(1);
				}
				DisplayNewDate();
			}
		}
	}

	void DisplayNewDate()
	{
		if (datetimetext != null)
			settext.ForceText(datetimetext,mydate.ToString("dd-MM-yyyy"));
	}

	public void SetDateTime(DateTime dt)
	{
		mydate = dt;
		DisplayNewDate();
	}

	public DateTime GetDateTime()
	{
		return (mydate);
	}
}
