﻿using UnityEngine;
using System.Collections;

public class ni_control_curvedisplay : MonoBehaviour
{
	float nexttime = 0.0f;
	int whoami;
	float	periodadd = 0.0f;
	public float	[] myvalues = new float[5];

	void Awake ()
	{
		string strwho = gameObject.name.Replace("clone_sample_h","");
		int.TryParse(strwho,out whoami);
		periodadd = whoami * 6;
		periodadd = 0.2f + periodadd / 1000.0f;
		nexttime = Time.time + periodadd;
	}
	
	void Update ()
	{
		if (Time.time > nexttime)
		{
			nexttime = Time.time + periodadd;
			for (int i=1;i<5;i++)
			{
				GameObject obj = gameObject.FindInChildren("curveline"+i);
				if (obj != null)
				{
					if (bufferme.HasKey("button_toggle_n"+i))
						obj.SetActive(false);
					else
						obj.SetActive(true);
				}
				obj = gameObject.FindInChildren("curvetext"+i);
				if (obj != null)
				{
					if (myvalues[i] != -10000)
					{
						if (bufferme.HasKey("button_toggle_n"+i))
							obj.SetActive(false);
						else
							obj.SetActive(true);
					}
					else
						obj.SetActive(false);
				}
			}
		}
	}
}
