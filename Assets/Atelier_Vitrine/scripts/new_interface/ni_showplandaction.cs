﻿using UnityEngine;
using System.Collections;

public class ni_showplandaction : MonoBehaviour
{
	public static bool wasIpressed = false;
	Vector3 startpos;
	public Camera 		m_ViewCamera;

	void Awake ()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
	}
	
	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			wasIpressed = true;
			startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
		}
		if (wasIpressed)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			startpos = vec;
			vec = gameObject.transform.position;
			vec.y += diffy;
			gameObject.transform.position = vec;
		}
		
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}
	}
}
