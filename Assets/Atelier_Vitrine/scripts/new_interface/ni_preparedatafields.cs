using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;
using System.Globalization;
using BestHTTP;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Security;
using System.Threading;

public class ni_preparedatafields : MonoBehaviour
{
	public static bool ONLINESTORAGE = true;
	public static float nonexistingfloat = -10000.0f;

	TextMesh tmDateTime;
	public static TimeSpan _elapsedtime;
	public static bool firstrun = true;

	public static DateTime _starteddatetime;
	static IDictionary FakeValues;
	public static float datacollectorperiod = 5*60.0f;		// 5min

	public static string [] cvsitemnames = null;
	public static float[,] cvsitems = null;

	public static int maxnrpoi = 10;
	public static GameObject [] pointofinterest = new GameObject[maxnrpoi];
	public static string [] poinofinteresttitle = new string[maxnrpoi];
	public static string [] poinofinterestscript = new string[maxnrpoi];
	public static int [] poinofinteresttype = new int[maxnrpoi];
	public static int [] poinofinterestnumber = new int[maxnrpoi];

	static float OverAllAdding = 0.0f;
	static float [] LastStatValue;
	static tk2dTextMesh generate_infodata;
	static tk2dTextMesh generate_timer;

	static float [,] averagebuffer = null;
	static int naveragebuffer = 0;
	static float [] averagebuffer_lasthour = null;
	float verificationtime;

	bool firstsave = false;

	public static long SAMPLETIME = 5*60;

	static bool   lastsavedjson_force = false;
	static string lastsavedjsontextvalue = "";
	static DateTime real_startedtimevalue;

	public static IDictionary	ConversionValues = null;

	static float SetupDemoLoop		= 10.0f;

	public static string guioutput = "";
	string guisave = "";
	string lastpressed = "";


    enum SchedulePeriodType
    {
        InPeriod = 0,
        BeforePeriod = 1,
        AfterPeriod = 2
    }

    private bool bMailStartOK = false;
    private bool bMailEndOK = false;
    private float tempoMail = 1f; // temporise le script a 1S




	static string [] Monthnames = new string[]
	{
		"Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"
	};


    void MailProductionStartEnd()
    {
        if (tempoMail > 0)
        {
            tempoMail -= Time.deltaTime;
            return;
        }
        tempoMail = 1f; // temporise le script a 1S

        if (Application.loadedLevelName == "generation_server")
        {
            SchedulePeriodType inside = IsCurrentTimeISideSchedule();
           
            switch (inside)
            {
                case SchedulePeriodType.BeforePeriod: // Before Period
                    bMailStartOK = false;
                    bMailEndOK = false;
                    break;

                case SchedulePeriodType.InPeriod: // In Period
                    if (!bMailStartOK)
                    {
                        StringBuilder valLstPoints = new StringBuilder();
                        bool bAuMoinsUneOK = false;

                        if (bufferme.HasKey("DataFields"))
                        {
                            int nrfields = bufferme.GetInt("DataFields");
                            int n;

                            for (n = 0; n < nrfields; n++)
                            {
                                string datanameid = bufferme.GetString("DataField_" + n);

                                float fVal = bufferme.GetFloat (datanameid + "_value" );
                                bool  bVal = bufferme.GetBool(datanameid + "_quality" );

                                if( ni_preparedatafields.ConversionValues.Contains(datanameid) ) // Variable en Communisation
                                {
                                    string xpath = ni_preparedatafields.ConversionValues[datanameid].ToString();

                                    if( (xpath.IndexOf( "SQL:", StringComparison.InvariantCultureIgnoreCase) != 0) && 
                                        (xpath.IndexOf( "@qualite='NEVER'", StringComparison.InvariantCultureIgnoreCase) < 0) )
                                    {
                                        if( (fVal > 0) && bVal )// Qui a une valeur et de qualité GOOD
                                            bAuMoinsUneOK = true;
                                    }
                                }

                                valLstPoints.Append( " - " + datanameid + ": " + fVal + " (" + (bVal? "GOOD" : "BAD") + ")\r\n" );
                            }
                        }

                        if (bAuMoinsUneOK)
                        {
                            ni_mail.MailMessage("ni_preparedatafields", "MailProductionStartEnd", "* Début de la période de production.\r\nValeurs:\r\n" + valLstPoints.ToString());
                            bMailStartOK = true;
                        }
                    }
                    bMailEndOK = false;
                    break;

                case SchedulePeriodType.AfterPeriod: // After Period
                    bMailStartOK = false;
                    if (!bMailEndOK)
                    {
                        StringBuilder valLstPoints = new StringBuilder();

                        if (bufferme.HasKey("DataFields"))
                        {
                            int nrfields = bufferme.GetInt("DataFields");
                            int n;

                            for (n = 0; n < nrfields; n++)
                            {
                                string datanameid = bufferme.GetString("DataField_" + n);

                                float fVal = bufferme.GetFloat (datanameid + "_value" );
                                bool  bVal = bufferme.GetBool(datanameid + "_quality" );
                               
                                valLstPoints.Append( " - " + datanameid + ": " + fVal + " (" + (bVal? "GOOD" : "BAD") + ")\r\n" );
                            }
                        }


                        ni_mail.MailMessage("ni_preparedatafields", "MailProductionStartEnd", "* Fin de la période de production.\r\nValeurs:\r\n" + valLstPoints.ToString()); 
                        bMailEndOK = true;
                    }
                    break;

                default:
                    break;
            }

            //Debug.Log("MailProductionStartEnd inside:" + inside);

        }
    }



	void SaveNow(HTTPRequest request)
	{
		/*
		guisave += "---<" + request.Uri.ToString() + ">---\n";
		guisave += "---[" + lastpressed + "]---\n";
		System.IO.File.WriteAllText(Application.streamingAssetsPath + "/report.txt",guisave);
		*/
	}

	void OnRequestConnect(HTTPRequest request, HTTPResponse response)
	{
		guioutput += "[request]";
		switch (request.State)
		{
		case HTTPRequestStates.Finished:
			guioutput += "Request Finished Successfully! " + response.DataAsText + "\n";
			guisave += "Request Finished Successfully! " + response.DataAsText + "\n";
			SaveNow(request);
			break;
		case HTTPRequestStates.Error:
			guioutput += "Request Finished with Error! " +
				(request.Exception != null ?
				 (request.Exception.Message + "\n" + request.Exception.StackTrace) :
			 "No Exception");
			guisave += "Request Finished with Error! " +
				(request.Exception != null ?
				 (request.Exception.Message + "\n" + request.Exception.StackTrace) :
				 "No Exception") + "\n";
			SaveNow(request);
			break;
		case HTTPRequestStates.Aborted:
			guioutput += "Request Aborted!";
			guisave += "Request Aborted!" + "\n";
			SaveNow(request);
			break;
			// Ceonnecting to the server timed out.
		case HTTPRequestStates.ConnectionTimedOut:
			guioutput += "Connection Timed Out!";
			guisave += "Connection Timed Out!" + "\n";
			SaveNow(request);
			break;
			// The request didn't finished in the given time.
		case HTTPRequestStates.TimedOut:
			guioutput += "Processing the request Timed Out!";
			guisave += "Processing the request Timed Out!" + "\n";
			SaveNow(request);
			break;
		}
	}

	void OnGUI()
	{
		if (Application.loadedLevelName != "generation_server")		return;
		/*
		GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = GUI.skin.textField.fontSize = 20;
		if (GUI.Button(new Rect(Screen.width-150,70,150,120),"Reset Auto3"))
		{
			ni_datarecovery.CleanAutomate1 = true;
//			PlayerPrefs.DeleteAll ();
//			PlayerPrefs.Save ();
		}
*/
		/*
		if (GUI.Button(new Rect(Screen.width-200,0,50,70),"C"))
		{
			guioutput = "";
		}
		if (GUI.Button(new Rect(Screen.width-350,0,150,70),"Connect Innerprox"))
		{
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
		*/

		/*
		if (GUI.Button(new Rect(Screen.width - 150,70,150,70),"true true true"))
		{
			lastpressed = "true true true";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,true,true,true);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
		if (GUI.Button(new Rect(Screen.width - 300,70,150,70),"false false false"))
		{
			lastpressed = "false false false";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,false,false,false);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
		if (GUI.Button(new Rect(Screen.width - 450,70,150,70),"true false false"))
		{
			lastpressed = "true false false";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,true,false,false);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
		if (GUI.Button(new Rect(Screen.width - 600,70,150,70),"false true false"))
		{
			lastpressed = "false true false";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,false,true,false);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}

		if (GUI.Button(new Rect(Screen.width - 150,140,150,70),"false false true"))
		{
			lastpressed = "false false true";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,false,false,true);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
		if (GUI.Button(new Rect(Screen.width - 300,140,150,70),"true true false"))
		{
			lastpressed = "true true false";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,true,true,false);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
		if (GUI.Button(new Rect(Screen.width - 450,140,150,70),"true false true"))
		{
			lastpressed = "true false true";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,true,false,true);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
		if (GUI.Button(new Rect(Screen.width - 600,140,150,70),"false true true"))
		{
			lastpressed = "false true true";
			HTTPManager.Proxy = new HTTPProxy(new Uri("http://SERV-52:8080/array.dll?Get.Routing.Script"),null,false,true,true);
			HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestConnect);
			request.Send();
		}
*/
		GUI.skin.label.fontSize = GUI.skin.box.fontSize = GUI.skin.button.fontSize = GUI.skin.textField.fontSize = 13;
		GUI.Label(new Rect(0,100,Screen.width,500),guioutput);

	}
	
	void Awake ()
	{
//		PlayerPrefs.DeleteAll ();
//		PlayerPrefs.Save ();

		guisave = "";
		GameObject testobj = null;

		if (Application.loadedLevelName.Contains("ScenePC_Workshop_VR"))
		{
			PlayerPrefs.DeleteAll ();
			PlayerPrefs.Save ();
		}

		network.ReadSettings();


		if (Application.loadedLevelName != "generation_server")
		{
			datacollectorperiod = 30.0f;
			if (!ONLINESTORAGE)
				datacollectorperiod = 60.0f;
		}
		else
		{
			Application.runInBackground = true;
			ni_admin_edit_schedule.CreateInitialSchedule();
		}

		bufferme.DeleteKey ("ResetAutomateIfAny");
		testobj = GameObject.Find ("generate_infodatamain");
		generate_infodata = null;
		if (testobj)
			generate_infodata = (tk2dTextMesh)testobj.GetComponent<tk2dTextMesh>();
		testobj = GameObject.Find ("generate_timermain");
		generate_timer = null;
		if (testobj)
			generate_timer = (tk2dTextMesh)testobj.GetComponent<tk2dTextMesh>();

		pointofinterest[1] = null;
		testobj = GameObject.Find ("pointofinterest1");
		if (testobj)
		{
			for (int i=0;i<maxnrpoi;i++)
			{
				pointofinterest[i] = GameObject.Find ("pointofinterest"+i);
				pointofinterest[i].SetActiveRecursively(false);
			}
		}

		testobj = gameObject.FindInChildren ("DateTime");
		if (testobj)
			tmDateTime = (TextMesh)gameObject.FindInChildren ("DateTime").GetComponent<TextMesh> ();
		else
			tmDateTime = null;

		#if UNITY_ANDROID && (!UNITY_EDITOR)
		WWW newwww = new WWW(Application.streamingAssetsPath + "/" + bufferme.GetClientId() + ".txt");
		while (!newwww.isDone)
		{
		}
		string text = newwww.text;
		#else
		string text = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/" + bufferme.GetClientId() + ".txt");
		#endif
		CreateDataFields (text);

		#if UNITY_ANDROID && (!UNITY_EDITOR)
		newwww = new WWW(Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "conversion.txt");
		while (!newwww.isDone)
		{
		}
		string textlink = newwww.text;
		#else
		string textlink = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "conversion.txt");
		#endif
		if (textlink != null)
			ConversionValues = (IDictionary)Json.Deserialize (textlink);

		#if UNITY_ANDROID && (!UNITY_EDITOR)
		newwww = new WWW(Application.streamingAssetsPath + "/" + bufferme.GetClientId()+"client_fake" + ".txt");
		while (!newwww.isDone)
		{
		}
		text = newwww.text;
		#else
		text = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/" + bufferme.GetClientId()+"client_fake" + ".txt");
		#endif

		FakeValues = (IDictionary)Json.Deserialize (text);

		if (bufferme.HasKey("DataFields"))
		{
			int nrfields = bufferme.GetInt("DataFields");
			averagebuffer = new float[512,nrfields];
			averagebuffer_lasthour = new float[nrfields];
			for (int n=0;n<nrfields;n++)
			{
				for (int nn=0;nn<512;nn++)
					averagebuffer[nn,n] = nonexistingfloat;
				averagebuffer_lasthour[n] = 0;
			}
			naveragebuffer = 0;
		}

//		ResetAllParameters();	// This should not be doe every boot up !!!
		verificationtime = Time.time + 60.0f * 3.0f+ datacollectorperiod;			// 3 minutes to check if this routine is alive
		StartCoroutine("LoadSavedClientData");

		if (!network.SYSTEMISON)
			real_startedtimevalue = DateTime.Now.AddSeconds(-60-datacollectorperiod*SetupDemoLoop);
		else
			real_startedtimevalue = DateTime.Now.AddSeconds(-60);

		if (!ONLINESTORAGE)
			ResetAllParameters ();

		ValidInTimeSchedule ();

		if (Application.loadedLevelName.ToLower().Contains("workshop"))
		{
			datacollectorperiod = 60;
		}
		if (Application.loadedLevelName != "getdatafromfiles")
			StartCoroutine("GenerateData");
	}

	IEnumerator LoadSavedClientData()
	{
		while (!network.GetJson("s"+bufferme.GetClientId(),"100","101",3))
		{
			yield return new WaitForSeconds (0.03f);
		}
		while (network.waitingnetwork)
		{
			yield return new WaitForSeconds (0.03f);
		}

		if ((network.retreivejson != "") && (network.retreivejson.Length > 10))
		{
			LoadClientData(network.retreivejson);
		}

	}

	void CreateDataFields(string jsontext)
	{
		int nrpoi = 0;
		int piodatanumber = 0;
        int nrfields = 0;
        int nrcurves = 0;


		IDictionary json = (IDictionary)Json.Deserialize (jsontext);
		foreach (var key in json.Keys)
		{
			if (key.ToString().Contains("PIO:"))
			{
				if (Application.loadedLevelName != "getdatafromfiles")
				{
					if (pointofinterest[1] != null)
					{
						string pioname = key.ToString().Replace("PIO:","");
						IDictionary piojson = (IDictionary)json[key.ToString()];
						// create PIO
						pointofinterest[nrpoi].SetActiveRecursively(true);
						poinofinteresttitle[nrpoi] = pioname;
						tk2dSprite sprite = (tk2dSprite)pointofinterest[nrpoi].GetComponent<tk2dSprite>();
						ni_pointofinterest_button piobutton = (ni_pointofinterest_button)pointofinterest[nrpoi].GetComponent<ni_pointofinterest_button>();
						
						GameObject piopanel = pointofinterest[nrpoi].FindInChildren("panel");
						piopanel.SetActive(false);
						foreach (var piokey in piojson.Keys)
						{
							if (piokey.ToString().Contains("PIOType:"))		// setup PIO type !!!
							{
								string piotype = piokey.ToString().Replace("PIOType:","");
								poinofinteresttype[nrpoi] = 0;
								switch (piotype)
								{
								case "data" :
									sprite.SetSprite(sprite.GetSpriteIdByName("picto_green"));
									piobutton.WhereToGoTo = "info_showdatafields";
									poinofinterestscript[nrpoi] = (string)piojson[piokey.ToString()];
									poinofinterestnumber[nrpoi] = piodatanumber;
									poinofinteresttype[nrpoi] = 1;
									piopanel.SetActive(true);
									tk2dTextMesh intm = (tk2dTextMesh)piopanel.FindInChildren("capturetext").GetComponent<tk2dTextMesh>();
									settext.ForceText(intm,pioname);
									piodatanumber++;
									break;
								}
							}
						}
						nrpoi++;
					}
				}
			}
			else
			{
				if (key.ToString().Contains("STAT"))
				{
					if (Application.loadedLevelName != "getdatafromfiles")
					{
						IDictionary statjson = (IDictionary)json[key.ToString()];
						
						//					string testjson = (string)Json.Serialize(statjson);
						//					Debug.Log ("TESTJSON:" + testjson);
						string gettext = (string)statjson["STATid"];
						int statid = 0;
						int.TryParse(gettext,out statid);
                        if (statid >= nrcurves)
						{
                            nrcurves = statid + 1;
						}

						bufferme.SetString("Curve_displays_name"+statid,(string)statjson["STATname"]);
						bufferme.SetString("Curve_displays_description"+statid,(string)statjson["STATdesc"]);
						bufferme.SetString("Curve_displays_variable"+statid,(string)statjson["STATvariable"]);
						bufferme.SetString("Curve_displays_secondvariable"+statid,(string)statjson["STATsecondvariable"]);
						bufferme.SetString("Curve_displays_form"+statid,(string)statjson["STATform"]);
						bufferme.SetString("Curve_displays_father"+statid,(string)statjson["father"]);
						bufferme.SetString("Curve_displays_fathervariable"+statid,(string)statjson["STATfathervariable"]);
						bufferme.SetString("Curve_displays_fathervariabledesc"+statid,(string)statjson["STATfathervariabledesc"]);


						string getvariable = (string)statjson["STATvariable"];
						if ((getvariable != null) && (getvariable != ""))
						{
							if (!bufferme.HasKey(getvariable+"_message_good"))
							{
								try
								{
									gettext = (string)statjson["message_good"];
								}
								catch
								{
									gettext = "";
								}
								if ((gettext != null) && (gettext != ""))
								{
									bufferme.SetString(getvariable+"_message_good",gettext);
									//										Debug.Log ("WARNING FOUND : "+getvariable+"_message_good  "+gettext);
								}
							}
							
							if (!bufferme.HasKey(getvariable+"_message_bad"))
							{
								
								try
								{
									gettext = (string)statjson["message_bad"];
								}
								catch
								{
									gettext = "";
								}
								if ((gettext != null) && (gettext != ""))
								{
									bufferme.SetString(getvariable+"_message_bad",gettext);
									//										Debug.Log ("WARNING FOUND : "+getvariable+"_message_bad  "+gettext);
								}
							}
							
							if (!bufferme.HasKey(getvariable+"_message_toogood"))
							{
								
								try
								{
									gettext = (string)statjson["message_toogood"];
								}
								catch
								{
									gettext = "";
								}
								if ((gettext != null) && (gettext != ""))
								{
									bufferme.SetString(getvariable+"_message_toogood",gettext);
									//										Debug.Log ("WARNING FOUND : "+getvariable+"_message_toogood  "+gettext);
								}
							}
						}
					}
				}
				else
				{
					if (key.ToString().Contains("CLIENTMAINLOOP"))
					{
						if (Application.loadedLevelName != "getdatafromfiles")
						{
							IDictionary statjson = (IDictionary)json[key.ToString()];
							string gettext;
							try
							{
								gettext = (string)statjson["call"];
							}
							catch
							{
								gettext = "";
							}
							if ((gettext != null) && (gettext != ""))
							{
								switch (gettext)
								{
								case "ni_regulardisplay" :
									gameObject.AddComponent<ni_regulardisplay>();
									break;
								case "ni_regularmainloop" :
									gameObject.AddComponent<ni_regularmainloop>();
									break;
								case "ni_regularverticaldisplay" :
									gameObject.AddComponent<ni_regularverticaldisplay>();
									break;
								case "ni_saintlizaigne_cirmeca" :
									gameObject.AddComponent<ni_saintlizaigne_cirmeca>();
									break;
								case "ni_saintlizaigne_robot" :
									gameObject.AddComponent<ni_saintlizaigne_robot>();
									break;
								case "ni_saintlizaigne_stock" :
									gameObject.AddComponent<ni_saintlizaigne_stock>();
									break;
								default:
									break;
								}

							}
							try
							{
								gettext = (string)statjson["name"];
							}
							catch
							{
								gettext = "";
							}
							if ((gettext != null) && (gettext != ""))
								bufferme.SetString("companyname",gettext);
						}
					}
					else
					{
						if (key.ToString().Contains("SAMPLETIME"))
						{
							SAMPLETIME = (long)json[key.ToString()];
						}
						else
						{
							bufferme.SetString("DataField_"+nrfields,key.ToString());
                            nrfields++;
							
							string content = (string)json[key.ToString()];
							if(content.Contains("Form:"))
							{
								content = content.Replace("Form:","");		// remove Form
								bufferme.SetString(key.ToString()+ "_form",content);
								/*
								string mydataname = key.ToString()+ "_form";
								if (!bufferme.HasKey(mydataname+"_min"))
								{
									bufferme.SetString(mydataname+"_min","0.0");
									bufferme.SetString(mydataname+"_max","100.0");
									bufferme.SetString(mydataname+"_slide1pos","0.0");
									bufferme.SetString(mydataname+"_slide2pos","0.0");
									bufferme.SetString(mydataname+"_slide3pos","0.0");
									bufferme.SetString(mydataname+"_slide1","0.0");
									bufferme.SetString(mydataname+"_slide2","0.0");
									bufferme.SetString(mydataname+"_slide3","0.0");
								}
								*/
							}
                                
                            if (!bufferme.HasKey(key.ToString() + "_value"))
                            {
                                bufferme.SetFloat(key.ToString() + "_value", 0.0f);
                                bufferme.SetBool( key.ToString() + "_quality", false ); // Flag la valeur Comme Incorrect
                            }

                            if (!bufferme.HasKey(key.ToString() + "_quality"))
                            {
                                bufferme.SetBool( key.ToString() + "_quality", false ); // Flag la valeur Comme Incorrect
                            }

							bufferme.SetInt(key.ToString()+ "_state",0);		// virgin variable !!!
						}
					}
				}
			}
		}


        bufferme.SetInt("DataFields",nrfields);
        bufferme.SetInt("Curve_displays",nrcurves);
        bufferme.SetInt("Curve_display_item",0);
	}
		
	void Update ()
	{
		bufferme.Save();
		if (Application.loadedLevelName != "generation_server")
		{
			if (Input.GetKeyUp(KeyCode.Escape))
				{
					Application.Quit();
				}
		}

		if (Application.loadedLevelName == "generation_server")
		{
			DateTime datetimechecknextday = DateTime.Now;
			if (datetimechecknextday.DayOfYear != bufferme.GetInt("datetimechecknextday"))
			{
				ni_datarecovery.CleanAutomate1 = true;
				bufferme.SetInt("datetimechecknextday",datetimechecknextday.DayOfYear);
			}
		}

		if (verificationtime < Time.time)
		{
			if (Application.loadedLevelName != "getdatafromfiles")
				StartCoroutine ("GenerateData");
		}

		if (bufferme.GetInt ("ButtonState") == 1)
		{
			if (tmDateTime)
				tmDateTime.text = DateTimeToString(DateTime.Now);
		}
		else
		{
			if (tmDateTime)
				tmDateTime.text = DateTimeToString(DateTime.Now);
		}


        // Gestion des mails de debut et fin de periode de production
        MailProductionStartEnd();
	}
	/*
	TimeSpan ElapsedTime()
	{
		string strmili = bufferme.GetString ("StartTime");
		long mili;
		long.TryParse (strmili, out mili);
		mili = mili / 1000;
		
		TimeSpan started = new TimeSpan(0,0,0,(int)mili,0);
		TimeSpan now = GetTimeSpan (DateTime.Now);
		now = now - started;
		return now;
	}
	*/

	public static TimeSpan GetTimeSpan(DateTime mytime)
	{
		DateTime epoch = new DateTime (1970, 1, 1, 0, 0, 0, 0); //.ToLocalTime ();
		TimeSpan span = (mytime - epoch);
		return span;
	}
	public static double GetMilliseconds(DateTime mytime)
	{
		DateTime epoch = new DateTime (1970, 1, 1, 0, 0, 0, 0); //.ToLocalTime ();
		TimeSpan span = (mytime - epoch);
		return span.TotalMilliseconds;
	}

	public static void ResetAllParameters()
	{

		int nrfields;
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				bufferme.SetFloat(datanameid + "_value",0.0f);
                bufferme.SetBool( datanameid + "_quality", false ); // Flag la valeur Comme Incorrect
				if (averagebuffer != null)
				{
					for (int nn=0;nn<512;nn++)
						averagebuffer[nn,n] = nonexistingfloat;
					averagebuffer_lasthour[n] = 0;
				}
			}
		}
		naveragebuffer = 0;
	}


	static string ReplaceWithProgramValues(string formula,string key,float val)
	{
		int myind = formula.IndexOf(key);
		int lastend = -1;
		while ((myind != -1) && (lastend != myind))
		{
			int endid = myind + key.Length;
			if (endid >= formula.Length)
			{
				formula = formula.Substring(0,myind) + val + formula.Substring(endid);
			}
			else
			{
				string endstr = formula.Substring(endid,1);
				if ((endstr == "+") || (endstr == "-") ||(endstr == "/") ||(endstr == "*") ||
				    (endstr == "(") ||(endstr == ")"))
				{
					formula = formula.Substring(0,myind) + val + formula.Substring(endid);
				}
			}
			lastend = myind;
			myind = formula.IndexOf(key);
		}
		return(formula);
	}

	public static float GetLastHourDataFieldItem(string item)
	{
		float 	back = 0.0f;
		int		nrfields;
		
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				if (datanameid == item)
				{
					back = averagebuffer_lasthour[n];
					break;
				}
			}
		}
		return(back);
	}


	public static float GetDataFieldItem(string item)
	{
		float 	back = 0.0f;

		if (bufferme.HasKey(item+"_value"))
			back = bufferme.GetFloat(item+"_value");
		return(back);
	}


	public static void UseDataFromJson(long jsontime,string retreivedjson)
	{
		IDictionary json = (IDictionary)Json.Deserialize(retreivedjson);

		int nrfields;
		
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				float inval = 0.0f;
                bool bQuality = false;

                if (json.Contains(datanameid))
                {
                    bQuality = float.TryParse((string)json[datanameid], out inval);
                }

                if (bQuality)
                {
                    bufferme.SetFloat(datanameid + "_value", (float)inval);
                    bufferme.SetBool(datanameid + "_quality", true);
                }
			}
		}
		CalculateTheFormulas(false);

//		Debug.Log(">>>"+"Robot_PiecesBonnes"+"_value:"+(float)bufferme.GetFloat("Robot_PiecesBonnes"+"_value"));
//		Debug.Log(">>>"+"Robot_PiecesMauvaises"+"_value:"+(float)bufferme.GetFloat("Robot_PiecesMauvaises"+"_value"));
//		Debug.Log(">>>"+"Robot_TotalPieces"+"_value:"+(float)bufferme.GetFloat("Robot_TotalPieces"+"_value"));

//		Debug.Log ("Robot_TauxRedement_value:" + bufferme.GetFloat("Robot_TauxRedement_value"));
	}


	static DateTime startedtimevalue;

    static SchedulePeriodType IsCurrentTimeISideSchedule()
	{
		DateTime dt = DateTime.Now;

		string datanameid = "";
		switch(dt.DayOfWeek)
		{
		case DayOfWeek.Monday :	datanameid = "monday";		break;
		case DayOfWeek.Tuesday :	datanameid = "tuesday";		break;
		case DayOfWeek.Wednesday :	datanameid = "wednesday";		break;
		case DayOfWeek.Thursday :	datanameid = "thursday";		break;
		case DayOfWeek.Friday :	datanameid = "friday";		break;
		case DayOfWeek.Saturday :	datanameid = "saturday";		break;
		case DayOfWeek.Sunday :	datanameid = "sunday";		break;
		}
		
		string strslide1 = bufferme.GetString(datanameid+"_slide1");
		string strslide2 = bufferme.GetString(datanameid+"_slide2");
		if ((strslide1 == null) || (strslide1.Length == 0))		return 0;
		if ((strslide2 == null) || (strslide1.Length == 0))		return 0;

		string strslide1_h = strslide1.Substring(0,2);
		string strslide1_m = strslide1.Substring(3,2);
		string strslide2_h = strslide2.Substring(0,2);
		string strslide2_m = strslide2.Substring(3,2);
		float fslide1h;
		float fslide2h;
		float fslide1m;
		float fslide2m;
		float.TryParse(strslide1_h,out fslide1h);
		float.TryParse(strslide1_m,out fslide1m);
		float.TryParse(strslide2_h,out fslide2h);
		float.TryParse(strslide2_m,out fslide2m);
		int slide1h = (int)fslide1h;
		int slide1m = (int)fslide1m;
		fslide1h = fslide1h + (fslide1m /60.0f);
		fslide2h = fslide2h + (fslide2m /60.0f);
		if (fslide1h > fslide2h)
		{
			slide1h = (int)fslide2h;
			slide1m = (int)fslide2m;
			float tmpf = fslide1h;
			fslide1h = fslide2h;
			fslide2h = tmpf;
		}
		float currenthour = ((float)dt.Minute)/60.0f;
		currenthour += (float)dt.Hour;
		//		Debug.Log (datanameid+" Slide1:"+fslide1+"  "+datanameid+" Slide2:"+fslide2);
		// check if hour between 2 timings
		if ((currenthour >= fslide1h) && (currenthour < fslide2h))		// not <= 
		{
			InputValueChanged(slide1h,slide1m);
		}
		if (IsInputValueChanged())
		{
			startedtimevalue = new DateTime (dt.Year, dt.Month, dt.Day, bufferme.GetInt("slide1h"), bufferme.GetInt("slide1m"), 0, 0); //.ToLocalTime ();
            return(SchedulePeriodType.InPeriod);
		}
        if (currenthour < fslide1h)		return SchedulePeriodType.BeforePeriod;
        return SchedulePeriodType.AfterPeriod;
	}

	public static string DateTimeToString(DateTime dt)
	{
		return (dt.Day + " " + ni_preparedatafields.Monthnames[dt.Month-1] + " " + dt.Year + ", " + dt.Hour.ToString("D2") + "h " + dt.Minute.ToString("D2"));
	}

	static bool ValidInTimeSchedule()
	{
		if (ONLINESTORAGE)
		{
			bool scheduleok = false;

			switch(network.SCHEDULETYPE)
			{
			case 0 :
                    if (IsCurrentTimeISideSchedule() == SchedulePeriodType.InPeriod)		scheduleok = true;
				break;
			case 1 :
				scheduleok = true;
				break;
			}
			if (scheduleok)
			{
				// StartDateTime
				//			started = started.AddHours(-1);
//				Debug.Log("Passing here");
				long val = (long)GetMilliseconds (startedtimevalue);
				bufferme.SetString ("StartTime", val.ToString());		//

				bufferme.SetString ("StartDateTime", DateTimeToString(startedtimevalue));		//
				_starteddatetime = startedtimevalue;
				_elapsedtime = DateTime.Now - startedtimevalue;
				
				if (DateTime.Now.Day != bufferme.GetInt ("ValidateLastGottonDay"))
				{
					bufferme.SetInt ("ValidateLastGottonDay",DateTime.Now.Day);
				}
				return true;
			}
		}
		else
		{
			long val = (long)GetMilliseconds (real_startedtimevalue);
			bufferme.SetString ("StartTime", val.ToString());		//
			bufferme.SetString ("StartDateTime", ni_preparedatafields.DateTimeToString(real_startedtimevalue));		//

			_starteddatetime = real_startedtimevalue;
			_elapsedtime = DateTime.Now - real_startedtimevalue;
			return true;
		}

		return false;
	}

	// Automatically generated Data

	public static void Generate1FakeData()
	{
		int nrfields = 0;
		Debug.Log("Generate1FakeData Generate1FakeData Generate1FakeData");
		if (network.SYSTEMISON)
		{
		}
		else
		{
			int loopme = 1;
			if (!ONLINESTORAGE)
			{
				if (firstrun)				loopme = (int)SetupDemoLoop;
			}
			for (int inloop=0;inloop<loopme;inloop++)
			{
//				Debug.Log("generate data");
				if (bufferme.HasKey ("DataFields"))
				{
					nrfields = bufferme.GetInt ("DataFields");
					int n;
					for (n=0; n<nrfields; n++)
					{
						string datanameid = bufferme.GetString ("DataField_" + n);
						string text = (string)FakeValues [datanameid];
						string text2 = "0";
						if ((text != null) && (text.Contains (":"))) {
							text2 = text.Substring (text.IndexOf (':') + 1);
							text = text.Substring (0, text.IndexOf (':'));
						}
						
						if (datanameid.Contains ("TempsEnMarche"))
						{
							
                            if (firstrun)
                            {
                                if (!ONLINESTORAGE)
                                {
                                    TimeSpan newtimespan = DateTime.Now.AddSeconds(-(SetupDemoLoop - loopme) * datacollectorperiod) - real_startedtimevalue;
                                    bufferme.SetFloat(datanameid + "_value", (float)newtimespan.TotalSeconds);
                                    bufferme.SetBool(datanameid + "_quality", true);
                                }
                                else
                                {
                                    bufferme.SetFloat(datanameid + "_value", (float)_elapsedtime.TotalSeconds);
                                    bufferme.SetBool(datanameid + "_quality", true);
                                }
                            }
                            else
                            {
                                bufferme.SetFloat(datanameid + "_value", (float)_elapsedtime.TotalSeconds);
                                bufferme.SetBool(datanameid + "_quality", true);
                            }
						}
						else
						{
							// claude
							//						if (pi_button_forcemachineonoff.machineon)
							{
								float inval;
								float.TryParse (text, out inval);
								if (inval != 0.0f)
								{
									int tmp = (int)inval;
									if (tmp < 0)
									{
										tmp = UnityEngine.Random.Range (0, -tmp);
										if (UnityEngine.Random.Range (0, 2) == 1)
											tmp = -tmp / 2;
									}
									else
									{
										if (datanameid.Contains("PiecesMauvaises"))
											tmp = UnityEngine.Random.Range (0, tmp);
										else
											tmp = UnityEngine.Random.Range (1, tmp);
									}
									
									float ftmp = bufferme.GetFloat (datanameid + "_value");
									if (ftmp == 0.0f)
									{
										float.TryParse (text2, out ftmp);
										bufferme.SetFloat (datanameid + "_value", ftmp);
                                        bufferme.SetBool (datanameid + "_quality", true);
									}
									
									inval = tmp + bufferme.GetFloat (datanameid + "_value");
									bufferme.SetFloat (datanameid + "_value", inval);
                                    bufferme.SetBool (datanameid + "_quality", true);

								}
							}
						}
					}
				}
			}
			if (firstrun)
				firstrun = false;
		}
		CalculateTheFormulas ();	
	}

	string clientdata_startlvalue = "";
	string clientdata_endlvalue = "";
	long clientdata_lvalue;

	void OnReceivedClientDataFinished(HTTPRequest request, HTTPResponse response)
	{
		if (response.IsSuccess)
		{
			GUI_display.ErrorMessage = response.DataAsText;
			//						Debug.Log (locallink.response.Text);
			long.TryParse(response.DataAsText,out clientdata_lvalue);
			clientdata_lvalue--;
			clientdata_startlvalue = clientdata_lvalue.ToString();
			clientdata_lvalue+=2;
			clientdata_endlvalue = clientdata_lvalue.ToString();
		}
		else
		{
			clientdata_startlvalue = "error";
		}
	}

	IEnumerator GenerateData()
	{
		if (Application.loadedLevelName == "generation_server")
		{
			StopCoroutine("NewGenerateData_Server");
			StartCoroutine("NewGenerateData_Server");
		}

//		bool firsttime = true;
		long validcounter = 0;
		int laststate = 1;

		yield return new WaitForSeconds (5.0f);

		if (ONLINESTORAGE)
		{
			while (!bufferme.connected_once)
			{
				//			Debug.Log("Wating for internet connection... " + Time.time);
				yield return new WaitForSeconds (1.0f);
			}
		}
//		Debug.Log ("Pass 0");

		if (Application.loadedLevelName == "generation_server")
		{
            if (IsCurrentTimeISideSchedule() == SchedulePeriodType.InPeriod)
			{
				firstsave = true;
				laststate = 0;		// restart within schedule do not reset machines!!!
			}
		}
//		Debug.Log ("TESTING 1");

		while (true)
		{
//			Debug.Log ("TESTING 2");

			verificationtime = Time.time + 60.0f * 3.0f + datacollectorperiod;			// 3 minutes to check if this routine is alive
			if (Application.loadedLevelName == "generation_server")
			{
				bool forcedsaves = false;
				float passedtime;
				/*
				if (firsttime)
				{
					firsttime = false;
					passedtime = Time.time + 2.0f;
				}
				else
					*/
				passedtime = Time.time + datacollectorperiod;

				Debug.Log ("Pass 1");
				while (passedtime > Time.time)
				{
					if (!network.SYSTEMISON)		passedtime = Time.time + datacollectorperiod;		// forced off!

                    SchedulePeriodType inside = IsCurrentTimeISideSchedule();
					DateTime mytimetest = DateTime.Now;

                    if (inside == SchedulePeriodType.InPeriod)		// inside
					{
						if (laststate == 1)
						{
							laststate = 0;
							firstsave = true;
							bufferme.SetInt ("ResetAutomateIfAny",1);

                            if( false ) // Désactive le Reset de maniere a ne pas avoir de bad Value
							    ResetAllParameters();		// reset parameters for a new day !!
							forcedsaves = true;
							break;				// force first save
						}
						if (firstsave)
						{
							if ((mytimetest.Minute == 59) && (mytimetest.Second > 58))
							{
								lastsavedjson_force = true;
								firstsave = false;
								forcedsaves = true;
								break;		// Save last sample per hour now sample now
							}
						}
						else
						{
							if ((mytimetest.Minute == 0)) // && (mytimetest.Second > 1))
							{
								firstsave = true;
								if (lastsavedjson_force)
								{
									long savejsontimedate = (long)GetMilliseconds(mytimetest);
									bufferme.AddToBuffer(lastsavedjsontextvalue,savejsontimedate.ToString(),"");
									lastsavedjsontextvalue = "";
									passedtime = Time.time + datacollectorperiod;
								}
							}
						}
					}
					else
					{
						lastsavedjsontextvalue = "";
						lastsavedjson_force = false;
                        if ((inside == SchedulePeriodType.AfterPeriod) && (laststate == 0))		// is outside schedule and was inside schedule
						{
							laststate = 2;
							firstsave = false;
							forcedsaves = true;
							break;				// force last save before schedule close
						}
					}
                    if (inside == SchedulePeriodType.BeforePeriod)	laststate = 1;		// before the start
                    if (inside == SchedulePeriodType.AfterPeriod)	laststate = 2;

					if (ONLINESTORAGE)
//					if ((!Application.loadedLevelName.Contains("DEMO") && (ONLINESTORAGE)))
					{
						if (generate_infodata)
						{
							settext.ForceText(generate_infodata,GUI_display.ErrorMessage);
						}
					}
					if (generate_timer)
					{
						float valuetoshow = (passedtime - Time.time);
						settext.ForceText(generate_timer,((int)valuetoshow).ToString());
					}

					yield return new WaitForSeconds (1.0f);
				}
//				Debug.Log ("Pass 2");

				// verify here if the schedule is on/off ... if off, keep on resetting the virtual button
				// if contains TempsEnMarche  -> use realtime here !!!! (in seconds)
				if (forcedsaves)
				{
					if (ValidInTimeSchedule())
					{
						CalculateTheFormulas ();	
						Send_BasicSample(DateTime.Now);
						StoreAndCalcLastHour();
					}
					else
					{
						CleanCalcLastHour();
					}
				}
//				Debug.Log ("Pass 5");
			}
			else
			{
				Debug.Log ("TESTING 3");
				if (validcounter <= 0)
				{
					validcounter = 4;
					StartCoroutine("LoadSavedClientData");
					StartCoroutine("LoadSlideShow");
				}
				validcounter--;

				Debug.Log ("TESTING 4");
				if (ONLINESTORAGE)
//				if ((!Application.loadedLevelName.Contains("DEMO") && (ONLINESTORAGE)))
				{
					Debug.Log ("TESTING 5");

					float passedtime = Time.time + datacollectorperiod;

					while (!network.GetJson("s"+bufferme.GetClientId(),"1","2",3))
					{
						yield return new WaitForSeconds (0.03f);
					}
					while (network.waitingnetwork)
					{
						yield return new WaitForSeconds (0.03f);
					}
					if (network.retreivejson != "")
					{
						if (network.retreivejson.Contains("\"empty\" : \"empty\""))
						{
							// machines not activated
							bufferme.SetString ("StartDateTime","-");
							bufferme.SetString ("LastHourSince","-");
							bufferme.SetInt ("ButtonState",0);
						}
						else
						{
							Debug.Log(network.retreivejson);
							bufferme.SetInt ("ButtonState",1);
							GetLastHourData(network.retreivejson);
						}
					}

//					Debug.Log ("GET1:"+bufferme.GetFloat("Robot_PiecesBonnes_value"));
// load JSON from internet
					clientdata_startlvalue = "";
					HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "clients/"+bufferme.GetClientId()+".txt"),HTTPMethods.Get, OnReceivedClientDataFinished);
					request.Proxy = bufferme.WhichProxy();
					//				request.SetHeader("Content-Type", "application/json; charset=UTF-8");
					request.Send();

					while (clientdata_startlvalue == "")
						yield return new WaitForSeconds(0.1f);

					if (clientdata_startlvalue != "new")
					{
						while(!network.GetJson(bufferme.GetClientId(),clientdata_startlvalue,clientdata_endlvalue,3))
						{
							yield return new WaitForSeconds (0.03f);
						}
						while (network.waitingnetwork)
						{
							yield return new WaitForSeconds (0.03f);
						}
						if (network.retreivejson.Length > 10)
						{
//							Debug.Log (">>>"+network.retreivejson);
							UseDataFromJson(clientdata_lvalue,network.retreivejson);
						}
						//						Debug.Log ("GET2:"+bufferme.GetFloat("Robot_PiecesBonnes_value"));
						//						Debug.Log ("Normal sample:"+network.retreivejson);
					}

					while (passedtime > Time.time)
					{
						yield return new WaitForSeconds (0.2f);
					}
				}
				else
				{
					Debug.Log ("TESTING 5 "+datacollectorperiod);
					{
//						Debug.Log ("TESTING");
						if (ni_statlegend.FORCEFAKESTATS)
						{
							Generate1FakeData();
							StoreAndCalcLastHour();
						}
						else
						{
							ValidInTimeSchedule();
							Generate1FakeData();
							Send_BasicSample(DateTime.Now);
							StoreAndCalcLastHour();
						}
					}
					yield return new WaitForSeconds (datacollectorperiod);
				}
			}

		}
		Debug.Log ("Pass endless");

	}

	void OnSlideShowFinished(HTTPRequest request, HTTPResponse response)
	{
		if (response.IsSuccess)
		{
			ni_slideshow myslideshow = (ni_slideshow)gameObject.GetComponent<ni_slideshow>();
			myslideshow.SlideShowInit(response.DataAsText);
		}
		else
		{

		}
	}

	IEnumerator LoadSlideShow()
	{
//		HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "clients/slideshow_"+bufferme.GetClientId()+".txt"),HTTPMethods.Get, OnSlideShowFinished);
//		request.Send();
		yield return new WaitForSeconds (0.2f);
	}


	public static string GetValueWithKey2D(string key)
	{
		int nrfields = bufferme.GetInt("DataFields");
		string ret = "";

		if (!bufferme.HasKey("DataFields"))		return("");

		for (int n=0;n<nrfields;n++)
		{
			string datanameid = bufferme.GetString("DataField_"+n);
			if (key == datanameid)
			{
				float val = bufferme.GetFloat(datanameid+"_value");
				ret = val.ToString("F2");
				break;
			}
		}
		return(ret);
	}

	public static string GetValueWithKeyInt(string key)
	{
		int nrfields = bufferme.GetInt("DataFields");
		string ret = "";
		
		if (!bufferme.HasKey("DataFields"))		return("");
		
		for (int n=0;n<nrfields;n++)
		{
			string datanameid = bufferme.GetString("DataField_"+n);
			if (key == datanameid)
			{
				float val = bufferme.GetFloat(datanameid+"_value");
				int temp = (int)val;
				ret = temp.ToString();
				break;
			}
		}
		return(ret);
	}

	/*
	void OnGUI()
	{
		float ypos = Screen.height * 2.5f / 20.0f;
		float xpos = 20;
		int nrfields;
		GUI.color = Color.black;
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				if (bufferme.GetInt(datanameid+"_state") == 0)
				{
					if (bufferme.HasKey(datanameid+"_form"))
						GUI.Label(new Rect(xpos,ypos,Screen.width/2, Screen.height/20),datanameid+" [FORM]: "+bufferme.GetFloat(datanameid+"_value"));
					else
						GUI.Label(new Rect(xpos,ypos,Screen.width/2, Screen.height/20),datanameid + ": " + bufferme.GetFloat(datanameid+"_value"));
					ypos += Screen.height / 25.0f;
				}
				if (ypos > Screen.height)
				{
					ypos = Screen.height * 2.5f / 20.0f;
					xpos = Screen.width / 2.0f;
				}

			}
		}
	}
	*/

	static void GetIntFromStringJson(IDictionary json, string key, string itemname)
	{
		string value = "";
		try
		{
			value = (string)json[key];
		}
		finally
		{
			if (value != "")
			{
				int myval;
				int.TryParse(value,out myval);
				bufferme.SetInt(itemname+key,myval);
			}
		}
	}
	
	static void GetFloatFromStringJson(IDictionary json, string key, string itemname)
	{
		string value = "";
		try
		{
			value = (string)json[key];
		}
		catch
		{
			value = "";
		}
		if ((value != null) && (value != ""))
		{
			float	myval;
			float.TryParse(value,out myval);
			bufferme.SetFloat(itemname+key,myval);
		}
	}
	
	static void GetStringFromJson(IDictionary json, string key, string itemname)
	{
		string value = "";
		try
		{
			value = (string)json[key];
		}
		catch
		{
			value = "";
		}
		if ((value != null) && (value != ""))
			bufferme.SetString(itemname+key,value);
	}

	static string PrepareDirectory(string rootDirectory,DateTime dt)
	{
		if (!Directory.Exists(rootDirectory))	Directory.CreateDirectory(rootDirectory);
		string path = rootDirectory + "/" + dt.Year.ToString("D4");
		if (!Directory.Exists(path))	Directory.CreateDirectory(path);
		path += "/" + dt.Month.ToString("D2");
		if (!Directory.Exists(path))	Directory.CreateDirectory(path);
		path += "/" + dt.Day.ToString("D2");
		if (!Directory.Exists(path))	Directory.CreateDirectory(path);
		path += "/";
		return (path);
	}


	static bool VerifyCSVValidColumn(long nn)
	{
		bool usethis = false;

		if (ni_statlegend.samplevalues[0,nn] != -10000)		usethis = true;
		if (ni_statlegend.samplevalues[1,nn] != -10000)		usethis = true;
		if (ni_statlegend.samplevalues[2,nn] != -10000)		usethis = true;
		if (ni_statlegend.samplevalues[4,nn] != -10000)		usethis = true;

		if (nn < (ni_statlegend.nrsubsamples-1))
		{
			if (ni_statlegend.samplevalues[0,nn+1] != -10000)		usethis = true;
			if (ni_statlegend.samplevalues[1,nn+1] != -10000)		usethis = true;
			if (ni_statlegend.samplevalues[2,nn+1] != -10000)		usethis = true;
			if (ni_statlegend.samplevalues[4,nn+1] != -10000)		usethis = true;
		}
//		return (true);
		return (usethis);
	}

	// SAVE REQUESTED CSV File
	public static void Savecsv(string jsonstr)
	{
		if (!bufferme.HasKey ("DataFields"))						return;
		string filename;
		#if (UNITY_IPHONE || UNITY_ANDROID) && (!UNITY_EDITOR)
		filename = Application.persistentDataPath + "/" + bufferme.GetClientId ()+"/out.csv";
		#else
		filename = Application.streamingAssetsPath + "/" + bufferme.GetClientId ()+"/out.csv";
		#endif

		IDictionary json = null;
		IDictionary innerjson = null;

		if (jsonstr != "")			json = (IDictionary)Json.Deserialize(jsonstr);

		StringBuilder sb = new StringBuilder();  
		string line = "sep=;";
		sb.AppendLine(line);
		// first line
		line = "//////////";
		for (long nn=0;nn<ni_statlegend.nrsubsamples;nn++)
		{
			if (VerifyCSVValidColumn(nn))
			{
				line=line + ";";
				line=line + ni_statlegend.samplenames[nn];
			}
		}
		line = line.Replace(".",",");
		sb.AppendLine(line);

		if (json != null)
		{
			// Additional lines DATAFIELDS
			long nrfields = bufferme.GetInt("DataFields");
			long n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				if (!bufferme.HasKey(datanameid+"_form"))
				{
					line = datanameid;
					for (long nn=0;nn<ni_statlegend.nrsubsamples;nn++)
					{
						if (VerifyCSVValidColumn(nn))
						{
							try
							{
								innerjson = (IDictionary)json["sample_c"+nn];
							}
							catch
							{
								break;
							}
							string textvalue = (string)innerjson[datanameid];
							float myvalue;
							float.TryParse(textvalue,out myvalue);
							line=line+";";
							line = line + myvalue;
						}
					}
					//			Debug.Log (line);
					line = line.Replace(".",",");
					sb.AppendLine(line);
				}
			}
		}
		// Additional lines FORMULAS

// use predefined values to fill csv here
		int statid = bufferme.GetInt("Curve_display_item");
		line = bufferme.GetString("Curve_displays_name"+statid);
		for (long nn=0;nn<ni_statlegend.nrsubsamples;nn++)
		{
			if (VerifyCSVValidColumn(nn))
			{
				if (ni_statlegend.samplevalues[0,nn] != -10000)
				{
					line=line + ";";
					line=line + ni_statlegend.samplevalues[0,nn];
				}
				else
					line=line + ";";
			}
		}
		line = line.Replace(".",",");
		sb.AppendLine(line);

		line = "Average N "+bufferme.GetString("Curve_displays_name"+statid);
		for (long nn=0;nn<ni_statlegend.nrsubsamples;nn++)
		{
			if (VerifyCSVValidColumn(nn))
			{
				if (ni_statlegend.samplevalues[1,nn] != -10000)
				{
					line=line + ";";
					line=line + ni_statlegend.samplevalues[1,nn];
				}
				else
					line=line + ";";

			}
		}
		line = line.Replace(".",",");
		sb.AppendLine(line);

		line = "Average N-1 "+bufferme.GetString("Curve_displays_name"+statid);
		for (long nn=0;nn<ni_statlegend.nrsubsamples;nn++)
		{
			if (VerifyCSVValidColumn(nn))
			{
				if (ni_statlegend.samplevalues[2,nn] != -10000)
				{
					line=line + ";";
					line=line + ni_statlegend.samplevalues[2,nn];
				}
				else
					line=line + ";";
			}
		}
		line = line.Replace(".",",");
		sb.AppendLine(line);

		line = "Average N-2 "+bufferme.GetString("Curve_displays_name"+statid);
		for (long nn=0;nn<ni_statlegend.nrsubsamples;nn++)
		{
			if (VerifyCSVValidColumn(nn))
			{
				if (ni_statlegend.samplevalues[4,nn] != -10000)
				{
					line=line + ";";
					line=line + ni_statlegend.samplevalues[4,nn];
				}
				else
					line=line + ";";
			}
		}
		line = line.Replace(".",",");
		sb.AppendLine(line);
		File.WriteAllText(filename, sb.ToString());                 
		sendemail.myattachment = filename;
		sendemail.Send();
	}

	public static void GetLastHourData(string retreivedjson)
	{
//		Debug.Log(">>>" + retreivedjson);
		IDictionary json = (IDictionary)Json.Deserialize(retreivedjson);
		int nrfields;

		bufferme.SetString ("StartDateTime",(string)json["startdatetime"]);
		bufferme.SetString ("LastHourSince",(string)json["lasthoursince"]);

		DateTime dt;

		string incometimestr = (string)json["startdatetime"];

		int years = 0;
		int month = 0;
		int day = 0;
		int hour = 0;
		int minute = 0;

		string syears;
		string sday;
		string shour;
		string sminute;

		int yindex = incometimestr.IndexOf(',') - 4;
		syears = incometimestr.Substring(yindex,4);
		int.TryParse(syears, out years);

		yindex = incometimestr.IndexOf(' ');
		sday = incometimestr.Substring(0,yindex);
		int.TryParse(sday, out day);

		string interstr = incometimestr.Substring(yindex+1);
		int secindex = interstr.IndexOf(' ');
		interstr = incometimestr.Substring(yindex+1,secindex);
		foreach(string mn in Monthnames)
		{
			if (mn == interstr)		break;
			month++;
		}

		yindex = incometimestr.IndexOf(',') +2;
		shour = incometimestr.Substring(yindex,2);
		sminute = incometimestr.Substring(yindex+4,2);
		int.TryParse(shour, out hour);
		int.TryParse(sminute, out minute);

//		Debug.Log(">>>" + years + " " + month + " " + day + " " + hour + " " +minute);
//		Debug.Log(">>>" + (string)json["startdatetime"]);

		DateTime incometime = new DateTime(years, month, day,hour,minute,0,0);

//		Debug.Log(">>>"+incometime.ToLongDateString() + " " + incometime.ToLongTimeString());
//		dt = DateTime.Parse ((string)json["startdatetime"]);
		dt = incometime;

		if (ONLINESTORAGE)
			_elapsedtime = DateTime.Now - dt;
		else
			_elapsedtime = DateTime.Now - real_startedtimevalue;

//		Debug.Log ("Elapsed time:" + _elapsedtime.Hours + ":" + _elapsedtime.Minutes + ":" +_elapsedtime.Seconds);
		//		
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_" + n);
				float fval;
				float.TryParse((string)json[datanameid],out fval);
				averagebuffer_lasthour[n] = fval;
			}
		}
		CalculateTheFormulas(true);
	}

	public static void CleanCalcLastHour()
	{
		GUI_display.ErrorMessage = "NO Broadcasting!!!";
		string json2 = "{ \"empty\" : \"empty\" }";
		bufferme.AddToBuffer(json2,"1","s");
	}

	public static void StoreAndCalcLastHour()
	{
		if (averagebuffer == null)		return;		//
		int nrfields;
		int n;
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			if (network.SYSTEMISON)
			{
				float divtest = naveragebuffer - (3600.0f/datacollectorperiod);
				int onehourago = (int)divtest;
				while (onehourago < 0)		onehourago += 512;
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_" + n);
					if (!bufferme.HasKey(datanameid+"_form"))
					{
                        bool bQuality = bufferme.GetBool(datanameid+"_quality");

                        if (bQuality)
                            averagebuffer[naveragebuffer, n] = bufferme.GetFloat(datanameid + "_value");
                        else
                            averagebuffer[naveragebuffer, n] = nonexistingfloat;


                        if (averagebuffer[onehourago, n] == nonexistingfloat)
                        {
                            if (averagebuffer[naveragebuffer, n] == nonexistingfloat)
                                averagebuffer_lasthour[n] = 0;
                            else
                                averagebuffer_lasthour[n] = averagebuffer[naveragebuffer, n];
                        }
						else
						{
                            if (averagebuffer[naveragebuffer, n] == nonexistingfloat)
                                averagebuffer_lasthour[n] = 0;
                            else
							    averagebuffer_lasthour[n] = averagebuffer[naveragebuffer,n] - averagebuffer[onehourago,n];
						}
					}
				}
				naveragebuffer++;
				if (naveragebuffer >= 512)		naveragebuffer = 0;		// looparound
				
				//			Debug.Log ("Filling lasthour index :" + naveragebuffer + "  Testing :"+onehourago);
				if (Application.loadedLevelName == "generation_server")
				{
					string json2 = "{ ";
					int nn = 0;
					
					DateTime specialdate = DateTime.Now.AddHours(-1);
					
					json2 = json2 + "\"startdatetime\" : \"" + bufferme.GetString ("StartDateTime") + "\",";
					if (DateTime.Compare(_starteddatetime, specialdate) >  0)
					{
						json2 = json2 + "\"lasthoursince\" : \"" + _starteddatetime.Hour.ToString("D2") + ":" + _starteddatetime.Minute.ToString("D2") + "\"";
					}
					else
						json2 = json2 + "\"lasthoursince\" : \"" + specialdate.ToShortTimeString() + "\"";
					
					for (n=0;n<nrfields;n++)
					{
						string datanameid = bufferme.GetString("DataField_" + n);
						if (!bufferme.HasKey(datanameid+"_form"))
						{
							json2 = json2 + ",";
							json2 = json2 + "\""+datanameid+"\" : \"" + averagebuffer_lasthour[n] + "\"";
							nn++;
						}
					}
					json2 = json2 + " }";
					//				Debug.Log ("Save data:" + json2);
					bufferme.AddToBuffer(json2,"1","s");
				}
			}
			else
			{
				// Fake demo values
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_" + n);
//					if (!bufferme.HasKey(datanameid+"_form"))
					{
//						averagebuffer[naveragebuffer,n] = bufferme.GetFloat(datanameid+"_value");
//						if (averagebuffer[onehourago,n] == nonexistingfloat)
                        if( true )
                        { 
                            bool bQuality = bufferme.GetBool(datanameid + "_quality");

                            if (bQuality)
                                averagebuffer_lasthour[n] = bufferme.GetFloat(datanameid + "_value");
                            else
                                averagebuffer_lasthour[n] = 0;

                        }
//						else
//						{
//							averagebuffer_lasthour[n] = averagebuffer[naveragebuffer,n] - averagebuffer[onehourago,n];
//						}
					}
				}
			}

		}
	}

	public static bool LoadSampleJsonFile(string filename)
	{
		return true;
	}

	//	return "ERP_" + dt.ToString("yyyyMMddhhmmss") + ".ini";
	public static bool LoadSampleIniFile(string filename)
	{
		return true;
	}


	// SAVE CLIENT DATA
// SAVE CLIENT DATA
// SAVE CLIENT DATA
// SAVE CLIENT DATA
	public static void SaveClientData()
	{
		string json = "{ ";
		json = json + "\"version\" : \"" + DateTimeToString(DateTime.Now) + "\",";
		json = json + "\"inputcommandtext\" : \"" + bufferme.GetString("inputcommandtext") + "\",";

		int nrfields;
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				if (n != 0)		json = json + ",";
				string datanameid = bufferme.GetString("DataField_"+n);
				json = json + "\""+datanameid+"\" : { ";
				if (bufferme.HasKey(datanameid+"_state"))
					json = json + "\"_state\" : \"" + bufferme.GetInt(datanameid+"_state") + "\",";
				if (bufferme.HasKey(datanameid+"_min"))
					json = json + "\"_min\" : \"" + bufferme.GetString(datanameid+"_min") + "\",";
				if (bufferme.HasKey(datanameid+"_max"))
					json = json + "\"_max\" : \"" + bufferme.GetString(datanameid+"_max") + "\",";
				if (bufferme.HasKey(datanameid+"_slide1pos"))
					json = json + "\"_slide1pos\" : \"" + bufferme.GetFloat(datanameid+"_slide1pos") + "\",";
				if (bufferme.HasKey(datanameid+"_slide2pos"))
					json = json + "\"_slide2pos\" : \"" + bufferme.GetFloat(datanameid+"_slide2pos") + "\",";
				if (bufferme.HasKey(datanameid+"_slide3pos"))
					json = json + "\"_slide3pos\" : \"" + bufferme.GetFloat(datanameid+"_slide3pos") + "\",";

				if (bufferme.HasKey(datanameid+"_slide1"))
					json = json + "\"_slide1\" : \"" + bufferme.GetString(datanameid+"_slide1") + "\",";
				if (bufferme.HasKey(datanameid+"_slide2"))
					json = json + "\"_slide2\" : \"" + bufferme.GetString(datanameid+"_slide2") + "\",";
				if (bufferme.HasKey(datanameid+"_slide3"))
					json = json + "\"_slide3\" : \"" + bufferme.GetString(datanameid+"_slide3") + "\",";

				if (bufferme.HasKey(datanameid+"_message_good"))
					json = json + "\"_message_good\" : \"" + bufferme.GetString(datanameid+"_message_good") + "\",";
				if (bufferme.HasKey(datanameid+"_message_bad"))
					json = json + "\"_message_bad\" : \"" + bufferme.GetString(datanameid+"_message_bad") + "\",";
				if (bufferme.HasKey(datanameid+"_message_toogood"))
					json = json + "\"_message_toogood\" : \"" + bufferme.GetString(datanameid+"_message_toogood") + "\",";

				json = json.Substring(0,json.LastIndexOf(","));
				json = json + " }";
			}
		}
		json = json + " }";

		bufferme.AddToBuffer(json,"100","s");		// INI FILE
/*
		var file = File.Open (Application.persistentDataPath + bufferme.GetClientId () + "_datafields.txt", FileMode.Create);
		file.Write (System.Text.Encoding.UTF8.GetBytes(json), 0, json.Length);
		file.Close ();
		*/
	}

	// GetJson
	public static void LoadClientData(string json)
	{
//		Debug.Log("SETTING:"+json);
		/*
		byte [] data = new byte[1024*4];

		var file = File.Open (Application.persistentDataPath + bufferme.GetClientId () + "_datafields.txt", FileMode.Open);
		file.Read (data, 0, 1024*4);
		file.Close ();
		string json = System.Text.Encoding.UTF8.GetString (data);
*/
		IDictionary djson = (IDictionary)Json.Deserialize (json);
		IDictionary localdic = null;

		int nrfields;

		string inputcommandtext = "";
		try
		{
			inputcommandtext = (string)djson["inputcommandtext"];
		}
		catch
		{
			inputcommandtext = "";
		}
		if (inputcommandtext == null)		inputcommandtext = "";

		bufferme.SetString("inputcommandtext",inputcommandtext);


		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				try
				{
					localdic = (IDictionary)djson[datanameid];
				}
				finally
				{
					if (localdic != null)
					{
						GetIntFromStringJson(localdic,"_state",datanameid);
						GetFloatFromStringJson(localdic,"_slide1pos",datanameid);
						GetFloatFromStringJson(localdic,"_slide2pos",datanameid);
						GetFloatFromStringJson(localdic,"_slide3pos",datanameid);

						GetStringFromJson(localdic,"_slide1",datanameid);
						GetStringFromJson(localdic,"_slide2",datanameid);
						GetStringFromJson(localdic,"_slide3",datanameid);

						GetStringFromJson(localdic,"_min",datanameid);
						GetStringFromJson(localdic,"_max",datanameid);

						GetStringFromJson(localdic,"_message_good",datanameid);
						GetStringFromJson(localdic,"_message_bad",datanameid);
						GetStringFromJson(localdic,"_message_toogood",datanameid);
					}
				}
			}
		}
	}




	// averagebuffer_lasthour is used as temp for calc
	public static void PrepareDisplayStatistics(string strjson,long nelements)
	{
		// 0 : normal
		// 1 : average n
		// 2 : average n-1
		// 3 : objectif
		// 4 : average n-2
		IDictionary json = (IDictionary)Json.Deserialize(strjson);
		IDictionary localjson;
		string tmpstr;
		int nn = 0;
		int inner;
		float [] averagebuffer_lasthour_backup = new float[averagebuffer_lasthour.Length];
		int curveitem = bufferme.GetInt("Curve_display_item");
//		Debug.Log ("CURVENR:"+bufferme.GetInt("Curve_display_item"));

		string variablename = bufferme.GetString("Curve_displays_variable"+curveitem);
//		Debug.Log ("DISPLAY STATS ON : " + variablename);
		string[] decodedformula = bufferme.GetString("Curve_displays_form"+curveitem).Split("/+-*()".ToCharArray(),    StringSplitOptions.RemoveEmptyEntries);
		float [,] parameterbuffers_n = new float[decodedformula.Length,nelements];
		float [,] parameterbuffers_n_1 = new float[decodedformula.Length,nelements];
		float [,] parameterbuffers_n_2 = new float[decodedformula.Length,nelements];

		for (nn=0;nn<averagebuffer_lasthour.Length;nn++)			averagebuffer_lasthour_backup[nn] = averagebuffer_lasthour[nn];

		if (bufferme.HasKey("DataFields"))
		{
			int nrfields = bufferme.GetInt("DataFields");
			for (nn=0;nn<nrfields;nn++)
			{
				string datanameid = bufferme.GetString("DataField_"+nn);
				bufferme.SetInt(datanameid+"_index",nn);
			}
// Fill master curve
			nn = 0;
			int failed = 3;
			while (failed > 0)
			{
				failed = 3;
				for (int getthree=0;getthree<3;getthree++)
				{
					string rootname = "sample_c";
					if (getthree == 1) rootname = "sample_b";
					if (getthree == 2) rootname = "sample_a";
					try
					{
						localjson = (IDictionary)json[rootname+nn];
					}
					catch
					{
						failed--;
						continue;
					}
					if (localjson == null)
					{
						failed--;
						continue;
					}
					
					try
					{
						tmpstr = (string)localjson["_unknown_"];
					}
					catch
					{
						tmpstr = "";
					}
					if ((tmpstr == "") || (tmpstr == null))
					{
						/*
						if (getthree == 0)
						{
							Debug.Log ("OK sample_c "+nn);
						}
						if (getthree == 1)
						{
							Debug.Log ("OK sample_b "+nn);
						}
						if (getthree == 2)
						{
							Debug.Log ("OK sample_a "+nn);
						}
*/
						foreach(var key in localjson.Keys)
						{
							if (bufferme.HasKey(key.ToString()+"_index"))
							{
								float fval;
								float.TryParse((string)localjson[key.ToString()],out fval);
								averagebuffer_lasthour[bufferme.GetInt(key.ToString()+"_index")] = fval;
							}
						}
						CalculateTheFormulas(true);
						// Get formula result and save in samplebuffer
						
						int ind = bufferme.GetInt(variablename+"_index");

						if (getthree == 0)
						{
							ni_statlegend.samplevalues[0,nn] = averagebuffer_lasthour[ind] * 100.0f;
							if (ni_statlegend.samplemax[0] < ni_statlegend.samplevalues[0,nn])
								ni_statlegend.samplemax[0] = ni_statlegend.samplevalues[0,nn];
							if (ni_statlegend.samplemin[0] > ni_statlegend.samplevalues[0,nn])
								ni_statlegend.samplemin[0] = ni_statlegend.samplevalues[0,nn];
						}

						inner = 0;
						
						foreach(string el in decodedformula)
						{
							if (bufferme.HasKey(el+"_index"))
							{
								ind = bufferme.GetInt(el+"_index");
								if (getthree == 0)
									parameterbuffers_n[inner,nn] = averagebuffer_lasthour[ind];
								if (getthree == 1)
									parameterbuffers_n_1[inner,nn] = averagebuffer_lasthour[ind];
								if (getthree == 2)
									parameterbuffers_n_2[inner,nn] = averagebuffer_lasthour[ind];
							}
							inner++;
						}
						
						int ii;
						float [] adders = new float[decodedformula.Length];
						for (ii=0;ii<decodedformula.Length;ii++)
						{
							adders[ii] = 0;
							for (int iii=0;iii<=nn;iii++)
							{
								if (getthree == 0)
									adders[ii] += parameterbuffers_n[ii,iii];
								if (getthree == 1)
									adders[ii] += parameterbuffers_n_1[ii,iii];
								if (getthree == 2)
									adders[ii] += parameterbuffers_n_2[ii,iii];
							}
						}
						// Now formule it !!!
						string formula = bufferme.GetString("Curve_displays_form"+curveitem);
						ii = 0;
						foreach(string el in decodedformula)
						{
							formula = formula.Replace(el,adders[ii].ToString());
							ii++;
						}
						MathFunctions.MathParser mp = new MathFunctions.MathParser ();
						decimal result = mp.Calculate (formula);
						if (getthree == 0)
							ni_statlegend.samplevalues[1,nn] = (float)result  * 100.0f;;
						if (getthree == 1)
							ni_statlegend.samplevalues[2,nn] = (float)result  * 100.0f;;
						if (getthree == 2)
							ni_statlegend.samplevalues[4,nn] = (float)result  * 100.0f;;

						if (getthree == 0)
						{
							if (ni_statlegend.samplemax[1] < ni_statlegend.samplevalues[1,nn])
								ni_statlegend.samplemax[1] = ni_statlegend.samplevalues[1,nn];
							if (ni_statlegend.samplemin[1] > ni_statlegend.samplevalues[1,nn])
								ni_statlegend.samplemin[1] = ni_statlegend.samplevalues[1,nn];
						}
						if (getthree == 1)
						{
							if (ni_statlegend.samplemax[2] < ni_statlegend.samplevalues[2,nn])
								ni_statlegend.samplemax[2] = ni_statlegend.samplevalues[2,nn];
							if (ni_statlegend.samplemin[2] > ni_statlegend.samplevalues[2,nn])
								ni_statlegend.samplemin[2] = ni_statlegend.samplevalues[2,nn];
						}
						if (getthree == 2)
						{
							if (ni_statlegend.samplemax[4] < ni_statlegend.samplevalues[4,nn])
								ni_statlegend.samplemax[4] = ni_statlegend.samplevalues[4,nn];
							if (ni_statlegend.samplemin[4] > ni_statlegend.samplevalues[4,nn])
								ni_statlegend.samplemin[4] = ni_statlegend.samplevalues[4,nn];
						}

						//				Debug.Log (nn + " Stat1: " + ni_statlegend.samplevalues[0,nn] + "  Stat2: " + ni_statlegend.samplevalues[1,nn]);
						
					}
				}
				nn++;
			}
		}
		for (nn=0;nn<averagebuffer_lasthour.Length;nn++)			averagebuffer_lasthour[nn] = averagebuffer_lasthour_backup[nn];

	}

	public static void CalcAverageOnBulk(string savetime,string strjson,long nelements,string prefix)
	{
//		Debug.Log ("IN:" + strjson);
		IDictionary json = (IDictionary)Json.Deserialize(strjson);
		IDictionary localjson;
		float	[] allvalues = null;
		int nrfields;
		int nn,n;

		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			allvalues = new float[nrfields];
			for (nn=0;nn<nrfields;nn++)
			{
				string datanameid = bufferme.GetString("DataField_"+nn);
				bufferme.SetInt(datanameid+"_index",nn);
				allvalues[nn] = 0;
			}
			nn = 0;
			while (true)
			{
				try
				{
					localjson = (IDictionary)json["sample"+nn];
				}
				catch
				{
					break;		// stop analysis
				}
				if (localjson == null)	break;

				foreach(var key in localjson.Keys)
				{
					string newtext = (string)localjson[key.ToString()];
					n = bufferme.GetInt(key.ToString()+"_index");
					float myval;
					float.TryParse(newtext,out myval);

					allvalues[n] = allvalues[n] + myval;
				}
				nn++;
			}
			if (nn > 0)
			{
//				for (nn=0;nn<nrfields;nn++)
//				{
//					allvalues[nn] = allvalues[nn] / nelements;
//				}

				// the last column are the real values of the "last hours" ... make json and save !!!
				string json2 = "{ ";
				nn = 0;
				
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_" + n);
					if (!bufferme.HasKey(datanameid+"_form"))
					{
						if (nn != 0)	json2 = json2 + ",";
						json2 = json2 + "\""+datanameid+"\" : \"" + allvalues[n] + "\"";
						nn++;
					}
				}
				json2 = json2 + " }";
//								Debug.Log ("OUT:" + json2);
				bufferme.AddToBuffer(json2,savetime,prefix);
			}
		}
	}

	public static void CalcAverageHour(string savetime,string strjson,long nelements)
	{
		IDictionary json = (IDictionary)Json.Deserialize(strjson);
		IDictionary localjson;
		int nn = 0;
		int nrfields,i,n;
		float	[,] allvalues = null;
		float	[] addtoall = null;
		float	[] references = null;
//		Debug.Log ("IN:" + strjson);

//		ni_datarecovery.savedata += "JSON : " + strjson;
//		System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",ni_datarecovery.savedata);

		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			for (nn=0;nn<nrfields;nn++)
			{
				string datanameid = bufferme.GetString("DataField_"+nn);
				bufferme.SetInt(datanameid+"_index",nn);
			}

			nn = 0;
			while (true)
			{
				try
				{
					localjson = (IDictionary)json["sample"+nn];
				}
				catch
				{
					break;		// stop analysis
				}
				if (localjson == null)	break;

				if (nn == 0)
				{
					references = new float[nrfields];
					allvalues = new float[nrfields,nelements];
					addtoall = new float[nrfields];
					for (n=0;n<nrfields;n++)
					{
						references[n] = 0;
						addtoall[n] = 0;
					}
				}
				foreach(var key in localjson.Keys)
				{
					string newtext = (string)localjson[key.ToString()];
					n = bufferme.GetInt(key.ToString()+"_index");
					float myval;
					float.TryParse(newtext,out myval);
					if ((nn == 0) || (allvalues[n,nn-1] <= (myval + addtoall[n])))
					{
						allvalues[n,nn] = myval + addtoall[n];
						if (nn == 0)
						{
							references[n] = myval;
						}
					}
					else
					{
						addtoall[n] = allvalues[n,nn-1];
						allvalues[n,nn] = myval + addtoall[n];
					}
				}
				nn++;
			}
			if (nn > 0)
			{
				// the last column are the real values of the "last hours" ... make json and save !!!
				string json2 = "{ ";
				nn = 0;
				
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_" + n);
					float valuetouse = (allvalues[n,nelements-1] - references[n]);
					if (datanameid.Contains("TempsEnMarche"))
					{
						if (valuetouse > 3600.0f)		valuetouse = 3600.0f;
					}

					if (!bufferme.HasKey(datanameid+"_form"))
					{
						if (nn != 0)	json2 = json2 + ",";
						json2 = json2 + "\""+datanameid+"\" : \"" + valuetouse + "\"";
						nn++;
					}
				}
				json2 = json2 + " }";
//				Debug.Log ("OUT:" + json2);
				bufferme.AddToBuffer(json2,savetime,"h");
			}
		}
	}














	// UNUSED
	// UNUSED
	// UNUSED
	// UNUSED
	// UNUSED

	public static void CreateStatisticsTable(long nrcolumns,DateTime start,long period)
	{
		if (!bufferme.HasKey ("DataFields"))						return;
		cvsitemnames = null;
		cvsitems = null;
		cvsitemnames = new string[nrcolumns+1];
		cvsitems = new float[bufferme.GetInt("DataFields",1),nrcolumns*2+1];
		cvsitemnames[0] = "++++++++++";
		for (int nn=0;nn<nrcolumns;nn++)
		{
			cvsitemnames[nn+1] = start.ToShortDateString() + " " + start.ToShortTimeString();
			start = start.AddMilliseconds(period);
		}
	}

	static void LoadJsonForStatAndAdd(string fname,string newkey,long column)
	{
		#if UNITY_ANDROID && (!UNITY_EDITOR)
		WWW newwww = new WWW(fname);
		while (!newwww.isDone)
		{
		}
		string text = newwww.text;
		#else
		string text = System.IO.File.ReadAllText(fname);
		#endif

		IDictionary sample = null;
		try
		{
			sample = (IDictionary)Json.Deserialize (text);
		}
		catch
		{
			Debug.Log ("Deserialize went wrong");
			return;
		}
		
		if (sample != null)
			sample = (IDictionary)sample[newkey];
		
		int nrfields = bufferme.GetInt("DataFields");
		int n;
		
		if (LastStatValue == null)
		{
			LastStatValue = new float[nrfields];
			for (n=0;n<nrfields;n++)
				LastStatValue[n] = 0.0f;
		}
		for (n=0;n<nrfields;n++)
		{
			//				if (bufferme.HasKey(datanameid+"_form"))
			string datanameid = bufferme.GetString("DataField_"+n);
			string income = "0";
			try
			{
				if (sample != null)
					income = (string)sample[datanameid];
			}
			catch
			{
				income = "0";
			}
			float val = 0.0f;
			float.TryParse(income,out val);
			
			bool keepondoing = true;
			while (keepondoing)
			{
				val = val + OverAllAdding;
				if (cvsitems [n, 1+column*2+0] < val)
				{
					if (column == 0)					cvsitems [n, 0] = val;
					cvsitems [n, 1+column*2+0] = val;
					if (column == 0)
						cvsitems [n, 1+column*2+1] = 0;
					else
						cvsitems [n, 1+column*2+1] = val - cvsitems [n, 1+((column-1)*2)+0];
					keepondoing = false;
				}
				else
				{
					if (cvsitems [n, 1+column*2+0] > val)
					{
						Debug.Log ("Machine reset");
						// we are in trouble :( this is the case when the machine was reset
						OverAllAdding = LastStatValue[n];
					}
					else
						keepondoing = false;
				}
			}
			LastStatValue[n] = val;
		}
	}

	static void CleanStats(long column)
	{
		for (int nn=0; nn<bufferme.GetInt("DataFields",1); nn++)
		{
			cvsitems [nn, 1+column*2+0] = 0.0f;
			cvsitems [nn, 1+column*2+1] = 0.0f;
		}
	}

	
	public static void FindhighestvaluesStats()
	{
		for (int nn=0; nn<bufferme.GetInt("DataFields",1); nn++)
		{
			cvsitems[nn,0] = 0.0f;
			for (int n=1;n<(cvsitemnames.Length-1)*2+1;n+=2)
			{
				if (cvsitems[nn,n] > cvsitems[nn,0])
					cvsitems[nn,0] = cvsitems[nn,n];
			}
		}
	}
	
	
	


	public static void LoadRequestedStatistics(DateTime start, DateTime end,long column)
	{
		long startperiod = (long)GetMilliseconds (start);
		long endperiod = (long)GetMilliseconds (end);
		long countthem = 0;
		string filename;
		#if (UNITY_IPHONE || UNITY_ANDROID) && (!UNITY_EDITOR)
		filename = Application.persistentDataPath + "/" + bufferme.GetClientId ();
		#else
		filename = Application.streamingAssetsPath + "/" + bufferme.GetClientId ();
		#endif
		
		filename = PrepareDirectory (filename,start);
		filename = filename.Substring(0,filename.LastIndexOf("/"));
		
		DirectoryInfo dir = new DirectoryInfo (filename);
		FileInfo[] info = dir.GetFiles ("*.txt",SearchOption.AllDirectories);
		System.Array.Sort(info, (f1, f2) => f1.Name.CompareTo(f2.Name));
		
		CleanStats (column);
		OverAllAdding = 0.0f;
		LastStatValue = null;
		
		foreach(FileInfo f in info)
		{
			long tmplng = 0;
			string tmpstr = f.Name.Replace(".txt","");
			long.TryParse(tmpstr,out tmplng);
			if (tmplng > endperiod) break;
			if (tmplng >= startperiod)
			{
				tmplng = tmplng / 1000;
				DateTime display = new DateTime(1970, 1, 1,0,0,0,0); //.ToLocalTime();
				display = display.AddSeconds(tmplng);
				LoadJsonForStatAndAdd(f.FullName,tmpstr,column);
				countthem++;
				//				Debug.Log (display.ToLongDateString() + " "+ display.ToLongTimeString());
			}
			//			Debug.Log ("FILE: " + "[" + f.Name +"]"+ f.FullName);
		}
	}








/// <summary>
/// Gets the name of the machine.
/// </summary>
/// <returns>The machine name.</returns>
/// <param name="id">Identifier.</param>
	public static string GetMachineName(int id)
	{
		string [] fatheritems = new string[100];
		
		int nritems = 0;
		int nrcurves = bufferme.GetInt("Curve_displays");
		for (int n=0;n<nrcurves;n++)
		{
			bool newitem = true;
			for (int nn=0;nn<nritems;nn++)
			{
				if (fatheritems[nn] == bufferme.GetString("Curve_displays_father"+n))
				{
					newitem = false;
				}
			}
			if (newitem)
			{
				fatheritems[nritems] = bufferme.GetString("Curve_displays_father"+n);
				nritems++;
			}
		}
		if (id >= nritems)		return "";
		return fatheritems[id];
	}

/// <summary>
/// Gets the nr of machines.
/// </summary>
/// <returns>The nr of machines.</returns>
	public static int GetNrOfMachines()
	{
		string [] fatheritems = new string[100];
		int nritems = 0;
		int nrcurves = bufferme.GetInt("Curve_displays");

		for (int n=0;n<nrcurves;n++)
		{
			bool newitem = true;
			for (int nn=0;nn<nritems;nn++)
			{
				if (fatheritems[nn] == bufferme.GetString("Curve_displays_father"+n))
				{
					newitem = false;
				}
			}
			if (newitem)
			{
				fatheritems[nritems] = bufferme.GetString("Curve_displays_father"+n);
				nritems++;
			}
		}
		return nritems;
	}



///////////////////////// New stuff
	TimeSpan startoftheday;

	public static void InputValueChanged(int slide1h = -1,int slide1m = -1)
	{
		if (!bufferme.HasKey("DailyReset"))
		{
			bufferme.SetInt("DailyReset",1);
			if (slide1h == -1)
				slide1h = DateTime.Now.Hour;
			if (slide1m == -1)
				slide1m = DateTime.Now.Minute;
			bufferme.SetInt("slide1m",slide1m);
		}
	}

	public static bool IsInputValueChanged()
	{
		return (bufferme.HasKey("DailyReset"));
	}

	IEnumerator NewGenerateData_Server()
	{
// check if we wrap a day
		int today = DateTime.Now.DayOfYear;
		if (bufferme.GetInt("DateTime.Now.DayOfYear") != today)
		{
			bufferme.SetInt("DateTime.Now.DayOfYear",today);
			bufferme.DeleteKey("DailyReset");
		}

		while (true)
		{
			yield return new WaitForSeconds (datacollectorperiod);
// send data
			CalculateTheFormulas ();	
			Send_BasicSample(DateTime.Now);
			StoreAndCalcLastHour();
		}
	}		


	public static void Send_BasicSample(DateTime dt)
	{
		long timedate = (long)GetMilliseconds(dt);

		string json2 = "{ ";
		bool bQualityOneOK = false;
		int nrfields;
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n, nn;
			for (nn = 0,n = 0; n < nrfields; n++)
			{
				string datanameid = bufferme.GetString("DataField_" + n);
				if (!bufferme.HasKey(datanameid + "_form"))
				{
					bool bQuality = bufferme.GetBool(datanameid + "_quality");

					if (bQuality)
					{
						if (nn != 0)
							json2 = json2 + ",";
						json2 = json2 + "\"" + datanameid + "\" : \"" + bufferme.GetFloat(datanameid + "_value") + "\"";
						nn++;

						if ( !datanameid.Contains("TempsEnMarche") ) // Si il a a autre chose que TempsEnMarche on permet la sauvegarde du json
							bQualityOneOK = true;
					}
				}
			}
		}
		else
			Debug.Log("ERROR NO CLIENT DATA");
		json2 = json2 + " }";

		if (lastsavedjson_force)
			lastsavedjsontextvalue = json2;
		else
			lastsavedjsontextvalue = "";


		if (bQualityOneOK)
		{
//			string json = "{ ";
//			json = json + "\"" + timedate + "\" : { " + json2 + " }";
//			Debug.Log (json);
			bufferme.AddToBuffer(json2, timedate.ToString(), "");
		}
	}

	static string alphabeth = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	static void CalculateTheFormulas(bool lasthour = false)
	{
		int nrfields;
		if (bufferme.HasKey("DataFields"))
		{
			nrfields = bufferme.GetInt("DataFields");
			int n;
			for (n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				if (bufferme.HasKey(datanameid+"_form"))
				{
					string formula = bufferme.GetString(datanameid+"_form");
					int nn;
					bool bQuality = true;

					for (nn=0;nn<nrfields;nn++)
					{
						string datanameid2 = bufferme.GetString("DataField_"+nn);
						//						Debug.Log (nn + ")" + datanameid2);

						float val;
						if (lasthour)
							val = averagebuffer_lasthour[nn];
						else
						{
							val = bufferme.GetFloat(datanameid2 + "_value");
							if (Application.loadedLevelName == "getdatafromfiles")
							{
								if (datanameid.Contains ("TempsEnMarche"))
								{
									val = (float)_elapsedtime.TotalSeconds;
								}
							}
							if( formula.IndexOf(datanameid2) >= 0 )
								bQuality &= bufferme.GetBool(datanameid2 + "_quality");
						}
						formula = ReplaceWithProgramValues(formula,datanameid2,val);
					}
					formula = ReplaceWithProgramValues(formula,"Time",1);
					// avoid /0 issues :
					//					if (datanameid == "Robot_CadenceReelle")
					//						Debug.Log ("Robot_CadenceReelle:" + formula);
					// remove nonexisting things here
					for (int scan = 0;scan < 26*2; scan ++)
					{
						char letter = alphabeth[scan];
						formula = formula.Replace(letter.ToString(),"0");
					}
					formula = formula.Replace("_","0");

					//					formula = formula.Replace("/0","/1");		// you are not serious here...
					//					Debug.Log (datanameid + " " + formula);
					MathFunctions.MathParser mp = new MathFunctions.MathParser ();

					decimal result = mp.Calculate (formula);
					if (lasthour)
						averagebuffer_lasthour[n] = (float)result;
					else
					{
						bufferme.SetFloat(datanameid + "_value", (float)result);
						bufferme.SetBool(datanameid + "_quality", bQuality );
					}
				}
			}
		}
	}

}
