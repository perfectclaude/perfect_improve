﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_admin_edit_schedule : MonoBehaviour
{
	static int MaxDataFields = 7;
	GameObject toclone;
	GameObject [] myobject = new GameObject[7];
	GameObject [] slide1 = new GameObject[7];
	GameObject [] slide2 = new GameObject[7];
	GameObject [] text1 = new GameObject[7];
	GameObject [] text2 = new GameObject[7];
	GameObject [] connector = new GameObject[7];
	GameObject [] connectortop = new GameObject[7];
	GameObject [] connectorbottom = new GameObject[7];

	public Camera 		m_ViewCamera;
	public static bool wasIpressed = false;
	Vector3 dragstartpos;
	GameObject draggingball = null;
	int draggingid = 0;
	
	void Awake()
	{
//		Debug.Log ("ni_admin_edit_schedule awake");
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		toclone = gameObject.FindInChildren ("toclone");
		toclone.SetActive (false);
//		Debug.Log ("ni_admin_edit_schedule awake end");
	}
	
	void OnDisable ()
	{
		StopAllCoroutines ();
	}

	public static void CreateInitialSchedule()
	{
		string datanameid = "monday";
		if (!bufferme.HasKey(datanameid+"_slide1pos"))
		{
// Generate first schedule values
			for (int i=0;i<7;i++)
			{
				switch(i)
				{
				case 0 :	datanameid = "monday";		break;
				case 1 :	datanameid = "tuesday";		break;
				case 2 :	datanameid = "wednesday";		break;
				case 3 :	datanameid = "thursday";		break;
				case 4 :	datanameid = "friday";		break;
				case 5 :	datanameid = "saturday";		break;
				case 6 :	datanameid = "sunday";		break;
				}
				// Slider page
				if ((i == 5) || (i == 6))
				{
					bufferme.SetFloat(datanameid+"_slide1pos",-1.26f);
					bufferme.SetString(datanameid+"_slide1","24h00");
					bufferme.SetFloat(datanameid+"_slide2pos",-1.26f);
					bufferme.SetString(datanameid+"_slide2","24h00");
				}
				else
				{
					if (i == 4)
					{
						bufferme.SetFloat(datanameid+"_slide1pos",-0.4120521f);
						bufferme.SetString(datanameid+"_slide1","08h00");
						bufferme.SetFloat(datanameid+"_slide2pos",-0.6293062f);
						bufferme.SetString(datanameid+"_slide2","12h00");
					}
					else
					{
						bufferme.SetFloat(datanameid+"_slide1pos",-0.4120521f);
						bufferme.SetString(datanameid+"_slide1","08h00");
						bufferme.SetFloat(datanameid+"_slide2pos",-0.8704552f);
						bufferme.SetString(datanameid+"_slide2","16h45");
					}
				}
			}
		}
	}

	void Update()
	{
		int n;
		Vector3 vec;
		int executedrag = -1;

		if (Input.GetMouseButtonUp (0))
		{
			if (draggingball != null)
			{
				string datanameid = "";
				switch(draggingid)
				{
				case 0 :	datanameid = "monday";		break;
				case 1 :	datanameid = "tuesday";		break;
				case 2 :	datanameid = "wednesday";		break;
				case 3 :	datanameid = "thursday";		break;
				case 4 :	datanameid = "friday";		break;
				case 5 :	datanameid = "saturday";		break;
				case 6 :	datanameid = "sunday";		break;
				}

//				Debug.Log ("slide1pos:"+slide1[draggingid].transform.localPosition.y);
//				Debug.Log ("slide2pos:"+slide2[draggingid].transform.localPosition.y);

				// Slider page
				bufferme.SetFloat(datanameid+"_slide1pos",slide1[draggingid].transform.localPosition.y);
				GameObject obj = slide1[draggingid].FindInChildren("value");
				tk2dTextMesh tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
//				Debug.Log ("slide1:"+tm.text);
				bufferme.SetString(datanameid+"_slide1",tm.text);
					
				bufferme.SetFloat(datanameid+"_slide2pos",slide2[draggingid].transform.localPosition.y);
				obj = slide2[draggingid].FindInChildren("value");
				tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
//				Debug.Log ("slide2:"+tm.text);
				bufferme.SetString(datanameid+"_slide2",tm.text);

				tk2dSprite sprite = draggingball.GetComponent<tk2dSprite>();
				sprite.color = Color.white;
				SetSliderValue(draggingball,true);
				executedrag = draggingid;
			}
			draggingball = null;
		}
		
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
			for (n=0;n<MaxDataFields;n++)
			{
				if (slide1[n].GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
				{
					dragstartpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
					draggingball = slide1[n];
					tk2dSprite sprite = draggingball.GetComponent<tk2dSprite>();
					sprite.color = Color.grey;
					draggingid = n;
					break;
					
				}
				if (slide2[n].GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
				{
					dragstartpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
					draggingball = slide2[n];
					tk2dSprite sprite = draggingball.GetComponent<tk2dSprite>();
					sprite.color = Color.grey;
					draggingid = n;
					break;
				}
			}
		}
		if (draggingball != null)
		{
			// Drag the BG here
			vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - dragstartpos.y;
			dragstartpos = vec;
			vec = draggingball.transform.position;
			vec.y += diffy;
			draggingball.transform.position = vec;
			vec = draggingball.transform.localPosition;

			if (vec.y > 0.0f)		vec.y = 0.0f;
			if (vec.y < -1.26f)		vec.y = -1.26f;
			draggingball.transform.localPosition = vec;
			SetSliderValue(draggingball);

			executedrag = draggingid;
		}

		if (executedrag != -1)
		{
			n = executedrag;

			vec = connector[n].transform.localPosition;
			vec.y = (slide1[n].transform.localPosition.y + slide2[n].transform.localPosition.y) / 2;
			connector[n].transform.localPosition = vec;
			float	distance = Mathf.Abs(slide1[n].transform.localPosition.y - slide2[n].transform.localPosition.y);
			distance = distance / 0.0415f;
			vec = connector[n].transform.localScale;
			vec.y = distance;
			connector[n].transform.localScale = vec;
			
			vec = connectortop[n].transform.localPosition;
			if (slide1[n].transform.localPosition.y < slide2[n].transform.localPosition.y)
			{
				vec.y = (0 + slide2[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide2[n].transform.localPosition.y);
			}
			else
			{
				vec.y = (0 + slide1[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide1[n].transform.localPosition.y);
			}
			connectortop[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectortop[n].transform.localScale;
			vec.y = distance;
			connectortop[n].transform.localScale = vec;
			
			vec = connectorbottom[n].transform.localPosition;
			if (slide1[n].transform.localPosition.y < slide2[n].transform.localPosition.y)
			{
				vec.y = (-1.26f + slide1[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide1[n].transform.localPosition.y+1.26f);
			}
			else
			{
				vec.y = (-1.26f + slide2[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide2[n].transform.localPosition.y+1.26f);
			}
			connectorbottom[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectorbottom[n].transform.localScale;
			vec.y = distance;
			connectorbottom[n].transform.localScale = vec;
		}

		for (n=0;n<MaxDataFields;n++)
		{
			if (slide1[n].transform.localPosition.y < slide2[n].transform.localPosition.y)
			{
				vec = text1[n].transform.localPosition;
				vec.y = 0.035f;
				text1[n].transform.localPosition = vec;
				vec = text2[n].transform.localPosition;
				vec.y = -0.035f;
				text2[n].transform.localPosition = vec;
			}
			else
			{
				vec = text1[n].transform.localPosition;
				vec.y = -0.035f;
				text1[n].transform.localPosition = vec;
				vec = text2[n].transform.localPosition;
				vec.y = 0.035f;
				text2[n].transform.localPosition = vec;
			}
		}

	}
	
	public void ReSetDisplay()
	{
		Vector3 vec;
		float ypos = 0.0f;
		float shifter = 0.0f;

		switch(DateTime.Now.DayOfWeek)
		{
		case DayOfWeek.Monday :		 shifter =0.0f;	break;
		case DayOfWeek.Tuesday :	 shifter =0.35f;		break;
		case DayOfWeek.Wednesday :	 shifter =0.7f;		break;
		case DayOfWeek.Thursday :	 shifter =1.05f;		break;
		case DayOfWeek.Friday :		 shifter =1.4f;	break;
		case DayOfWeek.Saturday :	 shifter =1.75f;		break;
		case DayOfWeek.Sunday :		 shifter =2.1f;	break;
		}

		if (bufferme.HasKey("Curve_displays"))
		{
			for (int n=0;n<MaxDataFields;n++)
			{
				DateTime testdatetime = DateTime.Now.AddDays(n);

				// depending the startingdate, recalculate ypos !!!				
				ypos = shifter - n*0.35f;
				while (ypos > 0.0f)			ypos -= 2.45f;
				while (ypos <= -2.45f)		ypos += 2.45f;

				string datanameid = "";
				switch(n)
				{
				case 0 :	datanameid = "monday";		break;
				case 1 :	datanameid = "tuesday";		break;
				case 2 :	datanameid = "wednesday";		break;
				case 3 :	datanameid = "thursday";		break;
				case 4 :	datanameid = "friday";		break;
				case 5 :	datanameid = "saturday";		break;
				case 6 :	datanameid = "sunday";		break;
				}

				GameObject obj = gameObject.FindInChildren(datanameid);
				if (obj == null)
				{
					obj = (GameObject)GameObject.Instantiate(toclone);
					obj.name = datanameid;
					obj.transform.parent = gameObject.transform;
				}
				obj.SetActiveRecursively(true);
				myobject[n] = obj;
				
				vec = new Vector3(-ypos,0,0);
				obj.transform.localPosition = vec;
				
				tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("name").GetComponent<tk2dTextMesh>();
				string showname = datanameid + " " + testdatetime.Day;
				settext.ForceText(tm,showname);
				
				slide1[n] = myobject[n].FindInChildren("slideball1");
				text1[n] = slide1[n].FindInChildren("value");
				slide2[n] = myobject[n].FindInChildren("slideball2");
				text2[n] = slide2[n].FindInChildren("value");
				connector[n] = myobject[n].FindInChildren("connector");
				connectortop[n] = myobject[n].FindInChildren("connectortop");
				connectorbottom[n] = myobject[n].FindInChildren("connectorbottom");


				{
					// Slider page
					if (bufferme.HasKey(datanameid+"_slide1pos"))
					{
						vec = slide1[n].transform.localPosition;
						vec.y = bufferme.GetFloat(datanameid+"_slide1pos");
						if (vec.y > 0.0f)		vec.y = 0.0f;
						if (vec.y < -1.26f)		vec.y = -1.26f;
						slide1[n].transform.localPosition = vec;
					}
					if (bufferme.HasKey(datanameid+"_slide2pos"))
					{
						vec = slide2[n].transform.localPosition;
						vec.y = bufferme.GetFloat(datanameid+"_slide2pos");
						if (vec.y > 0.0f)		vec.y = 0.0f;
						if (vec.y < -1.26f)		vec.y = -1.26f;
						slide2[n].transform.localPosition = vec;
					}
					SetSliderValue(slide1[n],true);
					SetSliderValue(slide2[n],true);
				}
			}
		}

		for (int n=0;n<MaxDataFields;n++)
		{
			vec = connector[n].transform.localPosition;
			vec.y = (slide1[n].transform.localPosition.y + slide2[n].transform.localPosition.y) / 2;
			connector[n].transform.localPosition = vec;
			float	distance = Mathf.Abs(slide1[n].transform.localPosition.y - slide2[n].transform.localPosition.y);
			distance = distance / 0.0415f;
			vec = connector[n].transform.localScale;
			vec.y = distance;
			connector[n].transform.localScale = vec;
			
			vec = connectortop[n].transform.localPosition;
			if (slide1[n].transform.localPosition.y < slide2[n].transform.localPosition.y)
			{
				vec.y = (0 + slide2[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide2[n].transform.localPosition.y);
			}
			else
			{
				vec.y = (0 + slide1[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide1[n].transform.localPosition.y);
			}
			connectortop[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectortop[n].transform.localScale;
			vec.y = distance;
			connectortop[n].transform.localScale = vec;
			
			vec = connectorbottom[n].transform.localPosition;
			if (slide1[n].transform.localPosition.y < slide2[n].transform.localPosition.y)
			{
				vec.y = (-1.26f + slide1[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide1[n].transform.localPosition.y+1.26f);
			}
			else
			{
				vec.y = (-1.26f + slide2[n].transform.localPosition.y) / 2;
				distance = Mathf.Abs(slide2[n].transform.localPosition.y+1.26f);
			}
			connectorbottom[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectorbottom[n].transform.localScale;
			vec.y = distance;
			connectorbottom[n].transform.localScale = vec;
		}

		toclone.SetActive (false);		
	}
	
	void OnEnable ()
	{
		ReSetDisplay ();
	}
	
	void SetSliderValue(GameObject slider,bool fixme = false)
	{
		GameObject father = slider.transform.parent.gameObject;
		float fmin = 0;
		float fmax = 24;
		float mypos = -slider.transform.localPosition.y;

		float val = fmin + mypos * (fmax - fmin) / 1.26f;
// add quarter here
		val += 0.24f;
		val = val * 4.0f;
		int ival = (int) val;
		val = (float)ival;
		val = val / 4.0f;

		ival = (int)val;
		float intermediate = val - ival;
		intermediate *= 60.0f;
		int mval = (int)intermediate;

		float newval = (val - fmin)*1.26f / (fmax - fmin);

		Vector3 tmppos = slider.transform.localPosition;
		if (fixme)
		{
			tmppos.y = - newval;
			slider.transform.localPosition = tmppos;
		}

		tk2dTextMesh tm = (tk2dTextMesh)slider.FindInChildren ("value").GetComponent<tk2dTextMesh> ();

		settext.ForceText (tm, ival.ToString("D2")+"h"+mval.ToString("D2"));

	}
}
