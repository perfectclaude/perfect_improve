﻿using UnityEngine;
using System.Collections;

public class ni_showdatafields : MonoBehaviour
{
	public GameObject []		module_3 = new GameObject[3];

	public GameObject []		module_2 = new GameObject[2];
	public tk2dTextMesh 	subtitle;

	public tk2dSprite		topborder;
	public GameObject		topicon;
	public GameObject		topinformation;

	public GameObject []		icon_3 = new GameObject[3];
	public GameObject []		info_3 = new GameObject[3];
//	public GameObject []		info_2 = new GameObject[2];

	public tk2dSlicedSprite	[]	topmodule_3 = new tk2dSlicedSprite[3];
	public tk2dSlicedSprite	[]	topmodule_2 = new tk2dSlicedSprite[3];

	public tk2dTextMesh	[,] item_info_3 = new tk2dTextMesh[3,3];

	public GameObject	[,] module_2_bigblock = new GameObject[3,2];
	public GameObject	[,] module_2_smallblock = new GameObject[3,2];

	public 	Texture2D mytexture;

	void Awake()
	{
		_Awake ();
	}

	void OnEnable()
	{
		_OnEnable ();
	}

	public void _Awake ()
	{
		subtitle = (tk2dTextMesh)gameObject.FindInChildren ("subtitle").GetComponent<tk2dTextMesh>();

		mytexture = new Texture2D(1,1,TextureFormat.RGB24,false);

		GameObject obj = gameObject.FindInChildren("whitebg");
		topicon = gameObject.FindInChildren ("whitebg").FindInChildren("icon");
		topinformation = gameObject.FindInChildren("information");

		topborder = (tk2dSprite)obj.FindInChildren("topborder").GetComponent<tk2dSprite>();

		for (int i=0;i<3;i++)
		{
			module_3[i] = gameObject.FindInChildren ("module_"+(i+1)+"_3");
			icon_3[i] = module_3[i].FindInChildren ("background").FindInChildren ("alert_icon");
			info_3[i] = module_3[i].FindInChildren ("background").FindInChildren ("information");
			topmodule_3[i] = (tk2dSlicedSprite)module_3[i].FindInChildren("ontop").GetComponent<tk2dSlicedSprite>();

			for (int n=0;n<3;n++)
			{
				item_info_3[i,n] = (tk2dTextMesh)module_3[i].FindInChildren("item_info_"+(n+1)).GetComponent<tk2dTextMesh>();
			}
		}

		for (int i=0;i<2;i++)
		{
			module_2[i] = gameObject.FindInChildren ("module_"+(i+1)+"_2");
//			info_2[i] = module_2[i].FindInChildren ("background").FindInChildren ("information");
			topmodule_2[i] = (tk2dSlicedSprite)module_2[i].FindInChildren("ontop").GetComponent<tk2dSlicedSprite>();
		}

		for (int i=0;i<3;i++)
		{
			for (int n=0;n<2;n++)
			{
				module_2_bigblock[i,n] = module_2[n].FindInChildren("module_item"+(i+1)).FindInChildren("bigblock");
				module_2_smallblock[i,n] = module_2[n].FindInChildren("module_item"+(i+1)).FindInChildren("smallblock");
			}
		}
		_ModuleAlert(false,false,false);
	}
	
	public void _ModuleSetText_3(int module, string one, string two, string three)
	{
		settext.ForceText(item_info_3[module,0],one);
		settext.ForceText(item_info_3[module,1],two);
		settext.ForceText(item_info_3[module,2],three);
	}

	public void _ModuleAlertSetText_2(string one, string two)
	{
		tk2dTextMesh tm;
		
//		tm = (tk2dTextMesh)info_2[0].GetComponent<tk2dTextMesh>();
//		settext.ForceText(tm,one);
//		tm = (tk2dTextMesh)info_2[1].GetComponent<tk2dTextMesh>();
//		settext.ForceText(tm,two);

		tm = (tk2dTextMesh)topinformation.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,one+two);
	}

	public void _ModuleAlertSetText_3(string one, string two, string three)
	{
		tk2dTextMesh tm;

		tm = (tk2dTextMesh)info_3[0].GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,one);
		tm = (tk2dTextMesh)info_3[1].GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,two);
		tm = (tk2dTextMesh)info_3[2].GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,three);

		tm = (tk2dTextMesh)topinformation.GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,one+two+three);
	}
		


	public void _ModuleSetRows_2(bool el00,bool el01,bool el02,bool el10,bool el11,bool el12)
	{
		if (el00)
			module_2_bigblock[0,0].transform.parent.gameObject.SetActive(true);
		else
			module_2_bigblock[0,0].transform.parent.gameObject.SetActive(false);
		if (el01)
			module_2_bigblock[1,0].transform.parent.gameObject.SetActive(true);
		else
			module_2_bigblock[1,0].transform.parent.gameObject.SetActive(false);
		if (el02)
			module_2_bigblock[2,0].transform.parent.gameObject.SetActive(true);
		else
			module_2_bigblock[2,0].transform.parent.gameObject.SetActive(false);

		if (el10)
			module_2_bigblock[0,1].transform.parent.gameObject.SetActive(true);
		else
			module_2_bigblock[0,1].transform.parent.gameObject.SetActive(false);
		if (el11)
			module_2_bigblock[1,1].transform.parent.gameObject.SetActive(true);
		else
			module_2_bigblock[1,1].transform.parent.gameObject.SetActive(false);
		if (el12)
			module_2_bigblock[2,1].transform.parent.gameObject.SetActive(true);
		else
			module_2_bigblock[2,1].transform.parent.gameObject.SetActive(false);
	}


	public void _ModuleSetValue(int column,int row,string text,bool size,Color col)
	{
		tk2dTextMesh tm = null;
		tk2dSprite	sprt = null;
		if (!module_2_bigblock[row,column].transform.parent.gameObject.active)	return;
		Color textcolor = Color.black;

		if (size)
		{
			col = Color.green;
			textcolor = Color.black;
			module_2_bigblock[row,column].SetActive(true);
			module_2_smallblock[row,column].SetActive(false);
			sprt = (tk2dSprite)module_2_bigblock[row,column].GetComponent<tk2dSprite>();
			tm = (tk2dTextMesh)module_2_bigblock[row,column].FindInChildren("name").GetComponent<tk2dTextMesh>();
		}
		else
		{
			col = Color.red;
			textcolor = Color.white;
			module_2_bigblock[row,column].SetActive(false);
			module_2_smallblock[row,column].SetActive(true);
			tm = (tk2dTextMesh)module_2_smallblock[row,column].FindInChildren("name").GetComponent<tk2dTextMesh>();
			sprt = (tk2dSprite)module_2_smallblock[row,column].GetComponent<tk2dSprite>();
		}
		tm.gameObject.transform.localScale = new Vector3(1.4f,1.4f,1.4f);
		settext.ForceText(tm,text);
		sprt.color = col;
		tm.color = textcolor;
	}

	public void _ModuleAlert(bool alert1,bool alert2,bool alert3)
	{
		tk2dSprite sprt;

		if (alert1 || alert2 || alert3)
		{
			topicon.SetActive(true);
			topinformation.SetActive(true);
			topborder.color = Color.red;
		}
		else
		{
			topicon.SetActive(false);
			topinformation.SetActive(false);
			topborder.color = Color.green;
		}

		icon_3[0].SetActive(true);
		sprt = (tk2dSprite)icon_3[0].GetComponent<tk2dSprite>();
		if (alert1)
		{
			topmodule_3[0].color = Color.red;
			topmodule_2[0].color = Color.red;
			sprt.SetSprite(sprt.GetSpriteIdByName("icon_exclamation"));
			info_3[0].SetActive(true);
//			info_2[0].SetActive(true);
		}
		else
		{
			topmodule_3[0].color = Color.green;
			topmodule_2[0].color = Color.green;
			sprt.SetSprite(sprt.GetSpriteIdByName("good"));
			info_3[0].SetActive(false);
//			info_2[0].SetActive(false);
		}

		icon_3[1].SetActive(true);
		sprt = (tk2dSprite)icon_3[1].GetComponent<tk2dSprite>();
		if (alert2)
		{
			topmodule_3[1].color = Color.red;
			topmodule_2[1].color = Color.red;
			sprt.SetSprite(sprt.GetSpriteIdByName("icon_exclamation"));
			info_3[1].SetActive(true);
//			info_2[1].SetActive(true);
		}
		else
		{
			topmodule_3[1].color = Color.green;
			topmodule_2[1].color = Color.green;
			sprt.SetSprite(sprt.GetSpriteIdByName("good"));
			info_3[1].SetActive(false);
//			info_2[1].SetActive(false);
		}

		icon_3[2].SetActive(true);
		sprt = (tk2dSprite)icon_3[2].GetComponent<tk2dSprite>();
		if (alert3)
		{
			topmodule_3[2].color = Color.red;
			sprt.SetSprite(sprt.GetSpriteIdByName("icon_exclamation"));
			info_3[2].SetActive(true);
		}
		else
		{
			topmodule_3[2].color = Color.green;
			sprt.SetSprite(sprt.GetSpriteIdByName("good"));
			info_3[2].SetActive(false);
		}
	}

	public void _OnEnable ()
	{
		module_3[0].SetActiveRecursively (false);
		module_3[1].SetActiveRecursively (false);
		module_3[2].SetActiveRecursively (false);

		module_2[0].SetActiveRecursively (false);
		module_2[1].SetActiveRecursively (false);
	}

	void Update ()
	{
        Debug.Log("OK");
	
	}
}
