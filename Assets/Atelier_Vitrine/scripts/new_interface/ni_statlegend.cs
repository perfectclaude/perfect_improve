﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

public class ni_statlegend : MonoBehaviour
{
	public static bool FAKESTATS = true;
	public static bool FORCEFAKESTATS = true;
	public static DateTime startdate;
	public static DateTime enddate;
	public static bool savexls = false;
//	public static string statlegendtitle = "title";

	public static long nrsubsamples = 0;
	public static float [,] samplevalues = null;
	public static string [] samplenames = null;
	public static float [] samplemax = null;
	public static float [] samplemin = null;
	public static int [] displaytype = new int[10];
	public static int drawstatorder = 0;
	public static bool savecsv = false;
	public static bool shownewstats = false;

	public Camera 		m_ViewCamera;


//	ni_dropdownmenu sampling = null;

	bool wasIpressed = false;
	Vector3 startpos;

	float ypos = 0.0f;
	float xpos = 0.0f;
	GameObject father;
	public static bool	savefake = false;
	int int_sampling = 0;

	static GameObject pages;

	GameObject curvecorner2;
	GameObject clone_h2;
	GameObject clone_v2;
	int 		nclones2;

	GameObject curvecorner;
	GameObject beacon;
	GameObject clone_h;
	GameObject clone_bar;
	GameObject clone_v;
	GameObject cloneimage;
	Texture2D curvetexture;
	long 		nclones;
	long 		nvalues;

	GameObject [] button_toggle = new GameObject[3];
	public static int button_toggle_index = 0;
	public static ni_getsetdatetimefromdropdowns starttime;
	public static ni_getsetdatetimefromdropdowns endtime;

	bool firstset = true;

	void Awake()
	{
		network.ReadSettings();

		button_toggle[0] = gameObject.FindInChildren("periodselector").FindInChildren("button_settoday");
		button_toggle[1] = gameObject.FindInChildren("periodselector").FindInChildren("button_week");
		button_toggle[2] = gameObject.FindInChildren("periodselector").FindInChildren("button_year");

		starttime = (ni_getsetdatetimefromdropdowns)gameObject.FindInChildren("starttime").GetComponent<ni_getsetdatetimefromdropdowns>();
		endtime = (ni_getsetdatetimefromdropdowns)gameObject.FindInChildren("endtime").GetComponent<ni_getsetdatetimefromdropdowns>();

		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

//		sampling = (ni_dropdownmenu)gameObject.FindInChildren("button_sampling").GetComponent<ni_dropdownmenu>();

		pages = gameObject.FindInChildren("pages");

		GameObject root = gameObject.FindInChildren("page");
		curvecorner = root.FindInChildren("curvecorner");
		beacon = root.FindInChildren("beacon");
		clone_h = root.FindInChildren("clone_sample_h0");
		clone_bar = clone_h.FindInChildren("bar");
		clone_v = root.FindInChildren("clone_sample_v0");

		root = gameObject.FindInChildren("page2");
		curvecorner2 = root.FindInChildren("curvecorner");
		clone_h2 = root.FindInChildren("clone_sample_h0");
		clone_v2 = root.FindInChildren("clone_sample_v0");

		cloneimage = gameObject.FindInChildren("textureorig");
		curvetexture = (Texture2D)cloneimage.GetComponent<Renderer>().material.mainTexture;

	}

	void Update()
	{
		DisplayToggle();
		if (firstset)
		{
			firstset = false;
//			starttime.SetDateTime(DateTime.Now);
//			endtime.SetDateTime(DateTime.Now);
		}

		if (savecsv)
		{
			FAKESTATS = false;
			savecsv = false;
//			int_sampling = sampling.GetSelected();
			StartCoroutine ("LoadMyStatistics");
		}

		if (shownewstats)
		{
			shownewstats = false;
			CreateStatistics();
		}

		if (savefake)
		{
			savefake = false;
			StartCoroutine("CreateFakeData");		// Create and Save fake data samples !!!
		}

		if (Input.GetMouseButtonDown(0))
		{
			wasIpressed = true;
			startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
		}
		if (wasIpressed)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffx = vec.x - startpos.x;
			startpos = vec;
			/*
			vec = pages.transform.position;
			vec.x += diffx;

			if (vec.x < 0)				vec.x = 0;
			if (vec.x > 2.7224f)		vec.x = 2.7224f;

			pages.transform.position = vec;
			*/
		}
		
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
		}

	}

	void OnDisable()
	{
// kill all clones
		for (int i=1;i<nclones;i++)
		{
			GameObject obj = gameObject.FindInChildren("clone_sample_h"+i);
			Destroy(obj);
		}
		nclones = 1;
		for (int i=1;i<nclones2;i++)
		{
			GameObject obj = gameObject.FindInChildren("clone_sample_h"+i);
			Destroy(obj);
		}
		nclones2 = 1;

		for (int i=1;i<nvalues;i++)
		{
			GameObject obj = gameObject.FindInChildren("clone_sample_v"+i);
			Destroy(obj);
		}
		for (int i=1;i<nvalues;i++)
		{
			GameObject obj = gameObject.FindInChildren("clone_sample_v"+i);
			Destroy(obj);
		}
		nvalues = 1;

        ni_endlessturning.waiting.SetActiveRecursively (false);

	}


	void DisplayLegend(int val,string objname)
	{
		GameObject obj = father.FindInChildren (objname);
		if (val == 0)
		{
			obj.SetActive(false);
			return;
		}
		Vector3 vec = obj.transform.localPosition;
		vec.y = ypos;
		vec.x = xpos;
		xpos += 1.3f;
		if (xpos == 2.6f)
		{
			xpos = 0.0f;
			ypos = ypos - 0.05f;
		}
		obj.transform.localPosition = vec;
		string datanameid = bufferme.GetString("DataField_"+(val-1));
		TextMesh tm = (TextMesh)obj.FindInChildren("text").GetComponent<TextMesh> ();
		tm.text = datanameid;
	}

	void OnEnable()
	{
		DisplayToggle();
		firstset = true;

		ypos = 0.0f;

	}

	void CreateStatistics()
	{
// force int_sampling depending the space between samples
//		int_sampling 
		TimeSpan span = enddate - startdate;
		
		int_sampling = 4;		// per hour
		
		if (span.TotalDays > 2)			int_sampling = 8;		// per day
		if (span.TotalDays > 31)			int_sampling = 10;		// per month

		StartCoroutine ("LoadMyStatistics");
/*
		GameObject title = gameObject.FindInChildren("place_top_border");
		title = title.FindInChildren("title");
		TextMesh tm.text = (TextMesh)title.GetComponent<TextMesh>();
		tm=statlegendtitle;
		*/
	}

	void FillSampleNamesFULL(int sampl,DateTime dt,int nrofsamples)
	{
		for (int i=0;i<nrofsamples;i++)
		{
			switch (sampl)
			{
			default :	// hour
				samplenames[i] = dt.Hour.ToString() + "h "+dt.Day.ToString() + "/" + dt.Month.ToString() + "/" + dt.Year.ToString();
				dt = dt.AddHours(1);
				break;
			case 8 :		// day
				samplenames[i] = dt.Day.ToString() + "/" + dt.Month.ToString() + "/" + dt.Year.ToString();
				dt = dt.AddDays(1);
				break;
			case 10 :		//month
				samplenames[i] = dt.Month.ToString() + "/" + dt.Year.ToString();
				dt = dt.AddMonths(1);
				break;
			case 11 :		//year
				samplenames[i] = dt.Year.ToString();
				dt = dt.AddYears(1);
				break;
			}
		}
	}

	void FillSampleNames(int sampl,DateTime dt,int nrofsamples)
	{
		for (int i=0;i<nrofsamples;i++)
		{
			switch (sampl)
			{
			default :	// hour
				samplenames[i] = dt.Hour.ToString() + "h";
				dt = dt.AddHours(1);
				break;
			case 8 :		// day
				samplenames[i] = dt.Day.ToString() + "\n" + ni_popselectmenu.MonthName[dt.Month-1];
				dt = dt.AddDays(1);
				break;
			case 10 :		//month
				samplenames[i] = ni_popselectmenu.MonthName[dt.Month-1] + "\n" + dt.Year.ToString();
				dt = dt.AddMonths(1);
				break;
			case 11 :		//year
				samplenames[i] = dt.Year.ToString();
				dt = dt.AddYears(1);
				break;
			}
		}
	}

	string [] monthnames = {  "Jan", "Fev", "Mar", "Avr", "Mai", "Jun", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec" };
	void FillSampleNamesMonth(int startmonth,int count)
	{
		for (int i=0;i<count;i++)
		{
			samplenames[i] = monthnames[startmonth];
			startmonth++;
			if (startmonth == 12)		startmonth = 0;
		}
	}

	string [] daynames = {  "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim" };

	void FillSampleNamesWeek(int startday,int count)
	{
		for (int i=0;i<count;i++)
		{
			samplenames[i] = daynames[startday];
			startday++;
			if (startday == 7)		startday = 0;
		}
	}


	string [] hournames = {  "8h", "9h", "10h", "11h", "12h", "13h", "14h", "15h", "16h" };
	
	void FillSampleNamesHours(int starthour,int count)
	{
		for (int i=0;i<count;i++)
		{
			samplenames[i] = hournames[starthour];
			starthour++;
			if (starthour == 9)		starthour = 0;
		}
	}
	

    void ShowClones( bool bShow )
    {
        bool[] bFinOne = new bool[4];

        bFinOne[0] = false;
        bFinOne[1] = false;
        bFinOne[2] = false;
        bFinOne[3] = false;

        GameObject parentofkills = clone_h.transform.parent.gameObject;
        for (int nrsample=1;nrsample<100;nrsample++)
        {
            GameObject obj = parentofkills.FindInChildren("clone_sample_h"+nrsample);
            if (obj != null)
            {
                obj.SetVisible(bShow);
                bFinOne[0] = bShow;
            }
            obj = parentofkills.FindInChildren("clone_sample_v"+nrsample);
            if (obj != null)
            {
                obj.SetVisible(bShow);
                bFinOne[1] = bShow;
            }
        }

        parentofkills = clone_h2.transform.parent.gameObject;
        for (int nrsample=1;nrsample<100;nrsample++)
        {
            GameObject obj = parentofkills.FindInChildren("clone_sample_h"+nrsample);
            if (obj != null)
            {
                obj.SetVisible(bShow);
                bFinOne[2] = bShow;
            }
            obj = parentofkills.FindInChildren("clone_sample_v"+nrsample);
            if (obj != null)
            {
                obj.SetVisible(bShow);
                bFinOne[3] = bShow;
            }
        }

        clone_h.SetVisible(bFinOne[0]);
        clone_v.SetVisible(bFinOne[1]);
        clone_h2.SetVisible(bFinOne[2]);
        clone_v2.SetVisible(bFinOne[3]);
       
    }

	IEnumerator LoadMyStatistics()
	{
		TextMesh tm;

//		Debug.Log ("LoadMyStatistics +++++");
		ni_button_panelup.pageup = false;

//		GameObject obj = gameObject.FindInChildren ("place_button_return");
//		obj = obj.FindInChildren ("text");
//		tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh> ();
		string mytext;
		if (startdate == enddate)						yield break;
		if (!bufferme.HasKey ("DataFields"))						yield break;


        ShowClones(false);

		samplevalues = null;
		samplenames = null;
		samplemax = null;
		samplemin = null;

		displaytype[0] = 1;
		displaytype[1] = 2;
		displaytype[2] = 2;
		displaytype[3] = 100;
		displaytype[4] = 2;

		drawstatorder = 0;
//		Debug.Log ("LoadMyStatistics 1");

		ni_endlessturning.waiting.SetActiveRecursively (true);

		string datanameid = bufferme.GetString("Curve_displays_variable"+bufferme.GetInt("Curve_display_item"));
		string strobjectif = bufferme.GetString(datanameid+"_slide3");
		float valobjectif = 100.0f;
		float.TryParse(strobjectif, out valobjectif);

		// kill samples ...
		GameObject parentofkills = clone_h.transform.parent.gameObject;
		for (int nrsample=1;nrsample<100;nrsample++)
		{
			GameObject obj = parentofkills.FindInChildren("clone_sample_h"+nrsample);
			if (obj != null)
				Destroy(obj);
			obj = parentofkills.FindInChildren("clone_sample_v"+nrsample);
			if (obj != null)
				Destroy(obj);
		}
		parentofkills = clone_h2.transform.parent.gameObject;
		for (int nrsample=1;nrsample<100;nrsample++)
		{
			GameObject obj = parentofkills.FindInChildren("clone_sample_h"+nrsample);
			if (obj != null)
				Destroy(obj);
			obj = parentofkills.FindInChildren("clone_sample_v"+nrsample);
			if (obj != null)
				Destroy(obj);
		}
		{
			GameObject obj = clone_h.FindInChildren("bar");
			Vector3 scalevec = obj.transform.localScale;
			scalevec.y = 0.0f;
			obj.transform.localScale = scalevec;
			Vector3 posvec = new Vector3(0,-100,0);

			obj = clone_h.FindInChildren("curvetext0");
			obj.transform.localPosition=posvec;
			obj = clone_h.FindInChildren("curvetext1");
			obj.transform.localPosition=posvec;
			obj = clone_h.FindInChildren("curvetext2");
			obj.transform.localPosition=posvec;
			obj = clone_h.FindInChildren("curvetext3");
			obj.transform.localPosition=posvec;
			obj = clone_h.FindInChildren("curvetext4");
			obj.transform.localPosition=posvec;
		}
		//		Debug.Log ("LoadMyStatistics 2" + ni_statlegend.button_toggle_index);

		//		if (true)
//		if ((Application.loadedLevelName.Contains("DEMO")))
//		Debug.Log("----FORCEFAKESTATS--- "+FORCEFAKESTATS);
		if (FORCEFAKESTATS)
		{
//			Debug.Log ("LoadMyStatistics " + ni_statlegend.button_toggle_index);
			if (ni_statlegend.button_toggle_index == 2)		// year
			{
				float tempd = 12.0f;
				nrsubsamples = (long)tempd;
				samplevalues = new float[5,(int)tempd];
				samplenames = new string[(int)tempd];
				samplemax = new float[5];
				samplemin = new float[5];
				
				samplemin[0] = 70.0f;
				samplemin[1] = 70.0f;
				samplemin[2] = 70.0f;
				samplemin[3] = 70.0f;
				samplemin[4] = 70.0f;
				
				samplemax[0] = 105.0f;
				samplemax[1] = 105.0f;
				samplemax[2] = 105.0f;
				samplemax[3] = 105.0f;
				samplemax[4] = 105.0f;
				
				FillSampleNamesMonth(0,12);
				
				samplevalues[0,0] = 94.0f;
				samplevalues[0,1] = 96.0f;
				samplevalues[0,2] = 97.0f;
				samplevalues[0,3] = 85.0f;
				samplevalues[0,4] = 96.0f;
				samplevalues[0,5] = 97.0f;
				samplevalues[0,6] = -10000.0f;
				samplevalues[0,7] = -10000.0f;
				samplevalues[0,8] = -10000.0f;
				samplevalues[0,9] = -10000.0f;
				samplevalues[0,10] = -10000.0f;
				samplevalues[0,11] = -10000.0f;
				
				samplevalues[1,0] = 94.0f;
				samplevalues[1,1] = 95.0f;
				samplevalues[1,2] = 96.0f;
				samplevalues[1,3] = 93.3f;
				samplevalues[1,4] = 94.0f;
				samplevalues[1,5] = 94.7f;
				samplevalues[1,6] = -10000.0f;
				samplevalues[1,7] = -10000.0f;
				samplevalues[1,8] = -10000.0f;
				samplevalues[1,9] = -10000.0f;
				samplevalues[1,10] = -10000.0f;
				samplevalues[1,11] = -10000.0f;
				
				samplevalues[2,0] = 88.0f;
				samplevalues[2,1] = 90.0f;
				samplevalues[2,2] = 89.0f;
				samplevalues[2,3] = 91.0f;
				samplevalues[2,4] = 92.0f;
				samplevalues[2,5] = 90.0f;
				samplevalues[2,6] = 90.0f;
				samplevalues[2,7] = 89.0f;
				samplevalues[2,8] = 88.0f;
				samplevalues[2,9] = 88.0f;
				samplevalues[2,10] = 87.0f;
				samplevalues[2,11] = 85.0f;
				
				for (int loop=0;loop<12;loop++)
				{
					samplevalues[3,loop] = valobjectif;
				}
				
				samplevalues[4,0] = 94.0f;
				samplevalues[4,1] = 96.0f;
				samplevalues[4,2] = 97.0f;
				samplevalues[4,3] = 85.0f;
				samplevalues[4,4] = 96.0f;
				samplevalues[4,5] = 97.0f;
				samplevalues[4,6] = 94.0f;
				samplevalues[4,7] = 90.0f;
				samplevalues[4,8] = 90.0f;
				samplevalues[4,9] = 92.0f;
				samplevalues[4,10] = 78.0f;
				samplevalues[4,11] = 83.0f;
			}

			if (ni_statlegend.button_toggle_index == 1)		// semaine
			{
				float tempd = 7.0f;
				nrsubsamples = (long)tempd;
				samplevalues = new float[5,(int)tempd];
				samplenames = new string[(int)tempd];
				samplemax = new float[5];
				samplemin = new float[5];
				
				samplemin[0] = 70.0f;
				samplemin[1] = 70.0f;
				samplemin[2] = 70.0f;
				samplemin[3] = 70.0f;
				samplemin[4] = 70.0f;
				
				samplemax[0] = 100.0f;
				samplemax[1] = 100.0f;
				samplemax[2] = 100.0f;
				samplemax[3] = 100.0f;
				samplemax[4] = 100.0f;
				
				FillSampleNamesWeek(0,7);
				
				samplevalues[0,0] = 94.0f;
				samplevalues[0,1] = 96.0f;
				samplevalues[0,2] = 97.0f;
				samplevalues[0,3] = 92.0f;
				samplevalues[0,4] = 96.0f;
				samplevalues[0,5] = -10000.0f;
				samplevalues[0,6] = -10000.0f;

				samplevalues[1,0] = 94.0f;
				samplevalues[1,1] = 95.0f;
				samplevalues[1,2] = 96.0f;
				samplevalues[1,3] = 85.3f;
				samplevalues[1,4] = 94.0f;
				samplevalues[1,5] = -10000.0f;
				samplevalues[1,6] = -10000.0f;

				samplevalues[2,0] = 88.0f;
				samplevalues[2,1] = 90.0f;
				samplevalues[2,2] = 89.0f;
				samplevalues[2,3] = 91.0f;
				samplevalues[2,4] = 92.0f;
				samplevalues[2,5] = -10000.0f;
				samplevalues[2,6] = -10000.0f;

				for (int loop=0;loop<7;loop++)
				{
					samplevalues[3,loop] = valobjectif;
				}
				
				samplevalues[4,0] = 95.0f;
				samplevalues[4,1] = 93.0f;
				samplevalues[4,2] = 94.0f;
				samplevalues[4,3] = 90.0f;
				samplevalues[4,4] = 93.0f;
				samplevalues[4,5] = -10000.0f;
				samplevalues[4,6] = -10000.0f;
				

			}

			if (ni_statlegend.button_toggle_index == 0)		// day
			{
				float tempd = 9.0f;
				nrsubsamples = (long)tempd;
				samplevalues = new float[5,(int)tempd];
				samplenames = new string[(int)tempd];
				samplemax = new float[5];
				samplemin = new float[5];
				
				samplemin[0] = 70.0f;
				samplemin[1] = 70.0f;
				samplemin[2] = 70.0f;
				samplemin[3] = 70.0f;
				samplemin[4] = 70.0f;
				
				samplemax[0] = 100.0f;
				samplemax[1] = 100.0f;
				samplemax[2] = 100.0f;
				samplemax[3] = 100.0f;
				samplemax[4] = 100.0f;
				
				FillSampleNamesHours(0,9);
				
				samplevalues[0,0] = 94.0f;
				samplevalues[0,1] = 96.0f;
				samplevalues[0,2] = 97.0f;
				samplevalues[0,3] = 85.0f;
				samplevalues[0,4] = 96.0f;
				samplevalues[0,5] = 97.0f;
				samplevalues[0,6] = 94.0f;
				samplevalues[0,7] = 90.0f;
				samplevalues[0,8] = 85.0f;

				samplevalues[1,0] = 94.0f;
				samplevalues[1,1] = 95.0f;
				samplevalues[1,2] = 96.0f;
				samplevalues[1,3] = 93.3f;
				samplevalues[1,4] = 94.0f;
				samplevalues[1,5] = 94.7f;
				samplevalues[1,6] = 90.0f;
				samplevalues[1,7] = 84.0f;
				samplevalues[1,8] = 72.0f;

				samplevalues[2,0] = 88.0f;
				samplevalues[2,1] = 90.0f;
				samplevalues[2,2] = 89.0f;
				samplevalues[2,3] = 91.0f;
				samplevalues[2,4] = 92.0f;
				samplevalues[2,5] = 90.0f;
				samplevalues[2,6] = 85.0f;
				samplevalues[2,7] = 80.0f;
				samplevalues[2,8] = 73.0f;

				for (int loop=0;loop<9;loop++)
				{
					samplevalues[3,loop] = valobjectif;
				}

				samplevalues[4,0] = 95.0f;
				samplevalues[4,1] = 90.0f;
				samplevalues[4,2] = 83.0f;
				samplevalues[4,3] = 80.0f;
				samplevalues[4,4] = 81.0f;
				samplevalues[4,5] = 82.0f;
				samplevalues[4,6] = 84.0f;
				samplevalues[4,7] = 88.0f;
				samplevalues[4,8] = 82.0f;

			}

			if (savexls)
			{
				mytext = "Sheet saved";
				ni_preparedatafields.Savecsv ("");
				savexls = false;
				ni_endlessturning.waiting.SetActiveRecursively (false);
                ShowClones(true);
				yield break;
			}
		}
		else
		{
			// 4 = 1h
			// 5 = 3h
			// 6 = 6h
			// 7 = 12h
			// 8 = day
			// 9 = week
			// 10 = month
			// 11 = year
			if (!savexls)
			{
				if (int_sampling < 4)		int_sampling = 4;
				if ((int_sampling > 4) && (int_sampling < 10))	int_sampling = 8;		// week sample is day sample
				if ((int_sampling == 9))		int_sampling = 10;
			}
			else
			{
				switch(ni_toggle_statpanelperiod.selectedperiod)
				{
				default :
					break;
				case 0 :
					int_sampling = 4;
					break;
				case 1 :
					int_sampling = 8;
					break;
				case 2 :
					int_sampling = 10;
					break;
				}
//				Debug.Log ("Sampling: " + int_sampling);
				// 4, 8 or 10 ... check button here
			}
//			Debug.Log ("Trying stats from : "+startdate.ToShortDateString() + " "+startdate.ToShortTimeString() + " - "+enddate.ToShortDateString() + " "+enddate.ToShortTimeString());

			long stime = (long)network.GetMiliseconds(startdate);
			long etime = (long)network.GetMiliseconds(enddate);
			long periodadded = 0;
			long periodtest = etime - stime;
			long biggeststat = 0;

			string [] injsons = new string[3];
			long [] fullperiods = new long[3];
			/*
			while(!network.GetIntermediateJson(bufferme.GetClientId(),stime.ToString(),etime.ToString(),5000,true,"ni_statlegend"))
			{
				yield return new WaitForSeconds(0.2f);
			}
			Debug.Log ("CLAUDE JSON "+stime+"-"+etime+":"+":"+network.retreiveintermediatejson);
			*/
			//			stime = stime - periodtest*3;
//			etime = etime - periodtest*3;
			stime = stime + periodtest;
			etime = etime + periodtest;
			long fullperiod = -1;

			for (int getthree = 0;getthree < 3;getthree++)
			{
				for (int innertel = 0;innertel< 5;innertel++)
				{
					if (getthree == 0)		innertel = 5;

					fullperiod++;
					stime = stime - periodtest;
					etime = etime - periodtest;
					Debug.Log ("Start:" + startdate.ToShortDateString() + " " + startdate.ToShortTimeString() + " " + stime);
					Debug.Log ("End:" + enddate.ToShortDateString() + " " + enddate.ToShortTimeString() + " " + etime);
					switch (int_sampling)
					{
					default :	// hour
						Debug.Log ("Hour request start");
						while(!network.GetIntermediateJson("h"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),5000,true,"ni_statlegend"))
						{
							yield return new WaitForSeconds(0.2f);
						}
						Debug.Log ("Hour request end");
						periodadded = 0;
						break;
					case 8 :		// day
						while(!network.GetIntermediateJson("d"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,true,"ni_statlegend"))
						{
							yield return new WaitForSeconds(0.2f);
						}
						periodadded = 1;
						break;
					case 10 :		//month
						while(!network.GetIntermediateJson("m"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,true,"ni_statlegend"))
						{
							yield return new WaitForSeconds(0.2f);
						}
						periodadded = 2;
						break;
					case 11 :		//year
						while(!network.GetIntermediateJson("y"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,true,"ni_statlegend"))
						{
							yield return new WaitForSeconds(0.2f);
						}
						periodadded = 3;
						break;
					}
					while (network.waitingnetworkintermediate)
					{
//						Debug.Log ("Waiting "+ Time.time);
						yield return new WaitForSeconds(0.2f);
					}
					if (network.retreiveintermediatejson.Length > 50)	break;	// more than 50 characters
				}
				yield return new WaitForSeconds(1.0f);
				Debug.Log ("JSON "+stime+"-"+etime+":"+ getthree+":"+network.retreiveintermediatejson);
				fullperiods[getthree] = fullperiod;
				injsons[2-getthree] = network.retreiveintermediatejson;
//				Debug.Log ("DONE "+getthree+"/3");

			}
//			Debug.Log ("Period0:" +periodtest * fullperiods[0]);
//			Debug.Log ("Period1:" +periodtest * fullperiods[1]);
//			Debug.Log ("Period2:" +periodtest * fullperiods[2]);
//			Debug.Log ("START PATHJSON");
			yield return new WaitForSeconds(0.2f);

			biggeststat = network.PathJson(periodadded,injsons[0],injsons[1],injsons[2],periodtest * fullperiods[1],periodtest * fullperiods[2]);
			yield return new WaitForSeconds(0.2f);
//			Debug.Log ("END PATHJSON");
			Debug.Log ("OUT:"+network.retreiveintermediatejson);

			if (biggeststat < 0)
			{
				ni_endlessturning.waiting.SetActiveRecursively (false);
                ShowClones(true);
				yield break;
			}
//			Debug.Log ("Ready Json, now fill in curve values");
				yield return new WaitForSeconds(0.2f);


			yield return new WaitForSeconds(0.2f);
			//			Debug.Log ("Mixed:"+network.retreiveintermediatejson);
			if ((network.n_retreiveintermediatejson > 0) && (network.retreiveintermediatejson.Length > 0))
			{
				float tempd = biggeststat;
				nrsubsamples = (long)tempd;
				samplevalues = new float[5,(long)tempd];
				samplenames = new string[(long)tempd];
				samplemax = new float[5];
				samplemin = new float[5];

				for (long nn=0;nn<5;nn++)
				{
					for (long nm=0;nm<tempd;nm++)
					{
						samplevalues[nn,nm] = -10000.0f;
					}
					samplemax[nn] = -100000.0f;
					samplemin[nn] = 100000.0f;
				}

				for (long loop=0;loop<tempd;loop++)
				{
					samplevalues[3,loop] = valobjectif;
				}

				if (savexls)
					FillSampleNamesFULL(int_sampling,network.dt_retreiveintermediatejson,(int)tempd);
				else
					FillSampleNames(int_sampling,network.dt_retreiveintermediatejson,(int)tempd);

				ni_preparedatafields.PrepareDisplayStatistics(network.retreiveintermediatejson,biggeststat);

				if (samplemax[0] < valobjectif)		samplemax[0] = valobjectif;
				if (samplemin[0] > valobjectif)		samplemin[0] = valobjectif;

				if (samplemax[0] < samplemax[2])		samplemax[0] = samplemax[2];
				if (samplemin[0] > samplemin[2])		samplemin[0] = samplemin[2];

				if (samplemax[0] < samplemax[4])		samplemax[0] = samplemax[4];
				if (samplemin[0] > samplemin[4])		samplemin[0] = samplemin[4];

				samplemax[0] = samplemax[0] * 1.05f;
				samplemin[0] = samplemin[0] * 0.95f;

				if (savexls)
				{
					Debug.Log ("Save xls " + nrsubsamples);
					yield return new WaitForSeconds(0.1f);
					ni_preparedatafields.Savecsv (network.retreiveintermediatejson);
					savexls = false;
					ni_button_panelup.pageup = false;
					ni_endlessturning.waiting.SetActiveRecursively (false);
                    ShowClones(true);
					yield break;
				}
			}
		}
//		Debug.Log ("LoadMyStatistics 5");

		Vector3 scalebar = clone_bar.transform.localScale;
		scalebar.x = 24.0f / nrsubsamples;
		clone_bar.transform.localScale = scalebar;

// Draw the curves here !!!
		float xpos = 0;
		float startvalue = samplemin [0];
		float tempvalue = samplemax [0] - startvalue;
		float stepvalue = 200.0f;

		float distance = beacon.transform.localPosition.x;
		distance = distance / nrsubsamples;
		float scalecalc;
		Vector3 [] LastCurvePos = new Vector3[4];

		Vector3 scaleitems = Vector3.one;
		if (nrsubsamples > 20)		scaleitems = new Vector3(0.7f,0.7f,0);
//		Debug.Log ("Nr of subsamples :" +nrsubsamples);
//		Debug.Log ("LoadMyStatistics 5.1  "+nrsubsamples);
		for (long i=0;i<nrsubsamples;i++)
		{
//			Debug.Log ("Value 0 index "+i +" = "+samplevalues[0,i]);

			GameObject clobj;
			if (i == 0)
				clobj = clone_h;
			else
			{
				clobj = GameObject.Instantiate(clone_h) as GameObject;
				clobj.transform.parent = clone_h.transform.parent;
			}

			clobj.name = "clone_sample_h"+i;
			Vector3 vec = clone_h.transform.localPosition;
			vec.x = (distance / 2.0f) + i * distance;
			clobj.transform.localPosition = vec;

			GameObject fathercloned = clobj;

			clobj = clobj.FindInChildren("text");
			tm = (TextMesh)clobj.GetComponent<TextMesh>();
			tm.text = samplenames[i];
			clobj.transform.localScale = scaleitems;

// Set Bar scales
			clobj = fathercloned.FindInChildren("bar");
			vec = clobj.transform.localScale;
			if (samplevalues[0,i] == -10000)
				scalecalc = 0;
			else
				scalecalc = (samplevalues[0,i] - samplemin [0]) / tempvalue;
			vec.y = scalecalc;
			clobj.transform.localScale = vec;

			vec = clobj.transform.localPosition;
			vec.y = 0.01f + (scalecalc * beacon.transform.localPosition.y)/2.0f;
			clobj.transform.localPosition = vec;

			ni_control_curvedisplay cloner = (ni_control_curvedisplay)fathercloned.GetComponent<ni_control_curvedisplay>();

			float mymaxvalue = samplevalues[1,i];
			float myminvalue = samplevalues[1,i];
			int minindex = 1;
			int maxindex = 1;
			int topindex = 1;
			int botindex = 1;
			float topadd = 0.0f;
			float botadd = 0.0f;
			//			int midmaxindex = 0;
//			int midminindex = 0;

			if (samplevalues[2,i] != -10000)
			{
				if (samplevalues[2,i] > mymaxvalue)
				{
					mymaxvalue = samplevalues[2,i];
					maxindex = 2;
				}
				if (samplevalues[2,i] < myminvalue)
				{
					myminvalue = samplevalues[2,i];
					minindex = 2;
				}
			}
			float middle = (myminvalue + mymaxvalue)/2;

			if (samplevalues[4,i] > samplevalues[3,i])
			{
				topindex = 4;
				botindex = 3;
			}
			else
			{
				topindex = 3;
				botindex = 4;
			}

			float lefttest = (samplevalues[topindex,i] - samplemin [0]) / tempvalue;
			float righttest = (samplevalues[maxindex,i] - samplemin [0]) / tempvalue;
			if ((samplevalues[topindex,i] != -10000) && (samplevalues[maxindex,i] != -10000))
			{
				if (lefttest < righttest +0.15f)
				{
					topadd = 0.15f;
				}
			}

			if ((samplevalues[botindex,i] != -10000) && (samplevalues[minindex,i] != -10000))
			{
//				Debug.Log ("Column:"+i+ "  left:"+lefttest+"   right:"+righttest);
				lefttest = (samplevalues[botindex,i] - samplemin [0]) / tempvalue;
				righttest = (samplevalues[minindex,i] - samplemin [0]) / tempvalue;
				if (lefttest > righttest -0.15f)
				{
					botadd = -0.22f;
				}
			}

//			Debug.Log ("LoadMyStatistics 5.2");

			for (int nn = 0;nn< 5; nn++)
			{
				cloner.myvalues[nn] = samplevalues[nn,i];
				if (samplevalues[nn,i] != -10000)
				{
					Color mycol;
					switch (nn)
					{
					default :
						mycol = new Color(30.0f/256.0f, 171.0f/256.0f,1.0f);
						break;
					case 1 :
						mycol = new Color(84.0f/256.0f, 191.0f/256.0f,1.0f);
						break;
					case 2 :
						mycol =  new Color(252.0f/256.0f, 54.0f/256.0f,1.0f);
						break;
					case 3 :
						mycol =  new Color(227.0f/256.0f, 255.0f/256.0f,53.0f/256.0f);
						break;
					case 4 :
						mycol =  new Color(255.0f/256.0f, 184.0f/256.0f,54.0f/256.0f);
						break;
					}
					scalecalc = (samplevalues[nn,i] - samplemin [0]) / tempvalue;
					if ((nn > 0) && (i != 0) && (samplevalues[nn,i-1] != -10000))
					{
						TraceLine(nn,fathercloned,mycol,new Vector3(LastCurvePos[nn-1].x-fathercloned.transform.position.x,
						                                             LastCurvePos[nn-1].y,0),
						          new Vector3(0,scalecalc * beacon.transform.localPosition.y,0));

					}
					if ((nn!=3)||(i==nrsubsamples-1))
					{
						clobj = fathercloned.FindInChildren("curvetext"+nn);
						clobj.SetActive(true);
						
						clobj.transform.localScale = scaleitems;
						
						tk2dSprite sprt = (tk2dSprite)clobj.FindInChildren("panel").GetComponent<tk2dSprite>();
						sprt.color = mycol;
						tm = (TextMesh)clobj.FindInChildren("intext").GetComponent<TextMesh>();
						tm.text = samplevalues[nn,i].ToString("0.00");
						vec = clobj.transform.localPosition;
						if (nn == 0)
							vec.y = 0.15f;
						else
						{
							float addme = -0.02f;
							if (nn == 1)
							{
								// Panel display -> must change here !!!
								GameObject paneltje = clobj.FindInChildren("panel");
								Vector3 vecscale = paneltje.transform.localScale;
								GameObject textadjust = clobj.FindInChildren("intext");
								
								if (nn == maxindex)
								{
									textadjust.transform.localPosition = new Vector3(textadjust.transform.localPosition.x,-0.009f,textadjust.transform.localPosition.z);
									addme = 0.13f;
									vecscale.y = -1;
								}
								else
								{
									textadjust.transform.localPosition = new Vector3(textadjust.transform.localPosition.x,-0.036f,textadjust.transform.localPosition.z);
									vecscale.y = 1;
								}
								paneltje.transform.localScale = vecscale;
							}

//							topindex = 4;
//							botindex = 3;

							if ((nn == 2) || (nn == 1) || (nn == 4)|| (nn == 3))
							{
								GameObject paneltje = clobj.FindInChildren("panel");
								Vector3 vecscale = paneltje.transform.localScale;
								GameObject textadjust = clobj.FindInChildren("intext");
								
								if ((nn == maxindex) || (nn == topindex))
								{
									textadjust.transform.localPosition = new Vector3(textadjust.transform.localPosition.x,-0.009f,textadjust.transform.localPosition.z);
									addme = 0.13f;
									if (nn == topindex)
										addme += topadd;
									vecscale.y = -1;
								}
								else
								{
									textadjust.transform.localPosition = new Vector3(textadjust.transform.localPosition.x,-0.036f,textadjust.transform.localPosition.z);
									if (nn == botindex)
										addme += botadd;
									vecscale.y = 1;
								}
								paneltje.transform.localScale = vecscale;
							}
							vec.y = scalecalc * beacon.transform.localPosition.y + addme;
						}
						vec.z = -0.4f + nn * 0.03f;
						clobj.transform.localPosition = vec;
					}
					if (nn > 0)
						LastCurvePos[nn-1] = new Vector3(fathercloned.transform.position.x,scalecalc * beacon.transform.localPosition.y,0);
				}
				else
				{
					clobj = fathercloned.FindInChildren("curvetext"+nn);
					clobj.SetActive(false);
				}
			}
		}
		nclones = nrsubsamples;
//		Debug.Log ("LoadMyStatistics 6");

		
		nclones2 = 4;
		distance = beacon.transform.localPosition.x * 0.8f;
		distance = distance / nclones2;

		float [] averages = new float[5];
		float [] naverages = new float[5];

		for (int nn = 0;nn<5;nn++)
		{
			int index;

			switch(nn)
			{
			default : index = 4;	break;
			case 1 : index = 2;		break;
			case 2 : index = 1;		break;
			case 3 : index = 3;		break;
			case 0 : index = 4;		break;
			}

			averages[nn] = 0;
			naverages[nn] = 0;

			for (int i=0;i<nrsubsamples;i++)
			{
				if (samplevalues[index,i] != -10000)
				{
					averages[nn] += samplevalues[index,i];
					naverages[nn] += 1.0f;
				}
			}

			if ((averages[nn] == 0) && (naverages[nn] == 0))
				averages[nn] = -10000;
			else
				averages[nn] = averages[nn] / naverages[nn];
		}

		for (int i=0;i<nclones2;i++)
		{
			GameObject clobj;
			if (i == 0)
				clobj = clone_h2;
			else
			{
				clobj = GameObject.Instantiate(clone_h2) as GameObject;
				clobj.SetActiveRecursively(true);
				clobj.transform.parent = clone_h2.transform.parent;
			}
			
			clobj.name = "clone_sample_h"+i;
			Vector3 vec = clone_h.transform.localPosition;
			vec.x = distance + i * distance;
			clobj.transform.localPosition = vec;
			tm = (TextMesh)clobj.FindInChildren("text").GetComponent<TextMesh>();

			GameObject fathercloned = clobj;
			Color mycol;

			switch (i)
			{
			case 2 :
				tm.text = "N";
				mycol = new Color(84.0f/256.0f, 191.0f/256.0f,1.0f);
				break;
			case 1 :
				tm.text ="N-1";
				mycol = new Color(252.0f/256.0f, 54.0f/256.0f,1.0f);
				break;
			case 3 :
				tm.text ="Obj";
				mycol = new Color(227.0f/256.0f, 255.0f/256.0f,53.0f/256.0f);
				break;
			default :
				tm.text ="N-2";
				mycol =  new Color(255.0f/256.0f, 184.0f/256.0f,54.0f/256.0f);
				break;
			}

			// Set Bar scales
			clobj = clobj.FindInChildren("bar");
			clobj.SetActive(true);
			if (averages[i] == -10000)
			{
				clobj.SetActive(false);
				clobj = fathercloned.FindInChildren("curvetext0");
				clobj.SetActive(false);
			}
			else
			{
				tk2dSprite sprite = (tk2dSprite)clobj.GetComponent<tk2dSprite>();
				sprite.color = mycol;
				
				vec = clobj.transform.localScale;
				scalecalc = (averages[i] - samplemin [0]) / tempvalue;		// recalc sample values
				vec.y = scalecalc;
				clobj.transform.localScale = vec;
				
				vec = clobj.transform.localPosition;
				vec.y = 0.01f + (scalecalc * beacon.transform.localPosition.y)/2.0f;
				clobj.transform.localPosition = vec;
				
				scalecalc = (averages[i] - samplemin [0]) / tempvalue;
				
				clobj = fathercloned.FindInChildren("curvetext0");

				tk2dSprite sprt = (tk2dSprite)clobj.FindInChildren("panel").GetComponent<tk2dSprite>();

				sprt.color = mycol;

				tm = (TextMesh)clobj.FindInChildren("intext").GetComponent<TextMesh>();
				tm.text = averages[i].ToString("0.00");
				vec = clobj.transform.localPosition;
				vec.y = 0.15f;
				clobj.transform.localPosition = vec;
			}
		}


		distance = beacon.transform.localPosition.y / tempvalue;

		if (tempvalue < 2000.0f)				stepvalue = 100.0f;
		if (tempvalue < 1000.0f)				stepvalue = 50.0f;
		if (tempvalue < 600.0f)				stepvalue = 25.0f;
		if (tempvalue < 400.0f)				stepvalue = 20.0f;
		if (tempvalue < 200.0f)				stepvalue = 15.0f;
		if (tempvalue < 150.0f)				stepvalue = 10.0f;
		if (tempvalue < 70.0f)				stepvalue = 5.0f;
		if (tempvalue < 20.0f)				stepvalue = 1.0f;
		
		float nrsamples = tempvalue / stepvalue;
		int _nrsamples = (int)nrsamples;
		ypos = clone_v.transform.localPosition.y;

		nvalues = _nrsamples;

		for (int nn=0;nn<_nrsamples+1;nn++)
		{
			int dispvalue = (int)startvalue;

			GameObject clobj;
			GameObject clobj2;
			if (nn == 0)
			{
				clobj = clone_v;
				clobj2 = clone_v2;
			}
			else
			{
				clobj = GameObject.Instantiate(clone_v) as GameObject;
				clobj.transform.parent = clone_v.transform.parent;
				clobj2 = GameObject.Instantiate(clone_v2) as GameObject;
				clobj2.transform.parent = clone_v2.transform.parent;
			}
			
			clobj.name = "clone_sample_v"+nn;
			clobj2.name = "clone_sample_v"+nn;
			Vector3 vec = clone_v.transform.localPosition;
			vec.y = ypos;
			clobj.transform.localPosition = vec;
			clobj2.transform.localPosition = vec;
			tm = (TextMesh)clobj.FindInChildren("text").GetComponent<TextMesh>();
			tm.text=dispvalue.ToString();
			tm = (TextMesh)clobj2.FindInChildren("text").GetComponent<TextMesh>();
			tm.text=dispvalue.ToString();
			ypos += distance * stepvalue;
			startvalue += stepvalue;
		}

		yield return new WaitForSeconds (0.03f);

        ShowClones(true);

//		ni_button_panelup.pageup = false;
		ni_endlessturning.waiting.SetActiveRecursively (false);
	}

	void TraceLine(int itemid,GameObject father, Color col, Vector3 vec1, Vector3 vec2)
	{
		Texture mytexture = cloneimage.GetComponent<Renderer>().material.mainTexture;
		GameObject newobj = GameObject.Instantiate(cloneimage) as GameObject;
		newobj.name = "curveline"+itemid;
		newobj.transform.parent = father.transform;
		newobj.transform.localPosition = new Vector3(0,0,0);
		newobj.transform.localScale = father.transform.localScale;
		CreateNewLine(newobj,col,vec1,vec2);

		newobj.GetComponent<Renderer>().material = new Material (Shader.Find("Sprites/Default"));

		newobj.GetComponent<Renderer>().material.mainTexture = mytexture;

	}

	void CreateNewLine(GameObject obj,Color color,Vector3 vec1,Vector3 vec2)
	{
		Vector3[]	vertex 		= new Vector3	[4];
		int[] 		triangle 	= new int		[3*2];
		Vector2[]	uv			= new Vector2	[4];		
		Color[]		col			= new Color		[4];
		float		decal  = 0.01f;

		if (Mathf.Abs(vec1.x - vec2.x) > Mathf.Abs(vec1.y - vec2.y))		// horizontal
		{
			vertex[0] = new Vector3(vec1.x,vec1.y-decal,0);
			vertex[1] = new Vector3(vec2.x,vec2.y-decal,0);
			vertex[2] = new Vector3(vec1.x,vec1.y+decal,0);
			vertex[3] = new Vector3(vec2.x,vec2.y+decal,0);
		}
		else
		{
			vertex[0] = new Vector3(vec1.x-decal,vec1.y,0);
			vertex[1] = new Vector3(vec1.x+decal,vec1.y,0);
			vertex[2] = new Vector3(vec2.x-decal,vec2.y,0);
			vertex[3] = new Vector3(vec2.x+decal,vec2.y,0);
		}

		col[0] = col[1] = col[2] = col[3] = color;
		
		uv[0]		= new Vector2(0,0);
		uv[1]		= new Vector2(1,0);
		uv[2]		= new Vector2(0,1);
		uv[3]		= new Vector2(1,1);
		
		triangle[0] = 0;
		triangle[1] = 2;
		triangle[2] = 1;
		triangle[3] = 2;
		triangle[4] = 3;
		triangle[5] = 1;
		
		Mesh m = new Mesh();
		
		m.vertices  = vertex;
		m.colors	= col;
		m.uv		= uv;
		m.triangles	= triangle;
		
		m.RecalculateNormals();
		m.RecalculateBounds();

		obj.GetComponent<MeshFilter>().mesh = m;
	}


	// Create and Save fake data samples !!!
	IEnumerator CreateFakeData()
	{
		ni_endlessturning.waiting.SetActiveRecursively (true);
//		ni_preparedatafields.SaveFakeDate(startdate, enddate);

		GameObject obj = gameObject.FindInChildren ("place_button_return");
		obj = obj.FindInChildren ("text");		tk2dTextMesh tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh> ();
		string mytext;
		DateTime start = startdate;
		DateTime end = enddate;
		TimeSpan halfaminute = new TimeSpan(0,0,(int)ni_preparedatafields.datacollectorperiod);
			
		ni_preparedatafields.ResetAllParameters ();
		ni_preparedatafields._elapsedtime = new TimeSpan(0,0,0,0,0);

		double stmil = ni_preparedatafields.GetMilliseconds (start);
		double edmil = ni_preparedatafields.GetMilliseconds (end);
		edmil = (edmil - stmil)/1000.0f;
		edmil = ni_preparedatafields.datacollectorperiod * 100.0f / edmil;
		stmil = 0.0f;

		while (DateTime.Compare(start, end) <  0)
		{
			ni_preparedatafields.Generate1FakeData();
			ni_preparedatafields.Save1SampleJsonFile(start);
			yield return new WaitForSeconds (0.03f);
			stmil = stmil + edmil;
			int res = (int)stmil;
			if (res > 100) res = 100;
//			mytext = "Load "+res+"%";

			start = start.AddSeconds(ni_preparedatafields.datacollectorperiod); // 
			ni_preparedatafields._elapsedtime = ni_preparedatafields._elapsedtime.Add(halfaminute);
		}
//		mytext = "Load 100%";

		ni_endlessturning.waiting.SetActiveRecursively (false);
	}


	void DisplayToggle()
	{
		tk2dSlicedSprite sprt;

		for (int i=0;i<3;i++)
		{
			sprt = (tk2dSlicedSprite)button_toggle[i].GetComponent<tk2dSlicedSprite>();
			sprt.color = Color.grey;
		}
		sprt = (tk2dSlicedSprite)button_toggle[button_toggle_index].GetComponent<tk2dSlicedSprite>();
		sprt.color = new Color(0.3f,1.0f,0.3f);
	}


	public static void MoveStatPage()
	{
		Vector3 vec = pages.transform.position;
		if (vec.x == 0)
			vec.x = 2.7224f;
		else
			vec.x = 0;
		pages.transform.position = vec;
	}
        
}
