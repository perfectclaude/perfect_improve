﻿using UnityEngine;
using System.Collections;

public class ni_admin_edit_datafields : MonoBehaviour
{
	static int MaxDataFields = 100;
	GameObject toclone;

	GameObject [] myobject = new GameObject[MaxDataFields];
	GameObject [] slide1 = new GameObject[MaxDataFields];
	GameObject [] slide2 = new GameObject[MaxDataFields];
	GameObject [] slide3 = new GameObject[MaxDataFields];
	GameObject [] text1 = new GameObject[MaxDataFields];
	GameObject [] text2 = new GameObject[MaxDataFields];

	GameObject [] connector = new GameObject[MaxDataFields];
	GameObject [] connectorleft = new GameObject[MaxDataFields];
	GameObject [] connectorright = new GameObject[MaxDataFields];
	int nrfields;
	public Camera 		m_ViewCamera;
	public static bool wasIpressed = false;
	Vector3 startpos;
	Vector3 dragstartpos;
	GameObject draggingball = null;
	int dragballindex = -1;


	void Awake()
	{
		/*
		Debug.Log ("Test");
		if (bufferme.HasKey("Cirmeca_TauxCadence_slide1"))
			Debug.Log ("SLIDE 1 exists");
		if (bufferme.HasKey("Cirmeca_TauxCadence_slide2"))
			Debug.Log ("SLIDE 2 exists");
*/
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		toclone = gameObject.FindInChildren ("toclone");
		toclone.SetActive (false);
	}
	
	void OnDisable ()
	{
		StopAllCoroutines ();
		ni_preparedatafields.SaveClientData ();
	}
	
	void Update()
	{
		int n;

		if (Input.GetMouseButtonDown(0))
		{
			wasIpressed = true;
			startpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
		}
		if (wasIpressed && (draggingball == null))
		{
			// Drag the BG here
			/*
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffy = vec.y - startpos.y;
			startpos = vec;
			vec = gameObject.transform.position;
			vec.y += diffy;
			gameObject.transform.position = vec;
			*/
		}
		
		if (Input.GetMouseButtonUp (0))
		{
			wasIpressed = false;
			draggingball = null;
			dragballindex = -1;
		}

		if (wasIpressed)
		{
			RaycastHit hitInfo;
			Ray ray;
			
			ray = m_ViewCamera.ScreenPointToRay(Input.mousePosition);
			if (Input.GetMouseButtonDown(0))
			{
				for (n=0;n<nrfields;n++)
				{
					if (slide1[n].GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
					{
						dragstartpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
						draggingball = slide1[n];
						dragballindex = n;
						break;
					}
					if (slide2[n].GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
					{
						dragstartpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
						draggingball = slide2[n];
						dragballindex = n;
						break;
					}
					if (slide3[n].GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
					{
						dragstartpos = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
						draggingball = slide3[n];
						dragballindex = n;
						break;
					}
				}
			}
		}

		if (draggingball != null)
		{
			// Drag the BG here
			Vector3 vec = m_ViewCamera.ScreenToWorldPoint(Input.mousePosition);
			float diffx = vec.x - dragstartpos.x;
			dragstartpos = vec;
			vec = draggingball.transform.position;
			vec.x += diffx;
			draggingball.transform.position = vec;
			vec = draggingball.transform.localPosition;
			if (vec.x < -0.955f)		vec.x = -0.955f;
			if (vec.x > 0.955f)		vec.x = 0.955f;
			draggingball.transform.localPosition = vec;
			SetSliderValue(draggingball);

		}

		n = dragballindex;
		if (n != -1)
		{
			Vector3 vec = connector[n].transform.localPosition;

			vec.x = (slide1[n].transform.localPosition.x + slide2[n].transform.localPosition.x) / 2;
			connector[n].transform.localPosition = vec;
			float	distance = Mathf.Abs(slide1[n].transform.localPosition.x - slide2[n].transform.localPosition.x);
			distance = distance / 0.042f;
			vec = connector[n].transform.localScale;
			vec.x = distance;
			connector[n].transform.localScale = vec;

			vec = connectorleft[n].transform.localPosition;
			if (slide1[n].transform.localPosition.x > slide2[n].transform.localPosition.x)
			{
				vec.x = (-0.955f + slide2[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(-0.955f - slide2[n].transform.localPosition.x);
			}
			else
			{
				vec.x = (-0.955f + slide1[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(-0.955f - slide1[n].transform.localPosition.x);
			}
			connectorleft[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectorleft[n].transform.localScale;
			vec.x = distance;
			connectorleft[n].transform.localScale = vec;
			
			vec = connectorright[n].transform.localPosition;
			if (slide1[n].transform.localPosition.x > slide2[n].transform.localPosition.x)
			{
				vec.x = (0.955f + slide1[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(0.955f - slide1[n].transform.localPosition.x);
			}
			else
			{
				vec.x = (0.955f + slide2[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(0.955f - slide2[n].transform.localPosition.x);
			}
			connectorright[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectorright[n].transform.localScale;
			vec.x = distance;
			connectorright[n].transform.localScale = vec;
		}
		/*
		for (n=0;n<nrfields;n++)
		{
			Vector3 vec;
			if (slide1[n].transform.localPosition.x < slide2[n].transform.localPosition.x)
			{
				vec = text1[n].transform.localPosition;
				vec.x = 0.045f;
				text1[n].transform.localPosition = vec;
				vec = text2[n].transform.localPosition;
				vec.x = -0.045f;
				text2[n].transform.localPosition = vec;
			}
			else
			{
				vec = text1[n].transform.localPosition;
				vec.x = -0.045f;
				text1[n].transform.localPosition = vec;
				vec = text2[n].transform.localPosition;
				vec.x = 0.045f;
				text2[n].transform.localPosition = vec;
			}
		}
		*/
	}
	
	public void ReSetDisplay()
	{
//		ni_preparedatafields.poinofinteresttitle[]
		Vector3 vec;
		float ypos2 = 0.25f;
		float ypos = 0;
		float xpos = -3.4f * 2.0f;
		string fathername = "";
		Vector3 tabpos = new Vector3(-0.9f,1.49f,10.0f);
		int tabindex = 0;

//		Debug.Log ("Selected machine:" + tabcenter.selectedtabobject);
//		int nrmachine = ni_preparedatafields.GetNrOfMachines();

		if (bufferme.HasKey("Curve_displays"))
		{
			nrfields = bufferme.GetInt("Curve_displays");

			for (int n=0;n<nrfields;n++)
			{
				bool fillsection = true;

				if (fathername != bufferme.GetString("Curve_displays_father"+n))
				{
					fathername = bufferme.GetString("Curve_displays_father"+n);
					ypos = 0.0f;
					ypos2 = 0.3f;
					xpos += 3.4f;
// Create tab clone
					/*
					GameObject tabobj = gameObject.transform.parent.gameObject.FindInChildren("tabname"+tabindex);

					if (tabobj == null)
					{
						tabobj = (GameObject)GameObject.Instantiate(tabtoclone);
						tabobj.transform.parent = gameObject.transform.parent;
						tabobj.transform.localPosition = tabpos;
						tabpos.x += 0.8f;
						tabobj.name = "tabname"+tabindex;

						tabobjects[tabindex] = tabobj;

						tk2dTextMesh innertm = (tk2dTextMesh)tabobj.FindInChildren("name").GetComponent<tk2dTextMesh>();
						settext.ForceText(innertm,fathername);
						tabobj.SetActive(true);
					}
					tk2dSlicedSprite ssprite = (tk2dSlicedSprite)tabobj.GetComponent<tk2dSlicedSprite>();
					if (tabindex == 0)
					{
						ssprite.color = new Color(0.78125f,0.78125f,0.78125f);
					}
					else
					{
						ssprite.color = Color.grey;
					}
					*/
					tabindex ++;
				}

				string datanameid = bufferme.GetString("Curve_displays_variable"+n);
				GameObject obj = gameObject.FindInChildren(datanameid);
				if (obj == null)
				{
					obj = (GameObject)GameObject.Instantiate(toclone);
					obj.name = datanameid;
					obj.transform.parent = gameObject.transform;
				}
				obj.SetActiveRecursively(true);
				myobject[n] = obj;
				
				vec = new Vector3(xpos,ypos2,0);
				obj.transform.localPosition = vec;
				
				tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("name").GetComponent<tk2dTextMesh>();
				string additional = "";

//				if (bufferme.HasKey(datanameid+"_form"))					additional = " [FORM]";
//				settext.ForceText(tm,datanameid+additional);

				settext.ForceText(tm,bufferme.GetString("Curve_displays_name"+n));		// set right names

				Debug.Log ("Curve item "+n+ "   " + datanameid);
				slide1[n] = myobject[n].FindInChildren("slideball1");
				slide2[n] = myobject[n].FindInChildren("slideball2");
				slide3[n] = myobject[n].FindInChildren("slideball3");
				text1[n] = slide1[n].FindInChildren("value");
				text2[n] = slide2[n].FindInChildren("value");
				connector[n] = myobject[n].FindInChildren("connector");
				connectorleft[n] = myobject[n].FindInChildren("connectorleft");
				connectorright[n] = myobject[n].FindInChildren("connectorright");

				ypos2 -= 0.48f;
				// 
				if (fillsection)
				{
					obj = myobject[n].FindInChildren("input_min");
					obj = obj.FindInChildren("value");
					tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
					if (bufferme.HasKey(datanameid+"_min"))
						settext.ForceText(tm,bufferme.GetString(datanameid+"_min"));
					
					obj = myobject[n].FindInChildren("input_max");
					obj = obj.FindInChildren("value");
					tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
					if (bufferme.HasKey(datanameid+"_max"))
						settext.ForceText(tm,bufferme.GetString(datanameid+"_max"));

					// Slider page
					if (bufferme.HasKey(myobject[n].name+"_slide1pos"))
					{
						vec = slide1[n].transform.localPosition;
						vec.x = bufferme.GetFloat(myobject[n].name+"_slide1pos");
						slide1[n].transform.localPosition = vec;
					}
					if (bufferme.HasKey(myobject[n].name+"_slide2pos"))
					{
						vec = slide2[n].transform.localPosition;
						vec.x = bufferme.GetFloat(myobject[n].name+"_slide2pos");
						slide2[n].transform.localPosition = vec;
					}
					if (bufferme.HasKey(myobject[n].name+"_slide3pos"))
					{
						vec = slide3[n].transform.localPosition;
						vec.x = bufferme.GetFloat(myobject[n].name+"_slide3pos");
						slide3[n].transform.localPosition = vec;
					}
					SetSliderValue(slide1[n]);
					SetSliderValue(slide2[n]);
					SetSliderValue(slide3[n]);
				}
			}
		}
		toclone.SetActive (false);

		Vector3 setvector = new Vector3(3.4f,0,0);
		gameObject.transform.localPosition = setvector;
		for (int n=0;n<nrfields;n++)
		{
			vec = connector[n].transform.localPosition;
			vec.x = (slide1[n].transform.localPosition.x + slide2[n].transform.localPosition.x) / 2;
			connector[n].transform.localPosition = vec;
			float	distance = Mathf.Abs(slide1[n].transform.localPosition.x - slide2[n].transform.localPosition.x);
			distance = distance / 0.042f;
			vec = connector[n].transform.localScale;
			vec.x = distance;
			connector[n].transform.localScale = vec;
			
			vec = connectorleft[n].transform.localPosition;
			if (slide1[n].transform.localPosition.x > slide2[n].transform.localPosition.x)
			{
				vec.x = (-0.955f + slide2[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(-0.955f - slide2[n].transform.localPosition.x);
			}
			else
			{
				vec.x = (-0.955f + slide1[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(-0.955f - slide1[n].transform.localPosition.x);
			}
			connectorleft[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectorleft[n].transform.localScale;
			vec.x = distance;
			connectorleft[n].transform.localScale = vec;
			
			vec = connectorright[n].transform.localPosition;
			if (slide1[n].transform.localPosition.x > slide2[n].transform.localPosition.x)
			{
				vec.x = (0.955f + slide1[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(0.955f - slide1[n].transform.localPosition.x);
			}
			else
			{
				vec.x = (0.955f + slide2[n].transform.localPosition.x) / 2;
				distance = Mathf.Abs(0.955f - slide2[n].transform.localPosition.x);
			}
			connectorright[n].transform.localPosition = vec;
			distance = distance / 0.0415f;
			vec = connectorright[n].transform.localScale;
			vec.x = distance;
			connectorright[n].transform.localScale = vec;
			
		}
	}
	
	void OnEnable ()
	{
		ReSetDisplay ();
		toclone.SetActive (false);

		StartCoroutine ("UpdateValues");
	}

	void SetSliderValue(GameObject slider)
	{
		GameObject father = slider.transform.parent.gameObject;
		GameObject max = father.FindInChildren ("input_max");
		max = max.FindInChildren ("value");
		tk2dTextMesh tmmax = (tk2dTextMesh)max.GetComponent<tk2dTextMesh> ();
		GameObject min = father.FindInChildren ("input_min");
		min = min.FindInChildren ("value");
		tk2dTextMesh tmmin = (tk2dTextMesh)min.GetComponent<tk2dTextMesh> ();
		float fmin, fmax;
		float.TryParse (tmmin.text, out fmin);
		float.TryParse (tmmax.text, out fmax);
		float mypos = slider.transform.localPosition.x + 0.955f;
		
		float val = fmin + mypos * (fmax - fmin) / 1.91f;
		
		tk2dTextMesh tm = (tk2dTextMesh)slider.FindInChildren ("value").GetComponent<tk2dTextMesh> ();
		settext.ForceText (tm, ((int)val).ToString ());
	}
	
	IEnumerator UpdateValues()
	{
		GameObject   obj;
		tk2dTextMesh tm;
		
		while (true)
		{
			int n;
			yield return new WaitForSeconds (5.0f);
			for (n=0;n<nrfields;n++)
			{
				yield return new WaitForSeconds (0.03f);

				bool fillsection = true;
				string datanameid = bufferme.GetString("Curve_displays_variable"+n);
				/*
				int state = bufferme.GetInt(datanameid+ "_state");
				switch (state)
				{
				case 0:
					break;
				case 1:
					fillsection = false;
					break;
				}
				*/
				/*
				bufferme.SetInt("Curve_displays",statid+1);
			bufferme.SetString("Curve_displays_name"+statid,(string)statjson["STATname"]);
			bufferme.SetString("Curve_displays_variable"+statid,(string)statjson["STATvariable"]);
			bufferme.SetString("Curve_displays_form"+statid,(string)statjson["STATform"]);
*/
				if (fillsection)
				{
					obj = myobject[n].FindInChildren("input_min");
					obj = obj.FindInChildren("value");
					tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
					if ((!bufferme.HasKey(myobject[n].name+"_min")) || (bufferme.GetString(myobject[n].name+"_min") != tm.text))
						bufferme.SetString(myobject[n].name+"_min",tm.text);
					
					obj = myobject[n].FindInChildren("input_max");
					obj = obj.FindInChildren("value");
					tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
					if ((!bufferme.HasKey(myobject[n].name+"_max")) || (bufferme.GetString(myobject[n].name+"_max") != tm.text))
						bufferme.SetString(myobject[n].name+"_max",tm.text);
					
					// Slider page
					if ((!bufferme.HasKey(myobject[n].name+"_slide1pos")) || (bufferme.GetFloat(myobject[n].name+"_slide1pos") != slide1[n].transform.localPosition.x))
						bufferme.SetFloat(myobject[n].name+"_slide1pos",slide1[n].transform.localPosition.x);
					obj = slide1[n].FindInChildren("value");
					tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
//					if ((!bufferme.HasKey(myobject[n].name+"_slide1")) || (bufferme.GetString(myobject[n].name+"_slide1") != tm.text))
					bufferme.DeleteKey(myobject[n].name+"_slide1");
					Debug.Log ("Save "+myobject[n].name+"_slide1 : " + tm.text);
					bufferme.SetString(myobject[n].name+"_slide1",tm.text);
					
					if ((!bufferme.HasKey(myobject[n].name+"_slide2pos")) || (bufferme.GetFloat(myobject[n].name+"_slide2pos") != slide2[n].transform.localPosition.x))
						bufferme.SetFloat(myobject[n].name+"_slide2pos",slide2[n].transform.localPosition.x);
					obj = slide2[n].FindInChildren("value");
					tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
//					if ((!bufferme.HasKey(myobject[n].name+"_slide2")) || (bufferme.GetString(myobject[n].name+"_slide2") != tm.text))
					bufferme.DeleteKey(myobject[n].name+"_slide2");
					bufferme.SetString(myobject[n].name+"_slide2",tm.text);
					
					if ((!bufferme.HasKey(myobject[n].name+"_slide3pos")) || (bufferme.GetFloat(myobject[n].name+"_slide3pos") != slide3[n].transform.localPosition.x))
						bufferme.SetFloat(myobject[n].name+"_slide3pos",slide3[n].transform.localPosition.x);
					obj = slide3[n].FindInChildren("value");
					tm = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();
					bufferme.DeleteKey(myobject[n].name+"_slide3");
//					if ((!bufferme.HasKey(myobject[n].name+"_slide3")) || (bufferme.GetString(myobject[n].name+"_slide3") != tm.text))
						bufferme.SetString(myobject[n].name+"_slide3",tm.text);
				}
			}
		}
	}
}
