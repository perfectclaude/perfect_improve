﻿using UnityEngine;
using System.Collections;
using System;

public class ni_manage_screens : MonoBehaviour
{
	static int MaxScreenStack = 10;
	static string	[] ScreenStack = new string[MaxScreenStack];
	static int nScreenStack = 0;
	static string lastusedscreen;
	static GameObject mygo;
	static GameObject	atelier;


	void Start ()
	{
	}

	void Awake ()
	{
//		PlayerPrefs.DeleteAll ();
//		PlayerPrefs.Save ();

		atelier = GameObject.Find("pi_atelier");
		if (atelier != null)
			atelier = atelier.FindInChildren ("pi_scene");
		mygo = gameObject;
		nScreenStack = 0;
		SetScreen("overlay_menu");

	}


	static void SetScreen(string screenname)
	{
		if (atelier != null)
		{
			if (screenname == "overlay_menu")
				atelier.SetActive (true);
			else
				atelier.SetActive (false);
		}
//		Debug.Log ("Trying to set " + screenname);
		mygo.SetActiveRecursively (true);
		foreach (Transform child in mygo.GetComponentsInChildren<Transform>())
		{
			if (child.gameObject.GetComponent<ni_iamascreen>() != null)
			{
				if (child.gameObject.name != screenname)
				{
					child.gameObject.SetActive(false);
				}
			}

			// Screen de type Popup sont caché en cas de changement de Screen
			if (child.gameObject.GetComponent<ni_iamapopup >() != null)
				child.gameObject.SetActive(false);
				
		}
		lastusedscreen = screenname;
	}
	
	public static void SetPreviousScreen()
	{
		if (nScreenStack == 0)						return;
		nScreenStack --;
		SetScreen(ScreenStack[nScreenStack]);
	}

	public static void SetNextScreen(string screenname)
	{
		if (screenname == lastusedscreen)
		{
			mygo.SetActiveRecursively (false);
		}
		ScreenStack[nScreenStack] = lastusedscreen;
		nScreenStack++;
		SetScreen (screenname);
	}

	void Update ()
	{
	
	}
}
