using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_getdata_saintelizaigne : MonoBehaviour
{
	float monday_start = 7.0f;
	float monday_end = 16.75f;
	float tuesday_start = 7.0f;
	float tuesday_end = 16.75f;
	float wednesday_start = 7.0f;
	float wednesday_end = 16.75f;
	float thursday_start = 7.0f;
	float thursday_end = 16.75f;
	float friday_start = 7.0f;
	float friday_end = 12.0f;
	float saturday_start = 0.0f;
	float saturday_end = 0.0f;
	float sunday_start = 0.0f;
	float sunday_end = 0.0f;

	DateTime [] alldates = new DateTime[5000];
	float [,] allvalues = new float[5000,50];
    bool [,] allvalues_quality = new bool[5000,50];
	float [] lastvalues = new float[50];
	float [] addvalues = new float[50];
	float [] subvalues = new float[50];
	int nallvalues = 0;

	int nrfields;
	tk2dTextMesh		tm;

	void Awake()
	{
//PlayerPrefs.DeleteAll();
//		PlayerPrefs.Save();

		tm = (tk2dTextMesh)GameObject.Find ("textje").GetComponent<tk2dTextMesh>();
	}

	void Update()
	{
		settext.ForceText(tm,GUI_display.ErrorMessage);
	}

	IEnumerator Start()
	{
/*
		Debug.Log ("Wait before clean");

		while (network.waitingnetwork)
			yield return null;
		Debug.Log ("Wait clean");
//		network.CleanDbase(bufferme.GetClientId());
//		Debug.Log ("Wait while before clean");
//		while (network.waitingnetwork)
//			yield return null;
		
		Debug.Log ("Wait create");
		network.CreateDbase(bufferme.GetClientId());
		while (network.waitingnetwork)
			yield return null;
*/
		yield return new WaitForSeconds(0.05f);

		nrfields = bufferme.GetInt("DataFields");

		StartCoroutine("looptroughtime");

		bufferme.SetFloat("Cirmeca_TempsEnMarche_value",0.0f);
        bufferme.SetBool("Cirmeca_TempsEnMarche_quality",false);

		bufferme.SetFloat("Robot_TempsEnMarche_value",0.0f);
        bufferme.SetBool("Robot_TempsEnMarche_quality",false);

        bufferme.SetFloat("Idec_TempsEnMarche_value",0.0f);
        bufferme.SetBool("Idec_TempsEnMarche_quality",false);


		bufferme.SetFloat("Cirmeca_TempsEnDefaut_value",0.0f);
        bufferme.SetBool("Cirmeca_TempsEnDefaut_quality",false);

		bufferme.SetFloat("Robot_TempsEnDefaut_value",0.0f);
        bufferme.SetBool("Robot_TempsEnDefaut_quality",false);

        bufferme.SetFloat("Idec_TempsEnDefaut_value",0.0f);
        bufferme.SetBool("Idec_TempsEnDefaut_quality",false);


		bufferme.SetFloat("Cirmeca_PiecesBonnes_value",0.0f);
        bufferme.SetBool("Cirmeca_PiecesBonnes_quality",false);

		bufferme.SetFloat("Robot_PiecesBonnes_value",0.0f);
        bufferme.SetBool("Robot_PiecesBonnes_quality",false);

        bufferme.SetFloat("Idec_PiecesBonnes_value",0.0f);
        bufferme.SetBool("Idec_PiecesBonnes_quality",false);

        bufferme.SetFloat("Cirmeca_CoupuresBarrieres_value",0.0f);
        bufferme.SetBool("Cirmeca_CoupuresBarrieres_quality",false);

		bufferme.SetFloat("Robot_PiecesMauvaises_value",0.0f);
        bufferme.SetBool("Robot_PiecesMauvaises_quality",false);


        bufferme.SetFloat("Idec_CorpsNC_value",0.0f);
        bufferme.SetBool("Idec_CorpsNC_quality",false);

        bufferme.SetFloat("Idec_PiecesMauvaisesDefTech_value",0.0f);
        bufferme.SetBool("Idec_PiecesMauvaisesDefTech_quality",false);

        bufferme.SetFloat("Idec_PiecesMauvaisesFuyard_value",0.0f);
        bufferme.SetBool("Idec_PiecesMauvaisesFuyard_quality",false);

        bufferme.SetFloat("Idec_PiecesMauvaisesDefRaccord_value",0.0f);
        bufferme.SetBool("Idec_PiecesMauvaisesDefRaccord_quality",false);


		bufferme.SetFloat("Stocks_affecte_ProduitsFinis_value",0.0f);
        bufferme.SetBool("Stocks_affecte_ProduitsFinis_quality",false);

		bufferme.SetFloat("Stocks_non_affecte_ProduitsFinis_value",0.0f);
        bufferme.SetBool("Stocks_non_affecte_ProduitsFinis_quality",false);

		bufferme.SetFloat("Stocks_affecte_Manoeuvres_value",0.0f);
        bufferme.SetBool("Stocks_affecte_Manoeuvres_quality",false);

		bufferme.SetFloat("Stocks_non_affecte_Manoeuvres_value",0.0f);
        bufferme.SetBool("Stocks_non_affecte_Manoeuvres_quality",false);

		bufferme.SetFloat("Tests_TauxAirEau_value",0.0f);
        bufferme.SetBool("Tests_TauxAirEau_quality",false);
	}

	string LoadFile(string path)
	{
		string txt;

		using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
		{
			using (StreamReader r = new StreamReader(file))
			{
				txt = r.ReadToEnd();
			}
		}
		return txt;
	}

	int machinetype = 0;

	bool StripLine(string textfile,int hour)
	{
		string [] lines = textfile.Split('\n');
		string valuetext;
		float valuefloat;
		bool result = false;

		foreach (string textline in lines)
		{
			if (textline.Contains("	Date="))
			{
//				Debug.Log (textline);
				string datetext = textline.Replace("	Date=","");
				datetext = datetext.Substring(11,2);
				int timeint;
				int.TryParse(datetext,out timeint);
				if (timeint != hour)		return(false); // quit loop for this file
			}
			if (textline.Contains("[Cirmeca]"))				machinetype = 1;
			if (textline.Contains("[Robot]"))				machinetype = 2;
			if (textline.Contains("[Stocks affecte]"))		machinetype = 3;
			if (textline.Contains("[Stocks non affecte]"))	machinetype = 4;
			if (textline.Contains("[Tests]"))				machinetype = 5;

			if (textline.Contains("	TempsEnMarche="))
			{
				valuetext = textline.Replace("	TempsEnMarche=","");
				float.TryParse(valuetext,out valuefloat);
                if (machinetype == 1)
                {
                    bufferme.SetFloat("Cirmeca_TempsEnMarche_value", valuefloat);
                    bufferme.SetBool("Cirmeca_TempsEnMarche_quality", true);
                }
                if (machinetype == 2)
                {
                    bufferme.SetFloat("Robot_TempsEnMarche_value", valuefloat);
                    bufferme.SetBool("Robot_TempsEnMarche_quality", true);
                }
				result = true;
			}
			if (textline.Contains("	TempsEnDefaut="))
			{
				valuetext = textline.Replace("	TempsEnDefaut=","");
				float.TryParse(valuetext,out valuefloat);
                if (machinetype == 1)
                {
                    bufferme.SetFloat("Cirmeca_TempsEnDefaut_value", valuefloat);
                    bufferme.SetBool("Cirmeca_TempsEnDefaut_quality", true);
                }
                if (machinetype == 2)
                {
                    bufferme.SetFloat("Robot_TempsEnDefaut_value", valuefloat);
                    bufferme.SetBool("Robot_TempsEnDefaut_quality", true);
                }
				result = true;
			}
			if (textline.Contains("	PiecesBonnes="))
			{
				valuetext = textline.Replace("	PiecesBonnes=","");
				float.TryParse(valuetext,out valuefloat);
                if (machinetype == 1)
                {
                    bufferme.SetFloat("Cirmeca_PiecesBonnes_value", valuefloat);
                    bufferme.SetBool("Cirmeca_PiecesBonnes_quality", true);
                }
                if (machinetype == 2)
                {
                    bufferme.SetFloat("Robot_PiecesBonnes_value", valuefloat);
                    bufferme.SetBool("Robot_PiecesBonnes_quality", true);
                }
				result = true;
			}
			if (textline.Contains("	PiecesMauvaises="))
			{
				valuetext = textline.Replace("	PiecesMauvaises=","");
				float.TryParse(valuetext,out valuefloat);
//				if (machinetype == 1)
//              {
//					bufferme.SetFloat("Cirmeca_PiecesBonnes_value",valuefloat);
//                  bufferme.SetBool("Cirmeca_PiecesBonnes_quality", true);
//              }
                if (machinetype == 2)
                {
                    bufferme.SetFloat("Robot_PiecesMauvaises_value", valuefloat);
                    bufferme.SetBool("Robot_PiecesMauvaises_quality", true);
                }
				result = true;
			}


			if (textline.Contains("	ProduitsFinis="))
			{
				valuetext = textline.Replace("	ProduitsFinis=","");
				float.TryParse(valuetext,out valuefloat);
                if (machinetype == 3)
                {
                    bufferme.SetFloat("Stocks_affecte_ProduitsFinis_value", valuefloat);
                    bufferme.SetBool("Stocks_affecte_ProduitsFinis_quality", true);
                }
                if (machinetype == 4)
                {
                    bufferme.SetFloat("Stocks_non_affecte_ProduitsFinis_value", valuefloat);
                    bufferme.SetBool("Stocks_non_affecte_ProduitsFinis_quality", true);
                }
				result = true;
			}

			if (textline.Contains("	Manoeuvres="))
			{
				valuetext = textline.Replace("	Manoeuvres=","");
				float.TryParse(valuetext,out valuefloat);
                if (machinetype == 3)
                {
                    bufferme.SetFloat("Stocks_affecte_Manoeuvres_value", valuefloat);
                    bufferme.SetBool("Stocks_affecte_Manoeuvres_quality", true);
                }
                if (machinetype == 4)
                {
                    bufferme.SetFloat("Stocks_non_affecte_Manoeuvres_value", valuefloat);
                    bufferme.SetBool("Stocks_non_affecte_Manoeuvres_quality", true);
                }
				result = true;
			}

			if (textline.Contains("	TauxAirEau="))
			{
				valuetext = textline.Replace("	TauxAirEau=","");
				float.TryParse(valuetext,out valuefloat);
                if (machinetype == 5)
                {
                    bufferme.SetFloat("Tests_TauxAirEau_value", valuefloat);
                    bufferme.SetBool("Tests_TauxAirEau_quality", true);
                }
				result = true;
			}
		}
		return (result);
	}

	DateTime lastdatetime;

	void SaveDateTimeJsonInformation(DateTime date, DateTime startdt)
	{
		int n,nn;

		// check lastdatetime and new date to see if we must save the 2 fake values
		if ((lastdatetime.Year == date.Year) &&
						(lastdatetime.Month == date.Month) &&
						(lastdatetime.Day == date.Day) &&
						(lastdatetime.Hour == date.Hour - 1))
		{
			if (nallvalues != 0)
			{
				int origid = nallvalues-1;
				int firstid = nallvalues;
				int secondid = nallvalues+1;

				DateTime roundtime = date;
				roundtime = roundtime.AddMilliseconds(-date.Millisecond);
				roundtime = roundtime.AddSeconds(-date.Second);
				roundtime = roundtime.AddMinutes(-date.Minute);
// re-save last sample twice !!
				roundtime = roundtime.AddSeconds(-2);
				DateTime roundtime2 = roundtime.AddSeconds(4);

				alldates[firstid] = roundtime;
				alldates[secondid] = roundtime2;

				for (nn=0,n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_"+n);

					if (!bufferme.HasKey(datanameid+"_form"))
					{
						if (datanameid.Contains("TempsEnMarche"))
						{
							TimeSpan span = (roundtime - startdt);
							allvalues[firstid,nn] = (float)span.TotalSeconds;			// datetime to startingdatetime
							span = (roundtime2 - startdt);
							allvalues[secondid,nn] = (float)span.TotalSeconds;
						}
						else
						{
							allvalues[firstid,nn] = allvalues[origid,nn];
							allvalues[secondid,nn] = allvalues[origid,nn];
						}
						nn++;
					}
				}
				nallvalues += 2;
			}
		}

		lastdatetime = date;
//		Debug.Log("SaveDateTimeJsonInformation  "+nallvalues+" "+date.ToShortDateString()+" "+date.ToShortTimeString());
//		Debug.Log("SaveDateTimeJsonInformation2 "+nallvalues+" "+startdt.ToShortDateString()+" "+startdt.ToShortTimeString());
		alldates[nallvalues] = date;

		for (nn=0,n=0;n<nrfields;n++)
		{
			string datanameid = bufferme.GetString("DataField_"+n);
			if (!bufferme.HasKey(datanameid+"_form"))
			{
				if (datanameid.Contains("TempsEnMarche"))
				{
					TimeSpan span = (date - startdt);
					allvalues[nallvalues,nn] = (float)span.TotalSeconds;			// datetime to startingdatetime
				}
				else
				{
					allvalues[nallvalues,nn] = bufferme.GetFloat(datanameid+"_value");
                    allvalues_quality[nallvalues,nn] = bufferme.GetBool(datanameid+"_quality");
				}

//				if (datanameid.Contains("Cirmeca_PiecesBonnes"))
//				{
//					Debug.Log ("Showval:"+allvalues[nallvalues,nn]);
//				}

				nn++;
			}
		}
		nallvalues++;
	}

	bool waittildone = false;
	IEnumerator CalculateAndJsonAllDailyInformation()
	{
		float [] lastvalues = new float[50];
		float [] addvalues = new float[50];
		int n,nn;

		if (nallvalues == 0)
		{
			nallvalues = 0;		// restart savings
			waittildone = false;
			yield break;
		}
//		Debug.Log ("Recalc "+nallvalues+" lines to save");
// recalculate all values
		for (int i=0;i<nallvalues;i++)
		{
			for (nn=0,n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				if (datanameid.Contains("TempsEnMarche"))
				{
					nn++;
				}
				else
				{
					if (!bufferme.HasKey(datanameid+"_form"))
					{
						/*
						if (datanameid.Contains("Cirmeca_PiecesBonnes"))
						{
							Debug.Log("sample :" + allvalues[i,nn] + alldates[i].ToShortTimeString());
						}
*/
						{
							if (i == 0)
							{
								lastvalues[nn] = allvalues[i,nn];
								subvalues[nn] = allvalues[i,nn];
								addvalues[nn] = 0.0f;
								allvalues[i,nn] = allvalues[i,nn] - subvalues[nn];
							}
							else
							{
								if (allvalues[i,nn] < lastvalues[nn])
								{
									addvalues[nn] = allvalues[i-1,nn];
								}
								lastvalues[nn] = allvalues[i,nn];
								allvalues[i,nn] = allvalues[i,nn] + addvalues[nn];
							}
						}
						nn++;
					}
				}
			}
		}
		for (int i=0;i<nallvalues;i++)
		{
			for (nn=0,n=0;n<nrfields;n++)
			{
				string datanameid = bufferme.GetString("DataField_"+n);
				if (!bufferme.HasKey(datanameid+"_form"))
				{
//if (datanameid.Contains("Cirmeca_PiecesBonnes"))
//{
//					Debug.Log ("SaveVal "+i+":"+allvalues[i,nn]);
//}

					bufferme.SetFloat(datanameid+"_value",allvalues[i,nn]);
                    bufferme.SetBool(datanameid+"_quality", allvalues_quality[i,nn]);
					nn++;
				}
			}
			// fill and save JSON
//			Debug.Log ("Saving "+i+ " " + alldates[i]);

			ni_preparedatafields.Send_BasicSample(alldates[i]);

			//			Debug.Log("Saving data "+i+"/"+nallvalues);
			yield return new WaitForSeconds(0.1f);
		}
//		yield return new WaitForSeconds(5.0f);		// wait sending a bit

		// better save all previous values here !!! to be used for

		alldates[0] = alldates[nallvalues-1];
		for (nn=0,n=0;n<nrfields;n++)
		{
			allvalues[0,nn] = allvalues[nallvalues-1,nn];
		}
		Debug.Log ("All saved..");
		nallvalues = 1;		// restart savings
		

		waittildone = false;
	}

	IEnumerator looptroughtime()
	{
		int yearcheck = 2014;		// starting year
		int day_start = 0;
		int day_end = 0;
		int day_start_min = 0;
		int day_end_min = 0;
		float temptime;

		int yearcheckend = DateTime.Now.Year;  // 2014
		int yearstartmonth = 10;
		int yearendmonth = 12;
		int startdayvalue = 24;
		//		Debug.Log ("Year:"+yearcheck + " - " + DateTime.Now.Year);

		/*
		{
			//								Debug.Log ("finding to calculate per hour day_start:"+day_start+"  day_end:"+day_end);
			DateTime startdate = new DateTime(2014, 11, 4,14,0,0,0); //.ToLocalTime();
			DateTime enddate = startdate.AddHours(1);
			long stime = (long)network.GetMiliseconds(startdate);
			long etime = (long)network.GetMiliseconds(enddate);
			
			DateTime sampledate = startdate.AddMinutes(30);
			long savetime = (long)network.GetMiliseconds(sampledate);
			
			network.GetIntermediateJson(bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,false,"ni_getdata_saintelizaigne looptroughtime day");
			while (network.waitingnetworkintermediate)
			{
				yield return new WaitForSeconds(0.2f);
			}
			Debug.Log (network.retreiveintermediatejson);
			if (network.retreiveintermediatejson.Length > 0)
			{
				ni_preparedatafields.CalcAverageHour(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson);
			}
			day_start++;
		}
*/

		while (yearcheck <= yearcheckend)
		{
			bool yearatleast1valid = false;
			string filename = Application.streamingAssetsPath + "/" + bufferme.GetClientId () + "/" + yearcheck;
//			Debug.Log ("checking:"+filename);
			if (Directory.Exists(filename))
			{
				int monthcheck = yearstartmonth;
				yearstartmonth = 1;					// reset to january

				while (((yearcheck != yearcheckend) && (monthcheck <= 12)) || ((yearcheck == yearcheckend) && (monthcheck <= yearendmonth)))
				{
					bool monthatleast1valid = false;

					filename = Application.streamingAssetsPath + "/" + bufferme.GetClientId () + "/" + yearcheck + "/" + monthcheck.ToString("D2");

					if (Directory.Exists(filename))
					{
						int daycheck = startdayvalue;
						startdayvalue = 1;
						while (daycheck <= 31)
						{
							Debug.Log (daycheck+"/"+monthcheck+"/"+yearcheck);

							bool dayatleast1valid = false;
							filename = Application.streamingAssetsPath + "/" + bufferme.GetClientId () + "/" + yearcheck + "/" + monthcheck.ToString("D2") + "/" + daycheck.ToString("D2");
					
							if (Directory.Exists(filename))
							{
								DateTime verifyday = new DateTime(yearcheck, monthcheck, daycheck,12,0,0,0); //.ToLocalTime();

								nallvalues = 0;

								switch (verifyday.DayOfWeek)
								{
								case DayOfWeek.Monday :
									day_start = (int)monday_start;
									temptime = (monday_start - day_start) * 60.0f;
									day_start_min = (int)temptime;
									day_end = (int)monday_end;
									temptime = (monday_end - day_end) * 60.0f;
									day_end_min = (int)temptime;
									break;
								case DayOfWeek.Tuesday :
									day_start = (int)tuesday_start;
									temptime = (tuesday_start - day_start) * 60.0f;
									day_start_min = (int)temptime;
									day_end = (int)tuesday_end;
									temptime = (tuesday_end - day_end) * 60.0f;
									day_end_min = (int)temptime;
									break;
								case DayOfWeek.Wednesday :
									day_start = (int)wednesday_start;
									temptime = (wednesday_start - day_start) * 60.0f;
									day_start_min = (int)temptime;
									day_end = (int)wednesday_end;
									temptime = (wednesday_end - day_end) * 60.0f;
									day_end_min = (int)temptime;
									break;
								case DayOfWeek.Thursday :
									day_start = (int)thursday_start;
									temptime = (thursday_start - day_start) * 60.0f;
									day_start_min = (int)temptime;
									day_end = (int)thursday_end;
									temptime = (thursday_end - day_end) * 60.0f;
									day_end_min = (int)temptime;
									break;
								case DayOfWeek.Friday :	
									day_start = (int)friday_start;
									temptime = (friday_start - day_start) * 60.0f;
									day_start_min = (int)temptime;
									day_end = (int)friday_end;
									temptime = (friday_end - day_end) * 60.0f;
									day_end_min = (int)temptime;
									break;
								case DayOfWeek.Saturday :
									day_start = (int)saturday_start;
									temptime = (saturday_start - day_start) * 60.0f;
									day_start_min = (int)temptime;
									day_end = (int)saturday_end;
									temptime = (saturday_end - day_end) * 60.0f;
									day_end_min = (int)temptime;
									break;
								case DayOfWeek.Sunday :
									day_start = (int)sunday_start;
									temptime = (sunday_start - day_start) * 60.0f;
									day_start_min = (int)temptime;
									day_end = (int)sunday_end;
									temptime = (sunday_end - day_end) * 60.0f;
									day_end_min = (int)temptime;
									break;
								}
								lastdatetime = DateTime.Today.AddDays(-2);
								nallvalues = 0;	// new day new reset
								for (int hour = 0;hour<24;hour++)
								{
									bool houratleast1valid = false;

									GUI_display.ErrorMessage = daycheck+"/"+monthcheck+"/"+yearcheck+" "+hour+"h";
									Debug.Log (daycheck+"/"+monthcheck+"/"+yearcheck+" "+hour+"h");
									for (int min = 0;min<60;min++)
									{
										yield return new WaitForSeconds(0.05f);		// wait between every day
										for (int sec = 0;sec<60;sec++)
										{
											bool validate = false;
// create filename
											int fakehour = hour;
											if (fakehour > 12)		fakehour = fakehour - 12;
											string targetfilename1 = "Automate_"+
																	  yearcheck+monthcheck.ToString("D2")+
													                  daycheck.ToString("D2")+
																		fakehour.ToString("D2")+
																		min.ToString("D2")+
																		sec.ToString("D2")+".ini";
											string targetfilename2 = "ERP_"+
												yearcheck+monthcheck.ToString("D2")+
													daycheck.ToString("D2")+
													fakehour.ToString("D2")+
													min.ToString("D2")+
													sec.ToString("D2")+".ini";
											if (File.Exists(filename+"/"+targetfilename1))
											{
												string textfile = LoadFile(filename+"/"+targetfilename1);
												if (StripLine(textfile,hour))
													validate = true;
											}
											if (File.Exists(filename+"/"+targetfilename2))
											{
												string textfile = LoadFile(filename+"/"+targetfilename2);
												StripLine(textfile,hour);
											}

											if (validate)
											{
												Debug.Log (yearcheck+"/"+monthcheck+"/"+daycheck + " " + hour+":"+min+":"+sec);

												if (((hour > day_start) && (hour < day_end)) ||
												    (hour == day_start) && (min >= day_start_min) ||
												    (hour == day_end) && (min < day_end_min))
												{
//													Debug.Log (daycheck.ToString ("D2")+"/"+monthcheck.ToString ("D2")+"/"+yearcheck + " "+hour.ToString ("D2")+"h"+min.ToString ("D2"));

													houratleast1valid = true;
													int newsec = sec;
													
													if (newsec < 5)	newsec = 5;
													if (newsec > 55)	newsec = 55;

													DateTime display = new DateTime(yearcheck, monthcheck, daycheck,hour,min,newsec,0); //.ToLocalTime();
//													display = display.AddHours(-1);

													DateTime startdisplay = new DateTime(yearcheck, monthcheck, daycheck,day_start,day_start_min,newsec,0); //.ToLocalTime();
//													startdisplay = startdisplay.AddHours(-1);
													SaveDateTimeJsonInformation(display,startdisplay);
												}
											}
										}
									}
									/*
//  TODO fetch hour stats here
									if (houratleast1valid)
									{
										dayatleast1valid = true;
										DateTime startdate = new DateTime(yearcheck, monthcheck, daycheck,hour,0,0,0); //.ToLocalTime();
										startdate = startdate.AddHours(-1); //-2);
										DateTime enddate = startdate.AddSeconds(60*60-5);
										Debug.Log (startdate.ToShortDateString() + " " + startdate.ToShortTimeString());
										Debug.Log (enddate.ToShortDateString() + " " + enddate.ToShortTimeString());
										
										long stime = (long)network.GetMiliseconds(startdate);
										long etime = (long)network.GetMiliseconds(enddate);
										
										DateTime sampledate = startdate.AddMinutes(30);
										long savetime = (long)network.GetMiliseconds(sampledate);
										
										network.GetIntermediateJson(bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,false);
										while (network.waitingnetworkintermediate)
										{
											yield return new WaitForSeconds(0.2f);
										}
										if (network.retreiveintermediatejson.Length > 0)
										{
											ni_preparedatafields.CalcAverageHour(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson);
										}
									}
*/
									yield return new WaitForSeconds(0.03f);
								}
							}
//							else
//								Debug.Log ("Year "+yearcheck+ " Month "+monthcheck+" day "+daycheck+ " not found");


							waittildone = true;
							StartCoroutine("CalculateAndJsonAllDailyInformation");
							while (waittildone)
							{
								yield return new WaitForSeconds(0.03f);
							}


// Wait sending to be done of all samples
							while (!bufferme.BufferEmpty())
							{
								yield return new WaitForSeconds(0.03f);
							}
//							yield break;

							while (day_start < day_end+1)
							{
								Debug.Log ("finding to calculate per hour day_start:"+day_start+"  day_end:"+day_end);
								dayatleast1valid = true;
								DateTime startdate = new DateTime(yearcheck, monthcheck, daycheck,day_start,0,0,0); //.ToLocalTime();
//								startdate = startdate.AddHours(-1);
								DateTime enddate = startdate.AddSeconds(60*60 + ni_preparedatafields.SAMPLETIME);
//								Debug.Log (startdate.ToShortDateString() + " " + startdate.ToShortTimeString());
//								Debug.Log (enddate.ToShortDateString() + " " + enddate.ToShortTimeString());
								
								long stime = (long)network.GetMiliseconds(startdate);
								long etime = (long)network.GetMiliseconds(enddate);
								
								DateTime sampledate = startdate.AddMinutes(30);
								long savetime = (long)network.GetMiliseconds(sampledate);
								
								network.GetIntermediateJson(bufferme.GetClientId(),stime.ToString(),etime.ToString(),300,false,"ni_getdata_saintelizaigne looptroughtime day");
								while (network.waitingnetworkintermediate)
								{
									yield return new WaitForSeconds(0.2f);
								}
								Debug.Log ("Receiving:"+network.retreiveintermediatejson);
								if (network.retreiveintermediatejson.Length > 0)
								{
									ni_preparedatafields.CalcAverageHour(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson);
								}
								day_start++;
							}

							while (!bufferme.BufferEmpty())
							{
								yield return new WaitForSeconds(0.03f);
							}

//  TODO fetch day stats here
							if (dayatleast1valid)
							{
								yearatleast1valid = true;
								DateTime startdate = new DateTime(yearcheck, monthcheck, daycheck,0,0,0,0); //.ToLocalTime();
								DateTime enddate = startdate.AddSeconds(24*60*60-5);

								Debug.Log ("Date started :"+startdate.ToShortDateString() + " - " + enddate.ToShortDateString());

								long stime = (long)network.GetMiliseconds(startdate);
								long etime = (long)network.GetMiliseconds(enddate);
								
								DateTime sampledate = startdate.AddHours(12);
								long savetime = (long)network.GetMiliseconds(sampledate);
								
								network.GetIntermediateJson("h"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),26,true,"ni_getdata_saintelizaigne looptroughtime day");
								while (network.waitingnetworkintermediate)
								{
									yield return new WaitForSeconds(0.2f);
								}
//								Debug.Log (network.retreiveintermediatejson);
								if (network.retreiveintermediatejson.Length > 0)
								{
									monthatleast1valid = true;
									ni_preparedatafields.CalcAverageOnBulk(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson,"d");
								}
							}
							yield return new WaitForSeconds(0.05f);		// wait between every day
							daycheck++;
						}
					}
//					else
//						Debug.Log ("Year "+yearcheck+ " Month "+monthcheck+" not found");


					if (monthatleast1valid)
					{
						DateTime startdate = new DateTime(yearcheck, monthcheck, 1,0,0,0,0);//.ToLocalTime();
						DateTime enddate = new DateTime(yearcheck, monthcheck, 1,0,0,0,0);//.ToLocalTime();
						enddate = enddate.AddMonths(1);
						enddate = enddate.AddHours(-1); //-2);
						long stime = (long)network.GetMiliseconds(startdate);
						long etime = (long)network.GetMiliseconds(enddate);
						
						DateTime sampledate = startdate.AddDays(15);
						long savetime = (long)network.GetMiliseconds(sampledate);
						
						network.GetIntermediateJson("d"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),34,true,"ni_getdata_saintelizaigne looptroughtime day");
						while (network.waitingnetworkintermediate)
						{
							yield return new WaitForSeconds(0.2f);
						}
						if (network.retreiveintermediatejson.Length > 0)
						{
							ni_preparedatafields.CalcAverageOnBulk(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson,"m");
						}
					}


					yield return new WaitForSeconds(0.05f);		// wait between every month
					monthcheck++;
				}
			}
//			else
//				Debug.Log ("Year "+yearcheck+ " not found");

// TODO fetch year stats here
			if (yearatleast1valid)
			{
				DateTime startdate = new DateTime(yearcheck, 1, 1,0,0,0,0);//.ToLocalTime();
				DateTime enddate = new DateTime((yearcheck+1), 1, 1,0,0,0,0);//.ToLocalTime();
				enddate = enddate.AddHours(-1); //-2);
				long stime = (long)network.GetMiliseconds(startdate);
				long etime = (long)network.GetMiliseconds(enddate);
				
				DateTime sampledate = startdate.AddDays(15);
				long savetime = (long)network.GetMiliseconds(sampledate);
				
				network.GetIntermediateJson("m"+bufferme.GetClientId(),stime.ToString(),etime.ToString(),34,true,"ni_getdata_saintelizaigne looptroughtime day");
				while (network.waitingnetworkintermediate)
				{
					yield return new WaitForSeconds(0.2f);
				}
				if (network.retreiveintermediatejson.Length > 0)
				{
					ni_preparedatafields.CalcAverageOnBulk(savetime.ToString(),network.retreiveintermediatejson,network.n_retreiveintermediatejson,"y");
				}
			}
			yield return new WaitForSeconds(0.03f);		// wait between every year
			yearcheck++;
		}
	}
}
