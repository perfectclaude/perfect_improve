﻿using UnityEngine;
using System.Collections;

public class ni_endlessturning : MonoBehaviour
{
	public static GameObject waiting = null;

	void Start()
	{
		waiting = gameObject;
		gameObject.SetActiveRecursively (false);
	}

	void Update ()
	{
		Vector3 vec = gameObject.transform.localEulerAngles;
		vec.z += -5.0f;
		gameObject.transform.localEulerAngles = vec;

	}
}
