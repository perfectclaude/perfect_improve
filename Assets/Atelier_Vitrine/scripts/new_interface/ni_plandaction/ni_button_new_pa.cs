﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_button_new_pa : buttonswap
{
//	IDictionary pajson;
//	GameObject info_editplandaction;
//	GameObject info_followplandaction;
	
	void Awake()
	{
//		info_editplandaction = GameObject.Find("info_editplandaction");
//		info_followplandaction = GameObject.Find("info_followplandaction");
		_Awake ();
	}

	void Start()
	{
//		pajson = ni_preparedatafields.LoadPlanDaction("testplanaction.txt");
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		ni_generate_pa_actionlines.EmptyEntirePlandaction();

		long creationdate = (long)network.GetMiliseconds(DateTime.Now);
		creationdate = creationdate / 1000;
		bufferme.SetString("planaction_startdate",creationdate.ToString());

		bufferme.SetString("planaction_name","---");
		bufferme.SetInt("planaction_machine",0);

		ni_manage_screens.SetNextScreen("info_editplandaction");

//		ni_preparedatafields.DistributeCleanJsonData(info_editplandaction,pajson);
//		ni_preparedatafields.DistributeCleanJsonData(info_followplandaction,pajson);
	}
	
}