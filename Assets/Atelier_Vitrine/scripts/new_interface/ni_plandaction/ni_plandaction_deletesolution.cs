﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_deletesolution :  buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_plandaction_listactions.popupinfotext.active == true)	return;

		yesno.phrase = "Do you want to delete the selected item ?";
		yesno.YesNo = true;
		
		StartCoroutine("WaitYesNo");
	}
	
	
	IEnumerator WaitYesNo()
	{
		while (yesno.YesNo)
		{
			yield return new WaitForSeconds(0.09f);
		}
		if (yesno.YesNoDecision)
		{
			string myname = gameObject.transform.parent.gameObject.name.Replace("plan_line","");
			int idnr;
			int.TryParse(myname,out idnr);		// delete this line !!!
			
			int nroflines = bufferme.GetInt("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+bufferme.GetString("planaction_currentissuenr")+"_n");

			int n;
			myname = bufferme.GetString("planaction_currentissuenr");
			int.TryParse(myname,out n);

			string fillit;
			for (int inn=0;inn<nroflines;inn++)
			{
				if (inn > idnr)
				{
					// loop trough all Actions
					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+n+"_"+inn))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+n+"_"+inn);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+n+"_"+(inn-1),fillit);
					
					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+n+"_"+inn))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+n+"_"+inn);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+n+"_"+(inn-1),fillit);
					
					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_name_"+n+"_"+inn))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_name_"+n+"_"+inn);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_name_"+n+"_"+(inn-1),fillit);
					
					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_date_"+n+"_"+inn))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_date_"+n+"_"+inn);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_date_"+n+"_"+(inn-1),fillit);
					
					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_datedone_"+n+"_"+inn))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_datedone_"+n+"_"+inn);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_datedone_"+n+"_"+(inn-1),fillit);
					
					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_valid_"+n+"_"+inn))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_valid_"+n+"_"+inn);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_valid_"+n+"_"+(inn-1),fillit);
				}
			}
			nroflines--;
			bufferme.SetInt("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+bufferme.GetString("planaction_currentissuenr")+"_n",nroflines);
			ni_plandaction_listactions.CloneLines();
			ni_plandaction_editplandaction.RestartListing();
		}
	}
}
