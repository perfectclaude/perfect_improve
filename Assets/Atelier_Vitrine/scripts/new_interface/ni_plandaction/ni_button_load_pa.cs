﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_button_load_pa : buttonswap
{
	GameObject linetoclone = null;
	GameObject filelist;
	GameObject fatherfilelist = null;
	public static IDictionary json;

	void Awake()
	{
		_Awake ();
		linetoclone = gameObject.transform.parent.gameObject.FindInChildren("toclone");
		filelist = gameObject.transform.parent.gameObject.FindInChildren("root_filelist");
		linetoclone.SetActive(false);
	}

	void OnEnable()
	{
		_OnEnable();
		if (linetoclone == null)		return;
		linetoclone.SetActive(false);
		StartCoroutine("FetchPAFileList");
	}

	void Start()
	{
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
	}

	public static void Pressed(string objectname)
	{
		{
			string strval = objectname.Replace("file_line_","");
			int val;
			int.TryParse(strval,out val);
			
			IDictionary	innerjson = null;
			try
			{
				innerjson = (IDictionary)json["sample"+val];
			}
			catch
			{
				innerjson = null;
			}
			if (innerjson == null)		return;
			// here you have the innir json, use it and fill in the needed information !!!
			ni_generate_pa_actionlines.LoadPlanDactionFromJson(innerjson);
		}
		ni_manage_screens.SetNextScreen("info_editplandaction");
	}

	string local_retreiveintermediatejson = "";
	void GetJsonBeforeOverwritten()
	{
		local_retreiveintermediatejson = network.retreiveintermediatejson;
//		Debug.Log ("Gotton Json:"+local_retreiveintermediatejson);
	}

	IEnumerator FetchPAFileList()
	{
		Vector3 posi = Vector3.zero;

		if (fatherfilelist != null)
		{
			Destroy(fatherfilelist);
		}

		fatherfilelist = new GameObject();
		fatherfilelist.transform.parent = filelist.transform;
		fatherfilelist.transform.localPosition = new Vector3(0.367f,0.26f,0);
		fatherfilelist.transform.localScale = Vector3.one;

		yield return new WaitForSeconds(0.2f);
		Debug.Log ("Passhere...");
		if (network.FORCENWOFF)
		{
			string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId () +"/testplanaction.txt";
			if (System.IO.File.Exists(pathname))
			{
				#if UNITY_ANDROID && (!UNITY_EDITOR)
				WWW newwww = new WWW(pathname);
				while (!newwww.isDone)
				{
				}
				local_retreiveintermediatejson = newwww.text;
				#else
				local_retreiveintermediatejson = System.IO.File.ReadAllText(pathname);
				#endif
			}
			else
				local_retreiveintermediatejson = "";
		}
		else
		{
			network.GetIntermediateJson("p"+bufferme.GetClientId(),"0","9999999999",100,false,"ni_button_load_pa",GetJsonBeforeOverwritten);
			while (network.waitingnetworkintermediate)
			{
				yield return new WaitForSeconds(0.2f);
			}
		}

		Debug.Log (local_retreiveintermediatejson);

		if ((local_retreiveintermediatejson != "") && (local_retreiveintermediatejson.Length > 10))
		{
//			Debug.Log("Filelist:"+local_retreiveintermediatejson);
			json = (IDictionary)Json.Deserialize(local_retreiveintermediatejson);
			int count = 0;

//			foreach (var key in json.Keys)
//				Debug.Log (key.ToString());

			while (true)
			{
				IDictionary	innerjson = null;
				try
				{
					innerjson = (IDictionary)json["sample"+count];
				}
				catch
				{
					innerjson = null;
				}
				if (innerjson == null)		break;		// stop my loop
//				Debug.Log ("Show line :"+count);

				GameObject obj = GameObject.Instantiate(linetoclone) as GameObject;
				obj.name = "file_line_"+count;
				obj.transform.parent = fatherfilelist.transform;
				obj.transform.localPosition = posi;
				posi.y -=0.16f;
				obj.transform.localScale = Vector3.one;

				Color col;
				if ((count % 2) == 0)
					col = new Color(0.95f,0.95f,0.95f);
				else
					col = new Color(0.85f,0.85f,0.85f);
				tk2dSlicedSprite sprite = (tk2dSlicedSprite)obj.FindInChildren("name").GetComponent<tk2dSlicedSprite>();
				sprite.color = col;
				sprite = (tk2dSlicedSprite)obj.FindInChildren("machine").GetComponent<tk2dSlicedSprite>();
				sprite.color = col;
				sprite = (tk2dSlicedSprite)obj.FindInChildren("date").GetComponent<tk2dSlicedSprite>();
				sprite.color = col;

				tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("name").FindInChildren("text").GetComponent<tk2dTextMesh>();
				settext.ForceText(tm,(string)innerjson["name"]);

				tm = (tk2dTextMesh)obj.FindInChildren("machine").FindInChildren("text").GetComponent<tk2dTextMesh>();
				long machinenr = (long)innerjson["machine"];
				settext.ForceText(tm,ni_preparedatafields.GetMachineName((int)machinenr));

				tm = (tk2dTextMesh)obj.FindInChildren("date").FindInChildren("text").GetComponent<tk2dTextMesh>();
				string datestr = (string)innerjson["startdate"];
				long datelong;
				long.TryParse(datestr,out datelong);
				DateTime display = new DateTime(1970, 1, 1,0,0,0,0); //.ToLocalTime();
				display = display.AddSeconds(datelong);
				settext.ForceText(tm,display.ToShortDateString());

				obj.SetActive(true);
				count++;
				yield return new WaitForSeconds(0.03f);

			}

		}
	}
}
