﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_plandaction_editplandaction : MonoBehaviour
{
	public static GameObject root_master;
	public static GameObject root_listing = null;
	public static GameObject root_vocabulary = null;
	public static GameObject plan_name = null;
	tk2dTextMesh tmplan_name;
	tk2dTextMesh tmplan_name2;
	string lastplanname = "";

	public static ni_plandaction_listactions listactions;
	public static ni_vocabulary vocabulaire;
	static bool RestartListingNow = false;

	public static void RestartListing()
	{
		RestartListingNow = true;
	}

	public static void RestartPage()
	{
		root_master.SetActive(false);
		root_listing.SetActive(false);
		root_vocabulary.SetActive(false);
		root_master.SetActive(true);
	}

	void Awake ()
	{
		root_master = gameObject.FindInChildren("root_master");
		root_listing = gameObject.FindInChildren("root_listing");
		plan_name = gameObject.FindInChildren("plan_name");
		tmplan_name = (tk2dTextMesh)plan_name.FindInChildren("edittext").GetComponent<tk2dTextMesh>();
		settext.ForceText(tmplan_name,bufferme.GetString("planaction_name"));

		listactions = (ni_plandaction_listactions)root_listing.GetComponent<ni_plandaction_listactions>();
		root_vocabulary = gameObject.FindInChildren("root_vocabulary");
		vocabulaire = (ni_vocabulary)root_vocabulary.FindInChildren("cloneroot").GetComponent<ni_vocabulary>();
	}

	void Start()
	{
		if (root_listing)
			root_listing.SetActive(false);
		if (root_vocabulary)
			root_vocabulary.SetActive(false);
	}

	void OnEnable()
	{
		if (root_listing)
			root_listing.SetActive(false);
		if (root_vocabulary)
			root_vocabulary.SetActive(false);
//		Debug.Log ("NAME LOADED:" + bufferme.GetString("planaction_name"));
		if (plan_name != null)
		{
			string strval = bufferme.GetString("planaction_startdate");
			long lval;
			long.TryParse(strval,out lval);
			DateTime mydate = new DateTime(1970, 1, 1,0,0,0,0); //.ToLocalTime();
			mydate = mydate.AddSeconds(lval);

			bufferme.SetString("planaction_startdate_text",mydate.ToShortDateString()+" "+mydate.ToShortTimeString());

			settext.ForceText(tmplan_name,bufferme.GetString("planaction_name"));
			lastplanname = tmplan_name.text;
		}

	}

	void Update ()
	{
		if (RestartListingNow)
		{
			RestartListingNow = false;
			root_master.SetActive(false);
			root_listing.SetActive(false);
			root_vocabulary.SetActive(false);
			root_listing.SetActive(true);
		}

		if (plan_name != null)
		{
			if (tmplan_name.text != lastplanname)
			{
				lastplanname = tmplan_name.text;
				bufferme.SetString("planaction_name",tmplan_name.text);
			}
		}
	}
}
