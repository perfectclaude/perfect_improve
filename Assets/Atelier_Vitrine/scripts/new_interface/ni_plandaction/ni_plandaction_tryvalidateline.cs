﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_plandaction_tryvalidateline : MonoBehaviour
{
	tk2dSlicedSprite		sprite;
	tk2dTextMesh	myname;
	tk2dTextMesh	action;
	tk2dTextMesh	date;
	tk2dTextMesh	datedone;
	Color			orange = new Color(1.0f,0.6f,0.2f);

	void Awake ()
	{
		sprite = (tk2dSlicedSprite)gameObject.GetComponent<tk2dSlicedSprite>();
		action = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("action").FindInChildren("text").GetComponent<tk2dTextMesh>();
		myname = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("name").FindInChildren("text").GetComponent<tk2dTextMesh>();
		date = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("date").FindInChildren("datetimetext").GetComponent<tk2dTextMesh>();
		datedone = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("datedone").FindInChildren("datetimetext").GetComponent<tk2dTextMesh>();
	}
	
	void Update ()
	{
		bool	red = false;

// verify and update the display ...
		if ((action.text == null) || (action.text.Length < 3))
		{
			red = true;
		}
		if ((myname.text == null) || (myname.text.Length < 3))
		{
			red = true;
		}
		if ((date.text == null) || (date.text.Length < 3))
		{
			red = true;
		}

		if (red)
			sprite.color = Color.red;
		else
		{
// check if date overdue
			if ((datedone.text == null) || (datedone.text.Length < 3))
				sprite.color = orange;
			else
			{
// get datetime
				DateTime dt1;
				dt1 = DateTime.Parse (date.text);
				DateTime dt2;
				dt2 = DateTime.Parse (datedone.text);
				if (DateTime.Compare(dt1, dt2) >  0)
				{
					sprite.color = Color.green;
				}
				else
				{
					sprite.color = Color.yellow;
				}
			}
		}
	}
}
