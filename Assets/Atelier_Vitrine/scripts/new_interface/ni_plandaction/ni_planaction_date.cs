﻿using UnityEngine;
using System.Collections;

public class ni_planaction_date : buttonswap
{
	bool checking = false;
	ni_getsetdatetimefromdropdowns funci = null;

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_plandaction_listactions.popupinfotext.active == true)	return;

		SNAILRUSHDATEPICKER.DatePicker = true;
		GameObject obj = gameObject;
		while (obj != null)
		{
			funci = (ni_getsetdatetimefromdropdowns)obj.GetComponent<ni_getsetdatetimefromdropdowns>();
			if (funci != null)	break;
			obj = obj.transform.parent.gameObject;
		}
		funci.checkdatepicker = true;
		checking = true;
	}

	void Update()
	{
		_Update ();
		if (checking)
		{
			if (!funci.checkdatepicker)
			{
				checking = false;

				string strval = gameObject.transform.parent.gameObject.name.Replace("plan_line","");
				int ival;
				int.TryParse(strval,out ival);
				bufferme.SetInt ("planaction_lineindex",ival);

				tk2dTextMesh datetimetext = (tk2dTextMesh)gameObject.FindInChildren("datetimetext").GetComponent<tk2dTextMesh>();

				bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_"+gameObject.name+"_"+bufferme.GetString("planaction_currentissuenr")+"_"+ival , datetimetext.text);
			}
		}
	}
}
