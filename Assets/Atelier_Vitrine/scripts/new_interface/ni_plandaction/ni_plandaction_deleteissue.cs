﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_deleteissue : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_plandaction_listactions.popupinfotext.active == true)	return;

		yesno.phrase = "Do you want to delete the selected item ?";
		yesno.YesNo = true;

		StartCoroutine("WaitYesNo");
	}


	IEnumerator WaitYesNo()
	{
		while (yesno.YesNo)
		{
			yield return new WaitForSeconds(0.09f);
		}
		if (yesno.YesNoDecision)
		{
			string myname = bufferme.GetString("planaction_currentissuenr");
			int idnr;
			int.TryParse(myname,out idnr);		// delete this line !!!

			int nroflines = bufferme.GetInt("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_n");
			
			for (int n=0;n<nroflines;n++)
			{
				if (n > idnr)
				{
					string fillit;
					int nrofactions;

					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_issue_"+n))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_issue_"+n);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_issue_"+(n-1),fillit);

					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+n))
						fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_issue_"+n);
					else
						fillit = "";
					bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+(n-1),fillit);

					if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+n+"_n"))
						nrofactions = bufferme.GetInt("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+n+"_n");
					else
						nrofactions = 0;
					bufferme.SetInt("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+(n-1)+"_n",nrofactions);

					for (int inn=0;inn<nrofactions;inn++)
					{
						// loop trough all Actions
						if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+n+"_"+inn))
							fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+n+"_"+inn);
						else
							fillit = "";
						bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+(n-1)+"_"+inn,fillit);

						if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+n+"_"+inn))
							fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+n+"_"+inn);
						else
							fillit = "";
						bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+(n-1)+"_"+inn,fillit);

						if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_name_"+n+"_"+inn))
							fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_name_"+n+"_"+inn);
						else
							fillit = "";
						bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_name_"+(n-1)+"_"+inn,fillit);

						if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_date_"+n+"_"+inn))
							fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_date_"+n+"_"+inn);
						else
							fillit = "";
						bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_date_"+(n-1)+"_"+inn,fillit);
						
						if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_datedone_"+n+"_"+inn))
							fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_datedone_"+n+"_"+inn);
						else
							fillit = "";
						bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_datedone_"+(n-1)+"_"+inn,fillit);

						if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_valid_"+n+"_"+inn))
							fillit = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_valid_"+n+"_"+inn);
						else
							fillit = "";
						bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_valid_"+(n-1)+"_"+inn,fillit);
					}
				}
			}
			nroflines--;
			bufferme.SetInt("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_n",nroflines);
			ni_plandaction_listactions.CloneLines();

			ni_plandaction_editplandaction.RestartListing();
		}
	}
}
