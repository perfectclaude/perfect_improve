﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_solution : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_plandaction_listactions.popupinfotext.active == true)	return;

		string strval = gameObject.transform.parent.gameObject.name.Replace("plan_line","");
		int ival;
		int.TryParse(strval,out ival);
		bufferme.SetInt ("planaction_lineindex",ival);

		bufferme.SetInt("vocabularyforceregulartreat",1);
		bufferme.SetInt ("vocabularytype",1);		// 0 is Action dico
		ni_plandaction_editplandaction.vocabulaire.savetype = "solution";					// change this if other item to vocabulate

		ni_plandaction_listactions.Vocabulary_TopDescription("Problème : " + bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_issue_"+bufferme.GetString("planaction_currentissuenr")));
//		string texttoshow = "Action for : " + bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_issue_"+bufferme.GetString("planaction_currentissuenr")+"_"+ival);
//		settext.ForceText(ni_plandaction_listactions.subtitle2,texttoshow);

		ni_plandaction_editplandaction.root_vocabulary.SetActive(true);
		ni_plandaction_editplandaction.root_listing.SetActive(false);
		ni_plandaction_editplandaction.root_master.SetActive(false);
	}

}
