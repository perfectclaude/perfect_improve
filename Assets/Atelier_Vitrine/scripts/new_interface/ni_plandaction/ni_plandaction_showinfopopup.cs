﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_showinfopopup : buttonswap
{
	public string popuptype = "issue";
	tk2dTextMesh	tm;

	void Awake()
	{
		_Awake();
		GameObject obj = GameObject.Find("info_editplandaction");
		obj = obj.FindInChildren("root_listing");
		obj = obj.FindInChildren("popupinfotext");

		tm = (tk2dTextMesh)obj.FindInChildren("value").GetComponent<tk2dTextMesh>();
	}
	
	public override void ButtonPressed ()
	{
		string mytext = "";

		base.ButtonPressed();
		if (ni_plandaction_listactions.popupinfotext.active == true)	return;

		if (popuptype == "issue")
		{
			bufferme.SetString("plandaction_popuptype",popuptype);
			if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+bufferme.GetString("planaction_currentissuenr")))
				mytext = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+bufferme.GetString("planaction_currentissuenr"));
			else
				mytext = "";
		}
		if (popuptype == "solution")
		{
			bufferme.SetString("plandaction_popuptype",popuptype);

			string strval = gameObject.transform.parent.gameObject.name.Replace("plan_line","");
			int ival;
			int.TryParse(strval,out ival);
			bufferme.SetInt ("planaction_lineindex",ival);

			if (bufferme.HasKey("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+bufferme.GetString("planaction_currentissuenr")+"_"+ival))
				mytext = bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+bufferme.GetString("planaction_currentissuenr")+"_"+ival);
			else
				mytext = "";
		}

		if (mytext != "")
			settext.ForceText(tm,mytext);
		else
			settext.ForceText(tm,"selectionnez ici pour entrez un text...");

		ni_plandaction_listactions.popupinfotext.SetActive(true);
	}
}
