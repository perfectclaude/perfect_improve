﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_addword : buttonswap
{
	void OnEnable()
	{
		_OnEnable();
		GameObject obj = gameObject.transform.parent.parent.gameObject;

		tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("value").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,"selectionnez ici pour entrez un text...");
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		GameObject obj = gameObject.transform.parent.parent.gameObject;

		tk2dTextMesh tm = (tk2dTextMesh)obj.FindInChildren("value").GetComponent<tk2dTextMesh>();
		ni_vocabulary.AddNewVocabulary(tm.text);
		ni_vocabulary.saveme = true;
		obj.SetActive(false);
	}
}
