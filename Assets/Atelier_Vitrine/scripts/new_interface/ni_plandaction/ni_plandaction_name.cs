﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_name : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_plandaction_listactions.popupinfotext.active == true)	return;

		ni_plandaction_listactions.Vocabulary_TopDescription("");
		bufferme.SetInt ("vocabularytype",2);		// 0 is Names dico
		ni_plandaction_editplandaction.vocabulaire.savetype = "name";					// change this if other item to vocabulate

		string strval = gameObject.transform.parent.gameObject.name.Replace("plan_line","");
		int ival;
		int.TryParse(strval,out ival);
		bufferme.SetInt ("planaction_lineindex",ival);

		string texttoshow = "Responable to : " + bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_solution_"+bufferme.GetString("planaction_currentissuenr")+"_"+ival);
		
		settext.ForceText(ni_plandaction_listactions.subtitle2,texttoshow);

		ni_plandaction_editplandaction.root_vocabulary.SetActive(true);
		ni_plandaction_editplandaction.root_listing.SetActive(false);
		ni_plandaction_editplandaction.root_master.SetActive(false);
	}
	
}
