﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_plusissue : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		bufferme.SetInt ("vocabularytype",0);		// 0 is ISSUE dico
		ni_plandaction_editplandaction.vocabulaire.savetype = "issue";					// change this if other item to vocabulate

		ni_plandaction_editplandaction.root_vocabulary.SetActive(true);
		ni_plandaction_editplandaction.root_listing.SetActive(false);
		ni_plandaction_editplandaction.root_master.SetActive(false);
	}
	

}
