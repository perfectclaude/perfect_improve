﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_masterbutton : buttonswap
{
	public string itemname;
	GameObject	clonedot;
	GameObject	clonefather = null;
	GameObject noballtext = null;

	void Awake()
	{
		_Awake ();
		clonedot = gameObject.FindInChildren("balltoclone");
		noballtext = gameObject.FindInChildren("noballtext");
	}

	void OnEnable()
	{
		clonedot.SetActive(false);
		Vector3 posi = new Vector3(-0.28f,-0.18f,-0.1f);
		_OnEnable();

		int selectedmachine = bufferme.GetInt("planaction_machine");
//		Debug.Log ("Find machine " + selectedmachine + " dots");
		string strval = gameObject.name.Replace("button_plandaction_master","");
		int buttonid;
		int.TryParse(strval,out buttonid);
		int nroflines = bufferme.GetInt("planaction"+selectedmachine+ "_" + buttonid+"_n");
		// create nroflinesballs
		if (clonefather != null)
		{
			Destroy(clonefather);
		}
		clonefather = new GameObject();
		clonefather.name = "clonefather";
		clonefather.transform.parent = gameObject.transform;
		clonefather.transform.localPosition = Vector3.zero;
		clonefather.transform.localScale = Vector3.one;

		if (nroflines > 0)
			noballtext.SetActive(false);
		else
			noballtext.SetActive(true);

		for (int n=0;n<nroflines;n++)
		{
			GameObject obj = GameObject.Instantiate(clonedot)as GameObject;
			obj.name = "ball"+n;
			obj.transform.parent = clonefather.transform;
			obj.transform.localPosition = posi;
			obj.transform.localScale = Vector3.one;
			obj.SetActive(true);
			posi.x += 0.07f;
			if (posi.x > 0.28f)
			{
				posi.x = -0.28f;
				posi.y -= 0.03f;
			}
			tk2dSprite sprite = (tk2dSprite)obj.GetComponent<tk2dSprite>();
			sprite.SetSprite(sprite.GetSpriteIdByName("new_pa_point1"));

			bool red = false;
			bool orange = false;

			int nsolutions = bufferme.GetInt("planaction"+selectedmachine+"_"+buttonid+"_solution_"+n+"_n");
			if (nsolutions == 0)		red = true;
			else
			{
				for (int myn=0;myn<nsolutions;myn++)
				{
					string income = bufferme.GetString("planaction"+selectedmachine+"_"+buttonid+"_solution_"+n+"_"+myn);
					if ((income == null) || (income.Length < 3))		red = true;
					income = bufferme.GetString("planaction"+selectedmachine+"_"+buttonid+"_name_"+n+"_"+myn);
					if ((income == null) || (income.Length < 3))		red = true;
					income = bufferme.GetString("planaction"+selectedmachine+"_"+buttonid+"_date_"+n+"_"+myn);
					if ((income == null) || (income.Length < 3))		red = true;

					income = bufferme.GetString("planaction"+selectedmachine+"_"+buttonid+"_datedone_"+n+"_"+myn);
					if ((income == null) || (income.Length < 3))		orange = true;
				}
			}
			if (red)
			{
				sprite.color = Color.red;
			}
			else
			{
				if (orange)
					sprite.color = new Color(1.0f,0.6f,0.2f);
				else
				{
					sprite.SetSprite(sprite.GetSpriteIdByName("new_pa_point2"));
					sprite.color = Color.white;
				}
			}

		}
	}

	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		string strval = gameObject.name.Replace("button_plandaction_master","");
		int buttonid;
		int.TryParse(strval,out buttonid);
		ni_plandaction_listactions.selecteditem = buttonid;
		bufferme.SetInt("planaction_currentitem",buttonid);

		ni_plandaction_editplandaction.listactions.CreateList(bufferme.GetString("planaction_name") + " " + bufferme.GetString("planaction_startdate_text") + " " + ni_preparedatafields.GetMachineName(ni_plandaction_listactions.selectedmachine)+ " " + itemname);
		ni_plandaction_editplandaction.root_listing.SetActive(true);
		ni_plandaction_editplandaction.root_master.SetActive(false);
	}
}
