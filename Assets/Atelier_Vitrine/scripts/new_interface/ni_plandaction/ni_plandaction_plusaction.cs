﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_plusaction : buttonswap
{
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
		if (ni_plandaction_listactions.popupinfotext.active == true)	return;

		bufferme.DeleteKey("vocabularyforceregulartreat");

		ni_plandaction_listactions.Vocabulary_TopDescription("Problème : " + bufferme.GetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_issue_"+bufferme.GetString("planaction_currentissuenr")));

		bufferme.SetInt ("vocabularytype",1);		// 0 is ISSUE dico
		ni_plandaction_editplandaction.vocabulaire.savetype = "solution";					// change this if other item to vocabulate

		string basicstring = "planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem;
		string endstring = bufferme.GetString("planaction_currentissuenr");
		int index = bufferme.GetInt(basicstring+"_solution_"+endstring+"_n");

		Debug.Log(basicstring+"_solution_"+endstring+"_n : "+index);

		bufferme.DeleteKey(basicstring+"_solution_"+endstring+"_"+index);
		bufferme.DeleteKey(basicstring+"_info_"+endstring+"_"+index);
		bufferme.DeleteKey(basicstring+"_name_"+endstring+"_"+index);
		bufferme.DeleteKey(basicstring+"_date_"+endstring+"_"+index);
		bufferme.DeleteKey(basicstring+"_datedone_"+endstring+"_"+index);
		bufferme.DeleteKey(basicstring+"_valid_"+endstring+"_"+index);

		ni_plandaction_editplandaction.root_vocabulary.SetActive(true);
		ni_plandaction_editplandaction.root_listing.SetActive(false);
		ni_plandaction_editplandaction.root_master.SetActive(false);
	}
	
	
}
