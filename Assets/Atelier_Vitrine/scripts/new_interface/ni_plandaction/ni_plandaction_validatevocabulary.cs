﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_plandaction_validatevocabulary : buttonswap
{
	ni_vocabulary myvocabulary;
	tk2dTextMesh phrase;
	pi_planactiontabs	tabcenter2 = null;

	void Awake()
	{
		_Awake();
		myvocabulary = (ni_vocabulary)gameObject.transform.parent.gameObject.GetComponent<ni_vocabulary>();
		phrase = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("phrase").GetComponent<tk2dTextMesh>();

		GameObject fatherofitems = gameObject.transform.parent.parent.parent.gameObject;
		fatherofitems = fatherofitems.FindInChildren("items_pa");
		if (fatherofitems != null)
		{
			tabcenter2 = (pi_planactiontabs)fatherofitems.GetComponent<pi_planactiontabs>();
		}
	}

	string KeepPhrase = "";

	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		if (ni_vocabulary.popup.active == true)		return;
// Vocab attributed
		if (ni_vocabulary.saveme)
		{
			ni_vocabulary.saveme = false;
// Serialize and save the JSON here !!!!
			string myjson = (string)Json.Serialize(ni_vocabulary.dictionarylevels[0]);	// get entire Json
//			Debug.Log ("Created Json: " + myjson);
			// try and save the vocabulary ... use MACHINE + ITEM to define which one to load
			int keytosave = 1+(bufferme.GetInt("planaction_machine")*1000 + bufferme.GetInt("planaction_currentitem"))*20 + bufferme.GetInt ("vocabularytype");
//			Debug.Log ("saving : "+ keytosave);
			if (network.FORCENWOFF)
			{
				string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId () +"/v" + bufferme.GetClientId() + keytosave.ToString()+".txt";
				System.IO.File.WriteAllText(pathname,myjson);
			}
			else
			{
				bufferme.AddToBuffer(myjson,keytosave.ToString(),"v");
			}
		}

//		Debug.Log ("Validated return : "+myvocabulary.savetype);
		if (bufferme.HasKey("vocabularyforceregulartreat"))
		{
			bufferme.DeleteKey("vocabularyforceregulartreat");
			int nroflines = bufferme.GetInt("planaction_lineindex");
			Debug.Log ("Line to change "+nroflines);
			bufferme.SetString("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_"+myvocabulary.savetype+"_"+tabcenter2.selectedtabobject+"_"+nroflines,phrase.text);
		}
		else
		{
			if (myvocabulary.savetype == "issue")
			{
				KeepPhrase = phrase.text;
				ni_plandaction_listactions.Vocabulary_TopDescription("Définisez le problème de \"" + KeepPhrase+ "\"");
				bufferme.SetInt ("vocabularytype",3);		// 0 is ISSUE dico
				ni_plandaction_editplandaction.vocabulaire.savetype = "issue2";					// change this if other item to vocabulate
				
				ni_plandaction_editplandaction.root_vocabulary.SetActive(true);
				ni_plandaction_editplandaction.root_listing.SetActive(false);
				ni_plandaction_editplandaction.root_master.SetActive(false);

				ni_vocabulary.ForceOnEnable();
				return;
			}
			else
			{
				if (myvocabulary.savetype == "issue2")
				{
					myvocabulary.selectedmachine = ni_plandaction_listactions.selectedmachine;
					myvocabulary.selecteditem = ni_plandaction_listactions.selecteditem;

					KeepPhrase = KeepPhrase + " " + phrase.text;
					int nroflines = bufferme.GetInt("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_n");
					bufferme.SetString("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_issue_"+nroflines,KeepPhrase);
					// must be empty !!! No solutions yet
					bufferme.SetInt("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_solution_"+nroflines+"_n",0);
					nroflines++;
					bufferme.SetInt("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_n",nroflines);


					Debug.Log ("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_n");
					Debug.Log ("Adding issue..."+nroflines);
				}
				else
				{
					if (myvocabulary.savetype == "solution")
					{
						// must increase nr of solutions here !!!!
						int nroflines = bufferme.GetInt("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_solution_"+tabcenter2.selectedtabobject+"_n");
						//					Debug.Log ("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_solution_"+tabcenter2.selectedtabobject+"_n : "+nroflines);
						bufferme.SetString("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_solution_"+tabcenter2.selectedtabobject+"_" + nroflines,phrase.text);
						//					Debug.Log("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_solution_"+tabcenter2.selectedtabobject+"_" + nroflines + "=" +phrase.text);
						nroflines++;
						bufferme.SetInt("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_solution_"+tabcenter2.selectedtabobject+"_n",nroflines);
						ni_plandaction_listactions.CloneLines();
					}
					else
					{
						int nroflines = bufferme.GetInt ("planaction_lineindex");
						bufferme.SetString("planaction"+myvocabulary.selectedmachine+"_" + myvocabulary.selecteditem+"_"+myvocabulary.savetype+"_"+tabcenter2.selectedtabobject+"_"+nroflines,phrase.text);
					}
				}
			}
		}

		ni_plandaction_editplandaction.root_vocabulary.SetActive(false);
		ni_plandaction_editplandaction.root_listing.SetActive(true);
		ni_plandaction_editplandaction.root_master.SetActive(false);
	}
}
