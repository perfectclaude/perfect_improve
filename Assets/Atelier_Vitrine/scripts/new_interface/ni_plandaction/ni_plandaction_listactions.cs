﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_listactions : MonoBehaviour
{
	static GameObject cloneline = null;
	static GameObject plusline;
	static GameObject fatherofclones = null;
	static GameObject mygameObject;
	tk2dTextMesh title = null;
	public static tk2dTextMesh title2 = null;
	public static tk2dTextMesh subtitle2 = null;

	public static int selectedmachine = 0;
	public static int selecteditem = 0;
	ni_plandaction_plusissue plusissue = null;
	static pi_planactiontabs	tabcenter2 = null;
	public static GameObject popupinfotext = null;

	void Awake ()
	{
		mygameObject = gameObject;
		cloneline = gameObject.FindInChildren("toclone");
		plusline = gameObject.FindInChildren("plus");
		plusissue = (ni_plandaction_plusissue)plusline.GetComponent<ni_plandaction_plusissue>();
		popupinfotext = gameObject.FindInChildren("popupinfotext");
//		Debug.Log ("Found :"+popupinfotext.name);

		GameObject obj = gameObject.transform.parent.gameObject;
		obj = obj.FindInChildren("whitebg");
		obj = obj.FindInChildren("topborder");
		title = (tk2dTextMesh)obj.FindInChildren("title").GetComponent<tk2dTextMesh>();

		GameObject fatherofitems = gameObject.FindInChildren("items_pa");
		if (fatherofitems != null)
		{
			tabcenter2 = (pi_planactiontabs)fatherofitems.GetComponent<pi_planactiontabs>();
		}

		title2 = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("root_vocabulary").FindInChildren("vocabtitle").GetComponent<tk2dTextMesh>();
		subtitle2 = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("root_vocabulary").FindInChildren("subtitle").GetComponent<tk2dTextMesh>();

		cloneline.SetActive(false);
		popupinfotext.SetActive(false);
	}
	
	public void CreateList(string incometitle)
	{
		if (title != null)
			settext.ForceText(title,incometitle);
		if (title2 != null)
			settext.ForceText(title2,"");
		if (subtitle2 != null)
			settext.ForceText(subtitle2,"");
	}

	public static void Vocabulary_TopDescription(string incometitle)
	{
		if (title2 != null)
			settext.ForceText(title2,incometitle);
	}

	public static void Vocabulary_title(string incometitle)
	{
		if (subtitle2 != null)
			settext.ForceText(subtitle2,incometitle);
	}

	void OnEnable()
	{
		if (popupinfotext != null)
			popupinfotext.SetActive(false);
//		Debug.Log ("Trying :"+popupinfotext.name);

		if (plusissue == null)		return;

		selectedmachine = bufferme.GetInt("planaction_machine");
		CloneLines();
	}

	static void DisplayItem(GameObject obj,string item,string textitem,string paramtype,int count)
	{
		GameObject newobj = obj.FindInChildren(item);
		tk2dSlicedSprite sprite = newobj.GetComponent<tk2dSlicedSprite>();
		newobj = newobj.FindInChildren(textitem);
		if (newobj != null)
		{
			tk2dTextMesh tm = (tk2dTextMesh)newobj.GetComponent<tk2dTextMesh>();
			string showtext = "";
			if (bufferme.HasKey("planaction"+selectedmachine+"_" + selecteditem+paramtype))
				showtext = bufferme.GetString("planaction"+selectedmachine+"_" + selecteditem+paramtype);
			settext.ForceText(tm,showtext);
		}

		if ((count % 2) == 0)
			sprite.color = new Color(0.9f,0.9f,0.9f);
		else
			sprite.color = new Color(0.7f,0.7f,0.7f);
	}

	public static void CloneLines()
	{
		Vector3 posi = new Vector3(0.1016f,0.308f,0.0f);

		if (cloneline == null)		return;
		cloneline.SetActive(false);

		if (fatherofclones != null)
		{
			plusline.transform.parent = mygameObject.transform;		// detatch plusline
			fatherofclones.transform.parent = null;
			Destroy (fatherofclones);
		}
		fatherofclones = new GameObject();
		fatherofclones.transform.parent = mygameObject.transform;
		fatherofclones.transform.localPosition = Vector3.zero;
		fatherofclones.transform.localScale = Vector3.one;
// recreate all clones
		int nroflines = bufferme.GetInt("planaction"+selectedmachine+"_"+selecteditem+"_n");
		if (nroflines == 0)
		{
			plusline.SetActive(false);
			return;
		}
		else
		{
			plusline.SetActive(true);
		}

// normally n must be the current selected tab
		int n = tabcenter2.selectedtabobject;

		bufferme.SetString("planaction_currentissuenr",n.ToString());
		nroflines = bufferme.GetInt("planaction"+selectedmachine+"_"+selecteditem+"_solution_"+n+"_n");
//		Debug.Log ("planaction"+selectedmachine+"_"+selecteditem+"_solution_"+n+"_n " + nroflines);
		for (int countn=0;countn<nroflines;countn++)
		{
			GameObject obj = GameObject.Instantiate(cloneline) as GameObject;
			obj.name = "plan_line"+countn;
			obj.transform.parent = fatherofclones.transform;
			obj.transform.localScale = Vector3.one;
			obj.transform.localPosition = posi;
			obj.SetActive(true);
// Set Texts
			GameObject newobj;
			/*
			newobj = obj.FindInChildren("issue").FindInChildren("text");
			tk2dTextMesh tm = (tk2dTextMesh)newobj.GetComponent<tk2dTextMesh>();
			string showtext = "";
			if (bufferme.HasKey("planaction"+selectedmachine+"_" + selecteditem +"_issue_"+n))
				showtext = bufferme.GetString("planaction"+selectedmachine+"_" + selecteditem+"_issue_"+n);
			settext.ForceText(tm,showtext);
			*/

			DisplayItem(obj,"action","text","_solution_"+n+"_"+countn,countn);
			DisplayItem(obj,"name","text","_name_"+n+"_"+countn,countn);
			DisplayItem(obj,"date","datetimetext","_date_"+n+"_"+countn,countn);
			DisplayItem(obj,"datedone","datetimetext","_datedone_"+n+"_"+countn,countn);

			posi.y -= 0.15f;
		}
		plusline.transform.parent = fatherofclones.transform;		// reattach plusline
		posi.y += 0.05f;

		plusline.transform.localPosition = posi;
		ni_plandaction_editplandaction.vocabulaire.selectedmachine = selectedmachine;
		ni_plandaction_editplandaction.vocabulaire.selecteditem = selecteditem;
	}

	void Update ()
	{
	
	}
}
