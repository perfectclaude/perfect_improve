﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;
using System.Globalization;
using BestHTTP;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Security;
using System.Threading;

public class ni_generate_pa_actionlines : MonoBehaviour
{
	GameObject clone;

	void Awake ()
	{
		clone = gameObject.FindInChildren("action0");
		Vector3 vec = clone.transform.position;
		float ypos = vec.y;

		for (int i=1;i<10;i++)
		{
			GameObject obj = (GameObject)GameObject.Instantiate(clone) as GameObject;
			vec.y = ypos - i * 0.16f;
			obj.transform.position = vec;
			obj.transform.parent = clone.transform.parent;
			obj.name = "action"+i;
		}
	}




	// WARNING !!! Max 100 machines / 100 Sections (we have 3-6 at the current stage)
	static string lastsavedPlanDaction = "";

	public static void SavePlanDaction(string filename)
	{
		int machine=bufferme.GetInt("planaction_machine");
		int nritems=0;
		bool savedonealready = false;

		// create json
		string json = "{ ";
		//		json = json + "\"version\" : \"" + DateTime.Now.ToLongDateString() + "\",";
		json = json + "\"name\" : \"" + bufferme.GetString("planaction_name") + "\",";
		json = json + "\"startdate\" : \"" + bufferme.GetString("planaction_startdate") + "\",";
		json = json + "\"machine\" : " + machine + ",";		// current machine

		for (machine=0;machine<100;machine++)
		{
			for (int nn=0;nn<100;nn++)
			{
				if (!bufferme.HasKey("planaction"+machine+"_"+nn+"_n"))		continue;		// no actions here ... just continue

				if (savedonealready)	json = json + ",";

				json = json + "\"" + machine + "_" + nn + "\" : { ";
				int nrlines = bufferme.GetInt("planaction"+machine+"_"+nn+"_n");

				for (int i=0;i<nrlines;i++)
				{
					if (bufferme.HasKey("planaction"+machine+"_"+nn+"_issue_"+i))
					{
						json = json + "\""+machine+"_"+nn+"_issue_"+i+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_issue_"+i) + "\",";
					}
					else
					{
						json = json + "\""+machine+"_"+nn+"_issue_"+i+"\" : \"\",";
					}

					if (bufferme.HasKey("planaction"+machine+"_"+nn+"_info_"+i))
					{
						json = json + "\""+machine+"_"+nn+"_info_"+i+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_info_"+i) + "\",";
					}
					else
					{
						json = json + "\""+machine+"_"+nn+"_info_"+i+"\" : \"\"";
					}

					int nraction = bufferme.GetInt("planaction"+machine+"_"+nn+"_solution_"+i+"_n");

					if (nraction > 0)	json = json + ",";

					// each issue should have X actions, including action, name, data, datedone, info, valid
					for (int n=0;n<nraction;n++)
					{
						if (n > 0)	json = json + ",";

						if (bufferme.HasKey("planaction"+machine+"_"+nn+"_solution_"+i+"_"+n))
						{
							json = json + "\""+machine+"_"+nn+"_solution_"+i+"_"+n+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_solution_"+i+"_"+n) + "\",";
						}
						else
						{
							json = json + "\""+machine+"_"+nn+"_solution_"+i+"_"+n+"\" : \"\",";
						}

						if (bufferme.HasKey("planaction"+machine+"_"+nn+"_date_"+i+"_"+n))
						{
							json = json + "\""+machine+"_"+nn+"_date_"+i+"_"+n+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_date_"+i+"_"+n) + "\",";
						}
						else
						{
							json = json + "\""+machine+"_"+nn+"_date_"+i+"_"+n+"\" : \"\",";
						}

						if (bufferme.HasKey("planaction"+machine+"_"+nn+"_datedone_"+i+"_"+n))
						{
							json = json + "\""+machine+"_"+nn+"_datedone_"+i+"_"+n+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_datedone_"+i+"_"+n) + "\",";
						}
						else
						{
							json = json + "\""+machine+"_"+nn+"_datedone_"+i+"_"+n+"\" : \"\",";
						}

						if (bufferme.HasKey("planaction"+machine+"_"+nn+"_name_"+i+"_"+n))
						{
							json = json + "\""+machine+"_"+nn+"_name_"+i+"_"+n+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_name_"+i+"_"+n) + "\",";
						}
						else
						{
							json = json + "\""+machine+"_"+nn+"_name_"+i+"_"+n+"\" : \"\",";
						}

						if (bufferme.HasKey("planaction"+machine+"_"+nn+"_valid_"+i+"_"+n))
						{
							json = json + "\""+machine+"_"+nn+"_valid_"+i+"_"+n+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_valid_"+i+"_"+n) + "\",";
						}
						else
						{
							json = json + "\""+machine+"_"+nn+"_valid_"+i+"_"+n+"\" : \"\",";
						}

						if (bufferme.HasKey("planaction"+machine+"_"+nn+"_info_"+i+"_"+n))
						{
							json = json + "\""+machine+"_"+nn+"_info_"+i+"_"+n+"\" : \"" + bufferme.GetString("planaction"+machine+"_"+nn+"_info_"+i+"_"+n) + "\"";
						}
						else
						{
							json = json + "\""+machine+"_"+nn+"_info_"+i+"_"+n+"\" : \"\"";
						}
					}

					if (i < nrlines-1)
						json = json + ",";
				}
				json = json + " }";
				savedonealready = true;
			}
		}


		json = json + " }";

		//j		Debug.Log ("SAVE:"+json);
		//		Debug.Log ("==Saving JSON pland'action");

		if (json != lastsavedPlanDaction)
		{
			lastsavedPlanDaction = json;
			if (network.FORCENWOFF)
			{
				string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId () +"/"+ filename;
				if (System.IO.File.Exists(pathname))
				{
					// load json
					#if UNITY_ANDROID && (!UNITY_EDITOR)
					WWW newwww = new WWW(pathname);
					while (!newwww.isDone)
					{
					}
					string fullfile = newwww.text;
					#else
					string fullfile = System.IO.File.ReadAllText(pathname);
					#endif

					IDictionary fulljson = (IDictionary)Json.Deserialize(fullfile);
					string newfullfile = "{ ";
					int indexcounter = 0;
					int indexcountersave = 0;

					IDictionary innerdict = (IDictionary)fulljson["sample"+indexcounter];
					while (innerdict != null)
					{
						if ((bufferme.GetString("planaction_name") != (string)innerdict["name"]))
						{
							if (indexcountersave != 0)						newfullfile += " , ";

							string myminijson = (string)Json.Serialize(innerdict);
							newfullfile += " \"sample" + indexcountersave + "\" : " + myminijson;
							indexcountersave++;
						}
						indexcounter++;
						innerdict = (IDictionary)fulljson["sample"+indexcountersave];
					}
					if (indexcountersave != 0)						newfullfile += " , ";
					newfullfile += "\"sample" + indexcountersave + "\" : " + json;

					newfullfile += " }";
					System.IO.File.WriteAllText(pathname,newfullfile);
				}
				else
				{
					System.IO.File.WriteAllText(pathname,"{  \"sample0\" : " + json + " }");
				}
			}
			else
			{		// JSON to network
				bufferme.AddToBuffer(json,bufferme.GetString("planaction_startdate"),"p","plan");		// keyword plan
			}
		}
	}

	static int maxplandactionleftovers = 25;


	public static void EmptyEntirePlandaction()
	{
		for (int n=0;n<maxplandactionleftovers;n++)
		{
			for (int nn=0;nn<maxplandactionleftovers;nn++)
			{
				if (bufferme.HasKey("planaction"+n+"_"+nn+"_n"))
				{
					bufferme.DeleteKeyQuick("planaction"+n+"_"+nn+"_n");
					for (int i=0;i<maxplandactionleftovers;i++)
					{
						//						if (bufferme.HasKey("planaction"+n+"_"+nn+"_issue_"+i))
						{
							bufferme.DeleteKeyQuick("planaction"+n+"_"+nn+"_issue_"+i);
							bufferme.DeleteKeyQuick("planaction"+n+"_"+nn+"_solution_"+i+"_n");
							bufferme.DeleteKeyQuick("planaction"+n+"_"+nn+"_info_"+i);
						}
					}
				}
			}
		}
		PlayerPrefs.Save();
	}

	public static void LoadPlanDactionFromJson(IDictionary json)
	{
		IDictionary itemjson;

		EmptyEntirePlandaction();

		bufferme.SetString("planaction_startdate",(string)json["startdate"]);
		bufferme.SetString("planaction_name",(string)json["name"]);
		bufferme.SetInt("planaction_machine",(int)((long)json["machine"]));

		for (int machine=0;machine<100;machine++)
		{
			for (int i=0;i<100;i++)
			{
				string keytofind = machine+"_"+i;
				itemjson = null;
				try
				{
					itemjson = (IDictionary)json[keytofind];
				}
				catch
				{
					itemjson = null;
				}
				if (itemjson != null)
				{
					int counter = 0;

					while (true)
					{
						string income = null;
						try
						{
							income = (string)itemjson[machine+"_"+i+"_issue_"+counter];
						}
						catch
						{
							income = null;
						}
						if (income != null)
						{
							bufferme.SetString("planaction"+machine+"_"+i+"_issue_"+counter,income);
						}
						else
							break;

						// all others MUST be here
						int actioncounter = 0;
						while (true)
						{
							try
							{
								income = (string)itemjson[machine+"_"+i+"_solution_"+counter+"_"+actioncounter];
							}
							catch
							{
								income = null;
							}
							if (income != null)
							{
								bufferme.SetString("planaction"+machine+"_"+i+"_solution_"+counter+"_"+actioncounter,income);
							}
							else
								break;
							// get the items now !!!
							income = (string)itemjson[machine+"_"+i+"_date_"+counter+"_"+actioncounter];
							bufferme.SetString("planaction"+machine+"_"+i+"_date_"+counter+"_"+actioncounter,income);
							income = (string)itemjson[machine+"_"+i+"_datedone_"+counter+"_"+actioncounter];
							bufferme.SetString("planaction"+machine+"_"+i+"_datedone_"+counter+"_"+actioncounter,income);
							income = (string)itemjson[machine+"_"+i+"_name_"+counter+"_"+actioncounter];
							bufferme.SetString("planaction"+machine+"_"+i+"_name_"+counter+"_"+actioncounter,income);
							income = (string)itemjson[machine+"_"+i+"_valid_"+counter+"_"+actioncounter];
							bufferme.SetString("planaction"+machine+"_"+i+"_valid_"+counter+"_"+actioncounter,income);
							income = (string)itemjson[machine+"_"+i+"_info_"+counter+"_"+actioncounter];
							bufferme.SetString("planaction"+machine+"_"+i+"_info_"+counter+"_"+actioncounter,income);

							actioncounter++;
						}
						bufferme.SetInt("planaction"+machine+"_"+i+"_solution_"+counter+"_n",actioncounter);		// solution counter

						counter++;
					}
					bufferme.SetInt("planaction"+machine+"_"+i+"_n",counter);
				}
			}
		}
	}

	public static void LoadPlanDaction(string filename)
	{
		#if UNITY_ANDROID && (!UNITY_EDITOR)
		WWW newwww = new WWW(Application.streamingAssetsPath + "/" + bufferme.GetClientId () +"/"+ filename);
		while (!newwww.isDone)
		{
		}
		string textjson = newwww.text;
		#else
		string textjson = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/" + bufferme.GetClientId () +"/"+ filename);
		#endif

		IDictionary json = (IDictionary)Json.Deserialize (textjson);
		LoadPlanDactionFromJson(json);
	}

}
