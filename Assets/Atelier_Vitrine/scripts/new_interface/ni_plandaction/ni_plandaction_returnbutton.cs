﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using MiniJSON;
using System.Text.RegularExpressions;

public class ni_plandaction_returnbutton : buttonswap
{
	void Awake()
	{
		_Awake ();
	}


	public override void ButtonPressed ()
	{
		if (ni_edittext.locked)		return;		// return doesn't work !!!
		base.ButtonPressed();

		if (ni_plandaction_editplandaction.root_master.active)
		{
			GameObject father = gameObject.transform.parent.gameObject;
			ni_iamascreen test = (ni_iamascreen)father.GetComponent<ni_iamascreen> ();
			while (test == null)
			{
				father = father.transform.parent.gameObject;
				test = (ni_iamascreen)father.GetComponent<ni_iamascreen> ();
			}
			if (father.name == "info_showdatafields")
			{
				for (int i=0;i<ni_preparedatafields.poinofinterestscript.Length;i++)
					Destroy (father.GetComponent(ni_preparedatafields.poinofinterestscript[i]));
			}

			ni_generate_pa_actionlines.SavePlanDaction("testplanaction.txt");

			ni_manage_screens.SetPreviousScreen();
		}
		else
		{
			if (ni_plandaction_editplandaction.root_vocabulary.active)
			{
				ni_plandaction_editplandaction.root_vocabulary.SetActive(false);
				ni_plandaction_editplandaction.root_listing.SetActive(true);
				ni_plandaction_editplandaction.root_master.SetActive(false);

				if (ni_vocabulary.saveme)
				{
					ni_vocabulary.saveme = false;
					string myjson = (string)Json.Serialize(ni_vocabulary.dictionarylevels[0]);	// get entire Json
					int keytosave = 1+(bufferme.GetInt("planaction_machine")*1000 + bufferme.GetInt("planaction_currentitem"))*20 + bufferme.GetInt ("vocabularytype");

					if (network.FORCENWOFF)
					{
						string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId () +"/v" + bufferme.GetClientId() + keytosave.ToString()+".txt";
						System.IO.File.WriteAllText(pathname,myjson);
					}
					else
					{
						bufferme.AddToBuffer(myjson,keytosave.ToString(),"v");
					}
				}
			}
			else
			{
				if (ni_plandaction_editplandaction.root_listing.active)
				{
					ni_plandaction_editplandaction.root_vocabulary.SetActive(false);
					ni_plandaction_editplandaction.root_listing.SetActive(false);
					ni_plandaction_editplandaction.root_master.SetActive(true);

					ni_plandaction_editplandaction.listactions.CreateList("PLAN D'ACTION -6M-");

					tk2dTextMesh tm = (tk2dTextMesh)ni_plandaction_editplandaction.plan_name.FindInChildren("edittext").GetComponent<tk2dTextMesh>();
					bufferme.SetString("planaction_name",tm.text);
					Debug.Log ("Trysaving...");
					ni_generate_pa_actionlines.SavePlanDaction("testplanaction.txt");
				}
			}
		}


	}
	
}