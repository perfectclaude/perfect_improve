﻿using UnityEngine;
using System.Collections;

public class ni_plandaction_validateinfo : buttonswap
{
	GameObject myfather;
	tk2dTextMesh	tm;

	void Awake()
	{
		_Awake();
		myfather = gameObject.transform.parent.parent.gameObject;
		tm = (tk2dTextMesh)gameObject.transform.parent.gameObject.FindInChildren("value").GetComponent<tk2dTextMesh>();
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();
// get the inputted text and copy it into the right TAB here ...
		if (bufferme.GetString("plandaction_popuptype") == "issue")
		{
			bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+bufferme.GetString("planaction_currentissuenr"),tm.text);
		}
		if (bufferme.GetString("plandaction_popuptype") == "solution")
		{
			bufferme.SetString("planaction"+ni_plandaction_listactions.selectedmachine+"_"+ni_plandaction_listactions.selecteditem+"_info_"+bufferme.GetString("planaction_currentissuenr")+"_"+bufferme.GetInt("planaction_lineindex"),tm.text);
		}

		myfather.SetActive(false);
	}
}
