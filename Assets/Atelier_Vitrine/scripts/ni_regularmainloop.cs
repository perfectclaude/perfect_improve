﻿using UnityEngine;
using System.Collections;

public class ni_regularmainloop : MonoBehaviour
{
	public static bool blockpopup = false;
	public static bool blockslide = false;
	public static int 			nrmachine;
	public static string		[,] itemnames;
	public static string		[,] fathervariables;
	public static string		[,] fathervariablesdesc;
	public static string		[,] seconditemnames;
	public static string		[,] itemtitles;
	public static string		[,] itemdescriptions;
	public static string		[] alerttexts;

	public static int 			[] nitemnames;
	public static int 			[] fathervalues;
	public static int			[,] formula;
	public static int			[,] secondformula;
	public static int			[,] straightvalue;
	public static int			[,] formulahour;
	public static bool 			[,] formulahour_valid;
	public static bool 			[,] formula_valid;
	public static float			[,] minmax;

	public static int			[] verifypopup;
	/*
	public static int robot_maxcadence;
	public static int robot_mincadence;
	public static int robot_maxrendement;
	public static int robot_minrendement;
	public static int robot_maxquality;
	public static int robot_minquality;

	public static int cirmeca_maxcadence;
	public static int cirmeca_mincadence;
	public static int cirmeca_maxrendement;
	public static int cirmeca_minrendement;

	public static int robot_cadence;
	public static int robot_rendement;
	public static int robot_cadence_hour;
	public static int robot_rendement_hour;
	public static int robot_qualite;
	public static int robot_qualite_hour;

	public static int cirmeca_cadence;
	public static int cirmeca_rendement;
	public static int cirmeca_cadence_hour;
	public static int cirmeca_rendement_hour;
	public static string cirmeca_alert1 = "";
	public static string cirmeca_alert2 = "";
	public static string robot_alert1 = "";
	public static string robot_alert2 = "";
*/

	float startdisplaytime = 0.0f;
	float slideshowtimer = 0.0f;
	float alertshowtimer = 0.0f;
	int slideshowstate = 0;

	int minval;
	int maxval;
//	int updatesequence = 0;


	void Awake ()
	{
		if (Application.loadedLevelName == "generation_server")		return;
		slideshowtimer = Time.time + 25.0f;
		slideshowstate = 0;
	}

	/*
	bool badvaluecirmeca = false;
	bool badvaluerobot = false;
	bool badvalueaireau = false;
*/

	IEnumerator DisplayScreen(int myval)
	{
		if (ni_edittext.locked)		yield break;

//		yield break;		// No alerts !!!

		ni_slideshow.SlideShowShowImage = false;
		ni_slideshow.SlideShowTimer = Time.time + 60;		// pause slideshow 1 minute
// got to main screen
		for (int i=0;i<10;i++)
		{
			ni_manage_screens.SetPreviousScreen();
			yield return new WaitForSeconds(0.1f);
		}

		ni_pointofinterest_button.MachineID = myval;
		ni_manage_screens.SetNextScreen("info_showdatafields");

		GameObject objfather = GameObject.Find ("info_showdatafields");
		GameObject obj = objfather.FindInChildren ("topborder");
		obj = obj.FindInChildren ("title");
		tk2dTextMesh textmesh = (tk2dTextMesh)obj.GetComponent<tk2dTextMesh>();

		settext.ForceText(textmesh,ni_preparedatafields.poinofinteresttitle[myval]);
		switch (ni_preparedatafields.poinofinterestscript[myval])
		{
		case "ni_regulardisplay" :
			objfather.AddComponent<ni_regulardisplay>();
			break;
		case "ni_regularmainloop" :
			objfather.AddComponent<ni_regularmainloop>();
			break;
		case "ni_regularverticaldisplay" :
			objfather.AddComponent<ni_regularverticaldisplay>();
			break;
		case "ni_saintlizaigne_cirmeca" :
			objfather.AddComponent<ni_saintlizaigne_cirmeca>();
			break;
		case "ni_saintlizaigne_robot" :
			objfather.AddComponent<ni_saintlizaigne_robot>();
			break;
		case "ni_saintlizaigne_stock" :
			objfather.AddComponent<ni_saintlizaigne_stock>();
			break;
		default:
			break;
		}

//		if (ni_preparedatafields.poinofinterestscript[myval] != "")
//			UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(objfather, "Assets/Atelier_Vitrine/scripts/ni_regularmainloop.cs (93,4)", ni_preparedatafields.poinofinterestscript[myval]);

	}

	void Start()
	{
		nrmachine = ni_preparedatafields.GetNrOfMachines();
		nitemnames = new int[nrmachine];
		fathervalues = new int[nrmachine];
		itemnames = new string[nrmachine,3];		// max 3 indicators per machine !!!
		fathervariables = new string[nrmachine,3];
		fathervariablesdesc = new string[nrmachine,3];
		seconditemnames = new string[nrmachine,3];
		itemtitles = new string[nrmachine,3];
		itemdescriptions = new string[nrmachine,3];
		formula = new int[nrmachine,3];
		formulahour_valid = new bool[nrmachine,3];
		formula_valid = new bool[nrmachine,3];
		secondformula = new int[nrmachine,3];
		formulahour = new int[nrmachine,3];
		straightvalue = new int[nrmachine,3];
		minmax = new float[nrmachine,2];
		alerttexts = new string[nrmachine];
		verifypopup = new int[nrmachine];
		startdisplaytime = Time.time + 10.0f;

		for (int nn=0;nn<nrmachine;nn++)
			verifypopup[nn] = 0;

		string [] fatheritems = new string[100];
		int nritems = 0;
		int nrcurves = bufferme.GetInt("Curve_displays");
		for (int n=0;n<nrcurves;n++)
		{
			bool newitem = true;
			for (int nn=0;nn<nritems;nn++)
			{
				if (fatheritems[nn] == bufferme.GetString("Curve_displays_father"+n))
				{
					itemnames[nn,nitemnames[nn]] = bufferme.GetString("Curve_displays_variable"+n);
					seconditemnames[nn,nitemnames[nn]] = bufferme.GetString("Curve_displays_secondvariable"+n);
					itemtitles[nn,nitemnames[nn]] = bufferme.GetString("Curve_displays_name"+n);
					itemdescriptions[nn,nitemnames[nn]] = bufferme.GetString("Curve_displays_description"+n);
					fathervariables[nn,nitemnames[nn]] = bufferme.GetString("Curve_displays_fathervariable"+n);
					fathervariablesdesc[nn,nitemnames[nn]] = bufferme.GetString("Curve_displays_fathervariabledesc"+n);
					formulahour_valid[nn,nitemnames[nn]] = true;
					formula_valid[nn,nitemnames[nn]] = true;
					nitemnames[nn]++;
					newitem = false;
				}
			}
			if (newitem)
			{
				fatheritems[nritems] = bufferme.GetString("Curve_displays_father"+n);
				itemnames[nritems,0] = bufferme.GetString("Curve_displays_variable"+n);
				seconditemnames[nritems,0] = bufferme.GetString("Curve_displays_secondvariable"+n);
				itemtitles[nritems,0] = bufferme.GetString("Curve_displays_name"+n);
				itemdescriptions[nritems,0] = bufferme.GetString("Curve_displays_description"+n);
				fathervariables[nritems,0] = bufferme.GetString("Curve_displays_fathervariable"+n);
				fathervariablesdesc[nritems,0] = bufferme.GetString("Curve_displays_fathervariabledesc"+n);
				nitemnames[nritems] = 1;
				formulahour_valid[nritems,0] = true;
				formula_valid[nritems,0] = true;
				nritems++;
			}
		}

	}

	void Update ()
	{
		if (Application.loadedLevelName == "generation_server")		return;

		tk2dSprite sprite;

		for (int n=0;n<nrmachine;n++)
		{
            sprite = ni_preparedatafields.pointofinterest[n].GetComponent<tk2dSprite>();
            sprite.SetSprite(sprite.GetSpriteIdByName("picto_green"));

			fathervalues[n] = (int)ni_preparedatafields.GetDataFieldItem (fathervariables[n,0]);
			for (int nn=0;nn<nitemnames[n];nn++)
			{
// each item
				GetMinMax(itemnames[n,nn],0,1000000);
				minmax[n,1] = maxval;
				minmax[n,0] = minval;

				secondformula[n,nn] = (int)ni_preparedatafields.GetDataFieldItem (seconditemnames[n,nn]);

				float fcadence = ni_preparedatafields.GetDataFieldItem (itemnames[n,nn]);
				straightvalue[n,nn] = (int)fcadence;
				formula_valid[n,nn] = true;
				formulahour_valid[n,nn] = true;

				fcadence = fcadence * 100.0f;
				formula[n,nn] = (int)fcadence;

//				Debug.Log ("Machine " + n + "   item "+nn+"   min:"+minval +"   max:"+maxval);
				if ((fcadence < minval)/* || (fcadence > maxval)*/)
				{
					formula_valid[n,nn] = false;
					sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
					alerttexts[n] = bufferme.GetString(itemnames[n,nn]+"_message_bad");
//					badvaluecirmeca = true;
//					cirmeca_alert1 = cirmeca_alert1 + bufferme.GetString("Cirmeca_TauxCadence"+"_message_bad"); // + "\n";
				}
				fcadence = ni_preparedatafields.GetLastHourDataFieldItem(itemnames[n,nn]) * 100.0f;
				formulahour[n,nn] = (int)fcadence;
				if ((fcadence < minval)/* || (fcadence > maxval)*/)
				{
					formulahour_valid[n,nn] = false;
					sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
					alerttexts[n] = bufferme.GetString(itemnames[n,nn]+"_message_bad");
					//					badvaluecirmeca = true;
//					cirmeca_alert2 = cirmeca_alert2 + bufferme.GetString("Cirmeca_TauxCadence"+"_message_toogood"); // + "\n";
				}
			}

			if (!ni_edittext.locked)
			{
				if ((!blockpopup) && (!blockslide) && (startdisplaytime < Time.time))
				{
					if (verifypopup[n] == 0)
					{
						if (sprite.spriteId != sprite.GetSpriteIdByName("picto_green"))
						{
							blockpopup = true;
							verifypopup[n] = 1;
							alertshowtimer = Time.time + 60.0f * 4.0f;
							StartCoroutine(DisplayScreen(n));
						}
					}
					else
					{
						if (sprite.spriteId == sprite.GetSpriteIdByName("picto_green"))
						{
							verifypopup[n] = 0;
						}
					}
				}
			}
		}

		if (!ni_edittext.locked)
		{
			if (!blockpopup)
			{
				//			Debug.Log("No More block... "+blockslide + "   "+slideshowtimer+ "   " + Time.time);
				if (!blockslide)
				{
					if (slideshowtimer < Time.time)
					{
						slideshowtimer = Time.time + 20.0f;
						blockslide = true;
						StartCoroutine(DisplayScreen(slideshowstate));
						slideshowstate++;
						//					if (slideshowstate == nrmachine)
						if (slideshowstate == 3)			// CHEAT FOR SL 3 MACHINES TO START
							slideshowstate = 0;
					}
				}
				else
				{
					if (slideshowtimer < Time.time)
					{
						slideshowtimer = Time.time + 20.0f;
						blockslide = false;
						ni_manage_screens.SetPreviousScreen();
					}
				}
			}
			else
			{
				if (alertshowtimer < Time.time)
				{
					blockpopup = false;
					ni_manage_screens.SetPreviousScreen();
				}
			}
		}




/*
		switch (updatesequence)
		{
		case 0:
			GetMinMax("Cirmeca_TauxCadence",20,150);
			cirmeca_maxcadence = maxval;
			cirmeca_mincadence = minval;
			GetMinMax("Cirmeca_TauxRedement",20,150);
			cirmeca_maxrendement = maxval;
			cirmeca_minrendement = minval;

			float fcadence = ni_preparedatafields.GetDataFieldItem ("Cirmeca_TauxCadence") * 100.0f;
			cirmeca_cadence = (int)fcadence;
			fcadence = ni_preparedatafields.GetLastHourDataFieldItem("Cirmeca_TauxCadence") * 100.0f;
			cirmeca_cadence_hour = (int)fcadence;
			updatesequence++;
			break;
		case 1:
			bool badvaluecirmecaold = badvaluecirmeca;
			cirmeca_alert1 = "";
			cirmeca_alert2 = "";

			float frendement = ni_preparedatafields.GetDataFieldItem ("Cirmeca_TauxRedement") * 100.0f;
			cirmeca_rendement = (int)frendement;
			frendement = ni_preparedatafields.GetLastHourDataFieldItem("Cirmeca_TauxRedement") * 100.0f;
			cirmeca_rendement_hour = (int)frendement;

			badvaluecirmeca = false;

			sprite = ni_preparedatafields.pointofinterest[0].GetComponent<tk2dSprite>();
			sprite.SetSprite(sprite.GetSpriteIdByName("picto_green"));
			if ((cirmeca_cadence < cirmeca_mincadence) || (cirmeca_cadence > cirmeca_maxcadence))
			{
				badvaluecirmeca = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				cirmeca_alert1 = cirmeca_alert1 + bufferme.GetString("Cirmeca_TauxCadence"+"_message_bad"); // + "\n";
			}

			if ((cirmeca_cadence_hour < cirmeca_mincadence) || (cirmeca_cadence_hour > cirmeca_maxcadence))
			{
				badvaluecirmeca = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				cirmeca_alert2 = cirmeca_alert2 + bufferme.GetString("Cirmeca_TauxCadence"+"_message_toogood"); // + "\n";
			}
			
			if ((cirmeca_rendement < cirmeca_minrendement) || (cirmeca_rendement > cirmeca_maxrendement))
			{
				badvaluecirmeca = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				cirmeca_alert1 = cirmeca_alert1 + bufferme.GetString("Cirmeca_TauxRedement"+"_message_bad"); // + "\n";
			}
			
			if ((cirmeca_rendement_hour < cirmeca_minrendement) || (cirmeca_rendement_hour > cirmeca_maxrendement))
			{
				badvaluecirmeca = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				cirmeca_alert2 = cirmeca_alert2 + bufferme.GetString("Cirmeca_TauxRedement"+"_message_toogood"); // + "\n";
			}
			if ((!badvaluecirmecaold) && (badvaluecirmeca))
				StartCoroutine(DisplayScreen(0));

			updatesequence++;
			break;
		case 2:
			GetMinMax("Robot_TauxCadence",20,150);
			robot_maxcadence = maxval;
			robot_mincadence = minval;
			GetMinMax("Robot_TauxRedement",20,150);
			robot_maxrendement = maxval;
			robot_minrendement = minval;
			GetMinMax("Robot_TauxQualite",20,150);
			robot_maxquality = maxval;
			robot_minquality = minval;

			frendement = ni_preparedatafields.GetDataFieldItem ("Robot_TauxRedement") * 100.0f;
			robot_rendement = (int)frendement;
			frendement = ni_preparedatafields.GetLastHourDataFieldItem("Robot_TauxRedement") * 100.0f;
			robot_rendement_hour = (int)frendement;
			updatesequence++;
			break;
		case 3:
			float fquality = ni_preparedatafields.GetDataFieldItem ("Robot_TauxQualite") * 100.0f;
			robot_qualite = (int)fquality;
			fquality = ni_preparedatafields.GetLastHourDataFieldItem("Robot_TauxQualite") * 100.0f;
			robot_qualite_hour = (int)fquality;
			updatesequence++;
			break;
		case 4:
			bool badvaluerobotold = badvaluerobot;

			robot_alert1 = "";
			robot_alert2 = "";

			fcadence = ni_preparedatafields.GetDataFieldItem ("Robot_TauxCadence") * 100.0f;
			robot_cadence = (int)fcadence;
			fcadence = ni_preparedatafields.GetLastHourDataFieldItem("Robot_TauxCadence") * 100.0f;
			robot_cadence_hour = (int)fcadence;

			sprite = ni_preparedatafields.pointofinterest[1].GetComponent<tk2dSprite>();
			sprite.SetSprite(sprite.GetSpriteIdByName("picto_green"));

			badvaluerobot = false;
			if ((robot_cadence < robot_mincadence) || (robot_cadence > robot_maxcadence))
			{
				badvaluerobot = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				robot_alert1 = robot_alert1 + bufferme.GetString("Robot_TauxCadence"+"_message_bad"); // + "\n";
			}

			if ((robot_cadence_hour < robot_mincadence) || (robot_cadence_hour > robot_maxcadence))
			{
				badvaluerobot = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				robot_alert2 = robot_alert2 + bufferme.GetString("Robot_TauxCadence"+"_message_toogood"); // + "\n";
			}

			if ((robot_rendement < robot_minrendement) || (robot_rendement > robot_maxrendement))
			{
				badvaluerobot = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				robot_alert1 = robot_alert1 + bufferme.GetString("Robot_TauxRedement"+"_message_bad"); // + "\n";
			}

			if ((robot_rendement_hour < robot_minrendement) || (robot_rendement_hour > robot_maxrendement))
			{
				badvaluerobot = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				robot_alert2 = robot_alert2 + bufferme.GetString("Robot_TauxRedement"+"_message_toogood"); // + "\n";
			}

			if ((robot_qualite < robot_minquality) || (robot_qualite > robot_maxquality))
			{
				badvaluerobot = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				robot_alert1 = robot_alert1 + bufferme.GetString("Robot_TauxQualite"+"_message_bad"); // + "\n";
			}

			if ((robot_qualite_hour < robot_minquality) || (robot_qualite_hour > robot_maxquality))
			{
				badvaluerobot = true;
				sprite.SetSprite(sprite.GetSpriteIdByName("picto_red"));
				robot_alert2 = robot_alert2 + bufferme.GetString("Robot_TauxQualite"+"_message_toogood"); // + "\n";
			}

			if ((!badvaluerobotold) && (badvaluerobot) && (!badvaluecirmeca))
				StartCoroutine(DisplayScreen(1));

			updatesequence=0;
			break;
		}
		*/
	}




	void GetMinMax(string itemname,int mi,int ma)
	{
		float tmp,max;
		float slide1,slide2;
		minval = mi;
		maxval = ma;

		if (bufferme.HasKey(itemname+"_slide1"))
		{
			float.TryParse(bufferme.GetString(itemname+"_slide1"), out slide1);
			if (bufferme.HasKey(itemname+"_slide2"))
			{
				float.TryParse(bufferme.GetString(itemname+"_slide2"), out slide2);
				if (slide1 > slide2)
				{
					tmp = slide1;
					slide1 = slide2;
					slide2 = tmp;
				}

				minval = (int)slide1;
				maxval = (int)slide2;
//				Debug.Log (itemname+"  found2 "+slide1+ "   "+slide2);

			}
		}
	}

}
