﻿using UnityEngine;
using System.Collections;

public class ball_raycasting : MonoBehaviour
{
	Camera mycam;
	GameObject redsphere;
	GameObject greensphere;
	GameObject [] timerobj = new GameObject[12];
	GameObject lastfocussed = null;
	float		timecount = 0.0f;
	bool		validated = false;

	void Awake ()
	{
		mycam = (Camera)gameObject.GetComponent<Camera>();
		redsphere = gameObject.FindInChildren("redsphere");
		greensphere = gameObject.FindInChildren("greensphere");
		greensphere.SetActive(false);
		for (int i=0;i<12;i++)
		{
			timerobj[i] = gameObject.FindInChildren("timer"+i);
			timerobj[i].transform.localScale = Vector3.zero;
		}
	}

	
	void Update ()
	{
		RaycastHit hitInfo;
		Ray ray;
		GameObject previous;

		ray = mycam.ScreenPointToRay(new Vector3(Screen.width/2,Screen.height/2,0));

		greensphere.SetActive(false);
		redsphere.SetActive(true);

		previous = lastfocussed;
		lastfocussed = null;
		for (int i=0;i<ni_preparedatafields.maxnrpoi;i++)
		{
			BoxCollider box = (BoxCollider)ni_preparedatafields.pointofinterest[i].GetComponent<BoxCollider>();
			if (box == null)
			{
				ni_preparedatafields.pointofinterest[i].AddComponent<BoxCollider>();
			}
			if (ni_preparedatafields.pointofinterest[i].GetComponent<Collider>().Raycast(ray, out hitInfo, 1.0e8f))
			{
				greensphere.SetActive(true);
				redsphere.SetActive(false);
				lastfocussed = ni_preparedatafields.pointofinterest[i];
			}
		}
		if ((lastfocussed == null) || (lastfocussed != previous))
		{
			for (int i=0;i<12;i++)
			{
				timerobj[i].transform.localScale = Vector3.zero;
			}
			timecount = 0.0f;
			validated = false;
		}
		else
		{
			if (validated)
			{
				for (int i=0;i<12;i++)
				{
					timerobj[i].transform.localScale = Vector3.zero;
				}
			}
			else
			{
				timecount += Time.fixedDeltaTime * 40.0f;
				for (int i=0;i<12;i++)
				{
					if (timecount > i)
					{
						int inttc = (int)timecount;
						float scal = timecount - inttc;
						if (inttc > i)
							scal = 1;
						timerobj[i].transform.localScale = new Vector3(scal,scal,scal);
					}
				}
				if (timecount > 12.0f)
				{
					validated = true;
					ni_pointofinterest_button poibut = (ni_pointofinterest_button)lastfocussed.GetComponent<ni_pointofinterest_button>();
					poibut.ButtonPressed();
				}
			}
		}

	}
}
