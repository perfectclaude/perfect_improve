﻿using UnityEngine;
using System.Collections;

public class brasMouvement : MonoBehaviour {
	public bool sens;
	public bool sens2; 
	public bool sens3; 
	// Use this for initialization
	void Start () {
		sens = true;
		sens2 = true;
		sens3 = true;
	}
	
	// Update is called once per frame
	void Update () {

		if (sens) {

			if (transform.rotation.z <0.66) {
				transform.Rotate (0, 0, -Time.deltaTime * 10);
			} else {
				if(sens2){
					if(transform.GetChild(0).GetChild(0).rotation.y>-0.52){
						transform.GetChild(0).GetChild(0).Rotate (0,-Time.deltaTime * 8 ,0 );
					}
					else{
						sens2=false;
					}
				}
				else{
					if(transform.GetChild(0).GetChild(0).rotation.y<-0.3484){
						transform.GetChild(0).GetChild(0).Rotate (0,Time.deltaTime * 8 ,0 );
					}
					else{
						sens = false;
						sens2=true;
					}
				}
			}
		} 
		else {
			if (transform.rotation.z > 0.48) {
				transform.Rotate (0, 0, Time.deltaTime * 10);
			} else {
				if(sens3){
					if(transform.GetChild(0).rotation.y<0.15){
						transform.GetChild(0).Rotate (0,-Time.deltaTime * 8,0 );
					}
					else{
						sens3=false;
					}
				}
			
				else{
					if(transform.GetChild(0).rotation.y>-0.03){
						transform.GetChild(0).Rotate (0,Time.deltaTime * 8 ,0 );
					}
					else{
						sens = true;
						sens3=true;
					}
				}
			}
		}
	}
}
