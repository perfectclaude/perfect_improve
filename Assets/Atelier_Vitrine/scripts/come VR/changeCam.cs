﻿using UnityEngine;
using System.Collections;

public class changeCam : MonoBehaviour {
	public static bool cameraBool;
	bool cameraBool2;
	public static GameObject cam1;
	public static GameObject cam2;
	GameObject cam3;
	public float time;
	// Use this for initialization
	void Start () {
		cam1 = GameObject.Find ("Cam1");
		cam2 = GameObject.Find ("Main Camera");

		cam1.SetActive (true);

	
		cameraBool = false;
		cam2.GetComponent<Camera>().clearFlags=CameraClearFlags.Nothing;
	}

	public static void change(){
		if (cameraBool) {

			cam1.SetActive (true);
			cameraBool = false;
			cam2.GetComponent<Camera>().clearFlags=CameraClearFlags.Nothing;
		} else {
			cam1.SetActive (false);
			cameraBool = true;
			cam2.GetComponent<Camera>().clearFlags=CameraClearFlags.Skybox;
		}

	}

}
