﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;
using BestHTTP;
using System.Xml;
using System.Xml.Serialization;
using System.Security;
using System.Threading;
//tmp
//using MiniJSON;
using System.Text.RegularExpressions;
using UnityEngine.UI;

// dbase : padexpos_perfect
// padexpos_perfect
// KafVgdH^zOh4

public class bufferme : MonoBehaviour
{
	// 
//	public static string clientid =  "0CC68B2D-D434_";
	public static string clientid = ""; // "sailiz2015_001";
	public static string clientkey = ""; //"CC68B2D-D434-4ED";

	public static string encryptionkey = "";
	/*
	public static string RequestUID()
	{
		#if UNITY_EDITOR
		return ("0CC68B2D-D434-4ED6-91C4-9A69F0F6630A");
		#else
		return(SystemInfo.deviceUniqueIdentifier);
		#endif
	}
	*/

	static string playerprefsprefix = "";

	public static string GetClientId()
	{
		string newclient = clientid.ToLower();
		newclient = newclient.Replace("-","_");
		return(newclient);
	}

	static bool playerprefsinit = false;

	static void InitFirstPlayerprefs()
	{
		if (playerprefsinit)		return;
		if (Application.loadedLevelName == "generation_server")
			playerprefsprefix = "";
		else
		{
			if (Application.loadedLevelName == "getdatafromfiles")
				playerprefsprefix = "get_";
			else
			{
				if (network.SYSTEMISON)
					playerprefsprefix = "cli_";
				else
					playerprefsprefix = "";
			}
		}
		playerprefsinit = true;
	}

	float verificationtime = 0.0f;

	void Awake()
	{
//		Debug.Log ("CLAUDE Awake called");
#if UNITY_ANDROID && (!UNITY_EDITOR)
		WWW newwww = new WWW(Application.streamingAssetsPath + "/clientsetting.txt");
		while (!newwww.isDone)
		{
		}
		string clientsetting = newwww.text;
#else
		string clientsetting = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/clientsetting.txt");
#endif
//		Debug.Log ("CLAUDE "+clientsetting);
		string [] clntsettings = clientsetting.Split(';');
		bufferme.clientid = clntsettings[0];
		bufferme.clientkey = clntsettings[1];

		encryptionkey = clientkey + clientid;
		if (!HasKey("bufferme_start"))			SetInt("bufferme_start",0);
		if (!HasKey("bufferme_end"))			SetInt("bufferme_end",0);
		verificationtime = Time.time + 15.0f;
		StartCoroutine("CheckInternetconnection");
	}

	void Update()
	{
//		Debug.Log ("clientid"+clientid);
//		Debug.Log ("clientkey"+clientkey);
		/*
		if (verificationtime < Time.time)
		{
			Debug.Log ("Killed, start again");
			StopCoroutine("CheckInternetconnection");
			StartCoroutine("CheckInternetconnection");
		}
		*/
	}


// Buffer me here
	// --------------------------------------
	public static void AddToBuffer(string json, string datetime, string specialclient,string keywords="")
	{
//		Debug.Log ("Adding:"+json);
		int		index = GetInt("bufferme_end");
		if (BufferEmpty())
		{
			index = 0;
			SetInt("bufferme_start",0);
		}
		SetString("bufferme"+index,json);
//		Debug.Log ("Adding to buffer:" + datetime);
		SetString("buffermedt"+index,datetime);	
		SetString("bufferclient"+index,specialclient+GetClientId());
		SetString("buffermekeywords"+index,keywords);
		index ++;
		SetInt("bufferme_end",index);
	}
	
	// --------------------------------------
	public static bool BufferEmpty()
	{
		if (GetInt("bufferme_start") == GetInt("bufferme_end"))		return(true);
		return (false);
	}

	int requestcount = 0;
	bool internet_testing = false;
	public static bool connected_once = false;
	public static bool proxywasnull = false;

	public static HTTPProxy WhichProxy()
	{
		if (proxywasnull)
			return(null);
		else
		{
			Debug.Log("Proxy:"+network.initialproxy);
			if (network.initialproxy == "")
				return (null);
			else
				return(new HTTPProxy(new Uri(network.initialproxy),null,true));
		}

	}


	void OnRequestFinished(HTTPRequest request, HTTPResponse response)
	{
		bool requestarrived = false;

		switch (request.State)
		{
		case HTTPRequestStates.Finished:
			if (response.IsSuccess)
			{
				//			Debug.Log ("INternetRequest finished 2");
				
				//			GUI_display.ErrorMessage = response.DataAsText;
				if (response.DataAsText.Contains("text exists"))
				{
					connected_once = true;
					requestarrived = true;
					GUI_display.ErrorMessage = "connected";
					//				Debug.Log ("INternetRequest finished 3");
					int		index = GetInt("bufferme_start");
					string		json;
					
					json = GetString("bufferme"+index);
					if (index < GetInt("bufferme_end"))
					{
						GUI_display.ErrorMessage = "SENDING:["+GetString("bufferclient"+index)+"]["+GetString("buffermedt"+index)+"]"+json;
						//							Debug.Log (GUI_display.ErrorMessage);
						//						Debug.Log (GUI_display.ErrorMessage);
						//						Debug.Log (json);
						json = Encrypt(json);
						//							Debug.Log ("Sending json "+GetString("bufferclient"+index));
						//					Debug.Log ("Datetime save:"+GetString("buffermedt"+index));
						network.SendJson(GetString("bufferclient"+index),json,GetString("buffermedt"+index),GetString("buffermekeywords"+index));
					}
				}
				else
					GUI_display.ErrorMessage = "NOT connected";
				
			}
			else
			{
			}
			break;
		case HTTPRequestStates.Error:
			break;
		case HTTPRequestStates.Aborted:
			break;
		case HTTPRequestStates.ConnectionTimedOut:
			break;
		case HTTPRequestStates.TimedOut:
			break;
		}

		if (!requestarrived)
		{
			requestcount++;
			if (requestcount == 5)
			{
				proxywasnull = true;
			}
			if (requestcount == 10)
			{
				requestcount = 0;
				proxywasnull = false;
			}
		}
		else
		{
			requestcount = 0;
			Debug.Log("Connected "+proxywasnull);
		}
		internet_testing = false;
		//		Debug.Log ("INternetRequest finished");
	}

	
	// --------------------------------------
	IEnumerator	 CheckInternetconnection()
	{
		bool		couldconnect;
		while(true)
		{	
//			GUI_display.ErrorMessage = "internetloop";
//			if (!BufferEmpty())
			{
				internet_testing = true;
				HTTPRequest request = new HTTPRequest(new Uri(network.WSUrl + "alive.txt"),HTTPMethods.Get, OnRequestFinished);
				if (proxywasnull)
					request.Proxy = null;
				else
					request.Proxy = new HTTPProxy(new Uri(network.initialproxy),null,true);
				//				request.SetHeader("Content-Type", "application/json; charset=UTF-8");
				request.Send();

				while (internet_testing)
				{
					yield return new WaitForSeconds(0.1f);
				}
				if ((network.waitingnetworkintermediate) || (network.waitingnetwork))
				{
					yield return new WaitForSeconds(0.3f);
				}
				else
				{
				}
			}

			if (connected_once)
			{
				if (BufferEmpty())
					yield return new WaitForSeconds(20.0f);
				
				if (Application.loadedLevelName == "getdatafromfiles")
					yield return new WaitForSeconds(0.2f);
				else
					yield return new WaitForSeconds(2.0f);
			}
		}
	}
	
	



	public static string GetString(string key)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		string decrypt = PlayerPrefs.GetString(newkey, "");
		if (decrypt == null || decrypt == "") return "";
		return (Decrypt(decrypt));
	}
	public static void SetString(string key,string value)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		string newvalue = Encrypt(value);
		PlayerPrefs.SetString(newkey,newvalue);
//		PlayerPrefs.Save();
	}
	public static bool HasKey(string key)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		return(PlayerPrefs.HasKey(newkey));
	}
	public static void    DeleteKeyQuick(string key)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		PlayerPrefs.DeleteKey(newkey);
	}
	public static void    DeleteKey(string key)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		PlayerPrefs.DeleteKey(newkey);
		PlayerPrefs.Save();
	}
	public static int GetInt(string key, int i = 0)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		if (PlayerPrefs.HasKey(newkey)) return (PlayerPrefs.GetInt(newkey));
		
		return i;   
	}
	public static void SetInt(string key,int value)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		PlayerPrefs.SetInt(newkey,value);
//		PlayerPrefs.Save();
	}
	public static float GetFloat(string key)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		return (PlayerPrefs.GetFloat(newkey));
	}

	public static void SetFloat(string key,float value)
	{
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		PlayerPrefs.SetFloat(newkey,value);
		//PlayerPrefs.Save();
	}

    public static bool GetBool(string key, bool b = false)
    {
        InitFirstPlayerprefs();
        key = playerprefsprefix + key;
        string newkey = Encrypt(key);
        if (PlayerPrefs.HasKey(newkey)) return (PlayerPrefs.GetInt(newkey) != 0);

        return b;   
    }

    public static void SetBool(string key,bool value)
    {
		InitFirstPlayerprefs();
		key = playerprefsprefix + key;
		string newkey = Encrypt(key);
		PlayerPrefs.SetInt(newkey,(value ? 1 : 0));
//        PlayerPrefs.Save();
    }

	public static void Save() 
	{
		PlayerPrefs.Save();
	}

	public static string Encrypt (string toEncrypt)
	{
//		return(toEncrypt);
		string createkey = clientkey + "XX" + clientid;
		string	mykey = createkey;

//		Debug.Log ("CLAUDE "+createkey);
		if (createkey.Length > 32)		mykey = createkey.Substring(0,32);

		 byte[] keyArray = UTF8Encoding.UTF8.GetBytes (mykey);
		 byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes (toEncrypt);
		
 		 RijndaelManaged rDel = new RijndaelManaged ();
 		 rDel.Key = keyArray;
 		 rDel.Mode = CipherMode.ECB;
		rDel.Padding = PaddingMode.PKCS7;
 // better lang support
		ICryptoTransform cTransform = rDel.CreateEncryptor ();
		byte[] resultArray = cTransform.TransformFinalBlock (toEncryptArray, 0, toEncryptArray.Length);
		return Convert.ToBase64String (resultArray, 0, resultArray.Length);
	}
 
	public static string Decrypt (string toDecrypt)
	{
//		return(toDecrypt);
		string createkey = clientkey + "XX" + clientid;
		string	mykey = createkey;
//		Debug.Log ("CLAUDE "+createkey);
		if (createkey.Length > 32)		mykey = createkey.Substring(0,32);

		byte[] keyArray = UTF8Encoding.UTF8.GetBytes (mykey);
		byte[] toEncryptArray = Convert.FromBase64String (toDecrypt);
		RijndaelManaged rDel = new RijndaelManaged ();
		rDel.Key = keyArray;
		rDel.Mode = CipherMode.ECB;
		rDel.Padding = PaddingMode.PKCS7;

		ICryptoTransform cTransform = rDel.CreateDecryptor ();
		byte[] resultArray = cTransform.TransformFinalBlock (toEncryptArray, 0, toEncryptArray.Length);
		return UTF8Encoding.UTF8.GetString (resultArray);

	}
	
	public static DateTime GetYearWeekStart(DateTime dt)
	{
		DateTime startdate = new DateTime(dt.Year,1,1);
		switch (startdate.DayOfWeek)
		{
		case DayOfWeek.Monday :				break;
		case DayOfWeek.Tuesday :		startdate = startdate.AddDays(-1);		break;
		case DayOfWeek.Wednesday :		startdate = startdate.AddDays(-2);		break;
		case DayOfWeek.Thursday :		startdate = startdate.AddDays(-3);		break;
		case DayOfWeek.Friday :			startdate = startdate.AddDays(-4);		break;
		case DayOfWeek.Saturday :		startdate = startdate.AddDays(-5);		break;
		case DayOfWeek.Sunday :			startdate = startdate.AddDays(-6);		break;
		}
		return (startdate);
	}

	public static int GetWeekNr(DateTime dt)
	{
		int weeknr = 1;

		DateTime startdate = GetYearWeekStart(dt);
		TimeSpan timedif = dt - startdate; 
		while (timedif.Days > 7)
		{
			weeknr++;
			startdate = startdate.AddDays(7);
			timedif = dt - startdate; 
		}
		return weeknr;
	}


}
