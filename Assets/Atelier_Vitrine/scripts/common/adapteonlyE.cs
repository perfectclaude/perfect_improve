﻿using UnityEngine;
using System.Collections;

public class adapteonlyE : MonoBehaviour
{
	Camera 	m_ViewCamera;

	void ResetPosition ()
	{
		if (m_ViewCamera == null)		return;
		Vector3 	vec;
		Vector3	res;

		vec.x = Screen.width;
		vec.y = Screen.height/2;
		vec.z = 0;
		res = m_ViewCamera.ScreenToWorldPoint (vec);
		res.y = gameObject.transform.position.y;
		res.z = gameObject.transform.position.z;
		gameObject.transform.position = res;
	}

	void Awake ()
	{
		if (GameObject.Find("Main Camera") != null)
			m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		else
			m_ViewCamera = null;
		ResetPosition();
	}

	void Update ()
	{
		if (!ni_edittext.scrollingup)
			ResetPosition();
	}
}
