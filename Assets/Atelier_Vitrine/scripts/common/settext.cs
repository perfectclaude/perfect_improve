using UnityEngine;
using System.Collections;

public class settext : MonoBehaviour
{
	public string m_key;
	
	void Awake ()
	{
		tk2dTextMesh textje = gameObject.GetComponent<tk2dTextMesh>();
		ForceText(textje,languagetreatment.GetTextWithKey(m_key));
	}
	
	void Update ()
	{
		Awake();
	}

	public static void ForceText(tk2dTextMesh mesh,string str)
	{
		mesh.text = str;
		mesh.maxChars = str.Length;
		mesh.Commit();
	}
}
