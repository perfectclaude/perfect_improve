using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class languagetreatment : MonoBehaviour
{
	public static string 		myLanguage;
	public static TextAsset					mytexts = null;
	public static string[] myList;
	
	void Awake ()
	{
		ReleadTexts();
	}
	
	void Update ()
	{
	
	}
	
	public static void ReleadTexts()
	{
		int			i;
		string		fname = "strings_en";
		
		switch (Application.systemLanguage)
		{
		default :							fname = "strings_en";			break;
		case SystemLanguage.French :		fname = "strings_fr";			break;
		case SystemLanguage.German :		fname = "strings_de";			break;
		case SystemLanguage.Spanish :		fname = "strings_es";			break;
		case SystemLanguage.Italian :		fname = "strings_it";			break;
		case SystemLanguage.Dutch :			fname = "strings_nl";			break;
		case SystemLanguage.Portuguese :	fname = "strings_pt";			break;			
		}
		mytexts = (TextAsset) Resources.Load(fname, typeof(TextAsset));
		myList = mytexts.text.Split('\n');
		
		i = myList.Length;
		
		while(i > 0)
		{
			i --;
			myList[i] = myList[i].Replace("\\n","\n");
		}
		
	}
	
	public static string GetTextWithKey(string key)
	{
		int		i;
		
		i = myList.Length;
		
		while(i > 0)
		{
			int 		index;
			string	textline;
		
			i --;
			if (myList[i].StartsWith(key))
			{
				index = myList[i].IndexOf('=');
				textline = myList[i].Substring(index+1);
				return (textline);
			}
		}
		return("Error In Languagefile : "+key);
	}
}
