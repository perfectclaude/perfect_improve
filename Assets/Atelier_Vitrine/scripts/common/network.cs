﻿using UnityEngine;
using System;
using MiniJSON;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using BestHTTP;
using System.Xml;
using System.Xml.Serialization;
using System.Security;
using System.Threading;

public class network : MonoBehaviour
{
	public static string WSUrl = "http://www.piserver.fr/api/perfectimprove/"; //"http://www.padexpose.com/";
	public static string secondairyWSUrl = "";
	public static string initialproxy = ""; //"http://SERV-52:8080/array.dll?Get.Routing.Script";

	public static bool SYSTEMISON = true;
	public static int SCHEDULETYPE = 0;		// schedule

	public static bool FORCENWOFF = true;
	bool DEBUGDISPLAY = false;

	static string _newclient = "";
	static string _newjson = "";
	static string _newkeywords = "";
	static string _datetime = "";
	static string _datetime2 = "";
	static string _getdatetime = "";
	static string _getdatetime2 = "";
	static bool _SendJson = false;
	static bool _GetJson = false;
	static bool _CleanDbase = false;
	static bool _CreateDbase = false;
	static bool _type = false;
	static int _maximum = 0;

	static string camefrom = "";

	public static bool waitingnetwork = false;
    private static float _time_waitingnetwork = 0;
    private HTTPRequest _request_waitingnetwork = null;
	public static string retreivejson = "";

	static bool _GetinterJson = false;
	public static bool waitingnetworkintermediate = false;
    private static float _time_waitingnetworkintermediate = 0;
    private HTTPRequest _request_waitingnetworkintermediate = null;
	public static string retreiveintermediatejson = "";
	public static long n_retreiveintermediatejson;
	public static DateTime dt_retreiveintermediatejson;

	int		pausecounter = 0;

	string errortext = "";

	public delegate void MyDelegate();
	protected static MyDelegate getjsoncallback = null;
	protected static MyDelegate getintermediatejsoncallback = null;

	public static bool SendJson(string client, string json,string datetime,string keywords)
	{
		if (waitingnetwork)	return false;
        _time_waitingnetwork = 0;
		waitingnetwork = true;
		_newkeywords = keywords;
		_newclient = client;
		_newjson = json;
		_datetime = datetime;
		_SendJson = true;
		return true;
	}



	public static bool GetIntermediateJson(string client, string datetime, string datetime2,int maximum,bool type,string come="",MyDelegate callback=null)
	{
		if (waitingnetworkintermediate)	return false;
        _time_waitingnetworkintermediate = 0;
		waitingnetworkintermediate = true;
		camefrom = come;
		getintermediatejsoncallback = callback;
		_newclient = client;
		_getdatetime = datetime;
		_getdatetime2 = datetime2;
		_maximum = maximum;
		_GetinterJson = true;
		_type = type;
		retreiveintermediatejson = "";
		return true;
	}

	public static bool GetJson(string client, string datetime, string datetime2,int maximum,MyDelegate callback=null)
	{
		if (waitingnetwork)	return false;
        _time_waitingnetwork = 0;
		waitingnetwork = true;
		getjsoncallback = callback;
		_newclient = client;
		_getdatetime = datetime;
		_getdatetime2 = datetime2;
		_maximum = maximum;
		_GetJson = true;
		retreivejson = "";
		return true;
	}
	
	public static bool CleanDbase(string client)
	{
		if (waitingnetwork)	return false;
        _time_waitingnetwork = 0;
		waitingnetwork = true;
		_newclient = client;
		_CleanDbase = true;
		return true;
	}

	public static bool CreateDbase(string client)
	{
		if (waitingnetwork)	return false;
        _time_waitingnetwork = 0;
		waitingnetwork = true;
		_newclient = client;
		_CreateDbase = true;
		return true;
	}


	public static void ReadSettings()
	{
		#if UNITY_ANDROID && (!UNITY_EDITOR)
		WWW newwww = new WWW(Application.streamingAssetsPath + "/clientsetting.txt");
		while (!newwww.isDone)
		{
		}
		string clientsetting = newwww.text;
		#else
		string clientsetting = System.IO.File.ReadAllText(Application.streamingAssetsPath + "/clientsetting.txt");
		#endif
		string [] clntsettings = clientsetting.Split(';');
		bufferme.clientid = clntsettings[0];
		bufferme.clientkey = clntsettings[1];
		WSUrl = clntsettings[2];

		if (clntsettings[3] == "off")
		{
			network.SYSTEMISON = false;
		}
		else
		{
			network.SYSTEMISON = true;
		}

		if (clntsettings[4] == "fake")
		{
			ni_statlegend.FAKESTATS = true;
			ni_statlegend.FORCEFAKESTATS = true;
			ni_preparedatafields.ONLINESTORAGE = false; 
			FORCENWOFF = true;
		}
		else
		{
			ni_statlegend.FAKESTATS = false;
			ni_statlegend.FORCEFAKESTATS = false;
			FORCENWOFF = false;
		}

		switch (clntsettings[5])
		{
		case "capture" :
			network.SCHEDULETYPE = 1;
			break;
		default :
			network.SCHEDULETYPE = 0;
			break;
		}

		initialproxy = clntsettings[6];
//		Debug.Log ("Proxy ServerManager :["+initialproxy+"]");
//		if (initialproxy != "")
//			HTTPManager.Proxy = new HTTPProxy(new Uri(initialproxy),null,true);

		ni_preparedatafields.guioutput = network.initialproxy + "\n";

//		initialproxy = "";
	}


	void Awake()
	{
		network.ReadSettings();
	}

	IEnumerator Start()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		/*
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
		Debug.Log ("Wait before clean");
		
		while (network.waitingnetwork)
			yield return null;
		Debug.Log ("Wait clean");
//		network.CleanDbase(bufferme.GetClientId());
//		Debug.Log ("Wait while before clean");
//		while (network.waitingnetwork)
//			yield return null;
		
		Debug.Log ("Wait create");
		network.CreateDbase(bufferme.GetClientId());
		while (network.waitingnetwork)
			yield return null;
*/

		yield return new WaitForSeconds(0.1f);
	}

	void OnApplicationPause(bool pauseStatus)
	{
		errortext = "";
		if (!pauseStatus)
		{
			if (pausecounter != 0)
			{
				StopAllCoroutines();
				waitingnetwork = false;
                _time_waitingnetwork= 0;

                waitingnetworkintermediate = false;
                _time_waitingnetworkintermediate = 0;
			}
			pausecounter++;
		}
	}
	
	void Update ()
	{
        // Gestion de deblocage de verroue lié a un plantage non prevu
        if (waitingnetwork)
        {
            bool tempo1s = ((int)_time_waitingnetwork < (int)(_time_waitingnetwork + Time.deltaTime));
            _time_waitingnetwork += Time.deltaTime;

            if( tempo1s && (_time_waitingnetwork > (60*1+8)) ) // Si toujours pas finit au bout de 1 minutes et 8 secondes c'est que ca du planté
            {
                ni_logger.LogWarn("network", "Update.waitingnetwork", "WAIT At"+_time_waitingnetwork.ToString() );

                if (_request_waitingnetwork != null)
                {
                    if ((_request_waitingnetwork.State == HTTPRequestStates.Processing) || // Si on est dans un cas de ConnectionTimout cela peux prendre plus de 1 minutes et 8 secondes donc attend toujours
                        (_request_waitingnetwork.State == HTTPRequestStates.Queued))
                    {

                        ni_logger.LogError("network", "Update.waitingnetwork", "ABORT At"+_time_waitingnetwork.ToString() );
                        _request_waitingnetwork.Abort();

                        // On ajoute qd meme une condition si on est dans l'extreme plantage genre que reqGetXML.State est sur processing et qu'il ne changera jamais
                        if( (_time_waitingnetwork) > (60*5+8) ) // Si toujours pas finit au bout de 5 minutes et 8 secondes c'est que ca du planté grave
                            waitingnetwork = false;    
                    }
                    else
                        waitingnetwork = false;
                }
                else
                    waitingnetwork = false;
            }
        }

        if (waitingnetworkintermediate)
        {
            bool tempo1s = ((int)_time_waitingnetworkintermediate < (int)(_time_waitingnetworkintermediate + Time.deltaTime));
            _time_waitingnetworkintermediate += Time.deltaTime;

            if( tempo1s && (_time_waitingnetworkintermediate > (60*1+8)) ) // Si toujours pas finit au bout de 1 minutes et 8 secondes c'est que ca du planté
            {
                ni_logger.LogWarn("network", "Update.waitingnetworkintermediate", "WAIT At"+_time_waitingnetworkintermediate.ToString() );

                if (_request_waitingnetworkintermediate != null)
                {
                    if ((_request_waitingnetworkintermediate.State == HTTPRequestStates.Processing) || // Si on est dans un cas de ConnectionTimout cela peux prendre plus de 1 minutes et 8 secondes donc attend toujours
                        (_request_waitingnetworkintermediate.State == HTTPRequestStates.Queued))
                    {

                        ni_logger.LogError("network", "Update.waitingnetworkintermediate", "ABORT At"+_time_waitingnetworkintermediate.ToString() );
                        _request_waitingnetworkintermediate.Abort();

                        // On ajoute qd meme une condition si on est dans l'extreme plantage genre que reqGetXML.State est sur processing et qu'il ne changera jamais
                        if( (_time_waitingnetworkintermediate) > (60*5+8) ) // Si toujours pas finit au bout de 5 minutes et 8 secondes c'est que ca du planté grave
                            waitingnetworkintermediate = false;    
                    }
                    else
                        waitingnetworkintermediate = false;
                }
                else
                    waitingnetworkintermediate = false;
            }
        }





		if (FORCENWOFF)
						return;

		if (_SendJson)
		{
			_SendJson = false;
			StartCoroutine("i_SendJson");
		}
		if (_CleanDbase)
		{
			_CleanDbase = false;
			StartCoroutine("i_CleanDbase");
		}
		if (_CreateDbase)
		{
			_CreateDbase = false;
			StartCoroutine("i_CreateDbase");
		}
		if (_GetJson)
		{
			_GetJson = false;
			StartCoroutine("i_GetJson");
		}
		if (_GetinterJson)
		{
			_GetinterJson = false;
			StartCoroutine("i_GetIntermediateJson");
		}
	}


	void i_SendJsonFinished(HTTPRequest request, HTTPResponse response)
	{
		switch (RetryHttp(response,request,"i_SendJsonFinished"))
		{
		case 0 :		// OK
			//			Debug.Log (response.DataAsText);
			//			Debug.Log ("Send message:" + "["+locallink.response.Text+"]");
			//			GUI_display.ErrorMessage = "OUT:"+locallink.response.Text;
			if (response.DataAsText.Length > 10)
			{
				//				Debug.Log ("Json error");
			}
			else
			{
				// json sent... continue !!!
				//				Debug.Log ("Json continue");
				int		index = bufferme.GetInt("bufferme_start");
				bufferme.DeleteKey("bufferme"+index);
				bufferme.DeleteKey("buffermedt"+index);
				bufferme.DeleteKey("bufferclient"+index);
				bufferme.DeleteKey("buffermekeywords"+index);
				index++;
				bufferme.SetInt("bufferme_start",index);
			}
			break;
		case 1 :		// fail
			break;
		case 2 :		// retry
			return;
		}
		waitingnetwork = false;
	}

	IEnumerator i_SendJson()
	{
		string	mymethod = "sendsamplejson.php";

		if (_datetime.Length < 11)
			mymethod = "sendjson.php";

		if (_newkeywords != "")
			mymethod = "sendjson_pa.php";

//		Debug.Log (WSUrl+mymethod+"?client="+_newclient+"&datetime="+_datetime+"&json="+_newjson);
        _request_waitingnetwork = new HTTPRequest(new Uri(WSUrl+mymethod), HTTPMethods.Post, i_SendJsonFinished);
        _request_waitingnetwork.Proxy = bufferme.WhichProxy();
        _request_waitingnetwork.AddField("client",_newclient);
        _request_waitingnetwork.AddField("datetime",_datetime);
        _request_waitingnetwork.AddField("json",_newjson);
		if (_newkeywords != "")
            _request_waitingnetwork.AddField ("words", _newkeywords);
        _request_waitingnetwork.Send();
		yield return new WaitForSeconds(0.1f);
	}


	void i_CreateDbaseFinished(HTTPRequest request, HTTPResponse response)
	{
		switch (RetryHttp(response,request,"i_CreateDbaseFinished"))
		{
		case 0 :		// OK
			Debug.Log (response.DataAsText);
			break;
		case 1 :		// fail
			break;
		case 2 :		// retry
			return;
		}
		waitingnetwork = false;
	}

	IEnumerator i_CreateDbase()
	{
		string	mymethod = "createdbase.php";

        _request_waitingnetwork = new HTTPRequest(new Uri(WSUrl+mymethod), HTTPMethods.Post, i_CreateDbaseFinished);
        _request_waitingnetwork.Proxy = bufferme.WhichProxy();
        _request_waitingnetwork.AddField("client",_newclient);
        _request_waitingnetwork.Send();
		GUI_display.ErrorMessage = "Creating";
		yield return new WaitForSeconds(0.1f);
	}


	void i_CleanDbaseFinished(HTTPRequest request, HTTPResponse response)
	{
		switch (RetryHttp(response,request,"i_CleanDbaseFinished"))
		{
		case 0 :		// OK
			Debug.Log (response.DataAsText);
			break;
		case 1 :		// fail
			break;
		case 2 :		// retry
			return;
		}
		waitingnetwork = false;
	}

	IEnumerator i_CleanDbase()
	{
		string	mymethod = "cleandbase.php";

        _request_waitingnetwork = new HTTPRequest(new Uri(WSUrl+mymethod), HTTPMethods.Post, i_CleanDbaseFinished);
        _request_waitingnetwork.Proxy = bufferme.WhichProxy();
        _request_waitingnetwork.AddField("client",_newclient);
        _request_waitingnetwork.Send();
		GUI_display.ErrorMessage = "Cleaning";
		yield return new WaitForSeconds(0.1f);
	}

	public static long PathJson(long period,string json1,string json2,string json3,long fulltime1,long fulltime2)
	{
		int origcount1 = 0;
		int origcount2 = 0;
		int origcount3 = 0;
		int createcount = 0;
		double valtime = 0;
		double newvaltime1 = 0;
		double newvaltime2 = 0;
		double newvaltime3 = 0;
		string jsonout = "{ ";
		DateTime usedt = new DateTime(1970, 1, 1,0,0,0,0); //.ToLocalTime();
		string str_time1 = "";
		string str_time2 = "";
		string str_time3 = "";
		string strtext;
		bool valid1 = true;
		bool valid2 = true;
		bool valid3 = true;
		IDictionary orig1 = null; // = (IDictionary)Json.Deserialize(json1);
		IDictionary orig2 = null; // = (IDictionary)Json.Deserialize(json2);
		IDictionary orig3 = null; // = (IDictionary)Json.Deserialize(json3);
		IDictionary savejson;
// Link all 3 jsons
		try		{	orig1 = (IDictionary)Json.Deserialize(json1);		}
		catch	{	valid1 = false;	}
		try		{	orig2 = (IDictionary)Json.Deserialize(json2);		}
		catch	{	valid2 = false;	}
		try		{	orig3 = (IDictionary)Json.Deserialize(json3);		}
		catch	{	valid3 = false;	}

		if (valid3 == false)		return (-1);

// fond smallest datetime
		try		{	if (valid1) str_time1 = (string)orig1["time"+origcount1];		}
		catch	{	valid1 = false;		}
		try		{	if (valid2) str_time2 = (string)orig2["time"+origcount2];		}
		catch	{	valid2 = false;		}
		try		{	if (valid3) str_time3 = (string)orig3["time"+origcount3];		}
		catch	{	valid3 = false;		}

		if (valid3 == false)		return (-1);

		if (str_time1 == "")		valid1 = false;
		if (str_time1 == null)		valid1 = false;
		if (str_time2 == "")		valid2 = false;
		if (str_time2 == null)		valid2 = false;
		if (str_time3 == "")		valid3 = false;
		if (str_time3 == null)		valid3 = false;
		if (valid1)	double.TryParse(str_time1,out newvaltime1);
		else newvaltime1 = 99422088200000;
		newvaltime1 += fulltime2;
		if (valid2)	double.TryParse(str_time2,out newvaltime2);
		else newvaltime2 = 99422088200000;
		newvaltime2 += fulltime1;
		if (valid3)	double.TryParse(str_time3,out newvaltime3);
		else newvaltime3 = 99422088200000;

//		Debug.Log ("fulltime " + fulltime1);
//		Debug.Log ("valid 1 " + newvaltime1);
//		Debug.Log ("valid 2 " + newvaltime2);
//		Debug.Log ("valid 3 " + newvaltime3);

		valtime = newvaltime1;
		if (valtime > newvaltime2)	valtime = newvaltime2;
		if (valtime > newvaltime3)	valtime = newvaltime3;

		usedt = usedt.AddMilliseconds(valtime);
		dt_retreiveintermediatejson = usedt;
		while (valid1 || valid2 || valid3)
		{
			if (createcount != 0)		jsonout = jsonout + ",";

			jsonout = jsonout + "\"time"+ createcount + "\" : \"" + valtime.ToString() + "\",";

			if ((!valid1) || ((newvaltime1 - 1000) > valtime))
				jsonout = jsonout + "\"sample_a"+ createcount + "\" : { \"_unknown_\" : \"_unknown_\" },";
			else
			{
				savejson = (IDictionary)orig1["sample"+origcount1];
				strtext = (string)Json.Serialize(savejson);
				jsonout = jsonout + "\"sample_a"+ createcount + "\" : " + strtext + ",";
				origcount1++;
				try
				{
					str_time1 = (string)orig1["time"+origcount1];
				}
				catch
				{
					valid1 = false;
				}
				if (str_time1 == "")		valid1 = false;
				if (str_time1 == null)		valid1 = false;
				if (valid1)	double.TryParse(str_time1,out newvaltime1);
				else newvaltime1 = 99422088200000;
				newvaltime1 += fulltime2;

			}

			if ((!valid2) || ((newvaltime2 - 1000) > valtime))
				jsonout = jsonout + "\"sample_b"+ createcount + "\" : { \"_unknown_\" : \"_unknown_\" },";
			else
			{
				savejson = (IDictionary)orig2["sample"+origcount2];
				strtext = (string)Json.Serialize(savejson);
				jsonout = jsonout + "\"sample_b"+ createcount + "\" : " + strtext + ",";
				origcount2++;
				try
				{
					str_time2 = (string)orig2["time"+origcount2];
				}
				catch
				{
					valid2 = false;
				}
				if (str_time2 == "")		valid2 = false;
				if (str_time2 == null)		valid2 = false;
				if (valid2)	double.TryParse(str_time2,out newvaltime2);
				else newvaltime2 = 99422088200000;
				newvaltime2 += fulltime1;
			}

			if ((!valid3) || ((newvaltime3 - 1000) > valtime))
				jsonout = jsonout + "\"sample_c"+ createcount + "\" : { \"_unknown_\" : \"_unknown_\" }";
			else
			{
				savejson = (IDictionary)orig3["sample"+origcount3];
				strtext = (string)Json.Serialize(savejson);
				jsonout = jsonout + "\"sample_c"+ createcount + "\" : " + strtext;
				origcount3++;
				try
				{
					str_time3 = (string)orig3["time"+origcount3];
				}
				catch
				{
					valid3 = false;
				}
				if (str_time3 == "")		valid3 = false;
				if (str_time3 == null)		valid3 = false;
				if (valid3)	double.TryParse(str_time3,out newvaltime3);
				else newvaltime3 = 99422088200000;
			}
			createcount++;

			switch (period)
			{
			default :
				usedt = usedt.AddHours(1);
				break;
			case 1 :
				usedt = usedt.AddDays(1);
				break;
			case 2 :
				usedt = usedt.AddMonths(1);
				break;
			case 3 :
				usedt = usedt.AddYears(1);
				break;
			}
			valtime = (long)GetMiliseconds(usedt);

//			if (createcount > 30)		break;
		}

		jsonout = jsonout + " }";
//		Debug.Log ("Nr of times:"+createcount);
//		Debug.Log (jsonout);
		retreiveintermediatejson = jsonout;
		n_retreiveintermediatejson = createcount;
		return (createcount);
	}

	void SetJsonData(string DataAsText)
	{
		if ((DataAsText == "{}") || (DataAsText == "{ }") || (DataAsText.Length < 16))
		{
			retreiveintermediatejson = "";
		}
		else
		{
			IDictionary	injson = null;
			try
			{
				injson = (IDictionary)Json.Deserialize(DataAsText);
			}
			finally
			{
				if (injson == null)
				{
					retreiveintermediatejson = "";
				}
				else
				{
					if (injson.Keys.Count <= 0)
					{
						retreiveintermediatejson = "";
					}
					else
					{
						long nn = 0;
						GUI_display.ErrorMessage = "";
						retreiveintermediatejson = "{ ";
						
						foreach(var key in injson.Keys)
						{
							string newtext = (string)injson[key.ToString()];
							if (nn != 0)		retreiveintermediatejson = retreiveintermediatejson + ",";
							retreiveintermediatejson = retreiveintermediatejson + "\"time"+nn+"\" : \""+key.ToString()+"\",";
							retreiveintermediatejson = retreiveintermediatejson + "\"sample"+ nn + "\" : " + bufferme.Decrypt(newtext);
							n_retreiveintermediatejson++;
							nn++;
						}
						retreiveintermediatejson = retreiveintermediatejson + " }";
//						Debug.Log ("JSON:" + retreiveintermediatejson);
					}
				}
			}
		}

	}
	
	void i_GetIntermediateJsonFinished(HTTPRequest request, HTTPResponse response)
	{
		n_retreiveintermediatejson = 0;
        Debug.Log("Fin: i_GetIntermediateJsonFinished" + (response != null ? " (" + response.StatusCode.ToString() + ")" : "") );

		switch (RetryHttp(response,request,"i_GetIntermediateJsonFinished"))
		{
		case 0 :		// OK
			// use response.DataAsText;
			retreiveintermediatejson = "";
			SetJsonData(response.DataAsText);
			break;
		case 1 :		// fail
			break;
		case 2 :		// retry
			return;
		}

		if (getintermediatejsoncallback != null)
			getintermediatejsoncallback();
		waitingnetworkintermediate = false;
	}

	IEnumerator i_GetIntermediateJson()
	{
		HTTPManager.MaxConnectionPerServer = 10;

//		if (_type)		// get for average !!!
		string	mymethod = "getjson.php";
//		string req = WSUrl+mymethod+"?client="+_newclient+"&datetime="+_getdatetime+"&enddatetime="+_getdatetime2+"&maximum="+_maximum.ToString();
//		Debug.Log (new Uri(req).ToString());
//		yield return new WaitForSeconds(0.1f);

//		HTTPRequest request = new HTTPRequest(new Uri(req), HTTPMethods.Get, i_GetIntermediateJsonFinished);
//		request.Send();

//		Debug.Log (WSUrl+mymethod+"?client="+_newclient+"&datetime="+_getdatetime+"&enddatetime="+_getdatetime2+"&maximum="+_maximum.ToString());
        _request_waitingnetworkintermediate = new HTTPRequest(new Uri(WSUrl+mymethod), HTTPMethods.Post, i_GetIntermediateJsonFinished);

        _request_waitingnetworkintermediate.Proxy = bufferme.WhichProxy();
        _request_waitingnetworkintermediate.AddField("client",_newclient);
        _request_waitingnetworkintermediate.AddField("datetime",_getdatetime);
        _request_waitingnetworkintermediate.AddField("enddatetime",_getdatetime2);
        _request_waitingnetworkintermediate.AddField("maximum",_maximum.ToString());
        _request_waitingnetworkintermediate.Send();

		GUI_display.ErrorMessage = "Getting intermediate" + camefrom;
		yield return new WaitForSeconds(0.1f);
	}

	void i_GetJsonFinished(HTTPRequest request, HTTPResponse response)
	{
		switch (RetryHttp(response,request,"i_GetJsonFinished"))
		{
		case 0 :		// OK
			//Debug.Log ("REQ: " + request.Uri + " (" + response.StatusCode.ToString () + ")" );
			//Debug.Log (response.DataAsText);
			if ((response.DataAsText == "{}") || (response.DataAsText == "{ }"))
			{
				retreivejson = "";
			}
			else
			{
				//				Debug.Log (locallink.response.DataAsText);
				IDictionary	injson = null;
				try
				{
					injson = (IDictionary)Json.Deserialize(response.DataAsText);
				}
				finally
				{
					if (injson != null)
					{
						GUI_display.ErrorMessage = "";
						foreach(var key in injson.Keys)
						{
							string newtext = (string)injson[key.ToString()];
							retreivejson = bufferme.Decrypt(newtext);
							break;
						}
					}
				}
			}
			break;
		case 1 :		// fail
			break;
		case 2 :		// retry
			return;
		}
		if (getjsoncallback != null)
			getjsoncallback();
		waitingnetwork = false;
	}

	IEnumerator i_GetJson()
	{
		string	mymethod = "getjson.php";

        _request_waitingnetwork = new HTTPRequest(new Uri(WSUrl+mymethod), HTTPMethods.Post, i_GetJsonFinished);
        _request_waitingnetwork.Proxy = bufferme.WhichProxy();
        _request_waitingnetwork.AddField("client",_newclient);
        _request_waitingnetwork.AddField("datetime",_getdatetime);
        _request_waitingnetwork.AddField("enddatetime",_getdatetime2);
        _request_waitingnetwork.AddField("maximum",_maximum.ToString());
        _request_waitingnetwork.Send();
//		Debug.Log ("Getting normal");
		GUI_display.ErrorMessage = "Getting normal";
		yield return new WaitForSeconds(0.1f);
	}


	public static double GetMiliseconds(DateTime mytime)
	{
		DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0); //.ToLocalTime();
		TimeSpan span = (mytime - epoch);
		return span.TotalMilliseconds;
	}

    public static DateTime GetDateTime(double myMs)
    {
        DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc); //.ToLocalTime();
        DateTime dtDateTime = epoch.AddMilliseconds( myMs ); //.ToLocalTime();
        return dtDateTime;
    }



	public static int RetryHttp(HTTPResponse response,HTTPRequest req = null, string erroradd = "")
	{
		bool internetfailed = false;
		bool callfailed = false;

		if (response == null)
		{
            switch (req.State)
            {
                case HTTPRequestStates.Aborted:
                    ni_logger.LogDebug("network", erroradd+".RetryHttp", "HTTP : Aborted");
                    callfailed = true;
                    break;


                default:
                    if (req.launchaborted)
                    {
                        ni_logger.LogDebug("network", erroradd+".RetryHttp", "HTTP : Aborted");
                        callfailed = true;
                    }
                    else
                    {
                        internetfailed = true;
                        ni_logger.LogDebug("network", erroradd + ".RetryHttp", "HTTP Error : No response (NULL) on " + req.State.ToString());
                    }
                    break;
            }
		}
		else
		{
			switch (req.State)
			{
			case HTTPRequestStates.Finished:
				if (!response.IsSuccess)
				{
                    ni_logger.LogDebug("network", erroradd+".RetryHttp", "HTTP Finished : Not response.IsSuccess");
					callfailed = true;
				}
				break;
			case HTTPRequestStates.Error:
                ni_logger.LogDebug("network", erroradd+".RetryHttp", "HTTP : HTTPRequestStates.Error (" + response.StatusCode.ToString() + ")");
				callfailed = true;
				break;
			case HTTPRequestStates.Aborted:
                ni_logger.LogDebug("network", erroradd+".RetryHttp", "HTTP : HTTPRequestStates.Aborted");
				callfailed = true;
				break;
			case HTTPRequestStates.ConnectionTimedOut:
                ni_logger.LogDebug("network", erroradd+".RetryHttp", "HTTP : HTTPRequestStates.ConnectionTimedOut");
				callfailed = true;
				break;
			case HTTPRequestStates.TimedOut:
                ni_logger.LogDebug("network", erroradd+".RetryHttp", "HTTP : HTTPRequestStates.TimedOut");
				callfailed = true;
				break;
			}
		}
		if (internetfailed)
		{
			if ((req != null) && (req.launchcounter < 8))		// eight retries
			{
                req.IsKeepAlive = false;
                req.DisableCache = true;

                HTTPConnection activeCon = HTTPManager.GetConnectionWith(req);
                if (activeCon != null)
                {
                    activeCon.Recycle();
                }

				req.Send();		// send it again
				return 2;
			}
			else
			{
			}
			return 1;
		}
		if (callfailed)		return 1;
		return 0;
	}

}
