﻿using UnityEngine;
using System.Collections;

public class demo_fadeout : MonoBehaviour
{
//	AudioSource audioSource;
//	AudioClip soundbutton1;
//	AudioClip soundbutton2;

	tk2dSprite  sprite;
	float		spritecolor;
	GameObject  startpoint,endpoint;
	GameObject	Camera3D;
	Camera		travelcamera;
	GameObject  go_plane;
	GameObject  go_plane2;
	GameObject  go_whiter;
	tk2dSprite  plane;
	tk2dSprite  plane2;
	tk2dSprite  whiter;

	void Awake ()
	{
		if (Application.loadedLevelName == "Demo_usine")
		{
			go_plane = gameObject.FindInChildren("plane");
			go_plane2 = gameObject.FindInChildren("plane2");
			go_whiter = gameObject.FindInChildren("whiter");
			plane = (tk2dSprite)go_plane.GetComponent<tk2dSprite>();
			plane2 = (tk2dSprite)go_plane2.GetComponent<tk2dSprite>();
			whiter = (tk2dSprite)go_whiter.GetComponent<tk2dSprite>();
		}

//		Debug.Log ("Test " + bufferme.GetString("Cirmeca_TauxCadence_slide1") + "   " + bufferme.GetString("Cirmeca_TauxCadence_slide2"));
		sprite = (tk2dSprite)gameObject.GetComponent<tk2dSprite> ();
		spritecolor = 1.0f;
		startpoint = GameObject.Find ("pi_atelier").FindInChildren ("startpoint");
		endpoint = GameObject.Find ("pi_atelier").FindInChildren ("endpoint");
		Camera3D = GameObject.Find ("pi_atelier").FindInChildren ("3D Camera");
		if (Camera3D == null)
			Camera3D = GameObject.Find ("pi_atelier").FindInChildren ("Main Camera");

		Camera3D.transform.position = startpoint.transform.position;
		Camera3D.transform.eulerAngles = startpoint.transform.eulerAngles;
		travelcamera = (Camera)Camera3D.GetComponent<Camera> ();
		travelcamera.fieldOfView = 29;
	}
	
	IEnumerator Start ()
	{
		while (true)
		{
		if (Application.loadedLevelName == "Demo_usine")
		{
			float alpha = 0.0f;
			float alpha2 = 0.0f;

			while (alpha < 1.0f)
			{
				alpha += Time.deltaTime;
				if (alpha >= 1.0f)		alpha = 1.0f;
				plane.color = new Color(1,1,1,alpha);
				yield return new WaitForSeconds(0.03f);
			}
			yield return new WaitForSeconds(1.0f);
			while (alpha2 < 1.0f)
			{
				alpha -= Time.deltaTime;
				alpha2 += Time.deltaTime;
				if (alpha2 >= 1.0f)		alpha2 = 1.0f;
				if (alpha < 0.0f)		alpha = 0.0f;
				plane.color = new Color(1,1,1,alpha);
				plane2.color = new Color(1,1,1,alpha2);
				yield return new WaitForSeconds(0.03f);
			}
			yield return new WaitForSeconds(1.0f);
			while (alpha2 > 0.0f)
			{
				alpha2 -= Time.deltaTime;
				if (alpha2 < 0.0f)		alpha2 = 0.0f;
				plane2.color = new Color(1,1,1,alpha2);
				yield return new WaitForSeconds(0.03f);
			}
			alpha = 1.0f;
			while (alpha > 0.0f)
			{
				alpha -= Time.deltaTime;
				if (alpha < 0.0f)		alpha = 0.0f;
				whiter.color = new Color(1,1,1,alpha);
				yield return new WaitForSeconds(0.03f);
			}

			go_plane.SetActive(false);
			go_plane2.SetActive(false);
			go_whiter.SetActive(false);
		}

		spritecolor = 1.0f;
/*
		while (true)
		{
			if (Input.GetMouseButtonDown(0))		break;
			yield return new WaitForSeconds (0.04f);
		}
		*/
		while (spritecolor > 0.0f)
		{
			spritecolor -= 0.02f * Time.deltaTime * 60.0f;
			if (spritecolor < 0.0f)		spritecolor = 0.0f;
			sprite.color = new Color (1, 1, 1, spritecolor);
			yield return new WaitForSeconds (0.05f);
		}

		Vector3 vecstart = startpoint.transform.position;
		Vector3 vecend = endpoint.transform.position;
		Vector3 vecstartrot = startpoint.transform.eulerAngles;
		Vector3 vecendrot = endpoint.transform.eulerAngles;
		Vector3 startfov = new Vector3 (29, 0, 0);
		Vector3 endfov = new Vector3(55, 0, 0);

		float t = 0.0f;
		while(t < 1.0f)
		{
			t += Time.deltaTime / 2.0f;
			Vector3 vec = Vector3.Lerp(vecstart, vecend, t);
			Vector3 vecrot = Vector3.Lerp(vecstartrot, vecendrot, t);
			Vector3 fov = Vector3.Lerp(startfov, endfov, t);
			Camera3D.transform.position = vec;
			Camera3D.transform.eulerAngles = vecrot;
			travelcamera.fieldOfView = fov.x;

			yield return new WaitForSeconds (0.05f);
		}
		ni_turnatelier.turnallowed = true;
		gameObject.SetVisible (false);

		if (Application.loadedLevelName == "Demo_usine")
		{
			yield return new WaitForSeconds (5.0f);
			// set needed variables here
			GameObject gobj = GameObject.Find("pointofinterest1");
			if (gobj != null)
			{
				ni_pointofinterest_button pib = (ni_pointofinterest_button)gobj.GetComponent<ni_pointofinterest_button>();
				pib.ButtonPressed();
			}
			yield return new WaitForSeconds (8.0f);
			ni_manage_screens.SetPreviousScreen();

			yield return new WaitForSeconds (3.0f);

			string displayname = ni_preparedatafields.GetMachineName(1);
			bufferme.SetString("Curve_display_rootname",displayname);		// now shows the "machine"
			
			ni_statlegend.button_toggle_index = 1;
			ni_statlegend.shownewstats = true;
			ni_manage_screens.SetNextScreen("statistics_view");
			yield return new WaitForSeconds (8.0f);
			ni_manage_screens.SetPreviousScreen();

			go_plane.SetActive(true);
			go_plane2.SetActive(true);
			go_whiter.SetActive(true);
			plane.color = new Color(1,1,1,0);
			plane2.color = new Color(1,1,1,0);
			whiter.color = new Color(1,1,1,1);
			Camera3D.transform.position = vecstart;
			Camera3D.transform.eulerAngles = vecstartrot;
			travelcamera.fieldOfView = startfov.x;
			gameObject.SetVisible (true);
			sprite.color = new Color (1, 1, 1, 1);
		}
			if (Application.loadedLevelName != "Demo_usine")
				break;
		}
	}
}
