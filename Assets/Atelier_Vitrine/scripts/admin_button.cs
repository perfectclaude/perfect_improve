﻿using UnityEngine;
using System.Collections;

public class admin_button : buttonswap
{
	GameObject father;
	
	public override void ButtonCamera()
	{
		ButtonSetCamera("Interface Camera");
	}
	
	public override void ButtonPressed ()
	{
		base.ButtonPressed();

		MessageBox.Hide();
		manage_Ecrans.AdminMode = true;
		ScreenAdminAbstract.IsReadOnly = (manage_Ecrans.MenuAdminScreen.ReadOnlyGameObject != null);

		manage_Ecrans.CallFromScreen(manage_Ecrans.screensManager.Current);
		manage_Ecrans.screensManager.Current = manage_Ecrans.MenuAdminScreen;

//		manage_Ecrans.remoteListener.ChangeMenu(manage_Ecrans.MenuAdminScreen);
	}
	
}
