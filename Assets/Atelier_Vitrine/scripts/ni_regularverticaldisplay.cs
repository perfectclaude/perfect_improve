﻿using UnityEngine;
using System.Collections;

public class ni_regularverticaldisplay : ni_showdatafields
{
	GameObject gfx_item1;
	GameObject gfx_item2;
	GameObject gfx_item3;
	Texture2D [] innertextures = new Texture2D[10];
	int ninnertextures = 0;

	void OnDestroy()
	{
		for (int i=0;i<ninnertextures;i++)
		{
			Destroy (innertextures[i]);
		}
	}

	GameObject LoadSetImage(string modname,string itemfile)
	{
		GameObject ret = gameObject.FindInChildren(modname);
		ret = ret.FindInChildren("gfx_item");
		if (ret)
		{
			ret.GetComponent<Renderer>().material = new Material (Shader.Find("Unlit/Transparent"));
			
			string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "/" + itemfile;
			//			Debug.Log ("Filename:"+pathname);
			if (System.IO.File.Exists(pathname))
			{
				byte[] imageData = System.IO.File.ReadAllBytes(pathname);
				if (imageData != null)
				{
					mytexture = new Texture2D(0,0,TextureFormat.RGBA32,false);
					mytexture.LoadImage(imageData);
					innertextures[ninnertextures] = mytexture;
					ninnertextures++;
					ret.GetComponent<Renderer>().material.mainTexture = mytexture;
					imageData = null;
				}
			}
		}
		return ret;
	}
	
	void Awake()
	{
		int machine = ni_pointofinterest_button.MachineID;
		_Awake ();
		// load images
		gfx_item1 = LoadSetImage("module_1_3","item_icon_" + machine + "_1.png");
		gfx_item2 = LoadSetImage("module_2_3","item_icon_" + machine + "_2.png");
		gfx_item3 = LoadSetImage("module_3_3","item_icon_" + machine + "_3.png");
		settext.ForceText (base.subtitle, "");
		_ModuleAlert(false,false,false);
	}

	void OnEnable()
	{
		int machine = ni_pointofinterest_button.MachineID;
		_OnEnable ();

		ni_button_ged ged = (ni_button_ged)gameObject.FindInChildren ("button_ged").GetComponent<ni_button_ged> ();
		//		ged.GedLink = "http://10.1.1.200/lizaigne";
		ged.GedLink = "GED/1_Fiches_instructions/RPC_INS_14_Poste_montage_des_manoeuvres_MANG_028_20_028_25.pdf";

		_ModuleAlert(false,false,false);
		switch (ni_regularmainloop.nitemnames[machine])
		{
		case 1 :
			module_3[0].SetActiveRecursively (true);
			module_3[1].SetActiveRecursively (false);
			module_3[2].SetActiveRecursively (false);
			break;
		case 2 :
			module_3[0].SetActiveRecursively (true);
			module_3[1].SetActiveRecursively (true);
			module_3[2].SetActiveRecursively (false);
			break;
		case 3 :
			module_3[0].SetActiveRecursively (true);
			module_3[1].SetActiveRecursively (true);
			module_3[2].SetActiveRecursively (true);
			break;
		}

		_ModuleSetText_3(0, "", "", "");
		_ModuleSetText_3(1, "", "", "");
		_ModuleSetText_3(2, "", "", "");

		tk2dTextMesh tm = (tk2dTextMesh)module_3[0].FindInChildren("background").FindInChildren("title").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,ni_regularmainloop.itemdescriptions[machine,0]);
		tm = (tk2dTextMesh)module_3[1].FindInChildren("background").FindInChildren("title").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,ni_regularmainloop.itemdescriptions[machine,1]);
		tm = (tk2dTextMesh)module_3[2].FindInChildren("background").FindInChildren("title").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,ni_regularmainloop.itemdescriptions[machine,2]);
	}
	
	void Start()
	{
	}

	void Update()
	{
		int machine = ni_pointofinterest_button.MachineID;

		switch (ni_regularmainloop.nitemnames[machine])
		{
		case 1 :
			_ModuleSetText_3(0, ni_regularmainloop.straightvalue[machine,0].ToString(), ni_regularmainloop.itemtitles[machine,0], "affecté : 0");
			break;
		case 2 :
			_ModuleSetText_3(0, ni_regularmainloop.straightvalue[machine,0].ToString(), ni_regularmainloop.itemtitles[machine,0], "affecté : 0");
			_ModuleSetText_3(1, ni_regularmainloop.straightvalue[machine,1].ToString(), ni_regularmainloop.itemtitles[machine,1], "affecté : 0");
			break;
		case 3 :
			_ModuleSetText_3(0, ni_regularmainloop.straightvalue[machine,0].ToString(), ni_regularmainloop.itemtitles[machine,0], "affecté : 0");
			_ModuleSetText_3(1, ni_regularmainloop.straightvalue[machine,0].ToString(), ni_regularmainloop.itemtitles[machine,1], "affecté : 0");
			_ModuleSetText_3(2, ni_regularmainloop.straightvalue[machine,0].ToString(), ni_regularmainloop.itemtitles[machine,2], "affecté : 0");
			break;
		}


//		_ModuleAlert(col1,col2,col3);
		
	}

	/*
	public Camera 		m_ViewCamera;
	bool col1 = false;
	bool col2 = false;
	bool col3 = false;

	*/
/*
	void CreateNewIcon(GameObject obj)
	{
		Vector3	tl = Vector3.zero;
		Vector3	br = Vector3.zero;
		
		Vector3[]	vertex 		= new Vector3	[4];
		int[] 		triangle 	= new int		[3*2];
		Vector2[]	uv			= new Vector2	[4];		
		Color[]		col			= new Color		[4];
		
		tl.y = -1;
		br.y = 1;
		
		tl.x = -1;			
		br.x =  1;
		
		vertex[0] = new Vector3(tl.x,tl.y,0);
		vertex[1] = new Vector3(br.x,tl.y,0);
		vertex[2] = new Vector3(tl.x,br.y,0);
		vertex[3] = new Vector3(br.x,br.y,0);
		
		col[0] = col[1] = col[2] = col[3] = Color.white;
		
		uv[0]		= new Vector2(0,0);
		uv[1]		= new Vector2(1,0);
		uv[2]		= new Vector2(0,1);
		uv[3]		= new Vector2(1,1);
		
		triangle[0] = 0;
		triangle[1] = 2;
		triangle[2] = 1;
		triangle[3] = 2;
		triangle[4] = 3;
		triangle[5] = 1;
		
		Mesh m = new Mesh();
		
		m.vertices  = vertex;
		m.colors	= col;
		m.uv		= uv;
		m.triangles	= triangle;
		
		m.RecalculateNormals();
		m.RecalculateBounds();
		
		obj.GetComponent<MeshFilter>().mesh = m;
	}
	*/

}