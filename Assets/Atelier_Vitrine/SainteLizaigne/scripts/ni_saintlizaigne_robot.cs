﻿using UnityEngine;
using System.Collections;

public class ni_saintlizaigne_robot : ni_showdatafields
{
	/*
	tk2dTextMesh pph;
	tk2dTextMesh bottominfo;
	bool col1 = false;
	bool col2 = false;
	bool col3 = false;

	void Awake()
	{
		_Awake ();
	}

	void LoadSetImage(GameObject ret,string itemfile)
	{
		if (ret)
		{
			ret.renderer.material = new Material (Shader.Find("Unlit/Transparent"));
			
			string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "/" + itemfile;
			byte[] imageData = System.IO.File.ReadAllBytes(pathname);
			if (imageData != null)
			{
				mytexture.LoadImage(imageData);
				ret.renderer.material.mainTexture = mytexture;
				mytexture = new Texture2D(1,1,TextureFormat.RGB24,false);
			}
		}
	}
	


	void SetModuleName(int id,string itemname,string infoname,string gfxname)
	{
		GameObject obj = module_2[0].FindInChildren("module_item"+(id+1));
		tk2dTextMesh tm = (tk2dTextMesh) obj.FindInChildren("item_name").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,itemname);
		tm = (tk2dTextMesh) obj.FindInChildren("info_name").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,infoname);

		obj = obj.FindInChildren("gfx_item");
		if (obj) 
		{
			if (gfxname == "")
				obj.SetActive(false);
			else
			{
				LoadSetImage(obj,gfxname);
			}
		}

		obj = module_2[1].FindInChildren("module_item"+(id+1));
		tm = (tk2dTextMesh) obj.FindInChildren("item_name").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,itemname);
		obj = obj.FindInChildren("gfx_item");
		if (obj) obj.SetActive(false);
	}


	void OnEnable()
	{
		_OnEnable ();
		module_2[0].SetActiveRecursively (true);
		module_2[1].SetActiveRecursively (true);

		ni_button_ged ged = (ni_button_ged)gameObject.FindInChildren ("button_ged").GetComponent<ni_button_ged> ();
//		ged.GedLink = "http://10.1.1.200/lizaigne";
		ged.GedLink = "GED/1_Fiches_instructions/RPC_INS_17_C_Cellule_robotisee_V1.pdf";
		_ModuleAlert(false,false,false);
		_ModuleSetRows_2(true,true,true,true,true,true);

		SetModuleName(0,"CADENCE EN %","pièces par heure","logo_robinet.png");
		SetModuleName(1,"RENDEMENT EN %","","");
		SetModuleName(2,"QUALITE EN %","de rejet\n\npièces à retraiter","logo_double.png");

		tk2dSprite sprt = (tk2dSprite)module_2[0].FindInChildren("alert_icon").GetComponent<tk2dSprite>();
		sprt.SetSprite(sprt.GetSpriteIdByName("icon_agenda"));
		sprt = (tk2dSprite)module_2[1].FindInChildren("alert_icon").GetComponent<tk2dSprite>();
		sprt.SetSprite(sprt.GetSpriteIdByName("icon_chrono"));

		pph = (tk2dTextMesh)module_2[0].FindInChildren("module_item1").FindInChildren("info_head").GetComponent<tk2dTextMesh>();
		bottominfo = (tk2dTextMesh)module_2[0].FindInChildren("module_item2").FindInChildren("info_head").GetComponent<tk2dTextMesh>();
		settext.ForceText(bottominfo,"");
		bottominfo = (tk2dTextMesh)module_2[0].FindInChildren("module_item3").FindInChildren("info_head").GetComponent<tk2dTextMesh>();

	}



	void Start()
	{
	}

	int updatesequence = 0;

	void Update()
	{
		switch (updatesequence)
		{
		case 0 : 
			int nrpieces = (int)ni_preparedatafields.GetDataFieldItem ("Robot_PiecesBonnes");
			settext.ForceText (base.subtitle, nrpieces + " bonnes pièces réalisées");

			updatesequence++;

			tk2dTextMesh tm = (tk2dTextMesh)module_2[0].FindInChildren("title").GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,"La journée depuis " + bufferme.GetString ("StartDateTime"));
			tm = (tk2dTextMesh)module_2[1].FindInChildren("title").GetComponent<tk2dTextMesh>();
			settext.ForceText(tm,"La dernière heure depuis " + bufferme.GetString ("LastHourSince"));

			//		_ModuleAlert(false,false,false);
			break;
		case 1 :
			float fcadencereel = ni_preparedatafields.GetDataFieldItem ("Robot_CadenceReelle");
			int cadencereel = (int)fcadencereel;
			updatesequence++;

			settext.ForceText(pph,cadencereel.ToString());
			break;
		case 2 :
			col1 = false;
			col2 = false;

			if ((ni_saintlizaigne_mainloop.robot_cadence < ni_saintlizaigne_mainloop.robot_mincadence) || (ni_saintlizaigne_mainloop.robot_cadence > ni_saintlizaigne_mainloop.robot_maxcadence))
			{
				col1 = true;
				_ModuleSetValue(0,0,ni_saintlizaigne_mainloop.robot_cadence.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(0,0,ni_saintlizaigne_mainloop.robot_cadence.ToString(),true,Color.green);
			
			if ((ni_saintlizaigne_mainloop.robot_cadence_hour < ni_saintlizaigne_mainloop.robot_mincadence) || (ni_saintlizaigne_mainloop.robot_cadence_hour > ni_saintlizaigne_mainloop.robot_maxcadence))
			{
				col2 = true;
				_ModuleSetValue(1,0,ni_saintlizaigne_mainloop.robot_cadence_hour.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(1,0,ni_saintlizaigne_mainloop.robot_cadence_hour.ToString(),true,Color.green);

			if ((ni_saintlizaigne_mainloop.robot_rendement < ni_saintlizaigne_mainloop.robot_minrendement) || (ni_saintlizaigne_mainloop.robot_rendement > ni_saintlizaigne_mainloop.robot_maxrendement))
			{
				col1 = true;
				_ModuleSetValue(0,1,ni_saintlizaigne_mainloop.robot_rendement.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(0,1,ni_saintlizaigne_mainloop.robot_rendement.ToString(),true,Color.green);
			
			if ((ni_saintlizaigne_mainloop.robot_rendement_hour < ni_saintlizaigne_mainloop.robot_minrendement) || (ni_saintlizaigne_mainloop.robot_rendement_hour > ni_saintlizaigne_mainloop.robot_maxrendement))
			{
				col2 = true;
				_ModuleSetValue(1,1,ni_saintlizaigne_mainloop.robot_rendement_hour.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(1,1,ni_saintlizaigne_mainloop.robot_rendement_hour.ToString(),true,Color.green);

			if ((ni_saintlizaigne_mainloop.robot_qualite < ni_saintlizaigne_mainloop.robot_minquality) || (ni_saintlizaigne_mainloop.robot_qualite > ni_saintlizaigne_mainloop.robot_maxquality))
			{
				col1 = true;
				_ModuleSetValue(0,2,ni_saintlizaigne_mainloop.robot_qualite.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(0,2,ni_saintlizaigne_mainloop.robot_qualite.ToString(),true,Color.green);

			int badelements = (int)ni_preparedatafields.GetDataFieldItem ("Robot_PiecesMauvaises");
			settext.ForceText(bottominfo,(100-ni_saintlizaigne_mainloop.robot_qualite) + "%\n\n"+badelements);

			
			if ((ni_saintlizaigne_mainloop.robot_qualite_hour < ni_saintlizaigne_mainloop.robot_minquality) || (ni_saintlizaigne_mainloop.robot_qualite_hour > ni_saintlizaigne_mainloop.robot_maxquality))
			{
				col2 = true;
				_ModuleSetValue(1,2,ni_saintlizaigne_mainloop.robot_qualite_hour.ToString(),false,Color.red);
			}
			else
				_ModuleSetValue(1,2,ni_saintlizaigne_mainloop.robot_qualite_hour.ToString(),true,Color.green);
			updatesequence = 0;
			_ModuleAlert(col1,col2,false);

			_ModuleAlertSetText_2(ni_saintlizaigne_mainloop.robot_alert1,ni_saintlizaigne_mainloop.robot_alert1);
			break;
		}

	}
	*/
}
