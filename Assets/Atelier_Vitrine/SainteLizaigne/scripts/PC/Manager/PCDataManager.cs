﻿// #define USE_RANDOM

using UnityEngine;
using System.Collections;
using Utils.Network;
using System;
using System.Collections.Generic;

//bufferme.DeleteKey ("ResetAutomateIfAny");

//	RestartAutomate ();




public class PCDataManager : MonoBehaviour
{

	protected const float SEC_TO_HOUR = 0.0002777777778f;

	#region Linked Components
	
	protected			ServerManager			panelsManager;					// Created
//	static			VW_AutomateManager		automateConnection;				// Created
	protected			VW_ERPManager			erpConnection;					// Created
	protected			List<SocketManager>		clients;						// Created
	protected			RemoteListener			remoteListener;					// Found
	#endregion

	#region Private Variables

	// Behaviour
	private				bool					started							= false;
	
	// (parsed) Parameters
	public				ParsedParametersPC		parameters						{ get; private set; }
	protected			string					FilePath;
		
	// Configuration	
	protected			float					ERPTimeout;
	protected			float					DataTimeout;
	protected			float					SendDataTimeout;
	protected			float					SaveTimeout;
	protected			float					WaitMouseTime					= 10;
	
	// Coupures
	protected static	DateTime				LastCoupureTime;
	protected static	bool					Coupure							= false;
	static	int						NbCoupures						= 0;	
	
	// Some booleans
	protected			bool					hasData;
	protected			bool					RedButtonEnabled				= true;
	protected			bool					StopCoroutines					= true;
	protected			bool					Reset							= false;

	// Data
	protected			DateTime				StartDate;
	protected			NetworkPackage			lastData;
	protected			NetworkPackage			firstHourData;
	protected			ERPData					erpData;
	protected			Queue<NetworkPackage>	dataHour;
	
	// Mouse
	protected			float					LastMouseTime					= 0;
	protected			Vector3					LastMousePosition;

	// Admin

	#endregion

	#region Unity callbacks
		
	protected void Start()
	{
		if (!started)
		{
			started = true;
			OnEnable();
		}
	}

	protected void OnEnable()
	{
		if (!started)
			return;

		StopCoroutines = false;
		this.Link();
		this.Init();
		this.ParseParameters();
		this.Configure();
		this.Connect();
	}

	protected void OnDisable()
	{
		StopCoroutines = true;

		this.Disconnect();
		this.Clean();
	}

	protected void Update()
	{
//		if (bufferme.HasKey ("ResetAutomateIfAny"))
//		{
//			bufferme.DeleteKey ("ResetAutomateIfAny");
//			RestartAutomate ();
//		}
	}

	#endregion

	#region Initialisations

	public PCDataManager() : base()
	{
		this.dataHour = new Queue<NetworkPackage>();
	}

	protected static T Get<T>() where T : Component
	{
		return GameObject.FindObjectOfType(typeof(T)) as T;
	}

	protected static T Get<T>(string name) where T : Component
	{
		GameObject g = GameObject.Find(name);
		if (g == null)
			return null;
		return g.GetComponent<T>();
	}

	protected void Link()
	{		
		this.FilePath		= Application.streamingAssetsPath;
		this.remoteListener = Get<RemoteListener>();

	}

	protected void Init()
	{
//		automateConnection		= this.gameObject.AddComponent<VW_AutomateManager>();
		this.erpConnection			= this.gameObject.AddComponent<VW_ERPManager>();
		this.panelsManager			= this.gameObject.AddComponent<ServerManager>();

	}

	protected void ResetAutomate()
	{
/*
		if (automateConnection)
		{
			automateConnection.Disconnect();
			GameObject.Destroy(automateConnection);
		}

		automateConnection = this.gameObject.AddComponent<VW_AutomateManager>();
		this.ConfigureAutomate();	
		*/
	}

	protected void Clean()
	{
//		if(automateConnection)
//			GameObject.Destroy(automateConnection);

		if(this.erpConnection)
			GameObject.Destroy(this.erpConnection);

		if(this.panelsManager)
			GameObject.Destroy(this.panelsManager);

//		automateConnection = null;
		this.erpConnection = null;
		this.panelsManager = null;
	}

	protected void ParseParameters()
	{
		this.parameters = new ParsedParametersPC(FilePath + "/Parameters_release_PC.ini");
	}

	protected void ConfigureAutomate()
	{
//		automateConnection.hostIP = this.parameters.IpAutomate;
//		automateConnection.hostPort = this.parameters.PortAutomate;
	}

	protected void ConfigureAdmin()
	{
		ScreenAdminAbstract.GetSettings				= this.GetParameters;
		ScreenAdminAbstract.OnAdminSettingChanged	+= OnLocalSettingChanged;
	}

	protected void ConfigureRemote()
	{
		if (this.remoteListener)
		{
			this.remoteListener.OnSettingUpdated			+= OnRemoteSettingChanged;
			this.remoteListener.OnCommandClientConnected	+= OnRemoteClientConnected;

			this.remoteListener.Configure(this.parameters);
		}
	}
	
	protected void Configure()
	{
		this.ERPTimeout						= this.parameters.ERPTimeout;
//		this.DataTimeout					= this.parameters.AutomateTimeout;
		this.SendDataTimeout				= this.parameters.UpdateScreensTimeout;
		this.SaveTimeout					= this.parameters.SaveTimeout;
		this.WaitMouseTime					= this.parameters.MouseTimeout;

		this.erpConnection.host				= this.parameters.IPHoteERP;
		this.erpConnection.db				= this.parameters.DbNameERP;
		this.erpConnection.user				= this.parameters.UserERP;
		this.erpConnection.pass				= this.parameters.PassERP;

//		this.ConfigureAutomate();	

		this.panelsManager.port				= this.parameters.ServeurPort;

		this.ConfigureRemote();
		this.ConfigureAdmin();
	}
			
	#endregion

	#region Network

	protected void SendClientData()
	{
		List<SocketManager> connected = new List<SocketManager>();
		foreach (SocketManager client in clients)
		{
			if (client.IsConnected)
			{
				if (this.hasData)
				{
					client.Send(AutomateAndERPInfos.GetBytes());
				}
				connected.Add(client);
			}
			else
			{
				GameObject.Destroy(client);
			}
		}
		this.clients = connected;
	}

	protected void ConnectAutomate()
	{
		/*
		automateConnection.OnDataReceived				+= OnDataReceived;
		automateConnection.OnDisconnectionByteReceived	+= OnDisconnectionByteReceived;
		automateConnection.OnDisconnection				+= OnAutomateLostConnection;
		automateConnection.OnResetDataByteReceived		+= OnResetByteReceived;

		StartCoroutine(AutomateConnectionCoroutine());
		*/
	}

	protected void Connect()
	{
		this.hasData = false;

//		ConnectAutomate();

		erpConnection.Connect();
		
		this.clients = new List<SocketManager>();

		this.panelsManager.OnClientConnected += (client) =>
		{
			clients.Add(client);
		};

		this.panelsManager.BindPortAndListen();
	}

	protected void Disconnect()
	{
//		if (automateConnection != null)
//			automateConnection.Disconnect();

		if (this.erpConnection != null)
			this.erpConnection.Close();

		if(this.panelsManager != null)
			this.panelsManager.Stop();

	}

	protected void OnAutomateLostConnection(SocketManager s)
	{
		this.OnAutomateDisconnected();
	}

	protected void OnDisconnectionByteReceived()
	{
		this.OnAutomateDisconnected();
	}

	protected void OnAutomateDisconnected()
	{
		if (remoteListener)
			remoteListener.enabled = false;

		this.Reset = true;
		this.ResetAutomate();
		this.Connect();
	}

	protected void OnDataReceived(NetworkPackage data)
	{
		this.hasData = true;
		this.lastData = data;
		this.dataHour.Enqueue(data);
	}

	protected void OnResetByteReceived()
	{
		this.lastData = new NetworkPackage();
		this.dataHour.Clear();
		this.StartDate = DateTime.Now;
	}

	protected void OnConnected()
	{
		if(!Reset)
			this.LaunchDataCoroutine();

		if (remoteListener)
			remoteListener.enabled = true;

		Reset = false;
	}

	protected bool MustContinueCoroutines()
	{
		return !StopCoroutines;
	}

	protected IEnumerator AutomateConnectionCoroutine()
	{
		/*
		do
		{
			yield return automateConnection.StartCoroutine(automateConnection.ConnectCoroutine());

			if (!automateConnection.IsConnected)
			{
				Debug.Log ("Automate not working...");
				yield return new WaitForSeconds(15);
			}
			else
			{		
				this.OnConnected();
			}
		} 
		while (!automateConnection.IsConnected);
		*/
		yield return null;
	}
	
	#endregion

	#region Remote events handlers

	private void OnRemoteSettingChanged(string name, string value)
	{
		this.parameters.ChangeSetting(name, value);
	}

	private void OnRemoteClientConnected(SocketManager client)
	{
		LastMouseTime = 0;

		Dictionary<string, string> dico = this.parameters.ToDictionary();
		foreach (KeyValuePair<string, string> pair in dico)
		{
			this.remoteListener.ChangeSetting(pair.Key, pair.Value);
		}
	}
	#endregion

	#region Local events Handlers

	private void OnLocalSettingChanged(string name, string value)
	{
		this.remoteListener.ChangeSetting(name, value);
		this.parameters.ChangeSetting(name, value);
	}


	#endregion

	#region Permanent Coroutines

	// Ces quatres coroutines s'executent 'parallèlement'
	protected void LaunchDataCoroutine()
	{
		StartCoroutine(AskERPCoroutine());
		StartCoroutine(UpdateDataCoroutine());
		StartCoroutine(AskDataCoroutine());
		StartCoroutine(SaveDataCoroutine());
	}

	protected IEnumerator AskERPCoroutine()
	{
		while (MustContinueCoroutines())
		{
			// yield return this.StartCoroutine(this.erpConnection.QueryCoroutine());
			// this.erpData = this.erpConnection.LastDataReceived;
			
			this.erpData = this.erpConnection.Query();
//			this.erpData.Write(FilePath);
			yield return new WaitForSeconds(ERPTimeout);
		}
	}

	protected IEnumerator AskDataCoroutine()
	{
		while (MustContinueCoroutines())
		{
//			automateConnection.Send(VW_AutomateManager.QueryType.DATA);
			yield return new WaitForSeconds(DataTimeout);
		}
	}

	protected IEnumerator UpdateDataCoroutine()
	{
		if (MustContinueCoroutines())
		{
			this.CleanDataHour();
			this.FirstUpdateData();
			this.UpdateData();

			while (MustContinueCoroutines())
			{
				yield return new WaitForSeconds(SendDataTimeout);

				if (RedButtonEnabled)
				{
					this.CleanDataHour();
					this.UpdateData();
				}

				this.SendClientData();
			}
		}
	}

	protected IEnumerator SaveDataCoroutine()
	{
		while (MustContinueCoroutines() && !hasData)
		{
			yield return new WaitForSeconds(DataTimeout);
		}

		while (MustContinueCoroutines())
		{
			this.SaveData();
			yield return new WaitForSeconds(SaveTimeout);
		}
	}
	
	#endregion

	#region Coroutine functions

	// Called every 'SendDataTimeout' seconds. Before 'SendData'
	protected void CleanDataHour()
	{
		NetworkPackage data;

		if (this.dataHour.Count > 0)
		{
			DateTime now = DateTime.Now;

			data = this.dataHour.Peek();
			while (this.dataHour.Count > 0 && (now - data.Date).Hours >= 1)
			{
				this.dataHour.Dequeue();

				if (this.dataHour.Count > 0)
				{
					data = this.dataHour.Peek();
				}
			}
		}
		else
		{
			data = new NetworkPackage();
		}

		this.firstHourData = data;
	}
	
	// Called once the first package has been received
	protected void FirstUpdateData()
	{
		this.StartDate = DateTime.Now;
	}

	// Called every 'SendDataTimeout' seconds. After 'CleanDataHour'
	protected void UpdateData()
	{
		if (hasData)
		{
			parameters.LoadVariables(); // Updates parameters (if possible)

			NetworkPackage hData = lastData - firstHourData;
			bool oneHour = ((DateTime.Now - this.StartDate).TotalHours >= 1);

			AutomateAndERPInfos.heuresInfos		= ComputeHeuresValues(StartDate);
//			AutomateAndERPInfos.cirmecaInfos	= ComputeCirmecaValues(oneHour, lastData, hData, parameters);
			AutomateAndERPInfos.generalInfos	= ComputeGeneralValues(erpData, parameters);
		}
	}


	// Called every 'SaveDataTimeout' seconds.
	protected void SaveData()
	{
//		if(this.RedButtonEnabled)
//			this.lastData.Write(FilePath);
	}

	#endregion

	#region Computation ZONE

	protected static HeuresInfos ComputeHeuresValues(DateTime start)
	{
		HeuresInfos heuresData = new HeuresInfos();

		heuresData.heureDebut = start;
		heuresData.heureCourante = DateTime.Now;
		heuresData.heureDerniere = heuresData.heureCourante - TimeSpan.FromHours(1);

		if (heuresData.heureDerniere < heuresData.heureDebut)
			heuresData.heureDerniere = heuresData.heureDebut;

		return heuresData;
	}





	protected static CirmecaInfos ComputeCirmecaValues(bool hour, NetworkPackage data, NetworkPackage dataHour, ParsedParametersPC parameters)
	{
		CirmecaInfos cirmecaData = new CirmecaInfos();
		/*
		Debug.Log ("Compute Cirmeca Values");
		Debug.Log ("PiecesBonnesCirmeca" + data.PiecesBonnesCirmeca);
		Debug.Log ("PiecesBonnesRobot" + data.PiecesBonnesRobot);
		Debug.Log ("PiecesMauvaisesRobot" + data.PiecesMauvaisesRobot);

		int nrfields;
//		float inval = 0;

		if (!Application.loadedLevelName.Contains("DEMO"))
		{
			if (bufferme.HasKey("DataFields"))
			{
				nrfields = bufferme.GetInt("DataFields");
				int n;
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_"+n);
					
					if (datanameid.Contains("TempsEnMarche"))
					{
					}
					else
					{
						// These values MUST be set by the dedicated
						if (pi_button_forcemachineonoff.machineon)
						{
							int nconv = 0;
							foreach (var key in ni_preparedatafields.ConversionValues.Keys)
							{
								if (key.ToString() == datanameid)
								{
									bufferme.SetFloat(datanameid+"_value",ERPData.gettingdata[nconv]);
                                    bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct                           
								}
								nconv++;
							}

							if (datanameid == "Cirmeca_PiecesBonnes")
                            {                     
								bufferme.SetFloat(datanameid+"_value",data.PiecesBonnesCirmeca);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
							else if (datanameid == "Cirmeca_CoupuresBarrieres")
                            {                     
								bufferme.SetFloat(datanameid+"_value",data.CoupuresBarrieresCirmeca);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
							else if (datanameid == "Robot_PiecesBonnes")
                            {                     
								bufferme.SetFloat(datanameid+"_value",data.PiecesBonnesRobot);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
							else if (datanameid == "Robot_PiecesMauvaises")
                            {                     
								bufferme.SetFloat(datanameid+"_value",data.PiecesMauvaisesRobot);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
						}
					}
				}
			}
		}
		return cirmecaData;
*/
		return cirmecaData;
	}
	
	protected static GeneralInfos ComputeGeneralValues(ERPData erpData, ParsedParametersPC parameters)
	{
		GeneralInfos generalData = new GeneralInfos();
		/*
		Debug.Log ("Compute GeneralValues Values");
		Debug.Log ("Stocks_affecte_ProduitsFinis" + erpData.StockProduitsFinisAffecte);
		Debug.Log ("Stocks_affecte_Manoeuvres" + erpData.StockManoeuvreAffecte);
		Debug.Log ("Stocks_non_affecte_ProduitsFinis" + erpData.StockProduitsFinisNonAffecte);
		Debug.Log ("Stocks_non_affecte_Manoeuvres" + erpData.StockManoeuvreNonAffecte);
		Debug.Log ("Tests_TauxAirEau" + erpData.TauxEchecTestsAirEau);
		*/

		ni_datarecovery.savedata += "----------ComputeGeneralValues-------\n";
		ni_datarecovery.savedata += "Stocks_affecte_ProduitsFinis" + erpData.StockProduitsFinisAffecte+"\n";
		ni_datarecovery.savedata += "Stocks_affecte_Manoeuvres" + erpData.StockManoeuvreAffecte+"\n";
		ni_datarecovery.savedata += "Stocks_non_affecte_ProduitsFinis" + erpData.StockProduitsFinisNonAffecte+"\n";
		ni_datarecovery.savedata += "Stocks_non_affecte_Manoeuvres" + erpData.StockManoeuvreNonAffecte+"\n";
		ni_datarecovery.savedata += "Tests_TauxAirEau" + erpData.TauxEchecTestsAirEau+"\n";
		System.IO.File.WriteAllText(Application.streamingAssetsPath + "/outpollhttp.txt",ni_datarecovery.savedata);

		int nrfields;
//		float inval;
		if (!Application.loadedLevelName.Contains("DEMO"))
		{
			if (bufferme.HasKey("DataFields"))
			{
				nrfields = bufferme.GetInt("DataFields");
				int n;
				for (n=0;n<nrfields;n++)
				{
					string datanameid = bufferme.GetString("DataField_"+n);
					
					if (datanameid.Contains("TempsEnMarche"))
					{
					}
					else
					{
						// These values MUST be set by the dedicated
						if (pi_button_forcemachineonoff.machineon)
						{
							int nconv = 0;
							foreach (var key in ni_preparedatafields.ConversionValues.Keys)
							{
								if (key.ToString() == datanameid)
								{
									bufferme.SetFloat(datanameid + "_value",ERPData.gettingdata[nconv]);
                                    bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
								}
								nconv++;
							}

							if (datanameid == "Stocks_affecte_ProduitsFinis")
                            {
								bufferme.SetFloat(datanameid+"_value",erpData.StockProduitsFinisAffecte);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
							else if (datanameid == "Stocks_affecte_Manoeuvres")
                            {
								bufferme.SetFloat(datanameid+"_value",erpData.StockManoeuvreAffecte);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
							else if (datanameid == "Stocks_non_affecte_ProduitsFinis")
                            {
								bufferme.SetFloat(datanameid+"_value",erpData.StockProduitsFinisNonAffecte);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
							else if (datanameid == "Stocks_non_affecte_Manoeuvres")
                            {
								bufferme.SetFloat(datanameid+"_value",erpData.StockManoeuvreNonAffecte);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
							else if (datanameid == "Tests_TauxAirEau")
                            {
								bufferme.SetFloat(datanameid+"_value",erpData.TauxEchecTestsAirEau);
                                bufferme.SetBool( datanameid + "_quality", true ); // Flag la valeur Comme Correct
                            }
						}
					}
				}
			}
		}

		return generalData;
	}




	protected static void GetMessageAndStateMin(ref string msg, ref bool state, float val, float min, string inf, string ok)
	{
		if (val < min)
		{
			msg = inf;
			state = false;
			return;
		}

		msg = ok;
		state = true;
	}

	protected static void GetMessageAndStateMinMax(ref string msg, ref bool state, int val, int min, int max, string inf, string ok, string sup)
	{
		if (val < min)
		{
			msg = inf;
			state = false;
			return;
		}

		if (val > max)
		{
			msg = sup;
			state = false;
			return;
		}

		msg = ok;
		state = true;
	}

	#endregion

	#region Getters
	
	public bool ConnectedToAutomate
	{
		get
		{
			return false;
//			return	automateConnection != null &&
//					automateConnection.IsConnected;
		}
	}

	private ParsedParametersPC GetParameters()
	{
		return this.parameters;
	}

	private int GetIndexOf(WorkshopElement element)
	{
		return -1;
	}


	#endregion


	public static void RestartAutomate()
	{
//		automateConnection.Send(VW_AutomateManager.QueryType.RESET);
		NbCoupures = 0;
	}

}
