﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe permettant de deboguer la classe PCDataManager
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
[System.Obsolete("Cette classe ne devrait pas être utilisée en build")]
public class DebugPCDataManager : PCDataManager
{
#if DEBUG_MODE
	public Color	GUIColor;
	public bool		ShowGUIData;

	private State state;

	private enum State
	{
		NONE,
		LAST_DATA,
		CONNECTIONS
	}

	static State ButtonState(State current, string legend, State state)
	{
		State res = current;

		if (current == state)
			GUI.enabled = false;

		if (GUILayout.Button(legend))
			res = state;

		GUI.enabled = true;

		return res;
	}
	
	void OnGUI()
	{
		if (ShowGUIData)
		{
			GUI.color = GUIColor;

			GUILayout.BeginHorizontal();
			state = ButtonState(state, "Automate", State.LAST_DATA);
			state = ButtonState(state, "Connections", State.CONNECTIONS);
			GUILayout.EndHorizontal();

			switch (state)
			{
				case State.LAST_DATA:
					this.ShowLastData();
					break;

				case State.CONNECTIONS:
					this.ShowConnections();
					break;
			}
		}
	}

	void ShowLastData()
	{
		if (this.automateConnection != null && this.automateConnection.IsConnected)
		{
			GUILayout.Label("Package size : " + this.automateConnection.packageSize);

			if (GUILayout.Button("Ask data"))
			{
				this.automateConnection.Send(VW_AutomateManager.QueryType.DATA);
			}

			if (GUILayout.Button("Reset data"))
			{
				this.automateConnection.Send(VW_AutomateManager.QueryType.RESET);
				this.automateConnection.Send(VW_AutomateManager.QueryType.DATA);
			}

			if (GUILayout.Button("Disconnect"))
			{
				this.automateConnection.Send(VW_AutomateManager.QueryType.DISCONNECT);
			}
		}
		else
		{
			if (GUILayout.Button("Connect"))
			{
				this.Connect();
			}
		}
	}

	void ShowConnections()
	{
		GUILayout.Label("Remote listener state : " + this.remoteListener.CurrentState.ToString());
		if (this.remoteListener.CurrentState == RemoteListener.State.CONNECTED)
		{
			GUILayout.Label("\t - " + this.remoteListener.EstablishedConnectionName);
		}

		GUILayout.Label("Data connections : " + this.clients.Count);
		foreach (SocketManager client in this.clients)
		{
			GUILayout.Label("\t - " + client.Name);
		}

		GUILayout.Label("Automate connection : " + this.ConnectedToAutomate);
		GUILayout.Label("ERP connection : " + this.erpConnection.IsConnected);
	}
#endif
}
