﻿using System;
using System.Globalization;
using System.IO;

/// <summary>
/// Données récupèrées depuis l'ERP
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public struct ERPData 
{
	public DateTime	Date;
	public int		StockProduitsFinisNonAffecte;   // 2028 20
    public int      StockManoeuvreNonAffecte;		// MANG 2028 20
    public int      StockProduitsFinisAffecte;      // 2028 20
    public int      StockManoeuvreAffecte;		    // MANG 2028 20
	public float	TauxEchecTestsAirEau;

	public static float	[] gettingdata = new float[100];

	private static string ToFileName(DateTime dt)
	{
		return "ERP_" + dt.ToString("yyyyMMddhhmmss") + ".ini";
	}

	public void Write(string rootDirectory)
	{
#if !UNITY_EDITOR
		int year = this.Date.Year;
		int month = this.Date.Month;
		int day = this.Date.Day;
	//	int hour = this.Date.Hour;

		string name = ToFileName(this.Date);

		string path = rootDirectory + "/" + year.ToString("D4");
		if (!Directory.Exists(path))
			Directory.CreateDirectory(path);

		path += "/" + month.ToString("D2");
		if (!Directory.Exists(path))
			Directory.CreateDirectory(path);

		path += "/" + day.ToString("D2");
		if (!Directory.Exists(path))
			Directory.CreateDirectory(path);

		path += "/" + name;
		using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
		{
			StreamWriter w = new StreamWriter(file);
			w.Write(this.ToString());
			w.Close();
		}
#endif
	}

	public override string ToString()
	{
		string str = string.Empty;

		str += "[General]\r\n";
		str += "\tDate=" + FR(this.Date) + "\r\n";
		str += "[Stocks affecte]\r\n";
		str += "\tProduitsFinis=" + this.StockProduitsFinisAffecte + "\r\n";
		str += "\tManoeuvres=" + this.StockManoeuvreAffecte + "\r\n";
        str += "[Stocks non affecte]\r\n";
        str += "\tProduitsFinis=" + this.StockProduitsFinisNonAffecte + "\r\n";
        str += "\tManoeuvres=" + this.StockManoeuvreNonAffecte + "\r\n";
		str += "[Tests]\r\n";
		str += "\tTauxAirEau=" + this.TauxEchecTestsAirEau;

		return str;
	}

	private static string FR(DateTime dt)
	{
		CultureInfo frFr = new CultureInfo("fr-FR");
		return dt.ToString(frFr);
	}
}
