﻿/// <summary>
/// Données récupèrées depuis le(s) fichier(s) de paramètres
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class ParsedParametersPC : InGameSettings
{
	#region Variables
	public string	DataFile						{ get; private set; }
	public string	PasswordAdmin					{ get; private set; }
	public string	IPHoteERP						{ get; private set; }
	public string	DbNameERP						{ get; private set; }
	public string	UserERP							{ get; private set; }
	public string	PassERP							{ get; private set; }
	public string	IpAutomate						{ get; private set; }
	public int		PortAutomate					{ get; private set; }
	public int		ServeurPort						{ get; private set; }
	public float	ERPTimeout						{ get; private set; }
	public float	AutomateTimeout					{ get; private set; }
	public float	UpdateScreensTimeout			{ get; private set; }
	public float	SaveTimeout						{ get; private set; }
	public float	MouseTimeout					{ get; private set; }
	public float	SwapScreensTimeout				{ get; private set; }
	public int		RemoteServeurPort				{ get; private set; }
	public float	BroadcastTimeout				{ get; private set; }
	public int		TimeoutRemoteController			{ get; private set; }
	#endregion

	public ParsedParametersPC(string path) : base()
	{
		IniFile file = IniFile.Load(path);

	// +--------------------------+-----------------+---------------------+---------+----------------------------------+	//
	// |		  VARIABLE		   |	  CLASS		 |		  PROPERTY		|	TYPE  |	  DEFAULT VALUE (or Mandatory)	  |	//
	// +--------------------------+-----------------+---------------------+---------+----------------------------------+	//
		
		DataFile					= file["Donnees"]	["CheminFichier"]	.ToPath		(path);
		PasswordAdmin				= file["Donnees"]	["MotDePasse"]		.ToString	("admin");
		IPHoteERP					= file["ERP"]		["AdresseIP"]		.ToString	();
		DbNameERP					= file["ERP"]		["BaseDeDonnees"]	.ToString	();
		UserERP						= file["ERP"]		["Utilisateur"]		.ToString	();
		PassERP						= file["ERP"]		["MotDePasse"]		.ToString	();
		IpAutomate					= file["Automate"]	["AdresseIP"]		.ToString	();
		PortAutomate				= file["Automate"]	["Port"]			.ToInt		();
		ServeurPort					= file["Serveur"]	["PortData"]		.ToInt		();
		RemoteServeurPort			= file["Serveur"]	["PortRemote"]		.ToInt		();
		ERPTimeout					= file["Timeouts"]	["ERP"]				.ToFloat	(200);
		AutomateTimeout				= file["Timeouts"]	["Automate"]		.ToFloat	(5);
		UpdateScreensTimeout		= file["Timeouts"]	["UpdateScreens"]	.ToFloat	(5);
		SwapScreensTimeout			= file["Timeouts"]	["SwapScreens"]		.ToFloat	(10);
		SaveTimeout					= file["Timeouts"]	["Saves"]			.ToFloat	(300);
		BroadcastTimeout			= file["Timeouts"]	["Broadcast"]		.ToFloat	(5);
		TimeoutRemoteController		= file["Timeouts"]	["iPad"]			.ToInt		(60);
		MouseTimeout				= file["Timeouts"]	["Souris"]			.ToFloat	(20);

		SHA1Password = SHA1(PasswordAdmin);
				
		this.LoadVariables();
	}

	public void LoadVariables()
	{
		IniFile file;

		try
		{
			file = IniFile.Load(DataFile);
		}
		catch (System.Exception e)
		{
			file = null;
			UnityEngine.Debug.Log(e);
		}

		if (file == null)
		{
			return;
		}

		this.LoadVariablesFrom(file);	
	}

	public override void ChangeSetting(string name, string value)
	{
		base.ChangeSetting(name, value);		// Sets the variable (property).
		if(!name.Contains("Password/"))
			this.SaveVariable(name, value);		// Save the variable in the file.	
	}

	private void SaveVariable(string name, string value)
	{
		IniFile file;

		try
		{
			file = IniFile.Load(DataFile);
		}
		catch (System.Exception e)
		{
			file = null;
			UnityEngine.Debug.Log(e);
		}

		if (file == null)
		{
			return;
		}

		int sep = name.IndexOf('/');
		string section = name.Remove(sep);
		string property = name.Substring(sep + 1);

		file[section][property].value = value;

		file.Save(this.DataFile);
	}


}
