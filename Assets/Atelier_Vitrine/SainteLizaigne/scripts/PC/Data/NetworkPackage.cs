﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

/// <summary>
/// Données récupèrées depuis l'Automate
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public struct NetworkPackage
{
	public uint		TempsEnMarcheCirmeca;
	public uint		TempsEnDefautCirmeca;
	public uint		PiecesBonnesCirmeca;
	public ushort	CoupuresBarrieresCirmeca;
	public uint		TempsEnMarcheRobot;
	public uint		TempsEnDefautRobot;
	public uint		PiecesBonnesRobot;
	public uint		PiecesMauvaisesRobot;
	public bool		MarcheArretCirmeca;
	public bool		MarcheArretRobot;
	public DateTime Date;

	private static uint GetChance(float chance) // ex: 1/3
	{
		float f = UnityEngine.Random.Range(0f, 1f);

		if (f <= chance)
			return 1;

		return 0;
	}

	public static NetworkPackage GetRandom(NetworkPackage previous, uint time, float chanceCirmeca, float chanceRobot, float chanceRejet)
	{		
		NetworkPackage rnd = new NetworkPackage();
		
		rnd.TempsEnMarcheCirmeca		= time;
		rnd.TempsEnDefautCirmeca		= 0;
		rnd.PiecesBonnesCirmeca			= GetChance(chanceCirmeca);
		rnd.CoupuresBarrieresCirmeca	= 0;
		rnd.TempsEnMarcheRobot			= time;
		rnd.TempsEnDefautRobot			= 0;
		rnd.PiecesBonnesRobot			= GetChance(chanceRobot);
		rnd.PiecesMauvaisesRobot		= GetChance(chanceRejet);
		rnd.MarcheArretCirmeca			= true;
		rnd.MarcheArretRobot			= true;
		
		rnd.Date = DateTime.Now;

		return rnd + previous;
	}

	//public static NetworkPackage Zero
	//{
	//	get
	//	{
	//		NetworkPackage data = new NetworkPackage();
	//
	//		data.TempsEnMarcheCirmeca		= 0;
	//		data.TempsEnDefautCirmeca		= 0;
	//		data.PiecesBonnesCirmeca		= 0;
	//		data.CoupuresBarrieresCirmeca	= 0;
	//		data.TempsEnMarcheRobot			= 0;
	//		data.TempsEnDefautRobot			= 0;
	//		data.PiecesBonnesRobot			= 0;
	//		data.PiecesMauvaisesRobot		= 0;
	//		data.MarcheArretCirmeca			= false;
	//		data.MarcheArretRobot			= false;
	//		data.Date						= DateTime.MinValue;
	//
	//		return data;
	//	}
	//}

	public static NetworkPackage operator+(NetworkPackage a, NetworkPackage b)
	{
		a.TempsEnMarcheCirmeca		+= b.TempsEnMarcheCirmeca;
		a.TempsEnDefautCirmeca		+= b.TempsEnDefautCirmeca;		
		a.PiecesBonnesCirmeca		+= b.PiecesBonnesCirmeca;	
		a.CoupuresBarrieresCirmeca	+= b.CoupuresBarrieresCirmeca;
		a.TempsEnMarcheRobot		+= b.TempsEnMarcheRobot;
		a.TempsEnDefautRobot		+= b.TempsEnDefautRobot;
		a.PiecesBonnesRobot			+= b.PiecesBonnesRobot;
		a.PiecesMauvaisesRobot		+= b.PiecesMauvaisesRobot;

		return a;
	}

	public static NetworkPackage operator-(NetworkPackage a, NetworkPackage b)
	{
		a.TempsEnMarcheCirmeca		-= b.TempsEnMarcheCirmeca;
		a.TempsEnDefautCirmeca		-= b.TempsEnDefautCirmeca;		
		a.PiecesBonnesCirmeca		-= b.PiecesBonnesCirmeca;	
		a.CoupuresBarrieresCirmeca	-= b.CoupuresBarrieresCirmeca;
		a.TempsEnMarcheRobot		-= b.TempsEnMarcheRobot;
		a.TempsEnDefautRobot		-= b.TempsEnDefautRobot;
		a.PiecesBonnesRobot			-= b.PiecesBonnesRobot;
		a.PiecesMauvaisesRobot		-= b.PiecesMauvaisesRobot;

		return a;
	}

	private static string ToFileName(DateTime dt)
	{
		return "Automate_" + dt.ToString("yyyyMMddhhmmss") + ".ini";
	}

	public void Write(string rootDirectory)
	{
#if !UNITY_EDITOR
		int year = this.Date.Year;
		int month = this.Date.Month;
		int day = this.Date.Day;
	//	int hour = this.Date.Hour;

		string name = ToFileName(this.Date);

		string path = rootDirectory + "/" + year.ToString("D4");
		if (!Directory.Exists(path))
			Directory.CreateDirectory(path);

		path += "/" + month.ToString("D2");
		if (!Directory.Exists(path))
			Directory.CreateDirectory(path);

		path += "/" + day.ToString("D2");
		if (!Directory.Exists(path))
			Directory.CreateDirectory(path);

		path += "/" + name;
		using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
		{
			StreamWriter w = new StreamWriter(file);
			w.Write(this.ToString());
			w.Close();
		}
#endif
	}

	public override string ToString()
	{
		string str = string.Empty;

		str += "[General]\r\n";
		str += "\tDate="				+ FR(this.Date)					+ "\r\n";
		str += "[Cirmeca]\r\n";
		str += "\tTempsEnMarche="		+ this.TempsEnMarcheCirmeca		+ "\r\n";
		str += "\tTempsEnDefaut="		+ this.TempsEnDefautCirmeca		+ "\r\n";
		str += "\tPiecesBonnes="		+ this.PiecesBonnesCirmeca		+ "\r\n";
		str += "\tCoupuresBarrieres="	+ this.CoupuresBarrieresCirmeca + "\r\n";
		str += "\tEnMarche="			+ FR(this.MarcheArretCirmeca)	+ "\r\n";
		str += "[Robot]\r\n";
		str += "\tTempsEnMarche="		+ this.TempsEnMarcheRobot		+ "\r\n";
		str += "\tTempsEnDefaut="		+ this.TempsEnDefautRobot		+ "\r\n";
		str += "\tPiecesBonnes="		+ this.PiecesBonnesRobot		+ "\r\n";
		str += "\tPiecesMauvaises="		+ this.PiecesMauvaisesRobot		+ "\r\n";
		str += "\tEnMarche="			+ FR(this.MarcheArretRobot)		+ "\r\n";

		return str;
	}

	private static string FR(bool b)
	{
		return b ? "Vrai" : "Faux";
	}

	private static string FR(DateTime dt)
	{
		CultureInfo frFr = new CultureInfo("fr-FR");
		return dt.ToString(frFr);
	}
}
