﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;
using System;
using System.IO;
//using System.Reflection;
//using System.Threading;

/// <summary>
/// Classe gérant la connexion du serveur avec l'ERP
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class VW_ERPManager : SQLManager 
{	
	string QUERY_AIR_EAU_FILE 	    = bufferme.GetClientId() + "/sql_queries/AirEau.query.txt";
	string QUERY_STOCK_PF_TNA_FILE    = bufferme.GetClientId() + "/sql_queries/StockPF.query.txt";
	string QUERY_STOCK_MANG_TNA_FILE  = bufferme.GetClientId() + "/sql_queries/StockMang.query.txt";
	string QUERY_STOCK_PF_TA_FILE     = bufferme.GetClientId() + "/sql_queries/StockAffectePF.query.txt";
	string QUERY_STOCK_MANG_TA_FILE   = bufferme.GetClientId() + "/sql_queries/StockAffecteMang.query.txt";

	private ERPData data;
	
	private static string GetSQL(string file)
	{
		string str = null;
		file = Application.streamingAssetsPath + "/" + file;
		using(FileStream f = new FileStream(file, FileMode.Open, FileAccess.Read))
		{
			using(TextReader i = new StreamReader(file))
			{
				str = i.ReadToEnd();
			}
		}
		return str;
	}
	
	public ERPData LastDataReceived { get; private set; }

	public ERPData Query()
	{
		string sql;
		Hashtable result;
		ERPData data = new ERPData();

		if (!this.IsConnected)
		{
			this.Connect();
		}

		if (this.IsConnected)
		{
			int nconv = 0;
			foreach (var value in ni_preparedatafields.ConversionValues.Values)
			{
				string sqlname = value.ToString();
				if (sqlname.Contains("SQL:"))
				{
					sqlname = sqlname.Replace("SQL:","");
					sqlname = bufferme.GetClientId() + "/sql_queries/" + sqlname + ".txt";
				
					sql = GetSQL(sqlname);
					result = this.QueryOne(sql);
					if (result != null)
					{
						ERPData.gettingdata[nconv] = (float)((double)result["Stock"]);
						switch(nconv)
						{
						case 11 :
							data.StockManoeuvreNonAffecte = (int)((double)result["Stock"]);
							break;
						case 10 :
							data.StockProduitsFinisNonAffecte = (int)((double)result["Stock"]);
							break;
						case 9 :
							data.StockManoeuvreAffecte = (int)((double)result["Stock"]);
							break;
						case 8 :
							data.StockProduitsFinisAffecte = (int)((double)result["Stock"]);
							break;
						case 12 :
							//Debug.Log(result["QTE_REBUS"].GetType().Name);
							
							double rebuts	= (double)result["QTE_REBUS"];
							double ok		= (double)result["QTE_OK"];
							double qte		= rebuts + ok;
							
							if (Near0(qte))
							{
								data.TauxEchecTestsAirEau = 0;
							}
							else
							{
								data.TauxEchecTestsAirEau = (float)(rebuts / qte);
							}								
							break;
						}
					}
				}
				nconv++;
			}
			/*
			sql = GetSQL(QUERY_STOCK_MANG_TNA_FILE);
			result = this.QueryOne(sql);
			if (result != null)
				data.StockManoeuvreNonAffecte = (int)((double)result["Stock"]);

            sql = GetSQL(QUERY_STOCK_PF_TNA_FILE);
			result = this.QueryOne(sql);
			if (result != null)
				data.StockProduitsFinisNonAffecte = (int)((double)result["Stock"]);

            sql = GetSQL(QUERY_STOCK_MANG_TA_FILE);
            result = this.QueryOne(sql);
            if (result != null)
                data.StockManoeuvreAffecte = (int)((double)result["Stock"]);

            sql = GetSQL(QUERY_STOCK_PF_TA_FILE);
            result = this.QueryOne(sql);
            if (result != null)
                data.StockProduitsFinisAffecte = (int)((double)result["Stock"]);


			sql = GetSQL(QUERY_AIR_EAU_FILE);
			result = this.QueryOne(sql);
			if (result != null)
			{
				//Debug.Log(result["QTE_REBUS"].GetType().Name);

				double rebuts	= (double)result["QTE_REBUS"];
				double ok		= (double)result["QTE_OK"];
				double qte		= rebuts + ok;

				if (Near0(qte))
				{
					data.TauxEchecTestsAirEau = 0;
				}
				else
				{
					data.TauxEchecTestsAirEau = (float)(rebuts / qte);
				}								
			}
			*/
		}

		data.Date = DateTime.Now;

		return data;
	}

	public static bool Near0(double d)
	{
		const double EPSILON = 0.001f;
		return d > -EPSILON && d < EPSILON; 
	}
}
