﻿using UnityEngine;
using System.Collections;
using Utils.Network;
using System;

using B = Utils.Network.Binary;

/// <summary>
/// Classe gérant la connexion du serveur avec l'automate
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class VW_AutomateManager : SocketManager
{
	const int PACKAGE_SIZE = 32;

	public enum QueryType
	{
		DISCONNECT	= 0,
		RESET		= 1,
		DATA		= 2,
		NOTHING		= 3
	}

	public bool							DebugPackage;

	public event Action					OnDisconnectionByteReceived;
	public event Action					OnResetDataByteReceived;
	public event Action<NetworkPackage>	OnDataReceived;
	public event Action					OnIdleByteReceived;

	private byte[] package;
	private bool disconnectionAsked;

	public string	hostIP;
	public int		hostPort;

	public int packageSize
	{
		get
		{
			if (package == null)
				return 0;

			return package.Length;
		}
	}

	protected override void OnConnection(bool first)
	{
		disconnectionAsked = false;
		StartCoroutine(this.ReceiveCoroutine(OnReceive));		
	}

	public void Send(QueryType type)
	{
		this.Send(new byte [1] { (byte)type });
	}

	private void OnManagePackage()
	{
		if (package != null && package.Length >= 1)
		{
			byte packageType = package[0];

			switch ((QueryType)packageType)
			{
				case QueryType.DISCONNECT: // Fin de connexion

					if (this.OnDisconnectionByteReceived != null)
						this.OnDisconnectionByteReceived();

					disconnectionAsked = true;

					B.ReducePackage(ref package, 1);
					OnManagePackage();
					break;
				case QueryType.RESET: // Réinitialisation des compteurs complètée

					if (this.OnResetDataByteReceived != null)
						this.OnResetDataByteReceived();

					B.ReducePackage(ref package, 1);
					OnManagePackage();
					break;
				case QueryType.DATA: // Réception des données

					if (package.Length >= PACKAGE_SIZE)
					{
						NetworkPackage d = new NetworkPackage();
						


					int nconv = 0;
					foreach (var value in ni_preparedatafields.ConversionValues.Values)
					{
						string sqlname = value.ToString();
						if (sqlname.Contains("AUT:"))
						{
							sqlname = sqlname.Replace("AUT:","");
							string [] myvalues = sqlname.Split(':');
							int bytepos = 0;
							int varsize = 0;
							int.TryParse(myvalues[0],out bytepos);
							int.TryParse(myvalues[1],out varsize);
							uint getvalue = BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, bytepos, varsize)), 0);

							ERPData.gettingdata[nconv] = (float)getvalue;		// new data

							switch (nconv)
							{
							case 0 :
								d.TempsEnMarcheCirmeca = getvalue;
								break;
							case 1 :
								d.TempsEnDefautCirmeca = getvalue;
								break;
							case 2 :
								d.PiecesBonnesCirmeca = getvalue;
								break;
							case 3 :
								d.CoupuresBarrieresCirmeca = (ushort)getvalue;
								break;
							case 4 :
								d.TempsEnMarcheRobot = getvalue;
								break;
							case 5 :
								d.TempsEnDefautRobot = getvalue;
								break;
							case 6 :
								d.PiecesBonnesRobot = getvalue;
								break;
							case 7 :
								d.PiecesMauvaisesRobot = getvalue;
								break;
							}
							nconv++;
						}
					}
					/*
						d.TempsEnMarcheCirmeca		= BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x01, 4)), 0);
						d.TempsEnDefautCirmeca		= BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x05, 4)), 0);
						d.PiecesBonnesCirmeca		= BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x09, 4)), 0);
						d.CoupuresBarrieresCirmeca	= BitConverter.ToUInt16(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x0d, 2)), 0);
						d.TempsEnMarcheRobot		= BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x0f, 4)), 0);
						d.TempsEnDefautRobot		= BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x13, 4)), 0);
						d.PiecesBonnesRobot			= BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x17, 4)), 0);
						d.PiecesMauvaisesRobot		= BitConverter.ToUInt32(B.FromNetworkBigEndian(B.GetPackagePart(package, 0x1b, 4)), 0);

						byte flags = package[0x1f];
						d.MarcheArretRobot = ((flags & 1) != 0);
						d.MarcheArretCirmeca = ((flags & 2) != 0);

*/
						B.ReducePackage(ref package, PACKAGE_SIZE);
						OnManagePackage();

						d.Date = DateTime.Now;

						if (this.OnDataReceived != null)
							this.OnDataReceived(d);
					}

					break;
				case QueryType.NOTHING: // Indication de présence bien recue

					if (this.OnIdleByteReceived != null)
						this.OnIdleByteReceived();

					B.ReducePackage(ref package, 1);
					OnManagePackage();
					break;

				default: // Indeterminé.
					package = null;
					break;
			}
			
			if (disconnectionAsked && (package == null || package.Length == 0))
			{
				this.Disconnect();
			}
		}
	}

	private void OnReceive(byte[] received)
	{
		if(DebugPackage)
			Debug.Log(B.PackageToString(received));

		if (package == null || package.Length == 0)
			package = received;
		else
			package = Binary.Join(new byte[][] { package, received });

		OnManagePackage();
		
		if(this.IsConnected)
		{
			StartCoroutine(this.ReceiveCoroutine(OnReceive));
		}
	}
	
	public IEnumerator ConnectCoroutine()
	{
		return this.ConnectCoroutine(hostIP, true, hostPort);
	}

}
