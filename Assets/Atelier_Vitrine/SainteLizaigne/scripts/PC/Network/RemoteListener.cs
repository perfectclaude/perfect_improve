﻿using UnityEngine;
using System.Collections;
using Utils.Network;
using System;

/// <summary>
/// Classe gérant les télécommandes du côté serveur
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class RemoteListener : MulticastManager
{
	/*

	#region Admin screens management

	public ScreenAdminAbstract[] Menus;

	private int GetIndexOf(ScreenAdminAbstract menu)
	{
		for (int i = 0; i < Menus.Length; i++)
		{
			if (Menus[i] == menu)
				return i;
		}

		return -1;
	}

	#endregion
*/

	#region Events

	public event Action<bool>					OnRedButtonClicked;
	public event Action<string, string>			OnSettingUpdated;
	public event Action<SocketManager>			OnCommandClientConnected;
	public event Action<SocketManager>			OnCommandClientDisconnected;
	public event Action<int>					OnWorkshopClicked;
	public event Action<int>					OnWorkshopClosed;
	public event Action<ScreenAdminAbstract>	OnAdminMenuOpened;
	public event Action							OnAdminMenuClosed;
		
	#endregion

	#region Getters

	public enum State
	{
		DISABLED,
		SEARCHING,
		CONNECTED,
		PENDING
	}

	public string EstablishedConnectionName
	{
		get
		{
			if (this.EstablishedConnection)
				return this.EstablishedConnection.Name;

			return null;
		}
	}

	public State CurrentState
	{
		get
		{
			if (!this.enabled)
				return State.DISABLED;

			if (this.HasJoinedBroadcast && this.ConnectionListener && this.ConnectionListener.IsListening)
				return State.SEARCHING;

			if (this.EstablishedConnection != null && EstablishedConnection.IsConnected)
				return State.CONNECTED;

			return State.PENDING;
		}
	}

	#endregion
	
	#region Updates controller
	/*
	public void CloseMenu()
	{
		this.Send(RemoteMessages.MSG_ADMIN_MENU, -1);
	}

	public void ChangeMenu(ScreenAdminAbstract menu)
	{
		int index = GetIndexOf(menu);
		if (index != -1)
		{
			this.Send(RemoteMessages.MSG_ADMIN_MENU, index);
		}
	}
*/
	public void ChangeSetting(string name, string value)
	{
		this.Send(RemoteMessages.MSG_CHANGE_SETTING, "[" + name + "|" + value + "]");
	}

	public void UpdateControllerRedButton(bool enabled)
	{
		this.Send(RemoteMessages.MSG_RED_BUTTON, (enabled ? "true" : "false"));
	}
		
	#endregion

	#region Unity callbacks
		
	public void OnEnable()
	{
		this.disabled = false;
		this.ConnectionListener = this.gameObject.AddComponent<ServerManager>();
		this.EstablishedConnection = null;

		this.ConnectionListener.OnClientConnected += OnClientConnected;
		this.ConnectionListener.OnClientDisconnected += OnClientDisconnected;
		this.ConnectionListener.TimeoutMessage = RemoteMessages.MSG_KICKED + "\n";
		this.ConnectionListener.ClientsConnectionTimeOut = clientTimeout;

		this.StartBroadcastAndListen();
	}

	public void OnDisable()
	{
		this.disabled = true;
		this.Stop();

		if (this.ConnectionListener)
		{
			this.ConnectionListener.Stop();
			GameObject.Destroy(this.ConnectionListener);
			this.ConnectionListener = null;
		}
		
		this.OnClientDisconnected(this.EstablishedConnection);
	}
	
	#endregion

	#region Network


	private ServerManager ConnectionListener;
	private SocketManager EstablishedConnection;

	private int		cmdPort;
	private int		dataPort;
	private float	clientTimeout;
	private float	broadcastTimeout;
	
	private bool	disabled = false;
	private string	Before = "";

	public bool HasClient
	{
		get
		{
			return this.EstablishedConnection != null && this.EstablishedConnection.IsConnected;
		}
	}

	private void Send(string query, object data)
	{
		this.Send(query + '=' + data.ToString());
	}

	private void Send(string query)
	{
		if (this.HasClient)
		{
#if VERBOSE
			GUILog.Add("Send : " + query, Color.blue);
#endif
			this.EstablishedConnection.Send_UTF8_BE(query + "\n");
		}
	}

	public void Configure(ParsedParametersPC parameters)
	{
		dataPort			= parameters.ServeurPort;
		cmdPort				= parameters.RemoteServeurPort;
		clientTimeout		= parameters.TimeoutRemoteController;
		broadcastTimeout	= parameters.BroadcastTimeout;
	}

	private void OnClientConnected(SocketManager client)
	{
		this.Stop();
		this.ConnectionListener.Stop();

		if (this.HasClient)
		{
			Debug.Log("New remote, kicked current one");
			this.Send(RemoteMessages.MSG_KICKED);
			this.EstablishedConnection.Disconnect();
			this.EstablishedConnection = null;
		}
		
		this.EstablishedConnection = client;
		this.Send(RemoteMessages.MSG_CONNECTED);
		this.EstablishedConnection.StartCoroutine(this.EstablishedConnection.ReceiveCoroutine(OnDataReceived));
		
		if(this.OnCommandClientConnected != null)
			this.OnCommandClientConnected(client);
	}

	private void OnClientDisconnected(SocketManager client)
	{
		if (client != null && client == this.EstablishedConnection)
		{			
			this.EstablishedConnection.Disconnect();
			this.EstablishedConnection = null;
			
			if(this.OnCommandClientDisconnected != null)
				this.OnCommandClientDisconnected(client);
		}

		if (this.EstablishedConnection == null && !this.HasJoinedBroadcast)
		{
			this.StartBroadcastAndListen();
		}
	}

	private void OnDataReceived(byte [] package)
	{
		string command, variable, name, value;
		int integer;
//		bool redButton;

		string data = Before + Binary.BytesToStrUTF8(Binary.FromNetworkBigEndian(package));
		int enter = data.IndexOf('\n');
		while (enter != -1)
		{
			Before = data.Substring(enter + 1);
			data = data.Remove(enter);
#if VERBOSE
			GUILog.Add("Received : " + data, Color.blue);
#endif
			if (data.Contains("="))
			{
				string[] parts = data.Split('=');
				command = parts[0].Trim();
				variable = parts[1].Trim();
			}
			else
			{
				command = data.Trim();
				variable = null;
			}

			switch (command)
			{
				// Not managed :
				//
				// - MSG_WAITING_CONTROLLER
				// - MSG_IDLE
				// - MSG_CONNECTED
				// 

				case RemoteMessages.MSG_CLICKED:

					if (int.TryParse(variable, out integer))
					{
						if (this.OnWorkshopClicked != null)
						{
							this.OnWorkshopClicked(integer);
						}
					}

					break;

				case RemoteMessages.MSG_CLOSED:

					if (int.TryParse(variable, out integer))
					{
						if (this.OnWorkshopClosed != null)
						{
							this.OnWorkshopClosed(integer);
						}
					}

					break;

				case RemoteMessages.MSG_CHANGE_SETTING:

					variable = variable.Replace("[", "").Replace("]", "");

					integer = variable.IndexOf("|");
					if(integer != -1)
					{
						name = variable.Remove(integer);
						value = variable.Substring(integer + 1);

						if (this.OnSettingUpdated != null)
							this.OnSettingUpdated(name, value);
					}

					break;
/*
				case RemoteMessages.MSG_RED_BUTTON:

					redButton = (variable == "true");
					if (redButton || variable == "false")
					{
						if (this.OnRedButtonClicked != null)
							this.OnRedButtonClicked(redButton);
					}

					break;

				case RemoteMessages.MSG_ADMIN_MENU:
					if (int.TryParse(variable, out integer))
					{
						if (integer == -1)
						{
							if (this.OnAdminMenuClosed != null)
							{
								this.OnAdminMenuClosed();
							}
						}
						else
						{
							if (this.OnAdminMenuOpened != null)
							{
								this.OnAdminMenuOpened(this.Menus[integer]);
							}
						}
					}
					break;
*/
				case RemoteMessages.MSG_DISCONNECTION:
					this.OnClientDisconnected(this.EstablishedConnection);
					break;
			}

			data = Before;
			enter = data.IndexOf('\n');
		}

		if(this.EstablishedConnection)
			this.EstablishedConnection.StartCoroutine(this.EstablishedConnection.ReceiveCoroutine(OnDataReceived));
	}
	
	private void StartBroadcastAndListen()
	{
		if (!HasJoinedBroadcast && !this.disabled)
		{
			if (this.JoinBroadcast(RemoteMessages.BROADCAST_PORT))
			{
				if (!this.ConnectionListener.IsListening)
					this.ConnectionListener.BindPortAndListen(cmdPort);

				this.StartCoroutine(BroadcastServer());
			}
		}
	}

	public IEnumerator BroadcastServer()
	{
		while (this.HasJoinedBroadcast)
		{
			if (EstablishedConnection == null)
			{
				string data = RemoteMessages.MSG_WAITING_CONTROLLER + 
								" cmd=" + cmdPort + 
								" data=" + dataPort +
								" timeout=" + clientTimeout
								+ "\n";

				this.Send(Binary.ToNetworkBigEndian(Binary.StrToBytesUTF8(data)));
			}

			yield return new WaitForSeconds(broadcastTimeout);
		}
	}

	#endregion
}
