﻿using UnityEngine;
using System;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

/// <summary>
/// Classe gérant les requêtes SQL
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class SQLManager : MonoBehaviour {

	string GetConnectionString(string serverAddress, string database, string user, string pass)
	{
		return	"Server="	+ serverAddress + ";" +
				"Database=" + database		+ ";" + 
				"Uid="		+ user			+ ";" +
				"Pwd="		+ pass			+ ";" + 
                "Connection Timeout=2;";
	}

	public string host = "127.0.0.1";
	public string db = "sql_manager";
	public string user = "root";
	public string pass = "";

	private SqlConnection connection;

	public bool IsConnected
	{
		get
		{
			return connection != null;
		}
	}

	public void Connect()
	{
		// claude
		connection = null;
//		return;

		string connectionString = this.GetConnectionString(host, db, user, pass);
		try
		{
			connection = new SqlConnection(connectionString);
			connection.Open(); 
		}
		catch (SqlException e)
		{
			connection = null;
			Debug.LogError(e.Message);
		}
	}

	public void Close()
	{
		if (connection != null)
			connection.Close();
	}

	public Hashtable QueryOne(string cmd)
	{
		Hashtable resultat = null;

		if (connection != null)
		{
			try
			{
				using (SqlCommand sql = this.connection.CreateCommand())
				{
					sql.CommandText = cmd;
					using (SqlDataReader reader = sql.ExecuteReader())
					{
						if (reader.Read())
						{
							resultat = new Hashtable();

							for (int i = 0; i < reader.FieldCount; i++)
							{
								resultat.Add(reader.GetName(i), reader.GetValue(i));
							}
						}
					}
				}
			}
			catch (SqlException e)
			{
				resultat = null;
				Debug.LogError(e.Message);
			}
		}

		return resultat;
	}

	public List<Hashtable> QueryMany(string cmd)
	{
		List<Hashtable> resultat = null;
		Hashtable currentEntry;

		if (connection != null)
		{
			try
			{
				using (SqlCommand sql = this.connection.CreateCommand())
				{
					sql.CommandText = cmd;
					using (SqlDataReader reader = sql.ExecuteReader())
					{
						resultat = new List<Hashtable>();
						while (reader.Read())
						{
							currentEntry = new Hashtable();

							for(int i = 0; i < reader.FieldCount; i++)
							{
								currentEntry.Add(reader.GetName(i), reader.GetSqlValue(i));
							}

							resultat.Add(currentEntry);
						}
					}
				}
			}
			catch (SqlException e)
			{
				resultat = null;
				Debug.LogError(e.Message);
			}
		}

		return resultat;
	}
}
