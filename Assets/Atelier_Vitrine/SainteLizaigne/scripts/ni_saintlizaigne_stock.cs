﻿using UnityEngine;
using System.Collections;

public class ni_saintlizaigne_stock : ni_showdatafields
{
	/*
	GameObject gfx_item1;
	GameObject gfx_item2;
	GameObject gfx_item3;
	public Camera 		m_ViewCamera;
	bool col1 = false;
	bool col2 = false;
	bool col3 = false;

	GameObject LoadSetImage(string modname,string itemfile)
	{
		GameObject ret = gameObject.FindInChildren(modname);
		ret = ret.FindInChildren("gfx_item");
		if (ret)
		{
			ret.renderer.material = new Material (Shader.Find("Unlit/Transparent"));

			string pathname = Application.streamingAssetsPath + "/" + bufferme.GetClientId() + "/" + itemfile;
//			Debug.Log ("Filename:"+pathname);
			byte[] imageData = System.IO.File.ReadAllBytes(pathname);
			if (imageData != null)
			{
				mytexture.LoadImage(imageData);
				ret.renderer.material.mainTexture = mytexture;
				mytexture = new Texture2D(1,1,TextureFormat.RGB24,false);
			}
		}
		return ret;
	}

	void Awake()
	{
		m_ViewCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
		_Awake ();
		gfx_item1 = LoadSetImage("module_1_3","logo_robinet.png");
		gfx_item2 = LoadSetImage("module_2_3","logo_manille.png");
		gfx_item3 = LoadSetImage("module_3_3","logo_bulles.png");

		settext.ForceText (base.subtitle, "");
		_ModuleAlert(false,false,false);
	}

	void OnEnable()
	{
		_OnEnable ();
		module_3[0].SetActiveRecursively (true);
		module_3[1].SetActiveRecursively (true);
		module_3[2].SetActiveRecursively (true);

		ni_button_ged ged = (ni_button_ged)gameObject.FindInChildren ("button_ged").GetComponent<ni_button_ged> ();
//		ged.GedLink = "http://10.1.1.200/lizaigne";
		ged.GedLink = "GED/1_Fiches_instructions/RPC_INS_14_Poste_montage_des_manoeuvres_MANG_028_20_028_25.pdf";

		_ModuleAlert(false,false,false);
		_ModuleSetText_3(0, "", "", "");
		_ModuleSetText_3(1, "", "", "");
		_ModuleSetText_3(2, "", "", "");

		tk2dTextMesh tm = (tk2dTextMesh)module_3[0].FindInChildren("background").FindInChildren("title").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,"Stock produits finis");
		tm = (tk2dTextMesh)module_3[1].FindInChildren("background").FindInChildren("title").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,"Stock manoeuvres");
		tm = (tk2dTextMesh)module_3[2].FindInChildren("background").FindInChildren("title").GetComponent<tk2dTextMesh>();
		settext.ForceText(tm,"Tests air-eau");
	}

	void Start()
	{
	}
	
	void Update()
	{
		_ModuleSetText_3(0, ni_preparedatafields.GetValueWithKeyInt("Stocks_affecte_ProduitsFinis"), "Produits finis", "affecté : 0");
		_ModuleSetText_3(1, ni_preparedatafields.GetValueWithKeyInt("Stocks_non_affecte_ProduitsFinis"), "Composants", "affecté : 0");
		_ModuleSetText_3(2, "11%", "de pièces fuyardes", "");	

		_ModuleAlert(col1,col2,col3);

	}
	*/
/*
	void CreateNewIcon(GameObject obj)
	{
		Vector3	tl = Vector3.zero;
		Vector3	br = Vector3.zero;
		
		Vector3[]	vertex 		= new Vector3	[4];
		int[] 		triangle 	= new int		[3*2];
		Vector2[]	uv			= new Vector2	[4];		
		Color[]		col			= new Color		[4];
		
		tl.y = -1;
		br.y = 1;
		
		tl.x = -1;			
		br.x =  1;
		
		vertex[0] = new Vector3(tl.x,tl.y,0);
		vertex[1] = new Vector3(br.x,tl.y,0);
		vertex[2] = new Vector3(tl.x,br.y,0);
		vertex[3] = new Vector3(br.x,br.y,0);
		
		col[0] = col[1] = col[2] = col[3] = Color.white;
		
		uv[0]		= new Vector2(0,0);
		uv[1]		= new Vector2(1,0);
		uv[2]		= new Vector2(0,1);
		uv[3]		= new Vector2(1,1);
		
		triangle[0] = 0;
		triangle[1] = 2;
		triangle[2] = 1;
		triangle[3] = 2;
		triangle[4] = 3;
		triangle[5] = 1;
		
		Mesh m = new Mesh();
		
		m.vertices  = vertex;
		m.colors	= col;
		m.uv		= uv;
		m.triangles	= triangle;
		
		m.RecalculateNormals();
		m.RecalculateBounds();
		
		obj.GetComponent<MeshFilter>().mesh = m;
	}
	*/

}