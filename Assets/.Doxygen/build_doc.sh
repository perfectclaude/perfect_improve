#!/bin/sh

doxygen   		./Doxyfile_build
./open_doc.sh

bytes=`wc -c ./err.log | cut -f7 -d' '`

if [ "$bytes" != "0" ]
then
	notepad++ ./err.log &
else
	rm ./err.log
fi