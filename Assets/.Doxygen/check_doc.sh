#!/bin/sh

doxygen   		./Doxyfile_check

bytes=`wc -c ./toDocumentate.txt | cut -f7 -d' '`

if [ "$bytes" != "0" ]
then
	cmd //C useRegex.bat
	notepad++ ./toDocumentate.txt &
else
	rm ./toDocumentate.txt	
	./open_doc.sh
fi