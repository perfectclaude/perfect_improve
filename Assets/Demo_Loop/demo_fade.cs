﻿using UnityEngine;
using System.Collections;

public class demo_fade : MonoBehaviour
{
	tk2dSprite plane;
	tk2dSprite plane2;
	tk2dSprite plane3;

	void Awake ()
	{
		plane = (tk2dSprite)GameObject.Find("plane").GetComponent<tk2dSprite>();
		plane2 = (tk2dSprite)GameObject.Find("plane2").GetComponent<tk2dSprite>();
		plane3 = (tk2dSprite)GameObject.Find("plane3").GetComponent<tk2dSprite>();
		StartCoroutine("fadinout");
	}
	
	IEnumerator fadinout()
	{
		float alpha = 0.0f;
		float alpha2 = 0.0f;
		float alpha3 = 0.0f;

		while (alpha < 1.0f)
		{
			alpha += Time.deltaTime;
			if (alpha >= 1.0f)		alpha = 1.0f;
			plane.color = new Color(1,1,1,alpha);
			yield return new WaitForSeconds(0.03f);
		}
		yield return new WaitForSeconds(1.0f);
		while (alpha2 < 1.0f)
		{
			alpha -= Time.deltaTime;
			alpha2 += Time.deltaTime;
			if (alpha2 >= 1.0f)		alpha2 = 1.0f;
			if (alpha < 0.0f)		alpha = 0.0f;
			plane.color = new Color(1,1,1,alpha);
			plane2.color = new Color(1,1,1,alpha2);
			yield return new WaitForSeconds(0.03f);
		}
		yield return new WaitForSeconds(1.0f);
		while (alpha3 < 1.0f)
		{
			alpha2 -= Time.deltaTime;
			alpha3 += Time.deltaTime;
			if (alpha3 >= 1.0f)		alpha3 = 1.0f;
			if (alpha2 < 0.0f)		alpha2 = 0.0f;
			plane2.color = new Color(1,1,1,alpha2);
			plane3.color = new Color(1,1,1,alpha3);
			yield return new WaitForSeconds(0.03f);
		}
		Application.LoadLevel("Demo_usine");
//		yield return new WaitForSeconds(1.0f);
/*
		while (alpha3 > 0.0f)
		{
			alpha3 -= Time.deltaTime;
			if (alpha3 < 0.0f)		alpha3 = 0.0f;
			plane3.color = new Color(1,1,1,alpha3);
			yield return new WaitForSeconds(0.03f);
		}
		*/
	}
}
