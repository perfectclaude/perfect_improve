﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;


public class EditorTools : EditorWindow
{
	[MenuItem("Window/EditorTools")]
	public static void  ShowWindow()
	{
		EditorWindow.GetWindow(typeof(EditorTools));
	}

	void OnEnable()
	{
	}

	void OnGUI()
	{
		if (GUILayout.Button("PlayerPrefs.DeleteAll()"))
		{
			PlayerPrefs.DeleteAll();
			PlayerPrefs.Save();
		}

	}
}
