//
//  ImageTools.h
//  MobileToolkit
//
//  Created by Ryan on 30/01/2015.
//
//

#import <Foundation/Foundation.h>

@interface ImageTools : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

-(id)init;
-(void)imagePickerController:(UIImagePickerController*)picker
       didFinishPickingMediaWithInfo:(NSDictionary*)info;

@end
