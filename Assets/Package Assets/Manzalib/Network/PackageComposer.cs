﻿using System;
using System.Collections.Generic;

public class PackageComposer
{
	private List<byte> package;
	public int Adress;

	public int Size
	{
		get
		{
			return Adress; // silly isn't it ? :)
		}
	}

	public PackageComposer()
	{
		this.package = new List<byte>();
		this.Adress = 0;
	}

	public PackageComposer(int size)
	{
		this.package = new List<byte>(size);
		this.Adress = 0;
	}

	public void InsertHeader()
	{
		this.InsertAt(42, 0);
	}

	public byte[] Package
	{
		get
		{
			return this.package.ToArray();
		}
	}

	public void InsertAt(byte[] bytes, int adress)
	{
		this.package.InsertRange(adress, new byte[bytes.Length]);
		this.SetAt(bytes, adress);
	}

	public void InsertAt(byte b, int adress)
	{
		this.package.Insert(adress, b);
	}

	public void InsertAt(int number, int adress)
	{
		InsertAt(BitConverter.GetBytes(number), adress);
	}

	public void SetAt(byte[] bytes, int adress)
	{
		int length = bytes.Length;

		if (adress + length > package.Count)
		{
			throw new IndexOutOfRangeException();
		}

		if (!BitConverter.IsLittleEndian)
		{
			for (int i = 0; i < length; i++)
			{
				package[adress + i] = bytes[i];
			}
		}
		else
		{
			for (int i = 0; i < length; i++)
			{
				package[adress + i] = bytes[(length - 1) - i];
			}
		}

		this.Adress = adress + length;
	}

	public void SetAt(byte b, int adress)
	{
		if (adress >= package.Count)
		{
			throw new IndexOutOfRangeException();
		}

		package[adress] = b;

		this.Adress = adress + 1;
	}

	public void SetAt(int number, int adress)
	{
		SetAt(BitConverter.GetBytes(number), adress);
	}

	public void SetAt(uint number, int adress)
	{
		SetAt(BitConverter.GetBytes(number), adress);
	}

	public void SetAt(long number, int adress)
	{
		SetAt(BitConverter.GetBytes(number), adress);
	}

	public void SetAt(DateTime dt, int adress)
	{
		SetAt(dt.ToBinary(), adress);
	}

	public void SetAt(ushort number, int adress)
	{
		SetAt(BitConverter.GetBytes(number), adress);
	}
	
	public void SetAt(bool b0, int adress)
	{
		SetAt(b0, false, false, false, false, false, false, false, adress);
	}

	public void SetAt(bool b0, bool b1, int adress)
	{
		SetAt(b0, b1, false, false, false, false, false, false, adress);
	}

	public void SetAt(bool b0, bool b1, bool b2, int adress)
	{
		SetAt(b0, b1, b2, false, false, false, false, false, adress);
	}

	public void SetAt(bool b0, bool b1, bool b2, bool b3, int adress)
	{
		SetAt(b0, b1, b2, b3, false, false, false, false, adress);
	}

	public void SetAt(bool b0, bool b1, bool b2, bool b3, bool b4, int adress)
	{
		SetAt(b0, b1, b2, b3, b4, false, false, false, adress);
	}

	public void SetAt(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5, int adress)
	{
		SetAt(b0, b1, b2, b3, b4, b5, false, false, adress);
	}

	public void SetAt(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5, bool b6, int adress)
	{
		SetAt(b0, b1, b2, b3, b4, b5, b6, false, adress);
	}

	public void SetAt(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5, bool b6, bool b7, int adress)
	{
		byte b = 0;

		if (b0)
			b += 1;
		if (b1)
			b += 2;
		if (b2)
			b += 4;
		if (b3)
			b += 8;
		if (b4)
			b += 16;
		if (b5)
			b += 32;
		if (b6)
			b += 64;
		if (b7)
			b += 128;

		SetAt(b, adress);
	}

	public void Add(byte[] bytes)
	{
		int diff = (this.Adress + bytes.Length - package.Count);
		if (diff > 0)
		{
			this.package.AddRange(new byte[diff]);
		}

		this.SetAt(bytes, this.Adress);
	}

	public void Add(byte b)
	{
		int diff = (this.Adress + 1 - package.Count);
		if (diff > 0)
		{
			this.package.Add(new byte());
		}

		this.SetAt(b, this.Adress);
	}

	public void Add(int number)
	{
		Add(BitConverter.GetBytes(number));
	}

	public void Add(uint number)
	{
		Add(BitConverter.GetBytes(number));
	}

	public void Add(long number)
	{
		Add(BitConverter.GetBytes(number));
	}

	public void Add(DateTime dt)
	{
		Add(dt.ToBinary());
	}

	public void Add(ushort number)
	{
		Add(BitConverter.GetBytes(number));
	}

	public void Add(bool b0)
	{
		Add(b0, false, false, false, false, false, false, false);
	}

	public void Add(bool b0, bool b1)
	{
		Add(b0, b1, false, false, false, false, false, false);
	}

	public void Add(bool b0, bool b1, bool b2)
	{
		Add(b0, b1, b2, false, false, false, false, false);
	}

	public void Add(bool b0, bool b1, bool b2, bool b3)
	{
		Add(b0, b1, b2, b3, false, false, false, false);
	}

	public void Add(bool b0, bool b1, bool b2, bool b3, bool b4)
	{
		Add(b0, b1, b2, b3, b4, false, false, false);
	}

	public void Add(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5)
	{
		Add(b0, b1, b2, b3, b4, b5, false, false);
	}

	public void Add(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5, bool b6)
	{
		Add(b0, b1, b2, b3, b4, b5, b6, false);
	}

	public void Add(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5, bool b6, bool b7)
	{
		byte b = 0;

		if (b0)
			b += 1;
		if (b1)
			b += 2;
		if (b2)
			b += 4;
		if (b3)
			b += 8;
		if (b4)
			b += 16;
		if (b5)
			b += 32;
		if (b6)
			b += 64;
		if (b7)
			b += 128;

		Add(b);
	}
}
