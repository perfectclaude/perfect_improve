﻿// #define VERBOSE

using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace Utils
{
	namespace Network
	{
		/// <summary>
		/// Manages a Multicast/Broadcast UDP 'Client'
		/// </summary>
		public class MulticastManager : MonoBehaviour
		{
			/// <summary>
			/// Durée des datagrammes dans le réseau.
			/// Evite d'avoir des messages qui restent indéfiniment dans le réseau et surcharge les machines
			/// </summary>
			public int			DatagramsTTL = 50;

			/// <summary>
			/// Coordonnées de la machine du dernier message reçu
			/// </summary>
			private IPEndPoint	LastReceivePoint;

			/// <summary>
			/// Données du dernier message reçu
			/// </summary>
			private byte[]		LastReceivedData;

			/// <summary>
			/// Thread qui attend et reçoit un message
			/// </summary>
			private Thread		RunningReceiveThread;

			/// <summary>
			/// Vrai si on a lancé la coroutine de reception des messages
			/// </summary>
			private bool		IsReceiveCoroutineStarted;

			/// <summary>
			/// Coordonées du groupe multicast contacté.
			/// Si broadcast : vaut {IPAddress.Broadcast, Port}
			/// </summary>
			private IPEndPoint	Group;			

			/// <summary>
			/// Adresse du groupe contacté
			/// Si broadcast : IPAddress.Broadcast
			/// </summary>
			private IPAddress	GroupAddress;	

			/// <summary>
			/// Client UDP
			/// </summary>
			private UdpClient	UDP;			

			/// <summary>
			/// Vrai si on est en mode broadcast
			/// </summary>
			private bool		broadcasting;	

			#region Group management

			/// <summary>
			/// Vrai si on a rejoint un groupe ou le broadcast
			/// </summary>
			public bool HasJoined
			{
				get
				{
					return UDP != null;
				}
			}

			/// <summary>
			/// Vrai si on a rejoint un groupe
			/// </summary>
			public bool HasJoinedGroup
			{
				get
				{
					return UDP != null && !broadcasting;
				}
			}

			/// <summary>
			/// Vrai si on a rejoint le broadcast
			/// </summary>
			public bool HasJoinedBroadcast
			{
				get
				{
					return UDP != null && broadcasting;
				}
			}

			/// <summary>
			/// Rejoint le broadcast
			/// </summary>
			/// <param name="port">Port à utiliser</param>
			/// <returns>Faux en cas d'erreur. (ex: groupe/broadcast déjà rejoint)</returns>
			public bool JoinBroadcast(int port)
			{
				try
				{
					if (this.HasJoined)
						throw new UnityException("Already joined");

					this.broadcasting = true;

					this.UDP = new UdpClient();
					
					this.UDP.ExclusiveAddressUse = false;

					this.UDP.Client.SetSocketOption(
						SocketOptionLevel.Socket, 
						SocketOptionName.ReuseAddress, true);

					this.UDP.ExclusiveAddressUse = false;
				
					this.UDP.Client.Bind(new IPEndPoint(IPAddress.Any, port));
#if VERBOSE
					GUILog.Add("UDP Port opened", Color.green);
#endif
					this.GroupAddress = IPAddress.Broadcast;
					this.Group = new IPEndPoint(this.GroupAddress, port);

					this.UDP.EnableBroadcast = true;
#if VERBOSE
					GUILog.Add("Broadcast joined", Color.green);
#endif
				}
#if VERBOSE
				catch (System.Exception e)
#else
				catch (System.Exception)
#endif
				{
					UDP = null;
#if VERBOSE
					GUILog.Add("Error: " + e.Message, Color.red);
#endif
				}

				return this.HasJoined;
			}

			/// <summary>
			/// Rejoint un groupe multicast
			/// </summary>
			/// <param name="groupIP">IP du groupe à utiliser</param>
			/// <param name="groupPort">Port à utiliser</param>
			/// <returns>Faux en cas d'erreur. (ex: groupe/broadcast déjà rejoint)</returns>
			public bool JoinGroup(string groupIP, int groupPort)
			{				
				try
				{
					if (this.HasJoined)
						throw new UnityException("Already joined");

					this.UDP = new UdpClient();

					this.UDP.ExclusiveAddressUse = false;

					this.UDP.Client.SetSocketOption(
						SocketOptionLevel.Socket, 
						SocketOptionName.ReuseAddress, true);

					this.UDP.ExclusiveAddressUse = false;

					this.UDP.Client.Bind(new IPEndPoint(IPAddress.Any, groupPort));
#if VERBOSE
					GUILog.Add("UDP Port opened", Color.green);
#endif

					this.GroupAddress = IPAddress.Parse(groupIP);
					this.Group = new IPEndPoint(this.GroupAddress, groupPort);

					this.UDP.JoinMulticastGroup(this.GroupAddress, DatagramsTTL);
					//UDP.MulticastLoopback = true;
					this.UDP.MulticastLoopback = false;
#if VERBOSE
					GUILog.Add("Group joined", Color.green);
#endif
				}
#if VERBOSE
				catch (System.Exception e)
#else
				catch (System.Exception)
#endif
				{
					this.UDP = null;
#if VERBOSE
					GUILog.Add("Error: " + e.Message, Color.red);
#endif
				}

				return this.HasJoined;
			}

			/// <summary>
			/// Se retire du broadcast/du groupe.
			/// Annule toutes les receptions en cours
			/// </summary>
			public void Stop()
			{
				if (this.UDP != null)
				{
					this.StopReceiveCoroutine();

					if (this.HasJoinedGroup)
					{
						try
						{
							this.UDP.DropMulticastGroup(this.GroupAddress);
#if VERBOSE
							GUILog.Add("Group left", Color.green);
#endif
						}
						catch(System.Exception e)
						{
							Debug.LogWarning("Couldn't drop multicast group : " + e.Message);
						}
					}

					this.UDP.Close();
#if VERBOSE
					GUILog.Add("UDP Port closed", Color.green);
#endif
					this.UDP = null;
				}
			}

			#endregion

			#region Transactions

			/// <summary>
			/// Envoie un message
			/// </summary>
			/// <param name="bytes">Message</param>
			protected void Send(byte[] bytes)
			{
				if (this.UDP != null)
				{
					try
					{
						this.UDP.Send(bytes, bytes.Length, this.Group);
					}
#if VERBOSE
					catch (System.Exception e)
#else
					catch (System.Exception)
#endif
					{
#if VERBOSE
						GUILog.Add("Error: " + e.Message, Color.red);
#endif
					}
				}
			}

			/// <summary>
			/// Appelé à la reception d'un message par ReceiveCoroutine
			/// </summary>
			/// <param name="data">Message recu</param>
			/// <param name="from">Machine emettrice</param>
			protected virtual void OnReceived(byte[] data, IPEndPoint from)	{ }

			/// <summary>
			/// Arrête ReceiveCoroutine
			/// </summary>
			protected void StopReceiveCoroutine()
			{
				if (this.IsReceiveCoroutineStarted)
				{
					if (this.RunningReceiveThread != null &&
						this.RunningReceiveThread.IsAlive)
					{
						try
						{
							this.RunningReceiveThread.Abort();
						}
						catch (System.Exception e)
						{
							Debug.LogWarning("Couldn't abort thread : " + e.Message);
						}

						this.RunningReceiveThread = null;
					}
				}
			}

			/// <summary>
			/// Coroutine de reception de messages
			/// </summary>
			/// <returns>(Coroutine)</returns>
			protected IEnumerator ReceiveCoroutine()
			{
				if (IsReceiveCoroutineStarted)
					throw new UnityException("Already running");

				if (UDP == null)
					throw new UnityException("Not started");
				
				IsReceiveCoroutineStarted = true;
				while (UDP != null && IsReceiveCoroutineStarted)
				{
					this.LastReceivedData = null;
					
#if VERBOSE
					GUILog.Add("Create receive thread", Color.cyan);
#endif
					this.RunningReceiveThread = new Thread(this.ReceiveThread);
		
#if VERBOSE
					GUILog.Add("Starts receive thread", Color.cyan);
#endif
					this.RunningReceiveThread.Start();

					while (
						this.RunningReceiveThread != null &&
						this.RunningReceiveThread.IsAlive)
					{
						yield return null;
					}

					this.RunningReceiveThread = null;
					
#if VERBOSE
					GUILog.Add("Receive thread finished", Color.cyan);
#endif
					if (this.LastReceivedData != null && this.LastReceivedData.Length != 0)
					{
#if VERBOSE
						GUILog.Add("Data received : " + 
							Binary.BytesToStrUTF8(Binary.FromNetworkBigEndian(
								this.LastReceivedData
							)), Color.green);
#endif
						this.OnReceived(this.LastReceivedData, this.LastReceivePoint);
					}
#if VERBOSE
					else 
					{
						GUILog.Add ("Data not received (not an error)", Color.yellow);	
					}
#endif
				}
				IsReceiveCoroutineStarted = false;
			}

			/// <summary>
			/// Routine du thread qui lit un message.
			/// </summary>
			private void ReceiveThread()
			{
				// Execution normale :
				try 
				{
					this.LastReceivePoint = new IPEndPoint(this.Group.Address, this.Group.Port);
					this.LastReceivedData = this.UDP.Receive(ref this.LastReceivePoint); // Appel bloquant.
				}

				// Si Abort a été appelé :
				catch(ThreadAbortException) 
				{
					this.LastReceivePoint = null;
					this.LastReceivedData = null;
				}
			}

			#endregion

		}
	}
}