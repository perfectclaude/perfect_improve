﻿// #define VERBOSE

using UnityEngine;
using System.Collections;

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using Utils.Network;

/// <summary>
/// Waits for client to connect
/// </summary>
public class ServerManager : ServerManager<SocketManager>
{

}

// namespace Utils
// {
// 	namespace Network
// 	{
		/// <summary>
		/// Waits for client to connect
		/// </summary>
		public class ServerManager<T> : MonoBehaviour
			where T:SocketManager
		{
			/// <summary>
			/// TimeOut the clients must get
			/// </summary>
			public float ClientsConnectionTimeOut;

			public string TimeoutMessage;
			
			/// <summary>
			/// Last client connected
			/// </summary>
			private TcpClient lastClient;

			/// <summary>
			/// Managed listener
			/// </summary>
			private TcpListener tcpListener;

			/// <summary>
			/// Called each time a client connects
			/// </summary>
			public event Action<T> OnClientConnected;

			/// <summary>
			/// Called each time a client disconnects
			/// </summary>
			public event Action<T> OnClientDisconnected;

			/// <summary>
			/// [Thread] Listen to the given port for client.
			/// </summary>
			private Thread ListeningThread;

			/// <summary>
			/// Is currently listening ?
			/// </summary>
			private bool Listening = false;

			public bool IsListening
			{
				get
				{
					return Listening;
				}
			}

			/// <summary>
			/// Port to bind
			/// </summary>
			public int port;
			
			/// <summary>
			/// Called by SocketManager when a client connection has been timed out
			/// </summary>
			/// <param name="client">Disconnected client</param>
			internal void OnClientTimedOut(SocketManager client)
			{
				if (!string.IsNullOrEmpty(TimeoutMessage))
				{
					Debug.Log("Client timed out. Kicked.");
					client.Send_UTF8_BE(TimeoutMessage);
				}

#if VERBOSE
				GUILog.Add("Client time out", Color.yellow);
#endif
				client.Disconnect();
				if (this.OnClientDisconnected != null)				
					this.OnClientDisconnected((T)client);				
				GameObject.Destroy(client);
			}

			/// <summary>
			/// Called by SocketManager when a client is disconnected
			/// </summary>
			/// <param name="client">Disconnected client</param>
			internal void ClientDisconnected(SocketManager client)
			{
#if VERBOSE
				GUILog.Add("Client disconnected", Color.yellow);
#endif
				if(this.OnClientDisconnected != null)
					this.OnClientDisconnected((T)client);
				GameObject.Destroy(client);
			}

			/// <summary>
			/// Binds a port to this server manager then listen to it
			/// </summary>
			/// <param name="port">Port to bind (and listen)</param>
			/// <param name="onClientConnected">Called when a client is connected</param>
			public bool BindPortAndListen(int port)
			{
				if (this.tcpListener != null)
					return false;

				this.port = port;

				IPAddress localAddr = IPAddress.Any;
				//IPAddress.Parse("127.0.0.1");

				try
				{
					this.tcpListener = new TcpListener(localAddr, port);
					this.tcpListener.Start();
				} 
				catch (SocketException e)
				{
					Debug.LogError("Error binding port " + port + "\n" + e.Message);
					return false;
				}
#if VERBOSE
				GUILog.Add("Starting listening", Color.green);
#endif
				this.Listening = true;
				StartCoroutine(WaitClientsCoroutine());

				return true;				
			}

			/// <summary>
			/// Binds a port to this server manager then listen to it
			/// </summary>
			/// <param name="onClientConnected">Called when a client is connected</param>
			public void BindPortAndListen()
			{
				BindPortAndListen(port);
			}

			/// <summary>
			/// Stops the listening thread if it's running and the listening coroutine
			/// </summary>
			public void Stop()
			{
				this.Listening = false;

				if (this.ListeningThread != null)
				{
#if VERBOSE
					GUILog.Add("Aborting waiting", Color.green);
#endif
					this.ListeningThread.Abort();
					this.ListeningThread = null;
				}

				if (this.tcpListener != null)
				{
#if VERBOSE
					GUILog.Add("Stoping listening", Color.green);
#endif
					this.tcpListener.Stop();
					this.tcpListener = null;
				}
			}

			/// <summary>
			/// Coroutine that loops until Listening has been set to false
			/// </summary>
			/// <returns>Coroutine</returns>
			private IEnumerator WaitClientsCoroutine()
			{
				while (this.Listening)
				{
					yield return StartCoroutine(WaitClientCoroutine());
				}

			}

			/// <summary>
			/// Coroutine that waits for a client to connect using a thread.
			/// 
			/// First we start a thread that will execute "WaitClientThread".
			/// Then we wait for the thread to finish and, if we are still listening,
			/// We handle the connected client.
			/// </summary>
			/// <returns></returns>
			private IEnumerator WaitClientCoroutine()
			{
#if VERBOSE
				GUILog.Add("Waiting client", Color.green);
#endif
				this.ListeningThread = new Thread(WaitClientThread);
				this.ListeningThread.Start();

				while (this.Listening && this.ListeningThread.IsAlive)
					yield return null;

				if (this.Listening)
				{
					T client = SocketManager.OnClientConnected<T>(this, this.lastClient);

#if VERBOSE
					GUILog.Add("New client connected", Color.green);
#endif
					if (this.OnClientConnected != null)
						this.OnClientConnected(client);

				}

				this.ListeningThread = null;
			}
			
			/// <summary>
			/// Waits for a client to be connected.
			/// This function takes time so it must be called by a thread.
			/// 
			/// <remarks>Nor lastClient nor tcpListener must be used if the thread is running</remarks>
			/// </summary>
			private void WaitClientThread()
			{
				lastClient = tcpListener.AcceptTcpClient();
			}
		}
//	}
//}