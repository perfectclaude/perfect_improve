﻿// #define VERBOSE

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

using Utils.Network;

// /// <summary>
// /// Manages a client Socket (TCP/IP)
// /// </summary>
// public class SocketManager : Utils.Network.SocketManager
// {
// }

// namespace Utils
// {
// 	namespace Network
// 	{

		/// <summary>
		/// Manages a client Socket (TCP/IP)
		/// </summary>
		public class SocketManager : MonoBehaviour
		{	
			/// <summary>
			/// The client to manage
			/// </summary>
			public TcpClient Client
			{
				get;
				private set;
			}

			/// <summary>
			/// True if the connection hadn't have any problem
			/// </summary>
			public bool IsConnected { get; private set; }

			/// <summary>
			/// If false : IsConnected remains true even if our connection has been cut.
			/// IsConnected becomes false if a connection try has been failed.
			/// </summary>
			private bool IsConstantConnection;

			private bool CanLaunchDisconnectionEvent;

			#region Debug

			/// <summary>
			/// To use within logs
			/// </summary>
			public string Name = "";

			public bool UseDebugLog = false;

			private void MyLog(object msg)
			{
				if (UseDebugLog)
					Debug.Log(msg);
			}

#if VERBOSE
			/// <summary>
			/// Uses GUILog ?
			/// </summary>
			public bool UseGUILog = false;
#endif

			/// <summary>
			/// Just append our name to the given message.
			/// If our name is null or empty : returns 'message' without changes
			/// </summary>
			/// <param name="message">Message</param>
			/// <returns>Formatted message with our name</returns>
			private string Named(string message)
			{
				if (!string.IsNullOrEmpty(this.Name))
					message = ("[" + this.Name + "] -> ") + message;

				return message;
			}


			#endregion

			#region Connection

			/// <summary>
			/// Host name or ip
			/// </summary>
			private string hostname;

			/// <summary>
			/// Host port
			/// </summary>
			private int port;

			/// <summary>
			/// Return true if the last Ping() call has been successful
			/// </summary>
			public bool IsLastPingOk { get; private set; }

			/// <summary>
			/// Contains last received message time.
			/// If no message has been received yet, contains the connection time.
			/// </summary>
			private float lastReceivedMessageTime = -1;

			public byte[] LastReceivedPackage { get; private set; }

			/// <summary>
			/// If negative : disabled.
			/// If positive : contains the maximum time the socket is sleeping.
			/// </summary>
			public float ConnectionTimeOut = -1;

			/// <summary>
			/// Called when the ConnectionTimeOut has been reached
			/// </summary>
			public event Action<SocketManager> OnTimeOut;

			/// <summary>
			/// Called if the client has been disconnected.
			/// </summary>
			public event Action<SocketManager> OnDisconnection;

			/// <summary>
			/// Returns the host's ip (using it's name)
			/// </summary>
			/// <param name="host">the host name</param>
			/// <returns>the host ip</returns>
			public static string GetIP(string host)
			{
				return Dns.GetHostAddresses(host)[0].ToString();
			}

			/// <summary>
			/// Connection to the host we have stored the name/port
			/// </summary>
			/// <param name="first">First connection : do not call OnHostDisconnected</param>
			private void Connect(bool first)
			{
				this.CanLaunchDisconnectionEvent = true;

				if (Client != null)
				{
#if VERBOSE
					if (UseGUILog)
						GUILog.Add(Named("Was already connected. Disconnect."), Color.cyan);
#endif
					this.TempDisconnect();
				}

#if VERBOSE
				if (UseGUILog)
					GUILog.Add(Named("Try to connect " + hostname + " at port " + port), Color.cyan);
#endif

				try
				{
					this.Client = new TcpClient(hostname, port);
				}
				catch
#if VERBOSE
 (Exception e)
#endif
				{
					this.Client = null;
#if VERBOSE
					if (UseGUILog)
						GUILog.Add(Named(e.Message), Color.red);
#endif
				}

				if (Client == null || !Client.Connected)
				{
					this.TempDisconnect();
					if (!first)
						this.OnHostDisconnected();
				}
				else
				{
				
					this.lastReceivedMessageTime = Time.time;
					this.OnConnection(first);
#if VERBOSE
					if (UseGUILog)
					{
						GUILog.Add(Named("Connected !"), Color.cyan);
					}
#endif
				}
			}

			protected virtual void OnConnection(bool first) { }

			/// <summary>
			/// Creates the client socket and connects it to the given server
			/// </summary>
			/// <param name="host">Host name (ip or dns)</param>
			/// <param name="port">Host port</param>
			/// <param name="alive">False if the connection is a 'connection per query' type</param>
			/// <param name="OnDisconnection">Action called on forced disconnection</param>
			/// <returns>On error, returns false</returns>
			public bool Connect(string host, int port, bool alive)
			{
#if UNITY_WEBPLAYER && VERBOSE     
		        if (Security.PrefetchSocketPolicy(GetIP(host), port))
		        {
		            GUILog.Add("PrefetchSocketPolicy : success");
		        }
		        else
		        {
		            GUILog.Add("PrefetchSocketPolicy : fail");
		        }
#endif
				this.IsConstantConnection = alive;
				this.hostname = host;
				this.port = port;
		
				this.LockReceive = false;		
				this.Connect(true);

				this.IsConnected = (this.Client != null);
				return this.IsConnected;
			}

			/// <summary>
			/// Coroutine that tries to contact the given host
			/// </summary>
			/// <param name="host">Host to contact</param>
			/// <param name="callback">Action to call at ping's end</param>
			/// <param name="ConnectionTimeOut">max ping time</param>
			/// <param name="ConnectionPingCheck">ping check time</param>
			/// <returns>(Coroutine)</returns>
			public IEnumerator Ping(string host, Action<bool> callback, 
				float ConnectionTimeOut, float ConnectionPingCheck)
			{
				float t = 0;

		Debug.Log ("TimeOut:"+ConnectionTimeOut);
				Ping p = new Ping(host);
				while (!p.isDone && t < ConnectionTimeOut)
				{
					yield return new WaitForSeconds(ConnectionPingCheck);
					t += ConnectionPingCheck;
#if VERBOSE
					GUILog.Add("Ping : " + t, Color.red);
#endif
				}
		
				IsLastPingOk = p.isDone;

				if(callback != null)
					callback(IsLastPingOk);
			}

			#region ConnectCoroutine overrides
			public IEnumerator ConnectCoroutine(string host)
			{
				return ConnectCoroutine(host, false, 843, 0.5f, 2);
			}

			public IEnumerator ConnectCoroutine(string host, bool alive)
			{
				return ConnectCoroutine(host, alive, 843, 0.5f, 2);
			}

			public IEnumerator ConnectCoroutine(string host, bool alive, int port)
			{
				return ConnectCoroutine(host, alive, port, 0.5f, 2);
			}

			#endregion

			/// <summary>
			/// Connection coroutine. (using ping)
			/// </summary>
			/// <param name="host">Host to contact</param>
			/// <param name="alive">False if the connection is a 'connection per query' type</param>
			/// <param name="port">Host's port</param>
			/// <param name="callback">Action to take once this function is finished</param>
			/// <param name="OnDisconnection">Action to take if the connection has been shut by the host</param>
			/// <param name="ConnectionPingTimeOut">Ping: max ping time</param>
			/// <param name="ConnectionPingCheck">Ping: ping checks time</param>
			/// <returns>(Coroutine)</returns>
			public IEnumerator ConnectCoroutine(string host, bool alive,
				int port, float ConnectionPingTimeOut, float ConnectionPingCheck)
			{
				yield return StartCoroutine(Ping(host, null, ConnectionPingTimeOut, ConnectionPingCheck));
								
				if (IsLastPingOk)
				{
					this.Connect(host, port, true);
				}
			}

			/// <summary>
			/// Called by ServerManager
			/// </summary>
			/// <param name="server">Server</param>
			/// <param name="tcpClient">Client connected</param>
			/// <returns>Our SocketManager interface</returns>
			internal static T OnClientConnected<T>(ServerManager<T> server, TcpClient tcpClient) where T:SocketManager
			{
				T client = server.gameObject.AddComponent<T>();

				client.Client				= tcpClient;
				client.IsConstantConnection = true;
				client.Name					= tcpClient.Client.RemoteEndPoint.ToString();
				client.IsConnected			= tcpClient.Connected;
#if VERBOSE		
				client.UseGUILog			= false;
#endif
		
				client.ConnectionTimeOut	= server.ClientsConnectionTimeOut;

				client.OnTimeOut			+= server.OnClientTimedOut;
				client.OnDisconnection		+= server.ClientDisconnected;

				return client;
			}

			#endregion

			#region Disconnection

			/// <summary>
			/// Called on forced disconnection
			/// </summary>
			protected void OnHostDisconnected()
			{
				this.IsConnected = false;

				this.TempDisconnect();
				if (CanLaunchDisconnectionEvent && this.OnDisconnection != null)
					OnDisconnection(this);
			}

			/// <summary>
			/// Temporary host disconnection
			/// </summary>
			private void TempDisconnect()
			{
				if (this.Client != null)
				{
					this.Client.Close();
					this.Client = null;
				}

				if (this.IsConstantConnection)
				{
					this.IsConnected = false;
				}

#if VERBOSE
				if (UseGUILog)
					GUILog.Add(Named("Disconnected !"), Color.cyan);
#endif
			}
			
			/// <summary>
			/// Normal disconnection
			/// </summary>
			public void Disconnect()
			{
				if (this.IsConnected)
				{
					this.lastReceivedMessageTime = -1;
					this.CanLaunchDisconnectionEvent = false;
					this.StopAllCoroutines();
					this.TempDisconnect();
					this.IsConnected = false;
				}
			}
			#endregion

			#region Message transactions

			#region Send messages

			public void Send(Stream toSend)
			{
				this.Send(toSend, 1024);
			}
			
			/// <summary>
			/// Sends a binary message to the socket
			/// </summary>
			/// <param name="toSend">streamed message to send</param>
			/// <param name="chunk_size">message packages size</param>
			public void Send(Stream toSend, int chunk_size)
			{
				if (!this.IsConnected)
					return;

				if (!toSend.CanRead)
					return;

				if (Client == null && !this.IsConstantConnection)
					this.Connect(false);

				if (Client == null)
					return;

				NetworkStream stream = Client.GetStream();

				if (!stream.CanWrite)
					return;

#if VERBOSE
				GUILog.Add(Named("Sending message"), Color.green);
#endif
				//MyLog("Send data to " + this.hostname + ":" + this.port); 

				try 
				{
					byte[] chunk;
					BinaryReader reader = new BinaryReader(toSend);
					chunk = reader.ReadBytes(chunk_size);
					while (chunk.Length > 0)
					{
						//MyLog("\tSend " + chunk.Length + " bytes to " + this.hostname + ":" + this.port);
						stream.Write(chunk, 0, chunk.Length);
						//MyLog("\tSended to " + this.hostname + ":" + this.port);
						chunk = reader.ReadBytes(chunk_size);
					}
				}
				catch(Exception)
				{
				}

				//MyLog("End send data to " + this.hostname + ":" + this.port);

#if VERBOSE
				if (this.Client.Connected)
				{
					GUILog.Add(Named("Message sent"), Color.green);
				}
				else
#else
				if(!this.Client.Connected)
#endif
				{
					this.OnHostDisconnected();
				}
			}

			/// <summary>
			/// Sends a binary message to the socket
			/// </summary>
			/// <param name="bytes">message to send</param>
			public void Send(byte[] bytes)
			{
				if (this.IsConnected)
				{
					if (UseDebugLog)
						Debug.Log("To send : " + Binary.PackageToString(bytes));

					this.Send(new MemoryStream(bytes));
				}
			}

			public void Send_UTF8_BE(string str)
			{
				this.Send(Binary.ToNetworkBigEndian(Binary.StrToBytesUTF8(str)));
			}

						
			#endregion

			#region Receive messages
			
			/// <summary>
			/// Receives data if data is available
			/// </summary>
			/// <returns>received bytes array. empty if nothing. null on error</returns>
			private byte[] ReceiveIfAvailable()
			{
				if (this.Client == null)
					return null;

				NetworkStream stream = Client.GetStream();

				if (!stream.CanRead)
					return null;

				if (!stream.DataAvailable)
					return new byte[0];

				byte[] data = null;

			//	MyLog("Check if data is available at " + this.hostname + ":" + this.port);

				try
				{
					List<byte[]> bytes = new List<byte[]>();
					int n = Client.Available;
					int total = n;
					//int read;

					//MyLog("\t" + n + " bytes are available at " + this.hostname + ":" + this.port);
					while (n > 0)
					{
						byte[] buffer = new byte[n];

						//MyLog("\t Reading " + n + " bytes at " + this.hostname + ":" + this.port);
						//read = 
							stream.Read(buffer, 0, n);
						//MyLog("\t " + read + " bytes read at  " + this.hostname + ":" + this.port);
						bytes.Add(buffer);

						n = Client.Available;
						//MyLog("\t" + n + " bytes are available at " + this.hostname + ":" + this.port);					
						total += n;
					}

					data = Binary.Join(bytes, total);
					if (UseDebugLog)
						Debug.Log("Received : " + Binary.PackageToString(data));
#if VERBOSE
					GUILog.Add(Named("Message extracted"), Color.green);
#endif
					this.lastReceivedMessageTime = Time.time;

					if (!IsConstantConnection)
						this.TempDisconnect();
				}				
#if VERBOSE
				catch (SocketException e)
#else
				catch(SocketException)
#endif
				{
#if VERBOSE
					GUILog.Add(e.Message, Color.red);
#endif

					this.OnHostDisconnected();
				}

				//MyLog("End check at " + this.hostname + ":" + this.port);

				return data;
			}

			#region Receive Loops
			
			/// <summary>
			/// Locks receive method if already launched
			/// </summary>
			private bool LockReceive = false;

				
			public IEnumerator ReceiveCoroutine(Action<byte[]> callback)
			{
				return ReceiveCoroutine(callback, null);
			}

			public IEnumerator ReceiveCoroutine(Action<byte[]> callback, Func<bool> stop)
			{
				return ReceiveCoroutine(callback, stop, 1);
			}

			/// <summary>
			/// Listen for answers (binary)
			/// </summary>
			/// <param name="callback">Called once we got an answer.</param>
			/// <param name="stop">If setted : this function will loop and stop once 'stop' returns true</param>
			/// <param name="timeout">Time to wait between two tries. If negative : No timeout</param>
			/// <returns>Coroutine</returns>
			public IEnumerator ReceiveCoroutine(Action<byte[]> callback, Func<bool> stop, float timeout)
			{
				while (LockReceive)
				{
					yield return null;
				}

				LockReceive = true;

				List<byte[]> final = new List<byte[]>();
				byte[] answer = null;
				float time = 0;
				bool isStop;
				int count = 0;

				do
				{
					if (this.Client != null)
					{
						isStop = false;

						do
						{						
#if VERBOSE && SHOW_TIMEOUT
							if(time == 0)
								GUILog.Add("No timeout", Color.blue);
							else
								GUILog.EditLast("Timeout: " + time, Color.blue);
#endif
							answer = ReceiveIfAvailable();
							if(stop != null)
								isStop = stop();

							yield return new WaitForSeconds(timeout);
							time += timeout;
							this.CheckTimeOut();
						} 
						while ((answer == null || answer.Length == 0) && this.Client != null && !isStop) ;
						
					}
					else
					{
#if VERBOSE
						GUILog.EditLast("Not connected.", Color.red);
#endif
					}

					if (answer != null)
					{
						final.Add(answer);
						count += answer.Length;
					}

					isStop = true;
					if (stop != null)
						isStop = stop();
				}
				while (!isStop);

				this.LastReceivedPackage = Binary.Join(final, count);
				if(callback != null)
					callback(this.LastReceivedPackage);
				
				LockReceive = false;
			}

			#endregion

			#endregion

			#endregion

			private void CheckTimeOut()
			{
				if (this.ConnectionTimeOut > 0 && this.lastReceivedMessageTime != -1 &&
					Time.time - this.lastReceivedMessageTime > this.ConnectionTimeOut)
				{
					if (this.OnTimeOut != null)
					{
						this.OnTimeOut(this);
					}
				}
			}

			#region Unity component messages

			/// <summary>
			/// Initializes our object (and starts it)
			/// </summary>
			private void Awake()
			{
				this.IsConnected = false;
				this.enabled = true;
			}

			/// <summary>
			/// Destroys this object (and stops the connection if any)
			/// </summary>
			public void Dispose()
			{
				StopAllCoroutines();

				if (this.Client != null)
				{
					this.OnHostDisconnected();
				}
			}

			#endregion
		}
// 	}
// }