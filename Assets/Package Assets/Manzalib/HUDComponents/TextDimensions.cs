﻿using UnityEngine;

public struct TextDimensions
{
	public int maxLinePos;
	public int maxLineIndex;
	public float maxLineWidth;
	public string maxLineText;
	public float height;
	public int nLines;

	public Rect GetRect()
	{
		Rect r = new Rect();

		r.width = maxLineWidth;
		r.height = height;

		return r;
	}

	public static float ComputeCurrentLineWidth(TextEditor editor, GUIStyle style)
	{/*
		int currentSelectPos = editor.selectPos;
		int currentPosition = editor.pos;
		editor.MoveLineStart();
		int lineStart = editor.pos;
		editor.MoveLineEnd();
//		int lineEnd = editor.pos;
		editor.selectPos = lineStart;
		string line = editor.SelectedText;
		editor.selectPos = currentSelectPos;
		editor.pos = currentPosition;

		return ComputeLineWidth(line, style.font, style.fontStyle, style.fontSize);
		*/
		return 0.0f;
	}

	public static int GetCharacterNear(float xOffset, string str, Font f, FontStyle style, int size)
	{
		CharacterInfo info;
		float width = 0;
		int i = 0;

		if (str != null)
		{
			foreach (char c in str)
			{
				f.GetCharacterInfo(c, out info, size, style);
				width += info.width;
				i++;

				if (Mathf.Abs(width - xOffset) < 0.2f)
					return i;
			}
		}

		return i;
	}

	public static TextDimensions ComputeTextDimensions(string str, Font f, FontStyle style, int size, float lineHeight)
	{
		TextDimensions dims = new TextDimensions();

		string[] lines = str.Split('\n');
		float width;
		int pos = 0;

		dims.nLines = 0;
		foreach (string line in lines)
		{
			width = ComputeLineWidth(line, f, style, size);
			if (dims.maxLineWidth < 0)
			{
				dims.maxLineWidth = width;
				dims.maxLineIndex = dims.nLines;
				dims.maxLineText = line;
				dims.maxLinePos = pos;
			}
			else if (width > dims.maxLineWidth)
			{
				dims.maxLineWidth = width;
				dims.maxLineIndex = dims.nLines;
				dims.maxLineText = line;
				dims.maxLinePos = pos;
			}

			pos += line.Length;
			dims.nLines++;
		}

		dims.height = dims.nLines * lineHeight;

		return dims;
	}

	public static float ComputeLineWidth(string str, Font f, FontStyle style, int size)
	{
		CharacterInfo info;
		float width = 0;
		foreach (char c in str)
		{
			f.GetCharacterInfo(c, out info, size, style);
			width += info.width;
		}
		return width;
	}
}
