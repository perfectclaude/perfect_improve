//#define USE_NO_STYLE

#if (UNITY_STANDALONE_OSX || UNITY_WEBPLAYER || UNITY_STANDALONE_WIN) && !UNITY_PHONE
#define MODE_PC
#else
#define MODE_PHONE
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Permet d'ordonnancer les controles dans un formulaire. (tab)
public class FormManager : MonoBehaviour 
{
	public GUITextField[] fields;

	public float screenHeight;
	public float screenWidth;

	private Dictionary<string, GUITextField> _namedFields;
	public Dictionary<string, GUITextField> NamedFields
	{
		get
		{
			if (_namedFields == null)
				CreateNamedFieldsDictionary();

			return _namedFields;
		}
	}

	public Dictionary<string, GUITextField> CreateNamedFieldsDictionary()
	{
		_namedFields = new Dictionary<string, GUITextField>();
		foreach (GUITextField tf in fields)
		{
			_namedFields.Add(tf.ControlName, tf);
		}
		return _namedFields;
	}

	void Start()
	{
		foreach (GUITextField field in fields)
		{
			field.manager = this;
		}
	}

#if MODE_PC	
	
	void OnGUI()
	{		
		float scale = Mathf.Min(Screen.height / screenHeight, Screen.width / screenWidth);
		var svMat = GUI.matrix;			
		GUI.matrix = svMat * Matrix4x4.Scale(new Vector3(scale, scale, 1));

		foreach (GUITextField field in fields)
		{
			if(field.enabled)
				field.DrawGUI();
		}

		GUI.matrix = svMat;
	}
#endif
}
