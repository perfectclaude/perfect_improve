﻿// CustomLineRenderer.cs
// http://answers.unity3d.com/questions/64716/can-i-render-nice-thick-lines-with-linerenderer-wi.html
// modifié pour opti/projet


//#define MODIF

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Utils
{
	namespace Components
	{
		/// <summary>
		/// Simple Line
		/// </summary>
		[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
		public class CustomLineRenderer : MonoBehaviour
		{
			/// <summary>
			/// Line mesh
			/// </summary>
			private Mesh m_Mesh;

			/// <summary>
			/// Line triangles
			/// </summary>
			private int[] m_Indices;

			/// <summary>
			/// Line UVs
			/// </summary>
			private Vector2[] m_UVs;

			/// <summary>
			/// Line vertex
			/// </summary>
			private List<Vector3> m_Points;

			/// <summary>
			/// Line width
			/// </summary>
			private float width = 1;

			/// <summary>
			/// Creates the mesh (empty)
			/// </summary>
			void Awake()
			{
				m_Mesh = new Mesh();
				m_Points = new List<Vector3>();
				GetComponent<MeshFilter>().sharedMesh = m_Mesh;
			}

			/// <summary>
			/// Finds the camera and creates the line
			/// </summary>
			void Start()
			{
				UpdateMesh(GameObject.FindGameObjectWithTag("HUDCamera").GetComponent<Camera>());
			}

			/// <summary>
			/// Creates the mesh using the current camera
			/// </summary>
			void OnWillRenderObject()
			{
				UpdateMesh(Camera.current);
			}

#if MODIF
	//note: dir is the path tangent, tangent is the binormal
	void UpdateMesh(Camera aCamera)
	{
		Vector3 localViewPos = transform.InverseTransformPoint(aCamera.transform.position);
		Vector3[] vertices = m_Mesh.vertices;
		Vector3[] normals = m_Mesh.normals;

		Vector3 oldTangent = Vector3.zero;
		Vector3 oldDir = Vector3.zero;

		for (int i = 0; i < m_Points.Count - 1; i++)
		{
			Vector3 faceNormal = (localViewPos - m_Points[i]).normalized;
			Vector3 dir = (m_Points[i + 1] - m_Points[i]);

			Vector3 tangent = Vector3.Cross(dir, faceNormal).normalized;
			Vector3 offset;

			if (i == 0)
			{
				offset = (oldTangent + tangent).normalized * width / 2.0f;
			}
			else
			{
				float alpha = (Mathf.PI - Mathf.Acos(Vector3.Dot(oldDir.normalized, dir.normalized))) / 2;
				float d = width / 2.0f / Mathf.Sin(alpha);
				d *= -Mathf.Sign(Vector3.Dot(tangent.normalized, oldDir.normalized));
				offset = ((dir.normalized - oldDir.normalized) / 2).normalized * d;
			}

			vertices[i * 2] = m_Points[i] - offset;
			vertices[i * 2 + 1] = m_Points[i] + offset;

			normals[i * 2] = normals[i * 2 + 1] = faceNormal;

			if (i == m_Points.Count - 2)
			{
				// last two points
				vertices[i * 2 + 2] = m_Points[i + 1] - tangent * width / 2.0f;
				vertices[i * 2 + 3] = m_Points[i + 1] + tangent * width / 2.0f;
				normals[i * 2 + 2] = normals[i * 2 + 3] = faceNormal;
			}

			oldDir = dir;
			oldTangent = tangent;
		}

		m_Mesh.vertices = vertices;
		m_Mesh.normals = normals;
		m_Mesh.uv = m_UVs;
		m_Mesh.SetTriangleStrip(m_Indices, 0);
		m_Mesh.RecalculateBounds();
	}
#else
			/// <summary>
			/// Creates the mesh (updates it)
			/// </summary>
			/// <param name="aCamera">Current camera</param>
			void UpdateMesh(Camera aCamera)
			{
				Vector3 localViewPos = transform.InverseTransformPoint(aCamera.transform.position);
				Vector3[] vertices = m_Mesh.vertices;
				Vector3[] normals = m_Mesh.normals;
				Vector3 oldTangent = Vector3.zero;
				//Vector3 oldDir = Vector3.zero;
				for (int i = 0; i < m_Points.Count - 1; i++)
				{
					Vector3 faceNormal = (localViewPos - m_Points[i]).normalized;
					Vector3 dir = (m_Points[i + 1] - m_Points[i]);
					Vector3 tangent = Vector3.Cross(dir, faceNormal).normalized;
					Vector3 offset = (oldTangent + tangent).normalized * width / 2.0f;

					vertices[i * 2] = m_Points[i] - offset;
					vertices[i * 2 + 1] = m_Points[i] + offset;
					normals[i * 2] = normals[i * 2 + 1] = faceNormal;
					if (i == m_Points.Count - 2)
					{
						// last two points
						vertices[i * 2 + 2] = m_Points[i + 1] - tangent * width / 2.0f;
						vertices[i * 2 + 3] = m_Points[i + 1] + tangent * width / 2.0f;
						normals[i * 2 + 2] = normals[i * 2 + 3] = faceNormal;
					}
					//oldDir = dir;
					oldTangent = tangent;
				}
				m_Mesh.vertices = vertices;
				m_Mesh.normals = normals;
				m_Mesh.uv = m_UVs;

#pragma warning disable 618								// removes obsolete warning handling.
//				m_Mesh.SetTriangleStrip(m_Indices, 0);	// is obsolete.
#pragma warning restore 618								// restore obsolete warning handling.

				m_Mesh.RecalculateBounds();
			}
#endif

			/// <summary>
			/// Sets the vertex count
			/// </summary>
			/// <param name="aCount">new count of vertex</param>
			public void SetVertexCount(int aCount)
			{
				aCount = Mathf.Clamp(aCount, 0, 0xFFFF / 2);
				if (m_Points.Count > aCount)
					m_Points.RemoveRange(aCount, m_Points.Count - aCount);
				while (m_Points.Count < aCount)
					m_Points.Add(new Vector3());

				m_Mesh.vertices = new Vector3[m_Points.Count * 2];
				m_Mesh.normals = new Vector3[m_Points.Count * 2];
				m_Indices = new int[m_Points.Count * 2];
				m_UVs = new Vector2[m_Points.Count * 2];
				for (int i = 0; i < m_Points.Count; i++)
				{
					m_Indices[i * 2] = i * 2;
					m_Indices[i * 2 + 1] = i * 2 + 1;
					m_UVs[i * 2] = m_UVs[i * 2 + 1] = new Vector2((float)i / (m_Points.Count - 1), 0);
					m_UVs[i * 2 + 1].y = 1.0f;
				}
			}

			/// <summary>
			/// Sets the position of a vertice
			/// </summary>
			/// <param name="aIndex">Vertice index</param>
			/// <param name="aPosition">Vertice position</param>
			public void SetPosition(int aIndex, Vector3 aPosition)
			{
				if (aIndex < 0 || aIndex >= m_Points.Count) return;
				m_Points[aIndex] = aPosition;
			}

			/// <summary>
			/// Sets the line width
			/// </summary>
			/// <param name="w">Line new width</param>
			public void SetWidth(float w)
			{
				this.width = w;
			}

			/// <summary>
			/// Sets the line color
			/// </summary>
			/// <param name="c">Line new color</param>
			public void SetColor(Color c)
			{
				var r = GetComponent<MeshRenderer>();
				//r.sharedMaterial = new Material(Shader.Find("Particles/Alpha Blended"));
				r.sharedMaterial = new Material(Shader.Find("Unlit/UnlitAlphaWithFade"));
				r.sharedMaterial.SetColor("_Color", c);
			}

		}
	}
}