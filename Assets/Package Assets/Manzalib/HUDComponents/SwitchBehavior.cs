﻿using UnityEngine;
using System;

//namespace Utils
//{
//	namespace Components
//	{
		/// <summary>
		/// Toggle button.
		/// </summary>
		public class SwitchBehavior : ButtonBehavior
		{
			/// <summary>
			/// Normal ON render image
			/// </summary>
			public Material onNormal;

			/// <summary>
			/// Normal OFF render image
			/// </summary>
			public Material offNormal;

			/// <summary>
			/// Hover ON render image
			/// </summary>
			public Material onOver;

			/// <summary>
			/// Hover OFF render image
			/// </summary>
			public Material offOver;

			/// <summary>
			/// Clicked ON render image
			/// </summary>
			public Material onClick;

			/// <summary>
			/// Clicked OFF render image
			/// </summary>
			public Material offClick;

			/// <summary>
			/// ON/OFF state
			/// </summary>
			private bool myState;

			/// <summary>
			/// Event : called on switch
			/// </summary>
			public event Action<SwitchBehavior, bool> OnSwitch;

			public bool StateSwitch
			{
				get
				{
					return myState;
				}
			}

			/// <summary>
			/// Sets current state
			/// </summary>
			/// <param name="newState">new state</param>
			public void SetState(bool newState)
			{
				myState = newState;

				if (this.button)
				{
					this.button.GetComponent<Renderer>().material = myState ? onNormal : offNormal;
				}

				if (this.buttonOver)
				{
					this.buttonOver.GetComponent<Renderer>().material = myState ? onOver : offOver;
				}

				if (this.buttonClicked)
				{
					this.buttonClicked.GetComponent<Renderer>().material = myState ? onClick : offClick;
				}

			}

			/// <summary>
			/// Starts the switch
			/// </summary>
			public void Start()
			{
				this.OnClick += (btn) =>
				{
					SetState(!myState);

					if (OnSwitch != null)
						OnSwitch(this, myState);
				};

				base.Awake();
			}
		}
// 	}
// }