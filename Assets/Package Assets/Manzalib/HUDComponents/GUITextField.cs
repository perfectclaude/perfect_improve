#region Defines symbols

#if (UNITY_STANDALONE_OSX || UNITY_WEBPLAYER || UNITY_STANDALONE_WIN) && !UNITY_PHONE
	#define MODE_PC
#else
	#define MODE_PHONE
#endif

#endregion

using UnityEngine;
using System;

[System.Serializable]
public class TextFieldPCSettings
{
	public Vector2 resolution;
	public int fontSize;
	public Vector2 size;
	public bool scrollBarMultiline;
}

[System.Serializable]
public class TextFieldPhoneSettings
{
#if UNITY_IPHONE || UNITY_EDITOR
	public TouchScreenKeyboardType type;
#endif
	public ButtonBehavior FocusButton;
	public Color colorIfFocused = Color.blue;
}

[AddComponentMenu("Scripts/Text/Text Field")]
public class GUITextField : MonoBehaviour
{
	#region Parameters

#if DEBUG_MODE
	public bool noStyle;
	public bool debugPosition;
#endif

	public Transform PlaceHolder;
	public int maxSize;
	public Font font;
	public Color color;
	public Renderer ifEmpty;
	public TextMesh ifNotFocused;
	public bool Password;
	public char PasswordMask = '*';
	public bool multiline = false;
	public bool IsNumber;
	public bool IsFlottant;
	public int shownLinesCount;
	public string ControlName;

	public TextFieldPCSettings pcSettings;
	public TextFieldPhoneSettings phoneSettings;

	#endregion

	#region Variables

	[HideInInspector]
	public FormManager manager;

	private string previousText_iPhone;
	private bool MustGiveFocus_PC;
	private string _text;
	private string writtenText;

#if MODE_PC
	private GUIStyle style;
#endif
	
#if MODE_PHONE
	private static TouchScreenKeyboard	Keyboard_iPhone = null;
	private static GUITextField			FocusedTextField_iPhone = null;
#endif

	#endregion

	#region Properties

	public bool IsFocused
	{
#if MODE_PC
		get;
		private set;
#endif
	
#if MODE_PHONE
		get 
		{
			return FocusedTextField_iPhone == this;
		}
#endif
	}

	public bool IsEmpty
	{
		get
		{
			return string.IsNullOrEmpty(this.Text);
		}
	}

	public string IfNotFocusedText
	{
		get
		{
			return this.ifNotFocused.text;
		}
		private set
		{
			if (FilterNotFocused != null)
				ifNotFocused.text = FilterNotFocused(this, value);
			else
				ifNotFocused.text = value;
		}
	}

	public float Number
	{
		get
		{
			string txt = Text;
			if (txt == null)
				return 0;
			txt = txt.Replace(",", ".");

			float result;
			if (!float.TryParse(txt, out result))
				return 0;

			return result;
		}
	}

	public string Text
	{
		get
		{
			return _text;
		}

		set
		{
			//if (_text != value)
			//{
			//	Debug.Log("Text changed (" + this.ControlName + ") : " + _text + " -> " + value);
			//}

			if (value == null)
				_text = string.Empty;
			else
				_text = value;

			if (IsNumber)
			{
				if (string.IsNullOrEmpty(value))
				{
					_text = "0";
				}
				else
				{
					_text = string.Empty;
					foreach (char c in value)
					{
						if (char.IsDigit(c) || c == ',' || c == '.')
							_text += c;
					}

					_text = float.Parse(_text.Replace(",", ".")).ToString();
					if (!IsFlottant)
					{
						int i = _text.IndexOf('.');
						if (i != -1)
							_text = _text.Remove(i);
					}
				}
			}

			if (ifNotFocused)
			{
				if (Password)
				{
					string stars = "";
					for (int i = 0; i < _text.Length; i++)
						stars += '*';

					this.IfNotFocusedText = stars;
				}
				else if (multiline && shownLinesCount > 0)
				{
					string[] lines = _text.Split('\n');
					if (lines.Length > shownLinesCount)
						this.IfNotFocusedText = string.Join("\n", lines, 0, shownLinesCount);
					else
						this.IfNotFocusedText = _text;
				}
				else
				{
					this.IfNotFocusedText = _text;
				}
			}

			if (ifEmpty)
				ifEmpty.enabled = string.IsNullOrEmpty(_text);
		}
	}

	#endregion

	#region Delegates

	public Func<GUITextField, string, string> FilterNotFocused;

	#endregion

	#region Events

	public event Action<GUITextField, bool> OnFocusChanged;
	public event Action<GUITextField> OnFinished;
	public event Action<GUITextField> OnReturnPressed;

	#endregion

	#region Unity callbacks

	public void Start()
	{
#if MODE_PHONE		
		this.StartPhoneMode();		
#endif
		this.Init();
	}

#if MODE_PC
	void OnGUI()
	{
		if (this.manager == null || !this.manager.enabled)
		{
			DetachedGUI();
		}
	}
#endif

#if MODE_PHONE
	void Update() 
	{
		//this.UpdatePhoneMode();
	}	
#endif

	#endregion
	
	#region Global methods

	public void Clear()
	{
#if MODE_PHONE
		Text = "";
#endif
#if MODE_PC
		Init();
#endif
	}

	private void Init()
	{
		writtenText = string.Empty;

#if MODE_PC
		this.style = null;
#endif
	}

	public void GiveFocus()
	{
		if (!IsFocused)
		{
#if MODE_PC
			MustGiveFocus_PC = true;
#endif

#if MODE_PHONE
			RemoveFocusToAny();
			FocusedTextField_iPhone = this;
#endif
		}
#if UNITY_IPHONE
		else if(Keyboard_iPhone != null)
		{
			Keyboard_iPhone.active = true;
		}
#endif
	}

	public static void RemoveFocusToAny()
	{
#if MODE_PC
		GUI.FocusControl(null);
#endif
#if MODE_PHONE

		if(Keyboard_iPhone != null)
		{
			Keyboard_iPhone.active = false;
		}
		
		var previous = FocusedTextField_iPhone;

		if(previous)
		{
			previous.Text = previous.previousText_iPhone;
		}

		FocusedTextField_iPhone = null;
		Keyboard_iPhone			= null;	
		
		if(	previous != null && 
			previous.OnFocusChanged != null)		
		{			
			previous.OnFocusChanged(previous, false);
		}				
		
#endif
	}

	public void RemoveFocus()
	{
		if (this.IsFocused)
		{
			RemoveFocusToAny(); // to us
		}
	}


	#endregion

	#region PC methods

#if MODE_PC
		
	#region Called by DrawGUI

	private Rect ComputePosition()
	{
		if (this.PlaceHolder == null)
			this.PlaceHolder = this.transform;

		if (!Camera.main)
			return new Rect(0, 0, this.pcSettings.size.x, this.pcSettings.size.y); // temp..

		Vector3 screenPoint = Camera.main.WorldToScreenPoint(this.PlaceHolder.position);
		screenPoint.y = Screen.height - screenPoint.y; // The axe changes.

		Vector3 guiPoint = GUIUtility.ScreenToGUIPoint(screenPoint);

		return new Rect(
			guiPoint.x,
			guiPoint.y,
			this.pcSettings.size.x,
			this.pcSettings.size.y
		);
	}

	private void ComputeFocus()
	{
		if (!string.IsNullOrEmpty(ControlName))
		{
			GUI.SetNextControlName(ControlName);
		}

		bool prevFocused = IsFocused;

		if (!string.IsNullOrEmpty(ControlName))
		{
			IsFocused = GUI.GetNameOfFocusedControl().Equals(ControlName);
		}
		else
		{
			IsFocused = false;
		}

		if (IsFocused != prevFocused && this.OnFocusChanged != null)
		{
			this.OnFocusChanged(this, IsFocused);
		}
	}

	private void DrawSingleLine(Rect pos, string toWrite)
	{
#if !UNITY_IPHONE
		Event e = Event.current;
		if (e.type == EventType.keyDown && e.keyCode == KeyCode.Return)
		{
			if (this.OnReturnPressed != null)
			{
				this.OnReturnPressed(this);
			}
		}
#endif
		if (Password)
		{
			writtenText = GUI.PasswordField(pos, toWrite, PasswordMask, maxSize, style);
		}
		else
		{
			writtenText = GUI.TextField(pos, toWrite, maxSize, style);
		}
	}

	private void DrawMultiLine(Rect pos, string toWrite)
	{
		writtenText = GUI.TextArea(pos, toWrite, style);
		
#if !DEBUG_MODE
		const bool noStyle = false;
#endif
		
		if (!noStyle && style.font && pcSettings.scrollBarMultiline)
		{
			TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
			TextDimensions dims = TextDimensions.ComputeTextDimensions(writtenText, style.font, style.fontStyle, style.fontSize, style.lineHeight);

			pos.width += 30;
			pos.height += 20;

			//Vector2 scrollPos = 
			GUI.BeginScrollView(pos, editor.scrollOffset, dims.GetRect());
			GUI.EndScrollView();
		}
	}

	private void DrawField(Rect pos, string toWrite)
	{
		if (multiline)
		{
			this.DrawMultiLine(pos, toWrite);
		}
		else
		{
			this.DrawSingleLine(pos, toWrite);
		}
	}

	private void DrawDebug(Rect pos)
	{
#if DEBUG_MODE
		if (debugPosition)
			GUI.Button(pos, "plop");
#endif
	}

	private void CheckFinished(int oldSize)
	{
		if (IsFocused && writtenText.Length == maxSize && oldSize != maxSize)
		{
			if (this.OnFinished != null)
			{
				this.OnFinished(this);
			}
		}
	}

	private void UpdateText()
	{
		if (IsFocused || !ifNotFocused)
		{
			if (!multiline)
			{
				writtenText = writtenText.Replace("\n", "");
			}

			Text = writtenText;
		}
	}

	private void ManageIfNotFocused(ref string toWrite)
	{
		if (ifNotFocused)
		{
			ifNotFocused.color = this.color;
			ifNotFocused.gameObject.SetActive(!this.IsFocused);
			if (!this.IsFocused)
			{
				toWrite = "";
			}
		}
	}

	private GUIStyle GetStyle()
	{
		GUIStyle style = new GUIStyle(GUI.skin.textField);

#if DEBUG_MODE
		if (noStyle)
			return style;
#endif

		GUIStyleState state = style.normal;
		state.background = null;
		state.textColor = this.color;

		style.onNormal = state;

		style.active = state;
		style.onActive = state;
		style.hover = state;
		style.onHover = state;
		style.focused = state;
		style.onFocused = state;

		style.font = this.font;
		style.fontSize = this.pcSettings.fontSize;

		return style;
	}

	#endregion

	public void DrawGUI()
	{
		if (Text == null) Text = string.Empty;

		if (Password && multiline)
		{
			Debug.LogWarning("Passwords can't be multiline! Turning multiline off");
			this.multiline = false;
		}

		Rect pos = ComputePosition();

		this.ComputeFocus();

		style = GetStyle();

		Color cursorColor = GUI.skin.settings.cursorColor;
		GUI.skin.settings.cursorColor = this.color;

		int oldSize = Text.Length;
		string toWrite = Text;

		this.ManageIfNotFocused(ref toWrite);

		int prevHC = GUIUtility.hotControl;
		GUIUtility.hotControl = 0;

		this.DrawField(pos, toWrite);

		GUIUtility.hotControl = prevHC;

		this.DrawDebug(pos);

		this.CheckFinished(oldSize);

		GUI.skin.settings.cursorColor = cursorColor;

		this.UpdateText();

		if (this.MustGiveFocus_PC)
		{
			this.MustGiveFocus_PC = false;
			GUI.FocusControl(this.ControlName);
		}
	}

	private void DetachedGUI()
	{
		float scale = Mathf.Min(Screen.height / this.pcSettings.resolution.y, Screen.width / this.pcSettings.resolution.x);
		var svMat = GUI.matrix;
		GUI.matrix = svMat * Matrix4x4.Scale(new Vector3(scale, scale, 1));

		this.DrawGUI();

		GUI.matrix = svMat;
	}

#endif

	#endregion

	#region Phone methods

#if MODE_PHONE
	
	private void StartPhoneMode ()
	{
		//Debug.Log("test " + this.name);

		if(this.phoneSettings.FocusButton)
			this.phoneSettings.FocusButton.OnClick += OnFocusButtonClicked;
		//else
			//Debug.Log("Without button, it will be hard !");
	}

	private void OnFocusButtonClicked(ButtonBehavior obj)
	{
		this.GiveFocus();		
	}


	// rechanger ca peut etre avec l'update
	//****************************************************************************************************
	//                                   ATTENTION 
	//____*****************************************************************************************************








	/*private void UpdatePhoneMode()
	{
		if(ifNotFocused)
		{
			ifNotFocused.color = IsFocused ? this.phoneSettings.colorIfFocused : this.color;
		}

		if (IsFocused)
		{
			if (Keyboard_iPhone == null)
			{
				Debug.Log("start text : " + Text);

				TextMesh ifEmptyTM;
				if (ifEmpty && (ifEmptyTM = ifEmpty.GetComponent<TextMesh>()))
				{
					Keyboard_iPhone = TouchScreenKeyboard.Open(Text, this.phoneSettings.type, false, multiline, Password, false, ifEmptyTM.text);
				}
				else
				{
					Keyboard_iPhone = TouchScreenKeyboard.Open(Text, this.phoneSettings.type, false, multiline, Password);
				}

				Keyboard_iPhone.active = true;
				previousText_iPhone = Text;

				if (this.OnFocusChanged != null)
				{
					this.OnFocusChanged(this, true);
				}
			}
			else
			{
				if(TouchScreenKeyboard.visible)
				{
					//Debug.Log("Keyboard text : " + Keyboard_iPhone.text);
					//Text = Keyboard_iPhone.text;
				}

				bool done = Keyboard_iPhone.done;
				bool canceled = Keyboard_iPhone.wasCanceled || 
					(Keyboard_iPhone.active && TouchScreenKeyboard.visible && TouchScreenKeyboard.area == (default(Rect)));

				if (done)
				{
					Debug.Log("Keyboard text : " + Keyboard_iPhone.text);
					Text = Keyboard_iPhone.text;

					if(canceled)
					{
						Text = previousText_iPhone;
					}
					else
					{
						if(this.OnReturnPressed != null)
							OnReturnPressed(this);
					}

					previousText_iPhone = Text;
					this.RemoveFocus();
				}
			}
		}
	}	
*/
#endif

	#endregion
}