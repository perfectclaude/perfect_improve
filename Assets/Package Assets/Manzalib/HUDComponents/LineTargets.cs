﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	namespace Components
	{
		/// <summary>
		/// Create a line using the given targets positions as vertex
		/// </summary>
		public class LineTargets : MonoBehaviour
		{
			/// <summary>
			/// Used line component
			/// </summary>
			private LineRenderer line;

			/// <summary>
			/// Targets (line points)
			/// </summary>
			public Transform[] targets;

			/// <summary>
			/// Used material
			/// </summary>
			public Material material;
			
			/// <summary>
			/// Line option: start color
			/// </summary>
			public Color startColor = Color.white;

			/// <summary>
			/// Line option: end color
			/// </summary>
			public Color endColor = Color.white;

			/// <summary>
			/// Line option: start width
			/// </summary>
			public float startWidth = 1;

			/// <summary>
			/// Line option: end width
			/// </summary>
			public float endWidth = 1;

			/// <summary>
			/// Frame: build line
			/// </summary>
			public void Update()
			{
				this.BuildLine();
			}

			/// <summary>
			/// Creates/Updates the line (color, width, vertex)
			/// </summary>
			private void BuildLine()
			{
				if (this.line == null)
				{
					this.line = this.gameObject.AddComponent<LineRenderer>();
				}

				this.line.material = this.material;
				this.line.SetColors(startColor, endColor);
				this.line.SetWidth(startWidth, endWidth);

				int c = targets.Length;
				line.SetVertexCount(c);

				for (int i = 0; i < c; i++)
				{
					line.SetPosition(i, this.targets[i].position);
				}
			}

		}
	}
}