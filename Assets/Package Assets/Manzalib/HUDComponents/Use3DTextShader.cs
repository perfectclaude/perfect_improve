﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	namespace Components
	{
		/// <summary>
		/// Allows the attached TextMesh to have a correct render queue
		/// </summary>
		[RequireComponent(typeof(TextMesh), typeof(MeshRenderer))]
		public class Use3DTextShader : MonoBehaviour
		{
			/// <summary>
			/// First frame. Sets the shader
			/// </summary>
			void Awake()
			{
				var mesh = this.GetComponent<MeshRenderer>();
				var text = this.GetComponent<TextMesh>();
				var shader = Shader.Find("GUI/3D Text Shader");

				if (text && mesh && shader)
				{
					mesh.material = new Material(shader);
					mesh.material.mainTexture = text.font.material.mainTexture;
					mesh.material.color = text.color;
				}
			}
		}
	}
}