﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	namespace Components
	{

		/// <summary>
		/// An HUD Object placed thanks to a world object
		/// </summary>
		public class WorldPlacedHUDComponent : MonoBehaviour
		{

			/// <summary>
			/// HUD Main camera (that will see this object)
			/// </summary>
			protected Camera HUDMainCamera;

			/// <summary>
			/// World Main camera (that sees the world object)
			/// </summary>
			protected Camera WorldMainCamera;

			/// <summary>
			/// The world object (placeholder)
			/// </summary>
			public Transform WorldTarget;

			/// <summary>
			/// Offset (related to the object)
			/// </summary>
			public Vector3 WorldOffset;

			/// <summary>
			/// Action to perform every frame.
			/// Places this object
			/// </summary>
			protected virtual void OnUpdate()
			{
				if (HUDMainCamera && WorldMainCamera && WorldTarget)
				{
					Vector3 position = this.WorldTarget.position;
					position += this.WorldTarget.TransformDirection(this.WorldOffset);
					position = Utils.UCamera.ConvertPosition(position, this.WorldMainCamera, this.HUDMainCamera);

					this.transform.position = position;
				}
			}

			/// <summary>
			/// Action to perform at the creation.
			/// Finds the main cameras
			/// </summary>
			protected virtual void OnAwake()
			{
				this.HUDMainCamera = GameObject.FindGameObjectWithTag("HUDCamera").GetComponent<Camera>();
				this.WorldMainCamera = Camera.main;
			}

			/// <summary>
			/// Called by unity. Calls OnAwake
			/// </summary>
			private void Awake()
			{
				this.OnAwake();
			}

			/// <summary>
			/// Called by unity. Calls OnUpdate
			/// </summary>
			private void Update()
			{
				this.OnUpdate();
			}
		}

	}
}