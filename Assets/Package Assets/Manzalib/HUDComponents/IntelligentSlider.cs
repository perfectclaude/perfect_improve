using UnityEngine;
using System.Collections;
using System;

public class IntelligentSlider : MonoBehaviour 
{
	public float EpsilonX = 0.1f;
	public float EpsilonY = 0.6f;

	public event Action<IntelligentSlider, float> OnValueChanged;

	public Renderer		Empty;
	public Renderer		Full;
	public Transform	Cursor;
	public GUITextField	LinkedValue;

	public float MinValue = 0;
	public float MaxValue = 1;

	public float Value
	{
		get
		{
			return Mathf.Lerp(MinValue, MaxValue, RatioValue);
		}
		set
		{
			SetRatio(Mathf.InverseLerp(MinValue, MaxValue, value));
		}
	}

	private float RatioValue;
	private float maxEmptyScale;
	private float maxFullScale;

	public bool inited { get; private set; }
	private float yPos;
	private float xMin;
	private float xMax;
	private float extn;

	public Camera customCamera;
	private bool IsSelected;
	
	private void Start()
	{
		if (inited)
			return;

		if (!customCamera)
			customCamera = Camera.main;

		if (!customCamera)
			throw new UnityException("I need a custom camera (or a main camera in scene)");

		if(Empty)
			maxEmptyScale	= Empty.transform.localScale.x;
		
		if(Full)
			maxFullScale	= Full.transform.localScale.x;

		if(!Empty && !Full)
			throw new UnityException("I need a full or empty element to work");

		if(!Empty)
		{
			Empty = Full;
			Full = null;
		}

		Bounds b = Empty.GetComponent<Renderer>().bounds;

		this.yPos = b.center.y;
		this.xMin = b.center.x - b.extents.x;
		this.xMax = b.center.x + b.extents.x;
		this.extn = (this.xMax - this.xMin) / 2;
		this.inited = true;

		this.Replace();
	}

	private void SetPositionX(Transform t, float x)
	{		
		Vector3 pos = t.position;
		pos.x = x;
		t.position = pos;
	}

	private void SetScaleX(Transform t, float x)
	{
		Vector3 scale = t.localScale;
		scale.x = x;
		t.localScale = scale;
	}

	public void ResetScales()
	{
		if (Empty)
			SetScaleX(Empty.transform, maxEmptyScale);

		if (Full)
			SetScaleX(Full.transform, maxFullScale);
	}

	public void Reinit()
	{
		this.inited = false;
		this.Start();
	}

	private void Replace()
	{
		if (!inited)
			return;

		RatioValue = Mathf.Clamp01(RatioValue);

		SetPositionX(Cursor, Mathf.Lerp(xMin, xMax, RatioValue));

		if (Full)
		{
			SetScaleX(Full.transform,		maxFullScale	* RatioValue);
			SetScaleX(Empty.transform,		maxEmptyScale	* (1 - RatioValue));
			SetPositionX(Full.transform,	xMin + (extn * RatioValue));
			SetPositionX(Empty.transform,	xMax - (extn * (1 - RatioValue)));
		}
		
		if (LinkedValue)
		{
			LinkedValue.Text = this.Value.ToString();
		}
	}
	
	private void Update()
	{
		Vector3 mPos = customCamera.ScreenToWorldPoint(Input.mousePosition);		
		
		float dy = Mathf.Abs(mPos.y - yPos);
		bool IsNear = dy < EpsilonY && mPos.x > xMin - EpsilonX && mPos.x < xMax + EpsilonX;
		
		if (IsSelected)
		{
			if (Input.GetMouseButtonUp(0))
			{
				IsSelected = false;
				if (OnValueChanged != null)
				{
					OnValueChanged(this, Value);
				}
			}
			else
			{
				RatioValue = Mathf.InverseLerp(xMin, xMax, mPos.x);
				Replace();
			}
		}
		else if (Input.GetMouseButtonDown(0))
		{
			if (IsNear)
			{
				IsSelected = true;
				RatioValue = Mathf.InverseLerp(xMin, xMax, mPos.x);
				Replace();
			}
		}
	}


	public void SetRatio(float p)
	{
		this.IsSelected = false;
		this.RatioValue = p;
		this.Replace();
	}
}
