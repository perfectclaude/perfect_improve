﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	using Maths;

	namespace Components
	{
		/// <summary>
		/// Links this content to a slider
		/// </summary>
		public class SliderContent : MonoBehaviour
		{
			/// <summary>
			/// Slider to link
			/// </summary>
			public SliderBehaviour Slider;

			/// <summary>
			/// Seen content area
			/// </summary>
			public Rect GrabArea;

			/// <summary>
			/// Total content area
			/// </summary>
			public Rect ModelArea;

			/// <summary>
			/// True if we have clicked
			/// </summary>
			private bool Clicked = false;

			/// <summary>
			/// transform's Y-position at click
			/// </summary>
			private float yClickPosition;

			/// <summary>
			/// mouse's Y-position at click
			/// </summary>
			private float yWhenClicked;

			/// <summary>
			/// True if we have slided
			/// </summary>
			private bool HasSlided = false;

			/// <summary>
			/// True if we had slided
			/// </summary>
			public bool IsSliding
			{
				get
				{
					return Slider.IsClicked || this.HasSlided;
				}
			}

			/// <summary>
			/// False if we can't move
			/// </summary>
			public bool CanSlide
			{
				get
				{
					return Slider.CanSlide;
				}
			}

			/// <summary>
			/// Starts behaviour
			/// </summary>
			private void OnEnable()
			{
				Slider.seenInterval = new Interval(GrabArea.yMin, GrabArea.yMax);
				Slider.totalSize = ModelArea.height;
				Slider.enabled = true;
			}

			/// <summary>
			/// Stops behaviour
			/// </summary>
			private void OnDisable()
			{
				Slider.enabled = false;
			}

			/// <summary>
			/// Shows areas at gizmos time
			/// </summary>
			void OnDrawGizmos()
			{
				Utils.UGizmos.DrawRect(this.GrabArea, Color.yellow);
				Utils.UGizmos.DrawRect(this.ModelArea, Color.red);
			}

			/// <summary>
			/// Called each frame to move
			/// </summary>
			public void Update()
			{
				Vector3 pos = this.transform.position;
				pos.y = Slider.yValue;
				this.transform.position = pos;

				Vector3 mousePosition = Input.mousePosition;
				mousePosition.x = mousePosition.x / Screen.width;
				mousePosition.y = mousePosition.y / Screen.height;

				Vector3 currentPosition = Utils.UCamera.GetMousePosition(Slider.cam, mousePosition);
				if (Input.GetMouseButtonDown(0) && this.GrabArea.Contains(currentPosition))
				{
					Clicked = true;
					yWhenClicked = this.transform.position.y;
					yClickPosition = currentPosition.y;
				}

				if (Input.GetMouseButtonUp(0))
				{
					Clicked = false;
					HasSlided = false;
				}

				if (Clicked)
				{
					float offset = currentPosition.y - yClickPosition;

					this.HasSlided = HasSlided || Mathf.Abs(offset) > 0.5f;
					this.Slider.SetValueFromY(yWhenClicked + offset);
				}
			}
		}
	}
}