using UnityEngine;
using System;

//namespace Utils
//{
//	namespace Components
//	{
		/// <summary>
		/// Button behavior.
		/// Allows to a gameObject to become a nice button
		/// </summary>
		public class ButtonBehavior : MonoBehaviour
		{

			/// <summary>
			/// The button current state
			/// </summary>
			public enum State
			{
				/// <summary>
				/// When the button is neither clicked or hovered
				/// </summary>
				NORMAL,

				/// <summary>
				/// When the mouse just clicked the button
				/// </summary>
				CLICKDOWN,

				/// <summary>
				/// When the mouse comes over the button
				/// </summary>
				MOUSEOVER,

				/// <summary>
				/// When the mouse was over the button, but leaves
				/// </summary>
				MOUSEOUT,

				/// <summary>
				/// When the button was clicked, but the mouse released
				/// </summary>
				CLICKUP
			}

			/// <summary>
			/// The button.
			/// </summary>
			public GameObject button;

			/// <summary>
			/// The button when over.
			/// </summary>
			public GameObject buttonOver;

			/// <summary>
			/// The button when clicked.
			/// </summary>
			public GameObject buttonClicked;

			/// <summary>
			/// Hide the button if the current state button GameObject is set to null (otherwise : it does nothing)
			/// </summary>
			public bool hideIfNull = false;

			/// <summary>
			/// Debug mode : will write in the Log what happens with the button
			/// </summary>
			public bool debug = false;

			/// <summary>
			/// Add a click sound
			/// </summary>
			public bool useClickSound;

			/// <summary>
			/// The camera used. If null, takes the main camera
			/// </summary>
			public Camera customCamera = null;

			/// <summary>
			/// The click sound.
			/// </summary>
			private AudioClip soundClick;

			/// <summary>
			/// The main audio source (for the 'click').
			/// </summary>
			private AudioSource mainAudioSource;

			/// <summary>
			/// The click sound path (where the click sound is located)
			/// </summary>
			public string clickSoundFile;

			/// <summary>
			/// Sound to play when the mouse comes over the button
			/// </summary>
			public AudioClip onEnterSound;

			/// <summary>
			/// Sound to play when the button has been clicked
			/// </summary>
			public AudioClip onClickSound;

			/// <summary>
			/// Option : is the button clicked at release ?
			/// </summary>
			public bool mouseUp = false;

			/// <summary>
			/// Option : should we desactivate the renderers gameObjects (or just the renderer component ?)
			/// </summary>
			public bool byActive = false;

			/// <summary>
			/// The function to call when a mouse is over at update
			/// </summary>
			public event Action<ButtonBehavior> OnMouseOver;

			/// <summary>
			/// The function to call when a mouse is out at update
			/// </summary>
			public event Action<ButtonBehavior> OnMouseOut;

			/// <summary>
			/// The function to call when the button is being clicked at update
			/// </summary>
			public event Action<ButtonBehavior> OnClick;

			public KeyCode Shortcut = KeyCode.None;

			/// <summary>
			/// The button state.
			/// </summary>
			private State state = State.NORMAL;

			/// <summary>
			/// "Click !"
			/// </summary>
			void DoClickSound()
			{
				mainAudioSource.PlayOneShot(soundClick);
			}

			/// <summary>
			/// Gets the button state.
			/// </summary>
			public State GetState()
			{
				return state;
			}

			/// <summary>
			/// Gets the button state.
			/// </summary>
			public void SetState(State s)
			{
				state = s;
			}

			/// <summary>
			/// Start this instance.
			/// </summary>
			public void Awake()
			{

				if (this.GetComponent<Collider>() == null)
				{
					Debug.LogWarning("add collider to " + this.name);
					this.gameObject.AddComponent<MeshCollider>();
					this.GetComponent<Collider>().isTrigger = true;
				}

				if (!this.GetComponent<Collider>().isTrigger)
				{
					Debug.LogWarning("set isTrigger = true to " + this.name);
					this.GetComponent<Collider>().isTrigger = true;
				}

				if (debug)
				{
					Debug.Log("Instantiate a buttonBehavior on " + this.name);
				}

				if (useClickSound)
				{
					soundClick = Resources.Load(clickSoundFile) as AudioClip;
					if (Camera.main != null)
						mainAudioSource = Camera.main.GetComponent<AudioSource>();
					if (!mainAudioSource)
					{
						//		Debug.LogWarning("Camera doesn't have AudioSource, clickSound disabled. (" + Application.loadedLevelName + ")");					
						useClickSound = false;
					}
				}

			}

			/// <summary>
			/// Locks the state and the button.. itself
			/// </summary>
			private bool _locked = false;

			/// <summary>
			/// Property : Locks the state and the button.. itself
			/// </summary>
			public bool locked
			{
				get
				{
					return _locked;
				}
				set
				{
					_locked = value;
					this.GetComponent<Collider>().enabled = !value;
				}
			}

			/// <summary>
			/// To hide the button (+ locks it)
			/// </summary>
			private bool _hidden = false;

			/// <summary>
			/// Property : To hide the button (+ locks it)
			/// </summary>
			public bool hidden
			{
				get
				{
					return _hidden;
				}
				set
				{
					if (value && !hidden)
					{
						locked = true;
						_hidden = true;

						if (byActive)
						{
							if (button != null) button.gameObject.SetActive(false);
							if (buttonOver != null) buttonOver.gameObject.SetActive(false);
							if (buttonClicked != null) buttonClicked.gameObject.SetActive(false);
						}
						else
						{
							if (button != null && button.GetComponent<Renderer>() != null) button.GetComponent<Renderer>().enabled = false;
							if (buttonOver != null && buttonOver.GetComponent<Renderer>() != null) buttonOver.GetComponent<Renderer>().enabled = false;
							if (buttonClicked != null && buttonClicked.GetComponent<Renderer>() != null) buttonClicked.GetComponent<Renderer>().enabled = false;
						}
						if (debug) Debug.Log("hidden");
					}
					else if (!value && hidden)
					{
						locked = false;
						_hidden = false;
						if (byActive)
						{
							if (button != null) button.gameObject.SetActive(true);
							if (buttonOver != null) buttonOver.gameObject.SetActive(false);
							if (buttonClicked != null) buttonClicked.gameObject.SetActive(false);
						}
						else
						{
							if (button != null && button.GetComponent<Renderer>() != null) button.GetComponent<Renderer>().enabled = true;
							if (buttonOver != null && buttonOver.GetComponent<Renderer>() != null) buttonOver.GetComponent<Renderer>().enabled = false;
							if (buttonClicked != null && buttonClicked.GetComponent<Renderer>() != null) buttonClicked.GetComponent<Renderer>().enabled = false;
						}
						if (debug) Debug.Log("shown");
					}
				}
			}

			/// <summary>
			/// Update this instance.
			/// </summary>
			/// <remarks>
			/// Little replica patch line 230 for the sound volume.
			/// </remarks>
			public void Update()
			{

				if (locked)
				{
					return;
				}

				if (useClickSound && mainAudioSource)
				{
					// <!> Special REPLICA.        
					//    mainAudioSource.volume = GameSave.soundVolume;
				}

				if (!this.GetComponent<Collider>().enabled)
				{
					Debug.LogWarning("Update : the collider is disabled !");
					return;
				}

				if (state == State.CLICKDOWN && Input.GetMouseButtonUp(0))
				{
					state = State.CLICKUP;
					if (mouseUp)
						whenUp();
					if (debug) Debug.Log("clickUp");
				}

				if (customCamera == null)
					customCamera = Camera.main;

				if (customCamera == null)
					return;

				if (!customCamera.enabled)
					return;

				if (this.Shortcut != KeyCode.None)
				{
					if (Input.GetKeyUp(this.Shortcut))
					{
						if (debug) Debug.Log(this.name + " click");
						if (OnClick != null) OnClick(this);

						if (this.onClickSound != null)
						{
							AudioSource.PlayClipAtPoint(this.onClickSound, this.transform.position);
						}

						if (useClickSound) DoClickSound();
						state = State.MOUSEOVER;
						return;
					}
				}

				RaycastHit hit;
				Ray ray = customCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, customCamera.transform.position.z));

				if (Physics.Raycast(ray, out hit))
				{
					if (hit.collider == this.GetComponent<Collider>())
					{
						if (state == State.CLICKUP)
						{
							if (debug) Debug.Log(this.name + " click");
							if (OnClick != null) OnClick(this);

							if (this.onClickSound != null)
							{
								AudioSource.PlayClipAtPoint(this.onClickSound, this.transform.position);
							}

							if (useClickSound) DoClickSound();
							state = State.MOUSEOVER;
						}

						else if (state != State.CLICKDOWN)
						{
							if (Input.GetMouseButtonDown(0))
							{
								state = State.CLICKDOWN;
								if (debug) Debug.Log(this.name + " clickDown");

								if (!mouseUp)
									whenDown();
							}
							else if (state != State.CLICKDOWN && state != State.MOUSEOVER)
							{
								state = State.MOUSEOVER;
								if (debug) Debug.Log(this.name + " over");

								whenOver();

								if (this.onEnterSound != null)
								{
									AudioSource.PlayClipAtPoint(this.onEnterSound, this.transform.position);
								}

								if (OnMouseOver != null) OnMouseOver(this);
							}
						}
						else
						{
							testOut();
						}
					}
					else
					{
						testOut();
					}
				}
				else
				{
					testOut();
				}
			}

			/// <summary>
			/// Checks if the button has been released (and the mouse out)
			/// Changes the button's state if that's the case
			/// </summary>
			private void testOut()
			{
				if (state != State.CLICKDOWN && state != State.MOUSEOUT)
				{
					state = State.MOUSEOUT;
					if (debug) Debug.Log(this.name + " out");

					whenOut();

					if (OnMouseOut != null) OnMouseOut(this);
				}
			}

			/// <summary>
			/// Displays the button when its state is Up
			/// </summary>
			public void whenUp()
			{
				if (hideIfNull || buttonClicked != null)
				{
					if (byActive)
					{
						if (button != null) button.gameObject.SetActive(false);
						if (buttonOver != null) buttonOver.gameObject.SetActive(false);
						if (buttonClicked != null) buttonClicked.gameObject.SetActive(true);
					}
					else
					{
						if (button != null && button.GetComponent<Renderer>() != null) button.GetComponent<Renderer>().enabled = false;
						if (buttonOver != null && buttonOver.GetComponent<Renderer>() != null) buttonOver.GetComponent<Renderer>().enabled = false;
						if (buttonClicked != null && buttonClicked.GetComponent<Renderer>() != null) buttonClicked.GetComponent<Renderer>().enabled = true;
					}
				}
			}

			/// <summary>
			/// Displays the button when its state is Down
			/// </summary>
			public void whenDown()
			{
				if (hideIfNull || buttonClicked != null)
				{
					if (byActive)
					{
						if (button != null) button.gameObject.SetActive(false);
						if (buttonOver != null) buttonOver.gameObject.SetActive(false);
						if (buttonClicked != null) buttonClicked.gameObject.SetActive(true);
					}
					else
					{
						if (button != null && button.GetComponent<Renderer>() != null) button.GetComponent<Renderer>().enabled = false;
						if (buttonOver != null && buttonOver.GetComponent<Renderer>() != null) buttonOver.GetComponent<Renderer>().enabled = false;
						if (buttonClicked != null && buttonClicked.GetComponent<Renderer>() != null) buttonClicked.GetComponent<Renderer>().enabled = true;
					}
				}
			}

			/// <summary>
			/// Displays the button when its state is Over
			/// </summary>
			public void whenOver()
			{
#if !(UNITY_IPHONE || UNITY_ANDROID)
				if (hideIfNull || buttonOver != null)
				{
					if (byActive)
					{
						if (button != null) button.gameObject.SetActive(false);
						if (buttonOver != null) buttonOver.gameObject.SetActive(true);
						if (buttonClicked != null) buttonClicked.gameObject.SetActive(false);
					}
					else
					{
						if (button != null && button.GetComponent<Renderer>() != null) button.GetComponent<Renderer>().enabled = false;
						if (buttonClicked != null) buttonClicked.GetComponent<Renderer>().enabled = false;
						if (buttonOver != null && buttonOver.GetComponent<Renderer>() != null) buttonOver.GetComponent<Renderer>().enabled = true;
					}
				}
#else
		whenOut();
#endif
			}

			/// <summary>
			/// Displays the button when its state is Out
			/// </summary>
			public void whenOut()
			{
				if (hideIfNull || button != null)
				{
					if (byActive)
					{
						if (button != null) button.gameObject.SetActive(true);
						if (buttonOver != null) buttonOver.gameObject.SetActive(false);
						if (buttonClicked != null) buttonClicked.gameObject.SetActive(false);
					}
					else
					{
						if (buttonOver != null && buttonOver.GetComponent<Renderer>() != null) buttonOver.GetComponent<Renderer>().enabled = false;
						if (buttonClicked != null && buttonClicked.GetComponent<Renderer>() != null) buttonClicked.GetComponent<Renderer>().enabled = false;
						if (button != null && button.GetComponent<Renderer>() != null) button.GetComponent<Renderer>().enabled = true;
					}
				}
			}
		}
// 	}
// }