﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	using Maths;

	namespace Components
	{		
		/// <summary>
		/// Simple user slider
		/// </summary>
		public class SliderBehaviour : MonoBehaviour
		{
			/// <summary>
			/// HUD Camera
			/// </summary>
			public Camera cam;

			/// <summary>
			/// Seen content interval
			/// </summary>
			public Interval seenInterval;

			/// <summary>
			/// Total content size
			/// </summary>
			public float totalSize;

			/// <summary>
			/// Seen content area
			/// </summary>
			private Rect Area;

			/// <summary>
			/// Elevator parent transform
			/// </summary>
			private Transform ascenseur;

			/// <summary>
			/// Top elevator object
			/// </summary>
			private Transform aTop;

			/// <summary>
			/// Scalable body elevator object
			/// </summary>
			private Transform aScaled;

			/// <summary>
			/// Bot elevator object
			/// </summary>
			private Transform aBot;

			/// <summary>
			/// Slider parent transform
			/// </summary>
			private Transform slider;

			/// <summary>
			/// Top slider object
			/// </summary>
			private Transform sTop;

			/// <summary>
			/// Scalable body slider object
			/// </summary>
			private Transform sScaled;

			/// <summary>
			/// Bot slider object
			/// </summary>
			private Transform sBot;

			/// <summary>
			/// Current slider value (0 <-> 1)
			/// </summary>
			public float value;

			/// <summary>
			/// Seen content size (height)
			/// </summary>
			private float seenSize;

			/// <summary>
			/// If true : we are using the elevator
			/// </summary>
			public bool IsClicked { get; private set; }

			/// <summary>
			/// If false : the value is fixed
			/// </summary>
			public bool CanSlide
			{
				get
				{
					return (seenSize < totalSize);
				}
			}

			/// <summary>
			/// Set value using content y-value
			/// </summary>
			/// <param name="y"></param>
			public void SetValueFromY(float y)
			{
				this.value = Mathf.Clamp01(
					Mathf.InverseLerp(this.seenInterval.max, this.seenInterval.max + this.totalSize - this.seenSize, y)
				);
				this.UpdateSliderSize();
			}

			/// <summary>
			/// Get content Y-value
			/// </summary>
			public float yValue
			{
				get
				{
					return Mathf.Lerp(this.seenInterval.max, this.seenInterval.max + this.totalSize - this.seenSize, value);
				}
			}

			//	public void OnGUI()
			//	{
			//		GUILayout.Label("Value : " + value);
			//		GUILayout.Label("Seen size : " + seenSize);
			//		GUILayout.Label("Total size : " + totalSize);
			//		GUILayout.Label("Y-Value : " + yValue);
			//		GUILayout.Label("Y-Value + Seen size : " + (yValue + seenSize));
			//	}

			/// <summary>
			/// Search child elements
			/// </summary>
			private void Awake()
			{
				this.ascenseur = this.transform.Find("ascenceur");
				this.aTop = this.ascenseur.transform.Find("tete_ascenceur");
				this.aScaled = this.ascenseur.transform.Find("mid_ascenceur");
				this.aBot = this.ascenseur.transform.Find("bas_ascenceur");

				this.slider = this.transform.Find("slider");
				this.sTop = this.slider.transform.Find("slider_tete");
				this.sScaled = this.slider.transform.Find("slider_mid");
				this.sBot = this.slider.transform.Find("slider_bas");

				this.cam = this.FindCamera();
			}

			/// <summary>
			/// Search camera if not found
			/// </summary>
			/// <returns>Found camera</returns>
			private Camera FindCamera()
			{
				if (this.cam != null)
					return this.cam;

				return Camera.main;
			}

			/// <summary>
			/// Starts behaviour
			/// </summary>
			private void OnEnable()
			{
				value = Mathf.Clamp01(value);
				this.UpdateAscenseurSize();
				this.UpdateSliderSize();
			}

			/// <summary>
			/// Updates elevator size
			/// </summary>
			private void UpdateAscenseurSize()
			{
				this.seenInterval.Check();
				this.seenSize = this.seenInterval.GetAmplitude();

				this.Area = new Rect();
				this.Area.width = 1;

				Vector3 pos = this.transform.position;
				this.ascenseur.position = pos;

				float extentTop = 0;
				if (this.aTop.GetComponent<Renderer>())
				{
					extentTop = this.aTop.GetComponent<Renderer>().bounds.extents.y;
				}

				float extentBot = 0;
				if (this.aBot.GetComponent<Renderer>())
				{
					extentBot = this.aBot.GetComponent<Renderer>().bounds.extents.y;
				}

				float currentSize = 1;
				if (this.aScaled.GetComponent<Renderer>())
				{
					currentSize = this.aScaled.GetComponent<Renderer>().bounds.size.y;
					this.Area.width = this.aScaled.GetComponent<Renderer>().bounds.size.x;
				}

				this.Area.height = seenSize;
				this.Area.center = this.transform.position;

				pos.y = seenInterval.max - extentTop;
				this.aTop.position = pos;

				pos.y = seenInterval.min + extentBot;
				this.aBot.position = pos;

				pos.y = (seenInterval.max + seenInterval.min) / 2;
				this.aScaled.position = pos;

				float wantedSize = (this.seenSize - 2 * (extentBot + extentTop));
				Vector3 scale = this.aScaled.localScale;

				if (scale.y != 0 && wantedSize != 0 && currentSize != 0)
				{
					scale.y = (scale.y * wantedSize) / currentSize;
					this.aScaled.localScale = scale;
				}
			}

			/// <summary>
			/// Sets slider size
			/// </summary>
			private void UpdateSliderSize()
			{
				float sliderSize;
				if (totalSize != 0)
				{
					sliderSize = seenSize * (seenSize / totalSize);
				}
				else
				{
					sliderSize = 0;
				}

				if (seenSize >= totalSize) // if(!CanSlide)
				{
					sliderSize = seenSize;
					value = 0;
				}

				float sliderExtent = sliderSize / 2;
				float yPos = Mathf.Lerp(this.seenInterval.max - sliderExtent, this.seenInterval.min + sliderExtent, value);

				Vector3 pos = this.transform.position;
				pos.y = yPos;
				pos.z -= 0.01f;

				this.slider.position = pos;

				float extentTop = 0;
				if (this.sTop.GetComponent<Renderer>())
				{
					extentTop = this.sTop.GetComponent<Renderer>().bounds.extents.y;
				}

				float extentBot = 0;
				if (this.sBot.GetComponent<Renderer>())
				{
					extentBot = this.sBot.GetComponent<Renderer>().bounds.extents.y;
				}

				float currentSize = 1;
				if (this.sScaled.GetComponent<Renderer>())
				{
					currentSize = this.sScaled.GetComponent<Renderer>().bounds.size.y;
				}

				pos.y = yPos + sliderExtent - extentTop;
				this.sTop.position = pos;

				pos.y = yPos - sliderExtent + extentBot;
				this.sBot.position = pos;

				pos.y = yPos;
				this.sScaled.position = pos;

				float wantedSize = sliderSize - 2 * (extentTop + extentBot);
				Vector3 scale = this.sScaled.localScale;

				if (scale.y != 0 && wantedSize != 0 && currentSize != 0)
				{
					scale.y = (scale.y * wantedSize) / currentSize;
					this.sScaled.localScale = scale;
				}
			}

			/// <summary>
			/// Sets slider position
			/// </summary>
			private void UpdateSliderValue()
			{
				Vector2 inputPosition = Input.mousePosition;

				inputPosition.x = (inputPosition.x / Screen.width);
				inputPosition.y = (inputPosition.y / Screen.height);

				Vector2 worldPosition = Utils.UCamera.GetMousePosition(this.cam, inputPosition);

				if (!IsClicked && Input.GetMouseButtonDown(0))
				{
					if (this.Area.Contains(worldPosition))
					{
						IsClicked = true;
					}
				}

				if (Input.GetMouseButtonUp(0))
				{
					IsClicked = false;
				}

				if (IsClicked)
				{
					float sliderSize;
					if (totalSize != 0)
					{
						sliderSize = seenSize * (seenSize / totalSize);
					}
					else
					{
						sliderSize = 0;
					}

					float min = this.seenInterval.min + sliderSize / 2;
					float max = this.seenInterval.max - sliderSize / 2;

					this.value = Mathf.Clamp01(Mathf.InverseLerp(max, min, worldPosition.y));
					this.UpdateSliderSize();
				}
			}

			/// <summary>
			/// Draws area at gizmos time
			/// </summary>
			private void OnDrawGizmos()
			{
				if (this.cam)
				{
					Vector2 pos = Input.mousePosition;

					pos.x = (pos.x / Screen.width);
					pos.y = (pos.y / Screen.height);

					Utils.UGizmos.DrawVector(Utils.UCamera.GetMousePosition(this.cam, pos), Color.yellow);
					if (Utils.UCamera.IsMouseOn(this.cam, pos, this.Area))
					{
						Utils.UGizmos.DrawRect(this.Area, Color.green);
					}
					else
					{
						Utils.UGizmos.DrawRect(this.Area, Color.red);
					}
				}
			}

			/// <summary>
			/// Updates slider value
			/// </summary>
			private void Update()
			{
				UpdateSliderValue();
			}

		}
	}
}