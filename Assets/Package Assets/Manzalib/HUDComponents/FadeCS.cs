using UnityEngine;
using System.Collections;
using System;

namespace Utils
{
	namespace Components
	{
		/// <summary>
		/// Fades the screen using Unity's GUI.
		/// </summary>
		public class FadeCS : MonoBehaviour
		{
			/// <summary>
			/// Fade texture
			/// </summary>
			public Texture2D texture;

			/// <summary>
			/// Fade speed
			/// </summary>
			private float fadeSpeed = 0;

			/// <summary>
			/// Draw depth
			/// </summary>
			public int drawDepth = -1000;

			/// <summary>
			/// Current alpha state
			/// </summary>
			public float alpha = 0;

			/// <summary>
			/// Called when the fade is finished
			/// </summary>
			public Action OnFaded = null;

			/// <summary>
			/// Speed sign (-1 or +1)
			/// </summary>
			private float fadeDir = -1;

			/// <summary>
			/// Sets the default fade speed
			/// </summary>
			void Start()
			{
				setFadeSpeed(0.4f);
			}

			/// <summary>
			/// Returns true if completely transparent
			/// </summary>
			/// <returns>True if completely transparent</returns>
			public bool IsTransparent()
			{
				return (alpha == 0.0f);
			}

			/// <summary>
			/// Returns true if completely opaque
			/// </summary>
			/// <returns>True if completely opaque</returns>
			public bool IsOpaque()
			{
				return (alpha == 1.0f);
			}

			/// <summary>
			/// Makes the texture transparent
			/// </summary>
			public void fadeIn()
			{
				fadeDir = -1;
			}

			/// <summary>
			/// Makes the texture opaque
			/// </summary>
			public void fadeOut()
			{
				fadeDir = 1;
			}

			/// <summary>
			/// Sets the fade speed
			/// </summary>
			/// <param name="speed">New speed</param>
			public void setFadeSpeed(float speed)
			{
				fadeSpeed = speed;
			}

			/// <summary>
			/// True if the fade speed is not null
			/// </summary>
			/// <returns>True if the fade speed is not null</returns>
			public bool IsFadingActive()
			{
				return this.fadeSpeed != 0;
			}

			/// <summary>
			/// Called during every GUI frame
			/// </summary>
			void OnGUI()
			{
				alpha += fadeDir * fadeSpeed * Time.deltaTime;
				alpha = Mathf.Clamp01(alpha);

				GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
				GUI.depth = drawDepth;

				if (texture)
				{
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
				}

				if (((this.IsOpaque() || this.IsTransparent()) && this.IsFadingActive()) && OnFaded != null)
				{
					OnFaded();
					OnFaded = null;
				}

			}

		}
	}
}