﻿using UnityEngine;
using System.Collections;

namespace Utils
{

	namespace Components
	{

		/// <summary>
		/// A simple generic gauge
		/// </summary>
		public class Gauge : MonoBehaviour
		{
			/// <summary>
			/// The gauge target (where the top should be)
			/// </summary>
			public Transform targetTop;

			/// <summary>
			/// Gauge's top
			/// </summary>
			public Renderer top;

			/// <summary>
			/// Gauge's bot
			/// </summary>
			public Renderer bot;

			/// <summary>
			/// Gauge's scalable body
			/// </summary>
			public Renderer mid;

			/// <summary>
			/// Size at start
			/// </summary>
			private float initialSize;

			/// <summary>
			/// Checks whether the gauge is initialized or not
			/// </summary>
			private bool inited = false;

			/// <summary>
			/// Starts the gauge
			/// </summary>
			void Start()
			{
				initialSize = mid.bounds.size.y;
				if (!inited)
					this.Init();
			}

			/// <summary>
			/// Initialises the gauge
			/// </summary>
			public virtual void Init()
			{
				inited = true;
			}

			/// <summary>
			/// Called to update gauge size/position
			/// </summary>
			public void UpdateGauge()
			{
				Vector3 pos = transform.position;

				bot.transform.position = pos;
				top.transform.position = new Vector3(pos.x, targetTop.position.y - top.bounds.extents.y, pos.z);

				float yScale = (top.transform.position.y - pos.y) / initialSize;

				Vector3 scale = mid.transform.localScale;
				scale.y = yScale;
				mid.transform.localScale = scale;

				mid.transform.position = pos + Vector3.up * (initialSize * yScale / 2);
			}

			/// <summary>
			/// Called every frame
			/// </summary>
			void Update()
			{
				if (top && bot && mid && targetTop)
					UpdateGauge();
			}
		}
	}
}