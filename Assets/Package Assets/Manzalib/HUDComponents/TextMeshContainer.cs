﻿//#define DEBUG_MODE
#define ALERT_ON_OVERFLOW

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Removes UnityEngine issue
/// </summary>
public class TextMeshContainer : Utils.Components.TextMeshContainer { }

namespace Utils
{
	namespace Components
	{
		/// <summary>
		/// Zone de text meshes
		/// </summary>
		public class TextMeshContainer : MonoBehaviour
		{
			/// <summary>
			/// Taille de la zone de texte
			/// </summary>
			public Vector2 dimensions;

			/// <summary>
			/// Espacement vertical et horizontal minimal
			/// </summary>
			public Vector2 minimumSpacing;

			/// <summary>
			/// Collection de text mesh (peut donc être donnée sous forme de tableau,
			/// de liste, ou d'une collection personnelle
			/// </summary>
			private ICollection<TextMesh> texts;

			/// <summary>
			/// Affiche la zone de texte en gizmos.
			/// Si DEBUG_MODE est actif : affiche la place que prend chaque Text Mesh
			/// </summary>
			public void OnDrawGizmos()
			{
				Rect area = new Rect();
				area.x = this.transform.position.x;
				area.y = this.transform.position.y - dimensions.y;
				area.width = dimensions.x;
				area.height = dimensions.y;

				UGizmos.DrawRect(area, Color.green);

#if DEBUG_MODE
				if (texts == null || texts.Count == 0)
					return;

				foreach (TextMesh t in texts)
				{
					UGizmos.DrawRect(Area.BoundsToRect(t.renderer.bounds), Color.red);
				}
#endif
			}

			/// <summary>
			/// Change la collection de text mesh
			/// </summary>
			/// <param name="texts">La collection de text mesh</param>
			public void SetTextMesh(ICollection<TextMesh> texts)
			{
				this.texts = texts;
			}

#if DEBUG_MODE
			/// <summary>
			/// Si mode DEBUG_MODE : met à jour la zone de texte
			/// en affichant des informations dans le GUI Layer
			/// </summary>
			private void OnGUI()
#else
			/// <summary>
			/// Si pas mode DEBUG_MODE : met à jour la zone de texte.
			/// </summary>
			private void Update()
#endif
			{
				// Resultat voulu :
				//
				// [°°°°°°°°°°°°°°°°°°°°°°°°°°°°]
				// [            vs              ]
				// [ S TEXT1 S TEEEEEXT2 S T3 S ] line 0
				// [            vs              ]
				// [    s TEXT4 s TEXTTT5 s     ] line 1
				// [            vs              ]
				// [____________________________]
				//
				// les espaces (verticaux) entre les lignes doivent valoir la même chose --> vs
				// les espaces (horizontaux) entre les text meshs au sein d'une même ligne doivent valoir la même chose.
				// les espaces (horizontaux) peuvent donc varier d'une ligne à l'autre --> S et s
				// on doit insérer autant de textmesh que l'on peut sur notre ligne.
				//
				// la largeur d'une ligne ne devraient pas exceder dimension.x
				// (cas possible : mot qui a lui seul a une largeur plus grande que dimension.x)
				//
				// une ligne ne devrait pas dépasser de la boite.
				// (cas possible : trop de mots pour pas assez d'espace)
				//
				// TEXT1, TEEEEEXT2 T3 TEXT4 et TEXTTT5 sont des textmeshs
				// vs est l'espacement vertical
				// S est l'espacement horizontal sur la ligne 0
				// s est l'espacement horizontal sur la ligne 1
				//


				if (texts == null || texts.Count == 0)
					return;

				// Remplis à la premiere boucle, pour la seconde
				float[] textWidth = new float[texts.Count];		// Taille des text mesh
				int[] textsLine = new int[texts.Count];			// Ligne des text mesh
				List<int> linesTextCount = new List<int>();		// Nombre de text mesh des lignes
				List<float> linesHeight = new List<float>();	// Taille des lignes (*) -> hauteur (max)
				List<float> linesWidth = new List<float>();		// Taille des lignes (*) -> largeur (somme)
				float totalLinesHeights = 0;					// Hauteur de l'ensemble des lignes (non vides)
				int countOfLines;								// Nombre de lignes

				// Indices
				int i = 0;
				int l = 0;

				// Première boucle
				int lineTextCount = 0;	// Nombre de text mesh de la ligne
				float lineHeight = 0;	// Taille de la ligne (*) -> hauteur (max)
				float lineWidth = 0;	// Taille de la ligne (*) -> largeur (somme)

				// Seconde boucle
				float spaceV;				// Espacement actuel vertical
				float spaceH;				// Espacement actuel horizontal
				float spaceBetweenLines;	// Espacement vertical (espace entre les lignes)
				float spaceBetweenTexts;	// Espacement horizontal de la ligne (espace entre les text mesh)
				int previousLine;			// Précédente ligne

				// (*) :	si les texts meshs se trouvant sur la ligne forment un seul text mesh, 
				//			ce serait la taille de ce textMesh

				// Première boucle
				foreach (TextMesh t in texts)
				{
					// On récupère la largeur du text mesh
					textWidth[i] = t.GetComponent<Renderer>().bounds.size.x;

					// Coup de gueule : si le mot est trop large
#if ALERT_ON_OVERFLOW
					if (textWidth[i] > dimensions.x)
					{
						Debug.LogWarning("X-Overflow " + this.name + " at : " + t.text);
					}
#endif

					// On depasserait dimension.x : nouvelle ligne.
					if (lineWidth + textWidth[i] + (minimumSpacing.x * (lineTextCount + 1)) > dimensions.x)
					{
						// ajoute la ligne précedente dans la liste
						linesHeight.Add(lineHeight);
						linesTextCount.Add(lineTextCount);
						linesWidth.Add(lineWidth);

						// on met à jour la hauteur des lignes
						totalLinesHeights += lineHeight;

						// on passe à la ligne suivante
						l++;

						// on initialise la nouvelle ligne
						lineHeight = 0;
						lineWidth = 0;
						lineTextCount = 0;
					}

					// On met à jour les infos de la ligne actuelle
					textsLine[i] = l;
					lineWidth += textWidth[i];
					lineTextCount++;
					lineHeight = Mathf.Max(t.GetComponent<Renderer>().bounds.size.y, lineHeight);

					// On passe au mot suivant
					i++;
				}

				// on met à jour la hauteur des lignes
				totalLinesHeights += lineHeight;

				// ajoute la derniere ligne dans la liste
				linesTextCount.Add(lineTextCount);
				linesWidth.Add(lineWidth);
				linesHeight.Add(lineHeight);

				// Coup de gueule : il manque de l'espace
#if ALERT_ON_OVERFLOW
				if (totalLinesHeights > dimensions.y)
				{
					Debug.LogWarning("Y-Overflow " + this.name + ". More space needed.");
				}
#endif

				// dernier indice + 1 --> le compte de ligne
				countOfLines = l + 1;

				// indices
				i = 0;
				l = 0;
				previousLine = 0;

				// espacements
				spaceBetweenLines = Mathf.Max(minimumSpacing.y, (dimensions.y - totalLinesHeights) / (countOfLines + 1));
				spaceBetweenTexts = Mathf.Max(minimumSpacing.x, (dimensions.x - linesWidth[l]) / (linesTextCount[l] + 1));
				spaceH = spaceBetweenTexts;
				spaceV = spaceBetweenLines;
#if DEBUG_MODE
				GUILayout.Label("Première ligne. spaceH : " + spaceH);
#endif

				// Seconde boucle
				foreach (TextMesh t in texts)
				{
					l = textsLine[i];
					if (previousLine != l)
					{
						spaceV += linesHeight[previousLine] + spaceBetweenLines;
						spaceBetweenTexts = Mathf.Max(minimumSpacing.x, (dimensions.x - linesWidth[l]) / (linesTextCount[l] + 1));
						spaceH = spaceBetweenTexts;
#if DEBUG_MODE
						GUILayout.Label("Changement de ligne. spaceH : " + spaceH);
#endif
					}

					t.transform.parent = this.transform;

					t.transform.position = this.transform.position
						+ Vector3.right * spaceH
						- Vector3.up * spaceV;

#if DEBUG_MODE
					GUILayout.Label(t.text + " : line " + l + " Width : " + textWidth[i]);
#endif

					spaceH += spaceBetweenTexts + textWidth[i];

					previousLine = l;
					i++;
				}

			}
		}

	}
}