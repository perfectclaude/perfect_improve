﻿using UnityEngine;
using System.Collections;

public class RoomLightMap : MonoBehaviour {
	
	public Texture2D [] Lightmapfar;
	public Texture2D [] Lightmapnear;
	
	// Use this for initialization
	void Start () {
		
		if((Lightmapfar != null)||(Lightmapnear != null))
		{
			long size = 0;
			if(Lightmapfar != null)
				size = Lightmapfar.Length;
			
			if(	(Lightmapnear != null)&&
				(Lightmapnear.Length>size))
			{
				size = Lightmapfar.Length;
			}
			
			LightmapData[] lightmapData = new LightmapData[size];
			
			if(Lightmapfar != null)
			for(int i=0; i<Lightmapfar.Length; i++)
			{
				lightmapData[i] = new LightmapData();
				lightmapData[i].lightmapFar = Lightmapfar[i];
			}
			if(Lightmapnear != null)
			for(int i=0; i<Lightmapnear.Length; i++)
			{
				lightmapData[i] = new LightmapData();
				lightmapData[i].lightmapNear = Lightmapnear[i];
			}
			LightmapSettings.lightmaps = lightmapData;
		}
		
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
}
