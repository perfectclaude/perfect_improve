﻿namespace Utils
{
	/// <summary>
	/// Object that has an Id.
	/// Every object (with the same type) should have a unique Id.
	/// </summary>
	public interface IIdentifiableObject
	{
		/// <summary>
		/// Object's id
		/// </summary>
		string Id
		{
			get;
		}
	}
}
