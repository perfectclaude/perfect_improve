﻿using UnityEngine;

namespace Utils
{
	/// <summary>
	/// A simple area 2D
	/// </summary>
	public interface IArea2D : IPositioned2D
	{
		/// <summary>
		/// Returns the area (as a Rect)
		/// </summary>
		/// <returns>the area</returns>
		Rect GetArea();

		/// <summary>
		/// Sets this area
		/// </summary>
		/// <param name="r">the new area</param>
		void SetArea(Rect r);
	}
}
