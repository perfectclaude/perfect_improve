﻿using UnityEngine;

namespace Utils
{
	/// <summary>
	/// A placed object
	/// </summary>
	public interface IPositioned2D
	{
		/// <summary>
		/// Returns the object position
		/// </summary>
		/// <returns>the position</returns>
		Vector2 GetPosition();

		/// <summary>
		/// Places the object
		/// </summary>
		/// <param name="v">The new object position</param>
		void SetPosition(Vector2 v);
	}
}
