﻿/**
 * @file CollectionUtils.cs
 * @brief Tools for Collection management 
 */

using System.Collections;
using System.Collections.Generic;

namespace Utils
{
	/// <summary>
	/// Collection management functions
	/// </summary>
	public static class Collection
	{
		/// <summary>
		/// Returns true if the collection contains every given value
		/// </summary>
		/// <example>
		/// ~~~~~~~~~~~~{.cs}
		///	 ICollection<string> myFirstLine = this.GetFirstLine(myFile);
		///	 bool ok = Utils.Collection.ContainsAll(myFirstLine, "field1", "field2", "field3");
		///	 if(!ok)
		///		 throw new System.FormatException("The file is corrupted");
		/// ~~~~~~~~~~~~
		/// </example>
		/// <typeparam name="T">The type of data our collection has</typeparam>
		/// <param name="collection">The collection to check</param>
		/// <param name="values">The values to check.</param>
		/// <returns>False if a value is not in the collection.</returns>
		public static bool ContainsAll<T>(ICollection<T> collection, params T[] values)
		{
			bool containsAll = true;

			int i = 0;
			int l = values.Length;

			while (containsAll && i < l)
			{
				containsAll = collection.Contains(values[i]);
				i++;
			}

			return containsAll;
		}

		/// <summary>
		/// Converts a collection to an array using its CopyTo method
		/// </summary>
		/// <typeparam name="T">The type of the objects the collection handles</typeparam>
		/// <param name="collection">The collection to convert</param>
		/// <returns>The converted array</returns>
		public static T[] ToArray<T>(ICollection<T> collection)
		{
			T[] array = new T[collection.Count];
			collection.CopyTo(array, 0);
			return array;
		}

		/// <summary>
		/// Find some IIdentifiable object within a collection using it's id
		/// </summary>
		/// <typeparam name="T">Type of the object</typeparam>
		/// <param name="objects">Collection</param>
		/// <param name="id">Id to find</param>
		/// <param name="caseSensitive">
		/// Case sensitive.
		/// <example>
		/// If true, if an object has an id "myID", if we search "MyId", this object can be found
		/// </example>
		/// </param>
		/// <returns>The found object</returns>
		public static T FindById<T>(ICollection<T> objects, string id, bool caseSensitive = true) where T : IIdentifiableObject
		{
			if (!caseSensitive)
				id = id.ToLower();

			string toCompare;
			foreach (T obj in objects)
			{
				toCompare = obj.Id;

				if (!caseSensitive)
					toCompare = toCompare.ToLower();

				if (toCompare.Equals(id))
					return obj;
			}

			return default(T);
		}
	}

}