﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Utils
{
	/// <summary>
	/// Mathematic functions.
	/// </summary>
	public static class Math
	{
		/// <summary>
		/// Returns the angle created by the two given position.
		/// Using the Z axis as normal. and X axis as base
		/// </summary>
		/// <param name="posA">first position</param>
		/// <param name="posB">last position</param>
		/// <returns>Degrees value</returns>
		public static float GetAngleZ(Vector3 posA, Vector3 posB)
		{
			return GetAngleZ(posB - posA); 
		}

		/// <summary>
		/// Returns the angle created by the given vector.
		/// Using the Z axis as normal. and X axis as base
		/// </summary>
		/// <param name="v">vector we want the angle</param>
		/// <returns>Degrees value</returns>
		public static float GetAngleZ(Vector3 v)
		{
			return  (Mathf.Atan2(v.y,v.x) * Mathf.Rad2Deg);
		}

		/// <summary>
		/// Returns an average position
		/// </summary>
		/// <param name="positions">Collection of positions</param>
		/// <param name="ifZero">Return value for empty collections</param>
		/// <returns>the average between every position or 'ifZero' if the collection is empty.</returns>
		public static Vector2 GetAverage(ICollection<Vector2> positions, Vector2 ifZero = default(Vector2))
		{
			int count = positions.Count;
			if (count == 0)
				return ifZero;

			Vector2 sum = Vector2.zero;
			foreach (var p in positions)
			{
				sum += p;
			}

			return sum / count;
		}

		/// <summary>
		/// Creates a Vector3 using a Vector2 (x and y) and a float (z)
		/// </summary>
		/// <param name="xy">The first two composants</param>
		/// <param name="z">The last one</param>
		/// <returns>A brand new Vector3</returns>
		public static Vector3 Compose(Vector2 xy, float z)
		{
			return new Vector3(xy.x, xy.y, z);
		}		

		/// <summary>
		/// A normalize composant-wise.
		/// This will clamp x and y between 0 and 1.
		/// </summary>
		/// <example>
		/// * (3, 2) => (1, 1) 
		/// * (3, 0.5f) => (1, 0.5f)
		/// </example>
		/// <remarks>
		/// If you want a directionnal vector, you must see: Vector2.Normalize()
		/// </remarks>
		/// <param name="v">The vector we want its components to be clamped</param>
		/// <returns>Our clamped vector</returns>
		public static Vector2 ClampXY01(Vector2 v)
		{
			v.x = Mathf.Clamp01(v.x);
			v.y = Mathf.Clamp01(v.y);
			return v;
		}			   

		/// <summary>
		/// Returns the translation to make after a scale if we want to use a pivot
		/// </summary>
		/// <param name="scaleValue">The scale value</param>
		/// <param name="ecart">space between some point of the rect and the pivot</param>
		/// <returns>The translation to apply to our rect (from the given point)</returns>
		public static Vector2 ScalePivotMove(float scaleValue, Vector2 ecart)
		{
			return (scaleValue - 1) * ecart;
		}
	}
}