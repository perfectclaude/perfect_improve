﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Utils
{
	using Maths;

	/// <summary>
	/// Mathematic area functions.
	/// </summary>
	public static class Area
	{
		/// <summary>
		/// Separates rect from fixedRect
		/// </summary>
		/// <param name="rect">The rect to move</param>
		/// <param name="fixedRect">The fixed rect</param>
		/// <returns>Returns the translation to apply to our rect</returns>
		public static Vector2 Separate(Rect rect, Rect fixedRect)
		{
			Rect inter;
			if (!IntersectAreas(fixedRect, rect, out inter))
			{
				return Vector2.zero;
			}

			Vector2 dir = rect.center - fixedRect.center;
			Vector2 res;

			if (inter.width < inter.height)
			{
				res.x = inter.width;
				if (dir.x == 0)
				{
					res.y = 0;
				}
				else
				{
					if (dir.x < 0)
						res.x = -res.x;

					res.y = (res.x * dir.y) / dir.x;
				}
			}
			else
			{
				res.y = inter.height;
				if (dir.y == 0)
				{
					res.x = 0;
				}
				else
				{
					if (dir.y < 0)
						res.y = -res.y;

					res.x = (res.y * dir.x) / dir.y;
				}
			}

			return res;
		}

		/// <summary>
		/// Converts a Rect to a Bounds object
		/// </summary>
		/// <param name="r">The rect to convert</param>
		/// <returns>The converted bound</returns>
		public static Bounds RectToBounds(Rect r)
		{
			return new Bounds(r.center, Vector3.up * r.height + Vector3.right * r.width);
		}

		/// <summary>
		/// Converts a Bounds object (3D) to a Rect object (2D)
		/// </summary>
		/// <param name="b">The given bounds object</param>
		/// <returns>The converted Rect object</returns>
		public static Rect BoundsToRect(Bounds b)
		{
			return CreateRect(b.min, b.max);
		}

		/// <summary>
		/// Compares two IArea2D
		/// </summary>
		/// <param name="a">The first area</param>
		/// <param name="b">The second area</param>
		/// <returns>A CompareTo value</returns>
		public static int CompareAreaYX(IArea2D a, IArea2D b)
		{
			Vector2 pA = GetRectPosition(a.GetArea());
			Vector2 pB = GetRectPosition(b.GetArea());

			int retval = pA.y.CompareTo(pB.y);
			if (retval == 0)
			{
				retval = pA.x.CompareTo(pB.x);
			}

			return retval;
		}

		/// <summary>
		/// An area comparer that uses CompareAreaYX
		/// </summary>
		private class AreaComparerYX : IComparer<IArea2D>
		{
			/// <summary>
			/// Compares two IArea2D
			/// </summary>
			/// <param name="a">The first area</param>
			/// <param name="b">The second area</param>
			/// <returns>A CompareTo value</returns>
			public int Compare(IArea2D a, IArea2D b)
			{
				return CompareAreaYX(a, b);
			}
		}

		/// <summary>
		/// Separates many IArea2D
		/// </summary>
		/// <param name="items">The different areas to separate</param>
		public static void Separate(IArea2D[] items)
		{
			System.Array.Sort(items, new AreaComparerYX());
			SeparateSorted(items);
		}

		/// <summary>
		/// Separates many IArea2D
		/// </summary>
		/// <param name="items">The different areas to separate</param>
		public static void Separate(List<IArea2D> items)
		{
			items.Sort(CompareAreaYX);
			SeparateSorted(items);
		}

		/// <summary>
		/// Separates many IArea2D
		/// </summary>
		/// <param name="sortedItems">The different sorted areas to separate</param>
		public static void SeparateSorted(IEnumerable<IArea2D> sortedItems)
		{
			Rect r;
			Rect p;
			Vector2 t;
			bool replaced;

			List<IArea2D> placedItems = new List<IArea2D>();

			foreach (var sortedItem in sortedItems)
			{
				r = sortedItem.GetArea();

				replaced = false;
				foreach (var placedItem in placedItems)
				{
					p = placedItem.GetArea();
					t = Separate2(r, p);
					if (t != Vector2.zero)
					{
						r = TranslateRect(r, t);
						replaced = true;
					}
				}

				if (replaced)
					sortedItem.SetArea(r);

				placedItems.Add(sortedItem);
			}
		}

		/// <summary>
		/// Seperates two rects. the rect must be moved at the top of fixedRect
		/// </summary>
		/// <param name="rect">The rect to move</param>
		/// <param name="fixedRect">The fixed rect</param>
		/// <returns>Returns the translation to apply to our rect</returns>
		public static Vector2 SeparateTop(Rect rect, Rect fixedRect)
		{
			Rect inter;
			if (!IntersectAreas(fixedRect, rect, out inter))
			{
				return Vector2.zero;
			}

			return new Vector2(0, fixedRect.yMax - rect.yMin);
		}

		/// <summary>
		/// Separates rect from fixedRect
		/// </summary>
		/// <param name="rect">The rect to move</param>
		/// <param name="fixedRect">The fixed rect</param>
		/// <returns>Returns the translation to apply to our rect</returns>
		public static Vector2 Separate2(Rect rect, Rect fixedRect)
		{
			Rect inter;
			if (!IntersectAreas(fixedRect, rect, out inter))
			{
				return Vector2.zero;
			}

			if (inter.width < inter.height)
			{
				if (rect.center.x < fixedRect.center.x)
				{
					return -(0.001f + rect.xMax - fixedRect.xMin) * Vector2.right;
				}

				return (0.001f + fixedRect.xMax - rect.xMin) * Vector2.right;
			}

			if (rect.center.y < fixedRect.center.y)
			{
				return -(0.001f + rect.yMax - fixedRect.yMin) * Vector2.up;
			}

			return (0.001f + fixedRect.yMax - rect.yMin) * Vector2.up;
		}
		
		/// <summary>
		/// Returns the rect at the intersection of the given two rects (areas)
		/// </summary>
		/// <param name="a">The first rect</param>
		/// <param name="b">The second rect</param>
		/// <param name="inter">The result</param>
		/// <returns>True if a and b intersects.</returns>
		public static bool IntersectAreas(Rect a, Rect b, out Rect inter)
		{
			float xMin = Mathf.Max(a.xMin, b.xMin);
			float xMax = Mathf.Min(a.xMax, b.xMax);
			float yMin = Mathf.Max(a.yMin, b.yMin);
			float yMax = Mathf.Min(a.yMax, b.yMax);

			if (xMin > xMax || yMin > yMax)
			{
				inter = default(Rect);
				return false;
			}

			inter = CreateRect(new Vector2(xMin, yMin), new Vector2(xMax, yMax));
			return true;
		}

		/// <summary>
		/// Returns the rect at the intersection of the given two rects (areas)
		/// </summary>
		/// <param name="a">The first rect</param>
		/// <param name="b">The second rect</param>
		/// <returns>True if a and b intersects.</returns>
		public static bool IntersectAreas(Rect a, Rect b)
		{
			float xMin = Mathf.Max(a.xMin, b.xMin);
			float xMax = Mathf.Min(a.xMax, b.xMax);
			float yMin = Mathf.Max(a.yMin, b.yMin);
			float yMax = Mathf.Min(a.yMax, b.yMax);

			return (xMin <= xMax && yMin <= yMax);
		}

		/// <summary>
		/// Returns the rect at the intersection of the given two rects (perimeter)
		/// <example>
		/// +-------------------+
		/// | +-----+		   |
		/// | |	 | <- child  | <- parent
		/// | +-----+		   |
		/// +-------------------+
		/// 
		/// In this example : 
		/// * IntersectRect returns false.
		/// * IntersectArea returns true (inter = child).
		/// </example>
		/// <remarks>
		/// If IntersectRect returns something, the result is the same as IntersectArea.
		/// IntersectRect merely verify if a is not contained in b (and b in a)
		/// </remarks>
		/// </summary>
		/// <param name="a">The first rect</param>
		/// <param name="b">The second rect</param>
		/// <param name="inter">The result</param>
		/// <returns>True if a and b intersects.</returns>
		public static bool IntersectRect(Rect a, Rect b, out Rect inter)
		{
			if (Contains(a, b))
			{
				inter = b;
				return false;
			}

			if (Contains(b, a))
			{
				inter = a;
				return false;
			}

			return IntersectAreas(a, b, out inter);
		}

		/// <summary>
		/// Returns true if the 'parent' Rect contains the 'child' Rect (without intersections)
		/// <example>
		/// +-------------------+
		/// | +-----+		   |
		/// | |	 | <- child  | <- parent
		/// | +-----+		   |
		/// +-------------------+
		/// </example>
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="child"></param>
		public static bool Contains(Rect parent, Rect child)
		{
			return parent.xMin < child.xMin && parent.xMax > child.xMax &&
					parent.yMin < child.yMin && parent.yMax > child.yMax;
		}

		/// <summary>
		/// Creates a new Rect with two points.
		/// the height and width will always be positive
		/// </summary>
		/// <param name="A">The first point</param>
		/// <param name="B">The second point</param>
		/// <returns>The rect between those two points</returns>
		public static Rect CreateRect(Vector2 A, Vector2 B)
		{
			Rect r = new Rect();

			r.xMin = Mathf.Min(A.x, B.x);
			r.xMax = Mathf.Max(A.x, B.x);
			r.yMin = Mathf.Min(A.y, B.y);
			r.yMax = Mathf.Max(A.y, B.y);

			return r;
		}

		/// <summary>
		/// Scales a vector according to a rect.
		/// (v must be in a rect 1x1. this function returns the vector if it were to be in the given rect)
		/// </summary>
		/// <param name="v">The vector to scale</param>
		/// <param name="r">The rect in which the vector must be</param>
		/// <returns>the scaled vector</returns>
		public static Vector2 Lerp(Vector2 v, Rect r)
		{
			v.x = (r.xMin + (v.x * r.width));
			v.y = (r.yMin + (v.y * r.height));

			return v;
		}

		/// <summary>
		/// Returns the vector that transforms baseRect to the position where it's basePos is at the same position
		/// as portRect's portPos
		/// </summary>
		/// <param name="baseRect">The rectangle to translate</param>
		/// <param name="basePos">The position within the rectangle to translate</param>
		/// <param name="portRect">The fixed rectangle</param>
		/// <param name="portPos">The position within the fixed rectangle where basePos must go</param>
		/// <returns>the translation vector to apply</returns>
		public static Vector2 Transpose(Rect baseRect, Vector2 basePos, Rect portRect, Vector2 portPos)
		{
			Vector2 lBasePos = Lerp(basePos, baseRect);
			Vector2 lPortPos = Lerp(portPos, portRect);

			return lPortPos - lBasePos;
		}

		/// <summary>
		/// Inverse Lerp(Vector2 v, Rect r)
		/// (Un)scales a vector according to a rect.
		/// 
		/// (v must be in the given rect. this function returns the vector if it were to be in a rect 1x1)
		/// </summary>
		/// <param name="v">The vector to (un)scale</param>
		/// <param name="r">The rect in which the vector is</param>
		/// <returns>the (un)scaled</returns>
		/// <see cref="Lerp(Vector2 v, Rect r)"/>
		public static Vector2 InverseLerp(Vector2 v, Rect r)
		{
			v.x = ((v.x - r.xMin) / r.width);
			v.y = ((v.y - r.yMin) / r.height);

			return v;
		}

		/// <summary>
		/// Scales a Rect proportionally, t times.
		/// Its center don't change
		/// </summary>
		/// <example>r: (1, 1, 2, 2) ; t: 4 => (-3, -3, 8, 8)</example>
		/// <param name="r">The rect to scale</param>
		/// <param name="t">The ratio for our new rect</param>
		/// <returns>The scaled rect</returns>
		public static Rect Lerp(Rect r, float t)
		{
			float width = r.width * t;
			float heigth = r.height * t;
			Vector2 center = r.center;

			Rect scaledRect = new Rect();
			scaledRect.width = width;
			scaledRect.height = heigth;
			scaledRect.center = center;

			return scaledRect;
		}

		/// <summary>
		/// Simply moves a rect.
		/// </summary>
		/// <param name="r">The rect to move</param>
		/// <param name="v">The vector that transforms our rect</param>
		/// <returns>The transformed rect</returns>
		public static Rect TranslateRect(Rect r, Vector2 v)
		{
			return CreateRect(new Vector2(r.xMin, r.yMin) + v, new Vector2(r.xMax, r.yMax) + v);
		}

		/// <summary>
		/// Returns a Rect position (not center, its bot-left corner.)
		/// </summary>
		/// <param name="r">The rect we want its position</param>
		/// <returns>The rect's position</returns>
		public static Vector2 GetRectPosition(Rect r)
		{
			return new Vector2(r.xMin, r.yMin);
		}

		/// <summary>
		/// Puts our toPlace rect within the bounds Rect.
		/// </summary>
		/// <param name="toPlace">the rect to place within bounds</param>
		/// <param name="bounds">the rect that limits where toPlace can be</param>
		/// <returns>toPlace or toPlace translated if it was not placed within bounds.</returns>
		/// <remarks>
		/// * If toPlace width is larger than bounds width, toPlace X center will be the same as bounds.
		/// * If toPlace height is taller that bounds height, toPlace Y center will be the same as bounds.
		/// * So, if toPlace contains bounds. toPlace center will be placed on the bounds center.
		/// </remarks>
		public static Rect Replace(Rect toPlace, Rect bounds)
		{
			Vector2 v;

			float w = toPlace.width;
			float h = toPlace.height;

			if (toPlace.width < bounds.width)
			{
				if (toPlace.xMin < bounds.xMin)
				{
					toPlace.xMin = bounds.xMin;
					toPlace.xMax = toPlace.xMin + w;
				}

				if (toPlace.xMax > bounds.xMax)
				{
					toPlace.xMax = bounds.xMax;
					toPlace.xMin = toPlace.xMax - w;
				}
			}
			else
			{
				v = toPlace.center;
				v.x = bounds.center.x;
				toPlace.center = v;
			}

			if (toPlace.height < bounds.height)
			{
				if (toPlace.yMin < bounds.yMin)
				{
					toPlace.yMin = bounds.yMin;
					toPlace.yMax = toPlace.yMin + h;
				}

				if (toPlace.yMax > bounds.yMax)
				{
					toPlace.yMax = bounds.yMax;
					toPlace.yMin = toPlace.yMax - h;
				}
			}
			else
			{
				v = toPlace.center;
				v.y = bounds.center.y;
				toPlace.center = v;
			}

			return toPlace;
		}

		/// <summary>
		/// Place the vector within the given bounds
		/// </summary>
		/// <param name="toPlace">The point to move</param>
		/// <param name="bounds">The area where the point should be</param>
		/// <returns>The point, within the area</returns>
		public static Vector2 Replace(Vector2 toPlace, Rect bounds)
		{
			if (toPlace.x < bounds.xMin)
			{
				toPlace.x = bounds.xMin;
			}

			if (toPlace.x > bounds.xMax)
			{
				toPlace.x = bounds.xMax;
			}

			if (toPlace.y < bounds.yMin)
			{
				toPlace.y = bounds.yMin;
			}

			if (toPlace.y > bounds.yMax)
			{
				toPlace.y = bounds.yMax;
			}

			return toPlace;
		}

		/// <summary>
		/// Place the vector within the given bounds
		/// </summary>
		/// <param name="toPlace">The point to move</param>
		/// <param name="bounds">The area where the point should be</param>
		/// <returns>The point, within the area</returns>
		public static Vector2 Replace(Vector3 toPlace, Rect bounds)
		{
			Vector2 toPlace2D = toPlace;
			toPlace2D = Replace(toPlace2D, bounds);
			toPlace.x = toPlace2D.x;
			toPlace.y = toPlace2D.y;

			return toPlace;
		}

		/// <summary>
		/// Replace within a plan
		/// </summary>
		/// <param name="p">Plan</param>
		/// <param name="worldPos">point to place</param>
		/// <param name="bounds">point's bounds</param>
		/// <returns>new position</returns>
		public static Vector3 ReplaceWithPlan(Plan p, Vector3 worldPos, Rect bounds)
		{
			Vector2 planPosition = p.FromWorld(worldPos);
			planPosition = Replace(planPosition, bounds);
			return p.ToWorld(planPosition);
		}

		/// <summary>
		/// Scales a rect with a pivot
		/// </summary>
		/// <param name="toScale">The rect to scale</param>
		/// <param name="scaleValue">The scale value</param>
		/// <param name="pivot">Where the pivot is</param>
		/// <returns>The new rect</returns>
		public static Rect ScalePivot(Rect toScale, float scaleValue, Vector2 pivot)
		{
			Vector2 center = toScale.center;
			Vector2 move = Math.ScalePivotMove(scaleValue, center - pivot);

			toScale = Lerp(toScale, scaleValue);
			toScale.center = center + move;

			return toScale;
		}

		/// <summary>
		/// Contains a rectangle that contains every given rect
		/// </summary>
		/// <param name="rects">The rectangles</param>
		/// <returns>A rectangle containing every given rectangle</returns>
		public static Rect GetContainer(params Rect[] rects)
		{
			if (rects == null || rects.Length == 0)
			{
				return default(Rect);
			}

			Rect container = rects[0];
			for (int i = 1; i < rects.Length; i++)
			{
				Rect toCompare = rects[i];
				container.xMin = Mathf.Min(toCompare.xMin, container.xMin);
				container.xMax = Mathf.Max(toCompare.xMax, container.xMax);
				container.yMin = Mathf.Min(toCompare.yMin, container.yMin);
				container.yMax = Mathf.Max(toCompare.yMax, container.yMax);
			}

			return container;
		}

		/// <summary>
		/// Contains a rectangle that contains every given rect
		/// </summary>
		/// <param name="rects">The rectangles</param>
		/// <returns>A rectangle containing every given rectangle</returns>
		public static Rect GetContainer(IEnumerable<Rect> rects)
		{
			Rect container = default(Rect);
			var e = rects.GetEnumerator();

			if (e.MoveNext())
			{
				container = e.Current;
				while (e.MoveNext())
				{
					Rect toCompare = e.Current;
					container.xMin = Mathf.Min(toCompare.xMin, container.xMin);
					container.xMax = Mathf.Max(toCompare.xMax, container.xMax);
					container.yMin = Mathf.Min(toCompare.yMin, container.yMin);
					container.yMax = Mathf.Max(toCompare.yMax, container.yMax);
				}
			}

			return container;
		}

		/// <summary>
		/// Adds the dimToAdd width/heigth to rect
		/// </summary>
		/// <param name="rect">The rect to enlarge</param>
		/// <param name="dimToAdd">Dimensions to add</param>
		/// <returns>New rect</returns>
		public static Rect AddDimension(Rect rect, Rect dimToAdd)
		{
			rect.width += dimToAdd.width;
			rect.height += dimToAdd.height;
			return rect;
		}
		
		/// <summary>
		/// Lerps two areas
		/// </summary>
		/// <param name="from">0-value rect</param>
		/// <param name="to">1-value rect</param>
		/// <param name="t">value</param>
		/// <returns>The t-value rect</returns>
		public static Rect Lerp(Rect from, Rect to, float t)
		{
			Rect result = default(Rect);

			result.xMin = Mathf.Lerp(from.xMin, to.xMin, t);
			result.yMin = Mathf.Lerp(from.yMin, to.yMin, t);
			result.xMax = Mathf.Lerp(from.xMax, to.xMax, t);
			result.yMax = Mathf.Lerp(from.yMax, to.yMax, t);

			return result;
		}
	}
}
