using UnityEngine;
using System.Collections;

namespace Utils
{
	/// <summary>
	/// Utils class related to text such as word wrapping
	/// </summary>
	public static class Text
	{
		/// <summary>
		/// Words delimiters
		/// </summary>
		public static char[] split = { ' ' };//{'\t',' ','	'};	

		/// <summary>
		/// Lines delimiters
		/// </summary>
		public static char[] returnC = { '\n' };

		/// <summary>
		/// Just like the name says : removes the spaces at the start and the end of the string
		/// </summary>
		/// <param name="text">The text to trim</param>
		/// <returns>the trimed text</returns>
		public static string removeSpacesStartAndEnd(string text)
		{
			string ret = text;
			char[] delimiters = { ' ', '\t', '\n', '\r', '\"' };
			ret = ret.TrimEnd(delimiters);
			ret = ret.TrimStart(delimiters);
			return ret;
		}

		/// <summary>
		/// Returns the length (render size) of the text
		/// </summary>
		/// <param name="textMesh">the text we want to know its object size</param>
		/// <returns>the text length</returns>
		public static float getLength(TextMesh textMesh)
		{
			return textMesh.GetComponent<Renderer>().bounds.size.x;
		}

		/// <summary>
		/// Returns the height (render size) of the text
		/// </summary>
		/// <param name="textMesh">the text we want to know its object size</param>
		/// <returns>The text Height </returns>
		public static float getHeight(TextMesh textMesh)
		{
			return textMesh.GetComponent<Renderer>().bounds.size.y / 2.61f;
		}

		/// <summary>
		/// Simply puts the text string within the textMesh and perform WordWrap
		/// using the etalon string.
		/// 
		/// A line must not be larger than the etalon string.
		/// </summary>
		/// <param name="textMesh">the textMesh in which we want the text to be</param>
		/// <param name="text">The text string we want to place in the textMesh</param>
		/// <param name="etalon">Contains a string used to compute the max line length</param>
		public static void SetText(TextMesh textMesh, string text, string etalon)
		{
			if (!textMesh.GetComponent<Renderer>())
			{
				throw new UnityException("I need a renderer");
			}

			bool rendEnabled = textMesh.GetComponent<Renderer>().enabled;
			textMesh.GetComponent<Renderer>().enabled = true;

			bool textEnabled = textMesh.gameObject.activeSelf;
			textMesh.gameObject.SetActive(true);

			textMesh.text = etalon;
			SetText(textMesh, text);

			textMesh.GetComponent<Renderer>().enabled = rendEnabled;
			textMesh.gameObject.SetActive(textEnabled);
		}

		/// <summary>
		/// Simply puts the text string within the textMesh and perform WordWrap
		/// using the previous textMesh text.
		/// 
		/// A line must not be larger than the etalon string.
		/// </summary>
		/// <param name="textMesh">the textMesh in which we want the text to be. Its previous string is used to compute the max line length</param>
		/// <param name="text">The text string we want to place in the textMesh</param>
		public static void SetText(TextMesh textMesh, string text)
		{
			float max = getLength(textMesh);
			SetText(textMesh, text, max);
		}

		/// <summary>
		/// Returns the length of the given text if it were to be in the given textMesh.
		/// </summary>
		/// <param name="textMesh">the textMesh that could receive the given string</param>
		/// <param name="etalon">the string we want to know the length</param>
		/// <returns>The size of the given string when stored in the given textMesh</returns>
		public static float getEtalonLength(TextMesh textMesh, string etalon)
		{
			string txt = textMesh.text;
			textMesh.text = etalon;
			float size = textMesh.GetComponent<Renderer>().bounds.size.x;
			textMesh.text = txt;

			return size;
		}

		/// <summary>
		/// Sets the textMesh text to the given string then return the size of the textMesh
		/// </summary>
		/// <param name="textMesh">the textMesh that will recieve our string</param>
		/// <param name="etalon">the string we want to know the size</param>
		/// <returns>the size of textMesh after the text setted</returns>
		public static float getEtalonLengthFast(TextMesh textMesh, string etalon)
		{
			textMesh.text = etalon;
			return textMesh.GetComponent<Renderer>().bounds.size.x;
		}
		
		/// <summary>
		/// Count how many times this character occurs in the given string
		/// </summary>
		/// <param name="txt">the string the character should be</param>
		/// <param name="c">the character we want to count</param>
		/// <returns>The count of the given character within the string</returns>
		public static int CountChar(string txt, char c)
		{
			char[] ary = txt.ToCharArray();
			int n = 0;

			for (int i = 0; i < ary.Length; i++)
			{
				if (ary[i] == c)
					n++;
			}

			return n;
		}

		/// <summary>
		/// Force the text to have a minimum line count.
		/// If the text doesn't have enough lines, Line feed will be added.
		/// </summary>
		/// <param name="textMesh">text that must have minLines lines</param>
		/// <param name="minLines">the minimum count of lines this textMesh must have</param>
		public static void SetMinLines(TextMesh textMesh, int minLines)
		{
			for (int i = CountChar(textMesh.text, '\n'); i <= minLines; i++)
			{
				textMesh.text += '\n';
			}
		}

		/// <summary>
		/// Simply perform WordWrap using the given length
		/// 
		/// A line must not be larger than lineLength
		/// </summary>
		/// <param name="textMesh">the textMesh to wrap</param>
		/// <param name="lineLength">the max line length</param>
		public static void SetText(TextMesh textMesh, float lineLength)
		{
			SetText(textMesh, textMesh.text, lineLength);
		}
		
		/// <summary>
		/// Simply puts the text string within the textMesh and perform WordWrap
		/// using the given length
		/// 
		/// A line must not be larger than  lineLength
		/// </summary>
		/// <param name="textMesh">the textMesh in which we want the text to be</param>
		/// <param name="text">The text string we want to place in the textMesh</param>
		/// <param name="lineLength">the max line length</param>
		public static void SetText(TextMesh textMesh, string text, float lineLength)
		{
			bool enabled = textMesh.GetComponent<Renderer>().enabled;
			bool activated = textMesh.gameObject.activeSelf;

			Transform parent = textMesh.transform.parent;
			textMesh.transform.parent = null;
			textMesh.gameObject.SetActive(true);
			textMesh.GetComponent<Renderer>().enabled = true;

			// Check : Chaine vide
			if (string.IsNullOrEmpty(text))
			{
				textMesh.text = "";
			}
			
			textMesh.text = "W";
			textMesh.text = "";

			string[] words;

			bool cutanywhere = false;
			if (cutanywhere)
			{
				char[] w = text.ToCharArray();
				int l = w.Length;

				words = new string[l];
				for (int i = 0; i < l; i++)
				{
					words[i] = w[i].ToString();
				}
			}
			else
			{
				words = text.Split(split, System.StringSplitOptions.RemoveEmptyEntries);
			}


			string result = "";						//< Texte final
			string currentLigneText = "";			//< Ligne courante
			float textSize = 0;						//< Taille (pixels) de la ligne courante

			// Pour chaque mots (qui peuvent contenir des entrées)
			for (int i = 0; i < words.Length; i++)
			{
				/* The word */
				string word = words[i];

				/* The first one */
				if (i == 0)
				{
					currentLigneText = word;
					result = word;

					text = text.Remove(0, word.Length);
				}

				/* The next */
				else
				{
					string delim = text[0].ToString();

					// On récupère les mots séparés par des entrées
					string[] parts = word.Split(returnC);

					// On gère le premier
					word = parts[0];

					// On récupère sa taille
					textMesh.text = currentLigneText + delim + word;
					textSize = getLength(textMesh);

					// Si la taille totale aurait dépassé la taille d'une ligne : Nouvelle ligne
					if (textSize > lineLength)
					{
						result += delim + "\n" + word;
						currentLigneText = word;
						//	height += ligneHeight;
					}

					// Sinon : on insère.
					else
					{
						result += (cutanywhere ? "" : delim) + word;
						currentLigneText += (cutanywhere ? "" : delim) + word;
					}

					// Pour toutes les entrées :
					for (int j = 1; j < parts.Length; j++)
					{
						word = parts[j];

						result += delim + "\n" + word;
						currentLigneText = word;
					}

					text = text.Remove(0, word.Length + 1);
				}

				textMesh.text = currentLigneText;
				textSize = getLength(textMesh);				
			}

			// On donne au texte du textMesh le resultat.
			textMesh.text = result;
			
			textMesh.gameObject.SetActive(activated);
			textMesh.GetComponent<Renderer>().enabled = enabled;
			textMesh.transform.parent = parent;
		}

		/// <summary>
		/// Performs a indentation foreach '-' in the textMesh.
		/// </summary>
		/// <param name="textMesh">textMesh to indent</param>
		public static void IndentationTiret(TextMesh textMesh)
		{
			textMesh.text = textMesh.text.Replace("-", "\t-");
		}
	
		/// <summary>
		/// Capitalize the given string
		/// <example>this very moment -> This very moment</example>
		/// </summary>
		/// <param name="txt">The string to Capitalize</param>
		/// <returns>The capitalized string</returns>
		public static string Capitalize(string txt) 
		{
			if(string.IsNullOrEmpty(txt))
				return txt;
			
			txt = txt.ToLower();
			string maj = txt[0].ToString().ToUpper();
			txt = maj + txt.Remove (0, 1);

			return txt;
		}

		/// <summary>
		/// Capitalize the given composed string
		/// <remarks>
		/// Capitalize first letter then every letter that follows a non-letter character
		/// </remarks>
		/// <example>
		/// this very moment -> This Very Moment
		/// a well-known person -> A Well-Known Person
		/// </example>
		/// </summary>
		/// <param name="txt">The string to Capitalize</param>
		/// <returns>The capitalized string</returns>
		public static string CapitalizeComposed(string txt)
		{
			txt = txt.ToLower();
			string res = "";
			bool mustCapitalize = true;
			foreach (char c in txt)
			{
				if (!char.IsLetter(c))
				{
					res += c;
					mustCapitalize = true;
				}
				else
				{
					if (mustCapitalize)
					{
						res += c.ToString().ToUpper();
						mustCapitalize = false;
					}
					else
					{
						res += c;
					}
				}
			}
			return res;
		}
	}
}