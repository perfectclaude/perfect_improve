﻿using UnityEngine;
using System.Collections;

/**
 * @brief The Unity3D Color class
 */
using UColor = UnityEngine.Color;

namespace Utils
{
	/// <summary>
	/// Color tools
	/// </summary>
	public static class UColor
	{
		/// <summary>
		/// Orange color
		/// </summary>
		public static readonly Color Orange = new Color(1, 0.5f, 0, 1);

		/// <summary>
		/// Creates a color with RGB(A) values between 0 and 255
		/// </summary>
		/// <param name="r">Red composant</param>
		/// <param name="g">Green composant</param>
		/// <param name="b">Blue composant</param>
		/// <param name="a">Alpha channel</param>
		/// <returns>The Color</returns>
		public static Color RGB(byte r, byte g, byte b, byte a = 255)
		{
			return new Color((float)r / 255, (float)g / 255, (float)b / 255, (float)a / 255);
		}
	}
}
