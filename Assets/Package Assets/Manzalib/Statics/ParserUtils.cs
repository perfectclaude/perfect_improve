using System.Text;

namespace Utils
{
	/// <summary>
	/// Utils class related to parsing and conversions
	/// </summary>
	public static class UParser
	{
		/// <summary>
		/// Converts a string to a bytes array using UTF8
		/// </summary>
		/// <param name="str">string to convert</param>
		/// <returns>bytes array converted</returns>
		public static byte[] StrToBytesUTF8(string str)
		{
			return Encoding.UTF8.GetBytes(str);
		}

		/// <summary>
		/// Converts a bytes array to a string using UTF8
		/// </summary>
		/// <param name="bytes">bytes array to convert</param>
		/// <returns>converted string</returns>
		public static string BytesToStrUTF8(byte[] bytes)
		{
			return new string(Encoding.UTF8.GetChars(bytes));
		}
	}
}