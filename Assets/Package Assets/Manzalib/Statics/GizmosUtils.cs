﻿/**
 * @file GizmosUtils.cs
 * @brief Contains tools to draw Gizmos
 */

using UnityEngine;
using System.Collections;

namespace Utils
{
	using Maths;

	/// <summary>
	/// Gizmos cool functions
	/// </summary>
	public static class UGizmos
	{
		/// <summary>
		/// Draw the bounds rect
		/// </summary>
		/// <param name="b">The bounds to show</param>
		/// <param name="c">The color to use</param>
		public static void DrawRect(Bounds b, Color c)
		{
			DrawRect(Area.BoundsToRect(b), c);
		}


		/// <summary>
		/// Draw a 2D Rect with Gizmos
		/// </summary>
		/// <param name="r">The Rect to draw</param>
		/// <param name="c">The color to use</param>
		public static void DrawRect(Rect r, Color c)
		{
			DrawQuad(Quad.FromRect(r), c);
		}

		/// <summary>
		/// Draw a vector that goes from r to (r + v)
		/// </summary>
		/// <param name="r">The rect</param>
		/// <param name="v">The vector</param>
		/// <param name="c">The color</param>
		public static void DrawRectVector(Rect r, Vector2 v, Color c)
		{
			Vector3 from = Math.Compose(Area.GetRectPosition(r), 0);
			Vector3 to = Math.Compose(v, 0);
			Color oldColor = Gizmos.color;

			Gizmos.color = c;
			Gizmos.DrawLine(from, to);
			Gizmos.color = oldColor;
		}

		/// <summary>
		/// Draw a vector that goes from r to (r + v)
		/// </summary>
		/// <param name="r">The rect</param>
		/// <param name="v">The vector</param>
		/// <param name="c">The color</param>
		public static void DrawRectVectorCenter(Rect r, Vector2 v, Color c)
		{
			Vector3 from = Math.Compose(r.center, 0);
			Vector3 to = Math.Compose(v, 0);
			Color oldColor = Gizmos.color;

			Gizmos.color = c;
			Gizmos.DrawLine(from, to);
			Gizmos.color = oldColor;
		}

		/// <summary>
		/// Draws a vector from the origin to v
		/// </summary>
		/// <param name="v">The position where the line will go</param>
		/// <param name="c">The color</param>
		public static void DrawVector(Vector2 v, Color c)
		{
			DrawVector(Vector2.zero, v, c);
		}

		/// <summary>
		/// Draws a vector
		/// </summary>
		/// <param name="from">Start point</param>
		/// <param name="to">End point</param>
		/// <param name="c">The color</param>
		public static void DrawVector(Vector2 from, Vector2 to, Color c)
		{
			Color oldColor = Gizmos.color;

			Gizmos.color = c;
			Gizmos.DrawLine(from, to);
			Gizmos.color = oldColor;
		}
		
		/// <summary>
		/// Draw a 2D Rect with Gizmos
		/// Within a plan defined by its origin and two vectors
		/// </summary>
		/// <param name="r">The rect</param>
		/// <param name="c">The color</param>
		/// <param name="p">The plan</param>
		public static void DrawRectInPlan(Plan p, Rect r, Color c)
		{
			DrawQuad(p.GetQuad(r), c);
		}

		/// <summary>
		/// Draws a quad
		/// </summary>
		/// <param name="q">quad to draw</param>
		/// <param name="c">color</param>
		public static void DrawQuad(Quad q, Color c)
		{
			Color oldColor = Gizmos.color;
			Gizmos.color = c;

			Gizmos.DrawLine(q.A, q.B);
			Gizmos.DrawLine(q.B, q.C);
			Gizmos.DrawLine(q.C, q.D);
			Gizmos.DrawLine(q.D, q.A);

			Gizmos.color = oldColor;
		}

		/// <summary>
		/// Draws a polygon on XOZ plan
		/// </summary>
		/// <param name="p">Polygon to draw</param>
		/// <param name="c">color</param>
		/// <param name="yValue">y offset</param>
		public static void DrawPolygonXOZ(Polygon p, Color c, float yValue = 0)
		{
			if (p.ArePointsDefined())
			{
				Color old = Gizmos.color;
				Gizmos.color = c;

				Vector3 p1, p2;

				for (int i = 1; i < p.Points.Length; i++)
				{
					p1 = p.Points[i - 1].position;
					p2 = p.Points[i].position;
					p1.y = yValue;
					p2.y = yValue;

					Gizmos.DrawLine(p1, p2);
				}

				p1 = p.Points[0].position;
				p2 = p.Points[p.Points.Length - 1].position;
				p1.y = yValue;
				p2.y = yValue;

				Gizmos.DrawLine(p1, p2);
				Gizmos.color = old;
			}
		}
	}
}