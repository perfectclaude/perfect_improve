﻿/**
 * @file CameraUtils.cs
 * @brief Tools for Camera Management
 */

using UnityEngine;

namespace Utils
{
	/// <summary>
	/// Camera tools
	/// </summary>
	public static class UCamera
	{
		/// <summary>
		/// Returns an orthographic camera field of view (a Rect)
		/// </summary>
		/// <param name="camera">The camera we want the field of view</param>
		/// <returns>The rect field of view or (0,0,0,0) if the camera is not OrthoGraphic</returns>
		public static Rect GetViewRect(Camera camera)
		{
			Rect camView = new Rect();

			if (camera.orthographic)
			{
				camView.height = 2 * camera.orthographicSize;
				camView.width = camView.height * camera.aspect;
				camView.center = camera.transform.position;
			}
			
			return camView;
		}

		/// <summary>
		/// Converts a position from a camera to another one.
		/// <example>
		/// We have two 'spaces' a normal world and an HUD space.
		/// We have units in normal world, and health bars in HUD space.
		/// Units has placeholders for health bar.
		/// 
		/// To know the health bars positions in the HUD space, we must convert
		/// the placeholder position from the world camera to the HUD camera
		/// 
		/// Vector3 HUDPosition = Utils.UCamera.ConvertPosition(this.tPlaceHolder.position, NormalCamera, HUDCamera);
		/// this.HealthBar.transform.position = HUDPosition;
		/// </example>
		/// </summary>
		/// <param name="inputPoint">Position to convert</param>
		/// <param name="inputCamera">Camera that sees the point at the given position</param>
		/// <param name="outputCamera">Camera we want the position from</param>
		/// <returns>The same screen point seen by outputCamera</returns>
		public static Vector3 ConvertPosition(Vector3 inputPoint, Camera inputCamera, Camera outputCamera)
		{
			Vector3 viewportPoint = WorldToNormalizedViewportPoint(inputCamera, inputPoint);
			Vector3 outputPoint = NormalizedViewportToWorldPoint(outputCamera, viewportPoint);
			return outputPoint;
		}
		
		/// <summary>
		/// Convert a point from the world to our custom viewport where x/y are the screen position and z, the depth within our camera range
		/// </summary>
		/// <param name="camera">The camera that sees the point</param>
		/// <param name="point">The world point to convert</param>
		/// <returns>The converted viewport point</returns>
		public static Vector3 WorldToNormalizedViewportPoint(Camera camera, Vector3 point)
		{
			point = camera.WorldToViewportPoint(point);

			if (camera.orthographic)
			{
				point.z = (2 * (point.z - camera.nearClipPlane) / (camera.farClipPlane - camera.nearClipPlane)) - 1f;
			}
			else
			{
				point.z = ((camera.farClipPlane + camera.nearClipPlane) / (camera.farClipPlane - camera.nearClipPlane))
				+ (1 / point.z) * (-2 * camera.farClipPlane * camera.nearClipPlane / (camera.farClipPlane - camera.nearClipPlane));
			}

			return point;
		}


		/// <summary>
		/// Convert a point from our custom viewport to the world
		/// <see cref="WorldToNormalizedViewportPoint"/>
		/// </summary>
		/// <param name="camera">The camera that wants to see the point</param>
		/// <param name="point">The viewport point to convert</param>
		/// <returns>The converted world point</returns>
		public static Vector3 NormalizedViewportToWorldPoint(Camera camera, Vector3 point)
		{
			if (camera.orthographic)
			{
				point.z = (point.z + 1f) * (camera.farClipPlane - camera.nearClipPlane) * 0.5f + camera.nearClipPlane;
			}
			else
			{
				point.z = ((-2 * camera.farClipPlane * camera.nearClipPlane) / (camera.farClipPlane - camera.nearClipPlane)) /
				(point.z - ((camera.farClipPlane + camera.nearClipPlane) / (camera.farClipPlane - camera.nearClipPlane)));
			}

			return camera.ViewportToWorldPoint(point);
		}

		/// <summary>
		/// Returns the world mouse position
		/// </summary>
		/// <param name="cam">The camera that sees the mouse</param>
		/// <param name="inputPos">The mouse position within the screen (x/y between 0 - 1)</param>
		/// <returns>the world mouse position</returns>
		public static Vector2 GetMousePosition(Camera cam, Vector2 inputPos)
		{
			Rect camView; // Camera field of view

			camView = GetViewRect(cam);
			inputPos = Area.Lerp(inputPos, camView);

			return inputPos;
		}

		/// <summary>
		/// Returns the world mouse position if the camera was to be centered at targetPos
		/// </summary>
		/// <param name="cam">The camera that sees the mouse</param>
		/// <param name="inputPos">The mouse position within the screen (x/y between 0 - 1)</param>
		/// <param name="targetPos">Where the camera sees the point</param>
		/// <returns>the world mouse position</returns>
		public static Vector2 GetMousePosition(Camera cam, Vector2 inputPos, Vector2 targetPos)
		{
			Rect camView; // Camera field of view
			
			camView = GetViewRect(cam);
			camView.center = targetPos;
			inputPos = Area.Lerp(inputPos, camView);
			
			return inputPos;
		}

		/// <summary>
		/// Returns true if the mouse is contained by our rect in the world
		/// </summary>
		/// <param name="cam">The camera that sees our mouse (and the rect)</param>
		/// <param name="inputPos">The mouse position within the screen (x/y between 0 - 1)</param>
		/// <param name="r">The rect (in the world)</param>
		/// <returns>True if the given rect contains the mouse in the world space</returns>
		public static bool IsMouseOn(Camera cam, Vector2 inputPos, Rect r)
		{
			Vector2 mouse = GetMousePosition(cam, inputPos);
			return r.Contains(mouse);
		}

		/// <summary>
		/// Returns a screen position (x/y between 0/1)
		/// </summary>
		/// <param name="cam">Screen camera</param>
		/// <param name="mousePosition">Mouse position</param>
		/// <returns>Normalized mouse position</returns>
		public static Vector3 GetInputPosition(Camera cam, Vector3 mousePosition)
		{
			Vector3 inputPosition;

			inputPosition.x = (mousePosition.x / cam.pixelWidth);
			inputPosition.y = (mousePosition.y / cam.pixelHeight);
			inputPosition.z = cam.nearClipPlane;

			return inputPosition;
		}
	}
}
