﻿using System;
using System.Collections.Generic;

namespace Utils
{	
	/// <summary>
	/// Some parser tools
	/// </summary>
	public static class Parser
	{
		/// <summary>
		/// CSV Int List separators.
		/// </summary>
		public static string SEP_LISTS = " ";


		/// <summary>
		/// First index number to give to unindexed headers
		/// </summary>
		public const int UNINDEXED_FIRST = 2;

		/// <summary>
		/// Get the parsed data as a Dictionary of columns
		/// </summary>
		/// <example>
		/// Title1 | Title2 | Title3
		///   0	|   1	|   2
		///   3	|   4	|   5
		///   
		/// -> result["Title1"][1] == 3
		/// </example>
		/// <param name="data">Parsed data</param>
		/// <returns>dictionary.</returns>
		public static IDictionary<string, string[]> GetColumns(IList<string>[] data)
		{
			IList<string> indexes = null;
			IDictionary<string, string[]> result = new SortedDictionary<string, string[]>();

			string lastIndex = string.Empty;
			int nRows = data.Length;
			if (nRows > 0)
			{
				int unindexed = UNINDEXED_FIRST;
				indexes = new List<string>();
				foreach (string index in data[0])
				{
					string i = index;
					if (string.IsNullOrEmpty(i))
					{
						i = lastIndex + unindexed;
						unindexed++;
					}
					else
					{
						lastIndex = index;
					}

					result.Add(i, new string[nRows - 1]);
					indexes.Add(i);
				}
			}

			for (int r = 1; r < nRows; r++)
			{
				int c = 0;
				foreach (string d in data[r])
				{
					result[indexes[c]][r - 1] = d;
					c++;
				}
			}

			return result;
		}

		/// <summary>
		/// Get the parsed data as a Dictionary of rows
		/// </summary>
		/// <example>
		/// Title1 |   1	|   2
		/// Title2 |   3	|   4
		/// Title3 |   5	|   6
		///   
		/// -> result["Title1"][1] == 2
		/// </example>
		/// <param name="data">Parsed data</param>
		/// <returns>dictionary.</returns>
		public static IDictionary<string, string[]> GetRows(IList<string>[] data)
		{
			IDictionary<string, string[]> result = new SortedDictionary<string, string[]>();

			int nRows = data.Length;
			for (int r = 0; r < nRows; r++)
			{
				int c = 0;
				string index = null;
				string[] array = new string[data[r].Count - 1];
				int unindexed = UNINDEXED_FIRST;
				string lastIndex = string.Empty;
				foreach (string d in data[r])
				{
					if (c == 0)
					{
						if (string.IsNullOrEmpty(d))
						{
							index = lastIndex + unindexed;
							unindexed++;
						}
						else
						{
							index = d;
							lastIndex = d;
						}
					}
					else
					{
						array[c - 1] = d;
					}
					c++;
				}

				result.Add(index, array);
			}

			return result;
		}

		/// <summary>
		/// Get a dictionary using a double index.
		/// </summary>
		/// <example>
		///	X	| Index 1 | Index 2 | Index 3
		/// Index A |	1	|	2	|	3  
		/// Index B |	4	|	5	|	6
		/// 
		/// -> result["Index 1"]["Index B"] --> 5
		/// </example>
		/// <param name="data">the parsed data</param>
		/// <returns>A dictionary using two keys</returns>
		public static IDictionary<string, IDictionary<string, string>> GetDouble(IList<string>[] data)
		{
			IList<string> indexesColumns = new List<string>();
			IDictionary<string, IDictionary<string, string>> result = new SortedDictionary<string, IDictionary<string, string>>();

			int nRows = data.Length;
			if (nRows > 0)
			{
				int unindexed = UNINDEXED_FIRST;
				indexesColumns = new List<string>();
				bool first = true;
				string lastIndex = string.Empty;
				foreach (string index in data[0])
				{
					if (first)
					{
						first = false;
					}
					else
					{
						string i = index;
						if (string.IsNullOrEmpty(i))
						{
							i = lastIndex + unindexed;
							unindexed++;
						}
						else
						{
							lastIndex = index;
							unindexed = UNINDEXED_FIRST;
						}

						result.Add(i, new Dictionary<string, string>());
						indexesColumns.Add(i);
					}
				}
			}

			for (int r = 1; r < nRows; r++)
			{
				int c = 0;
				string rowIndex = null;

				foreach (string d in data[r])
				{
					if (c == 0)
					{
						if (string.IsNullOrEmpty(d))
						{
							rowIndex = "Id" + r;
						}
						else
						{
							rowIndex = d;
						}
					}
					else if (!string.IsNullOrEmpty(d))
					{
						string colIndex = indexesColumns[c - 1];
						result[colIndex].Add(rowIndex, d);
						//Debug.Log("[" + colIndex + "][" + rowIndex + "] = " + d);
					}
					c++;
				}
			}

			return result;
		}

		/// <summary>
		/// Parse a CSV Float
		/// </summary>
		/// <param name="s">the csv float</param>
		/// <param name="defval">value to return if the string wasn't parsed (null or format error)</param>
		/// <returns>the float</returns>
		public static float ParseFloat(string s, int defval = 0)
		{
			if (string.IsNullOrEmpty(s))
				return defval;

			float res;
			if (float.TryParse(s.Replace(',', '.'), out res))
				return res;

			return defval;
		}

		// <summary>
		/// Parse a CSV int
		/// </summary>
		/// <param name="s">the csv int</param>
		/// <param name="defval">value to return if the string wasn't parsed (null or format error)</param>
		/// <returns>the int</returns>
		public static int ParseInt(string s, int defval = 0)
		{
			if (string.IsNullOrEmpty(s))
				return defval;

			int res;
			if (int.TryParse(s, out res))
				return res;

			return defval;
		}

		// <summary>
		/// Parse a CSV byte
		/// </summary>
		/// <param name="s">the csv byte</param>
		/// <param name="defval">value to return if the string wasn't parsed (null or format error)</param>
		/// <returns>the byte</returns>
		public static byte ParseByte(string s, byte defval = 0)
		{
			if (string.IsNullOrEmpty(s))
				return defval;

			byte res;
			if (byte.TryParse(s, out res))
				return res;

			return defval;
		}

		/// <summary>
		/// Parse a collection of string into an array of int
		/// </summary>
		/// <param name="s">the collection of strings</param>
		/// <returns>the array of int</returns>
		public static int[] ParseArray(ICollection<string> s)
		{
			List<int> result = new List<int>();
			foreach (string str in s)
			{
				if (!string.IsNullOrEmpty(str))
				{
					result.Add(Parser.ParseInt(str));
				}
			}

			return result.ToArray();
		}

		// <summary>
		/// Parse a CSV Bool.
		/// <remarks>
		/// Is false if : s is null, empty, "false", "faux", 0.
		/// Not case-sensitive.
		/// 
		/// Otherwise : true.
		/// </remarks> 
		/// </summary>
		/// <param name="s">the csv bool</param>
		/// <returns>the bool</returns>
		public static bool ParseBool(string s)
		{
			if (string.IsNullOrEmpty(s))
				return false;

			s = s.ToLower();
			if (s == "false" || s == "faux")
				return false;

			int integer;
			if (int.TryParse(s, out integer))
			{
				return (integer != 0);
			}

			return true;
		}

		/// <summary>
		/// Parse a CSV Enum
		/// </summary>
		/// <typeparam name="T">The enum</typeparam>
		/// <param name="s">the csv enum</param>
		/// <param name="defaultValue">If the value is not found</param>
		/// <returns>the enum value or default value on error</returns>
		public static T ParseEnum<T>(string s, T defaultValue) where T : struct, IConvertible
		{
			try
			{
				return ParseEnum<T>(s);
			}
			catch (ArgumentException)
			{
				return defaultValue;
			}
		}

		/// <summary>
		/// Parse a CSV Enum
		/// </summary>
		/// <typeparam name="T">The enum</typeparam>
		/// <param name="s">the csv enum</param>
		/// <returns>the enum value</returns>
		public static T ParseEnum<T>(string s) where T : struct, IConvertible
		{
			return (T)Enum.Parse(typeof(T), s, true);
		}

		// <summary>
		/// Parse a CSV Integer collection
		/// </summary>
		/// <param name="s">the csv int list</param>
		/// <returns>an array of int</returns>
		public static int[] ParseIntList(string s)
		{
			string[] values = s.Split(SEP_LISTS.ToCharArray());
			int[] list = new int[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				list[i] = ParseInt(values[i]);
			}
			return list;
		}
	}
}