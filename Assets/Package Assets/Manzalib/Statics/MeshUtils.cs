﻿using UnityEngine;
using System.Collections;

using M = UnityEngine.Mesh;

namespace Utils
{
	/// <summary>
	/// Contains tools for mesh management
	/// </summary>
	public class UMesh 
	{
		/// <summary>
		/// Creates a simple Quad Game Object
		/// </summary>
		/// <returns>a quad gameObject</returns>
		public static GameObject CreateQuadGO()
		{
			// V1 : Utiliser la primitive.
			GameObject g = GameObject.CreatePrimitive(PrimitiveType.Quad);
			GameObject.Destroy(g.GetComponent<Collider>());
			return g;
		}
	}
	
}
