﻿using UnityEngine;
using System.Collections.Generic;

namespace Utils
{
	/// <summary>
	/// Tools for gameObjects
	/// </summary>
	public static class Objects
	{
		/// <summary>
		/// Returns the size of a gameObject (with(out) children)
		/// </summary>
		/// <param name="g">gameObject</param>
		/// <returns>the size of the gameObject</returns>
		public static Rect GetSize(GameObject g)
		{
			var bounds = new List<Bounds>();
			GetBounds(g.transform, bounds);

			int nBounds = bounds.Count;
			if (nBounds == 0)
			{
				return default(Rect);
			}

			Vector2 min = bounds[0].min;
			Vector2 max = bounds[0].max;

			for (int i = 1; i < nBounds; i++)
			{
				Bounds b = bounds[i];
				min.x = Mathf.Min(min.x, b.min.x);
				min.y = Mathf.Min(min.y, b.min.y);
				max.x = Mathf.Max(max.x, b.max.x);
				max.y = Mathf.Max(max.y, b.max.y);
			}

			return Utils.Area.CreateRect(min, max);
		}

		/// <summary>
		/// Returns its bounds and its children bounds
		/// </summary>
		/// <param name="t">The transform element</param>
		/// <param name="bounds">Every children bounds</param>
		public static void GetBounds(Transform t, List<Bounds> bounds)
		{
			if (t.GetComponent<Renderer>())
			{
				bounds.Add(t.GetComponent<Renderer>().bounds);
			}

			foreach (Transform tChild in t.transform)
			{
				GetBounds(tChild, bounds);
			}
		}

		/// <summary>
		/// Returns a transform child from its name (recursively)
		/// </summary>
		/// <param name="parent">The transform parent (can't be returned)</param>
		/// <param name="name">The name of the transform to find</param>
		/// <returns>The found child or null</returns>
		public static Transform FindTransformChild(Transform parent, string name)
		{
			Transform inChild;
			foreach (Transform child in parent)
			{
				if (child.name == name)
				{
					return child;
				}

				inChild = FindTransformChild(child, name);
				if (inChild != null)
				{
					return inChild;
				}
			}

			return null;
		}
	}
}