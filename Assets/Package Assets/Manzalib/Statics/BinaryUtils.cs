using System;
using System.Collections.Generic;

namespace Utils
{
	namespace Network
	{
		public static class Binary
		{
			/// <summary>
			/// Converts a string to a bytes array using UTF8
			/// </summary>
			/// <param name="str">string to convert</param>
			/// <returns>bytes array converted</returns>
			public static byte[] StrToBytesUTF8(string str)
			{
				return System.Text.Encoding.UTF8.GetBytes(str);
			}

			/// <summary>
			/// Converts a bytes array to a string using UTF8
			/// </summary>
			/// <param name="bytes">bytes array to convert</param>
			/// <returns>converted string</returns>
			public static string BytesToStrUTF8(byte[] bytes)
			{
				return new string(System.Text.Encoding.UTF8.GetChars(bytes));
			}

			/// <summary>
			/// Joins a bytes arrays collection.
			/// </summary>
			/// <param name="toJoin">bytes arrays to join</param>
			/// <param name="count">bytes count</param>
			/// <returns>the joined bytes array</returns>
			public static byte[] Join(IEnumerable<byte[]> toJoin, int count)
			{
				byte[] result = new byte[count];

				int i = 0, j;
				foreach (byte[] a in toJoin)
				{
					for (j = 0; j < a.Length; j++)
					{
						result[i] = a[j];
						i++;
					}
				}

				return result;
			}

			/// <summary>
			/// Joins a bytes arrays collection.
			/// </summary>
			/// <param name="toJoin">bytes arrays to join</param>
			/// <returns>the joind bytes array</returns>
			public static byte[] Join(IEnumerable<byte[]> toJoin)
			{
				List<byte> result = new List<byte>();

				int i;
				foreach (byte[] a in toJoin)
				{
					for (i = 0; i < a.Length; i++)
					{
						result.Add(a[i]);
					}
				}

				return result.ToArray();
			}

			public static byte[] GetPackagePart(byte[] package, int address, int size)
			{
				byte[] part = new byte[size];

				for (int i = 0; i < size; i++)
				{
					part[i] = package[address + i];
				}

				return part;
			}

			public static void ReducePackage(ref byte[] package, int size)
			{
				if (package.Length == size)
				{
					package = null;
				}
				else
				{
					byte[] newPackage = new byte[package.Length - size];
					Array.Copy(package, size, newPackage, 0, newPackage.Length);
					package = newPackage;
				}
			}
			
			public static string PackageToString(byte[] package)
			{
				string str = "";

				foreach (byte b in package)
				{
					str += b.ToString("X2") + ' ';
				}

				return str;
			}

			public static byte[] InvertBytes(byte[] bytes)
			{
				byte[] result = new byte[bytes.Length];

				for (int i = 0; i < bytes.Length; i++)
				{
					result[i] = bytes[bytes.Length - i - 1];
				}

				return result;
			}

			public static byte[] FromNetworkLittleEndian(byte[] bytes)
			{
				if (BitConverter.IsLittleEndian)
				{
					return bytes;
				}

				return InvertBytes(bytes);
			}

			public static byte[] FromNetworkBigEndian(byte[] bytes)
			{
				if (!BitConverter.IsLittleEndian)
				{
					return bytes;
				}

				return InvertBytes(bytes);
			}

			public static byte[] ToNetworkLittleEndian(byte[] bytes)
			{
				if (BitConverter.IsLittleEndian)
				{
					return bytes;
				}

				return InvertBytes(bytes);
			}

			public static byte[] ToNetworkBigEndian(byte[] bytes)
			{
				if (!BitConverter.IsLittleEndian)
				{
					return bytes;
				}

				return InvertBytes(bytes);
			}
		}
	}
}