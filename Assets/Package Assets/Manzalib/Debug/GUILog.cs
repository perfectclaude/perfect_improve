﻿// #define TEST // Determine if a non GUILog version exists.

using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Collections;

/// <summary>
/// Log avec le GUI d'Unity
/// </summary>
public class GUILog : MonoBehaviour
{

	/// <summary>
	/// Message
	/// </summary>
	struct Message
	{
		/// <summary>
		/// Texte du message
		/// </summary>
		public readonly string Text;

		/// <summary>
		/// Couleur du message
		/// </summary>
		public readonly Color Color;

		/// <summary>
		/// Fabrique un message
		/// </summary>
		/// <param name="text">Son texte</param>
		/// <param name="color">Sa couleur</param>
		public Message(string text, Color color)
		{
			this.Text = text;
			this.Color = color;
		}
	}

	public static bool UseList = true;
	public bool useList = true;
	public int setCapacity = -1;
	public static int capacity = -1;
	public bool debugLogAtAdd = false;
	public bool getDebugLogMessages = false;
	public static bool DebugLogAtAdd = false;
	public bool FullScreen = true;
	public Rect PartialScreenRect;

	/// <summary>
	/// Liste de message du log
	/// </summary>
	static List<Message> log;
	static Message last;

	/// <summary>
	/// Fabrique notre liste de message
	/// </summary>
	static GUILog()
	{
		log = new List<Message>();
	}

	/// <summary>
	/// Ajoute un message (blanc)
	/// </summary>
	/// <param name="str">message</param>
	public static void Add(string str)
	{
		Add(str, Color.white);
	}

	/// <summary>
	/// Ajoute un message
	/// </summary>
	/// <param name="str">message</param>
	/// <param name="c">couleur du message</param>
	public static void Add(string str, Color c)
	{
		if (UseList)
		{
			while (capacity > 0 && capacity <= log.Count)
			{
				log.RemoveAt(0);
			}
		}

		last = new Message(str, c);

		if (UseList)
		{
			log.Add(last);
		}

		if (DebugLogAtAdd)
		{
			Debug.Log(str);
		}
	}

	/// <summary>
	/// Ajoute un message
	/// </summary>
	/// <param name="str">message</param>
	/// <param name="c">couleur du message</param>
	public static void EditLast(string str, Color c)
	{
		if (UseList)
		{
			if (log.Count > 0)
				log.RemoveAt(log.Count - 1);
		}

		Add(str, c);
	}

	/// <summary>
	/// Vide le log
	/// </summary>
	public static void Clear()
	{
		if (UseList)
		{
			log.Clear();
		}

		last = new Message("", Color.white);
	}

	// -----------------------------------------------------------------//

	// /// <summary>
	// /// Zone d'affichage du log
	// /// </summary>
	// private Rect rect;
	// 
	// /// <summary>
	// /// Si vrai : affiche le log dans toute la fenêtre
	// /// </summary>
	// public bool wholeScreen = true;

	/// <summary>
	/// Valeur de la scrollbar
	/// </summary>
	private Vector2 scrollPosition = Vector2.zero;

	public void Awake()
	{
		DebugLogAtAdd = debugLogAtAdd;
		capacity = setCapacity;
		UseList = useList;

		if (!useList)
			log = null;
	}

	public void OnEnable()
	{
		Application.RegisterLogCallback(HandleLog);
	}

	public void OnDisable()
	{
		Application.RegisterLogCallback(null);
	}

	private void HandleLog(string log, string stacktrace, LogType type)
	{
		if (getDebugLogMessages && !stacktrace.Contains("HandleLog"))
		{
			Color c;
			bool isImportant = false;
			switch (type)
			{
				case LogType.Assert:
				case LogType.Error:
				case LogType.Exception:
					c = Color.red;
					isImportant = true;
					break;

				case LogType.Log:
					c = Color.blue;
					break;

				case LogType.Warning:
					c = Color.yellow;
					break;

				default:
					c = Color.white;
					break;
			}

			Add("[DebugLog] " + log, c);
			//if (isImportant)
			{
				Add("[DebugLogStack] " + stacktrace, Color.magenta);
			}
		}
	}

	/// <summary>
	/// Affiche le log
	/// </summary>
	public void OnGUI()
	{
		if (!FullScreen)
		{
			GUI.BeginGroup(PartialScreenRect);
		}

#if UNITY_STANDALONE
		bool btnClicked;

		if (!FullScreen)
			btnClicked = GUILayout.Button("Print", GUILayout.Width(this.PartialScreenRect.width));
		else
			btnClicked = GUILayout.Button("Print");

		if (btnClicked)
		{
			PrintLog();
		}

		if (!FullScreen)
			btnClicked = GUILayout.Button("Clear", GUILayout.Width(this.PartialScreenRect.width));
		else
			btnClicked = GUILayout.Button("Clear");

		if (btnClicked)
		{
			Clear();
		}
#endif

		DebugLogAtAdd = debugLogAtAdd;
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);

		if (UseList)
		{
			foreach (Message msg in log)
			{
				LabelMessage(msg);
			}
		}
		else
		{
			LabelMessage(last);
		}

		GUILayout.EndScrollView();

		if (!FullScreen)
		{
			GUI.EndGroup();
		}
	}

	private void LabelMessage(Message msg)
	{
		Color old = GUI.color;
		GUI.color = msg.Color;

		if (!FullScreen)
			GUILayout.Label(msg.Text, GUILayout.Width(this.PartialScreenRect.width));
		else
			GUILayout.Label(msg.Text);

		GUI.color = old;
	}

	private void PrintLog()
	{
		FileStream f = new FileStream(
				Application.dataPath + "/GUILog.txt",
				FileMode.Create, FileAccess.Write);

		StreamWriter w = new StreamWriter(f);

		if (UseList)
		{
			foreach (Message m in log)
				w.WriteLine(m.Text);
		}
		else
		{
			w.WriteLine(last.Text);
		}

		f.Close();
	}
}
