﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class ExtraSettings
{
	private static bool definesToogled;

	[PreferenceItem("Extra settings")]
	public static void PreferenceGUI()
	{
		PlayerSettings.MTRendering = EditorGUILayout.Toggle("Multi thread rendering", PlayerSettings.MTRendering);
		
		BuildTargetGroup target = EditorUserBuildSettings.selectedBuildTargetGroup;
		CustomDefines(target, "VERBOSE", "DEBUG_MODE", "TEST", "USE_RANDOM");			
	}

	private static string Cap(string str)
	{		
		if (str == null)
			return null;		

		if (str.Length <= 1)
			return str.ToUpper();

		return char.ToUpper(str[0]) + str.Substring(1).ToLower();	
	}

	private static string DefineNameToHumanString(string str)
	{
		string[] parts = str.Split('_');
		for(int i = 0; i < parts.Length; i++)
		{
			parts[i] = Cap(parts[i]); 
		}
		return string.Join(" ", parts) + " mode";
	}


	private static void CustomDefines(BuildTargetGroup target, params string[] customDefines)
	{		
		definesToogled = EditorGUILayout.Foldout(definesToogled, " Defines for " + target.ToString());
		if (definesToogled)
		{
			EditorGUI.indentLevel+=2;
			
			Dictionary<string, bool> dicoCustomDefines = new Dictionary<string, bool>();
			foreach (string define in customDefines)
				dicoCustomDefines.Add(define, false);

			string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(target);
			string[] defines = definesString.Split(';');
			string result = null;

			foreach (string define in defines)
			{
				if (dicoCustomDefines.ContainsKey(define))
					dicoCustomDefines[define] = true;
				else if (result == null)
					result = define;
				else
					result += ';' + define;
			}

			foreach (string define in customDefines)
			{
				dicoCustomDefines[define] = EditorGUILayout.Toggle(DefineNameToHumanString(define), dicoCustomDefines[define]);
				if (dicoCustomDefines[define])
				{
					if (result == null)
						result = define;
					else
						result += ';' + define;
				}
			}

			PlayerSettings.SetScriptingDefineSymbolsForGroup(target, result);

			EditorGUILayout.HelpBox("These options are for the " + target + " target group only !", MessageType.Info);

			EditorGUI.indentLevel-=2;
		}
		
	}
}
