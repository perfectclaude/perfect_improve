﻿using UnityEngine;
using UnityEditor;


public static class RemoveHUDShadow {

	[MenuItem("Actions/Remove Shadow options recursively")]
	public static void RemoveShadows()
	{
		Transform[] tArray = Selection.transforms;
		foreach(Transform t in tArray)
		{
			RemoveShadows(t);
		}
	}

	private static void RemoveShadows(Transform t)
	{
		Renderer r = t.GetComponent<Renderer>();
		if (r)
		{
			if (r.castShadows || r.receiveShadows)
				Debug.Log("Edit " + r.name);

			r.castShadows = false;
			r.receiveShadows = false;
		}

		foreach (Transform tChild in t)
		{
			RemoveShadows(tChild);
		}
	}

}
