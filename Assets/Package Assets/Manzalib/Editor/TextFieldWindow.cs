using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class TextFieldWindow : EditorWindow
{
	private Vector2 scroll;
	
	[MenuItem("Actions/Show textfields window")]
	static void InitWindow()
	{
		TextFieldWindow window = (TextFieldWindow)EditorWindow.GetWindow(
		   typeof(TextFieldWindow),
		   false,
		   "TextFields",
		   true
	   );

		window.init();
	}

	protected void init()
	{
		this.autoRepaintOnSceneChange = true;
		this.scroll = Vector2.zero;
	}

	public void OnGUI()
	{
		const int ObjectTextFieldWidth = 250;
		const int ControlNameFieldWidth = 250;
		
		scroll = EditorGUILayout.BeginScrollView(scroll, false, false);

		Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GUITextField));
		GUITextField current = null;
		Color defColor = GUI.color;
		float prevA;

		List<string> UsedControlNames = new List<string>();
		List<string> ConflictingNames = new List<string>();
		foreach (Object o in objects)
		{
			current = o as GUITextField;
			if (current.gameObject.hideFlags != HideFlags.HideInHierarchy)
			{
				if (UsedControlNames.Contains(current.ControlName))
				{
					if (!ConflictingNames.Contains(current.ControlName))
						ConflictingNames.Add(current.ControlName);
				}
				else
				{
					UsedControlNames.Add(current.ControlName);
				}
			}
		}

		foreach (Object o in objects)
		{
			current = o as GUITextField;
			if(current.gameObject.hideFlags != HideFlags.HideInHierarchy)
			{
				EditorGUILayout.BeginHorizontal();

				GUI.enabled = false;
				prevA = defColor.a;				
				defColor.a = (current.gameObject.activeInHierarchy) ? 2 : 1;
				GUI.color = (current.enabled) ? new Color(0, 1, 0, defColor.a) : defColor;															
				EditorGUILayout.ObjectField(o, typeof(GUITextField), true, GUILayout.Width(ObjectTextFieldWidth));
				GUI.enabled = true;
				defColor.a = prevA;
				GUI.color = defColor;

				GUI.color = ConflictingNames.Contains(current.ControlName) ? new Color(1, 0, 0, 1) : defColor;
				EditorGUILayout.LabelField(current.ControlName, GUILayout.Width(ControlNameFieldWidth));
				GUI.color = defColor;			

				EditorGUILayout.EndHorizontal();
			}
		}

		EditorGUILayout.EndScrollView();
	}

	public void Update()
	{		
		this.Repaint();
	}
}
