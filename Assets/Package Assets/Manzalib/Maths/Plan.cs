﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	namespace Maths
	{
		/// <summary>
		/// A simple plan
		/// </summary>
		public struct Plan
		{
			/// <summary>
			/// Plan's origin
			/// </summary>
			public Vector3 origin;

			/// <summary>
			/// Plan's i vector
			/// </summary>
			public Vector3 right;

			/// <summary>
			/// Plan's j vector
			/// </summary>
			public Vector3 top;

			/// <summary>
			/// Creates a new plan
			/// </summary>
			/// <param name="origin">Origin</param>
			/// <param name="i">i vector (right)</param>
			/// <param name="j">j vector (top)</param>
			public Plan(Vector3 origin, Vector3 i, Vector3 j)
			{
				this.origin = origin;
				this.right = i;
				this.top = j;
			}

			/// <summary>
			/// Converts a plan position to a world position
			/// </summary>
			/// <param name="position">Plan position</param>
			/// <returns>World position</returns>
			public Vector3 ToWorld(Vector2 position)
			{
				return this.origin +
						this.right * position.x +
						this.top * position.y;
			}

			/// <summary>
			/// Converts a world position to a plan position
			/// </summary>
			/// <param name="worldPos">World position</param>
			/// <returns>Plan position</returns>
			public Vector2 FromWorld(Vector3 worldPos)
			{
				Vector3 relPosition = worldPos - this.origin;

				Vector3 rightProjection = Vector3.Project(relPosition, this.right);
				Vector3 topProjection = Vector3.Project(relPosition, this.top);

				Vector2 position;
				position.x = rightProjection.magnitude;
				position.y = topProjection.magnitude;

				if (rightProjection.normalized == -this.right.normalized)
					position.x *= -1;

				if (topProjection.normalized == -this.top.normalized)
					position.y *= -1;

				return position;
			}

			/// <summary>
			/// Converts a position from a plan to another one
			/// </summary>
			/// <param name="v">Source Plan position</param>
			/// <param name="from">Source Plan</param>
			/// <param name="to">Destination Plan</param>
			/// <returns>Destination Plan position</returns>
			public static Vector2 ToAnother(Vector2 v, Plan from, Plan to)
			{
				Vector3 world = from.ToWorld(v);
				return to.FromWorld(world);
			}

			/// <summary>
			/// Converts an plan area to a Quad
			/// </summary>
			/// <param name="r">Area</param>
			/// <returns>Quad</returns>
			public Quad GetQuad(Rect r)
			{
				Quad q = new Quad();

				q.A = this.origin + this.right * r.xMin + this.top * r.yMin; // botleft
				q.B = this.origin + this.right * r.xMax + this.top * r.yMin; // botright
				q.C = this.origin + this.right * r.xMax + this.top * r.yMax; // topright
				q.D = this.origin + this.right * r.xMin + this.top * r.yMax; // topleft

				return q;
			}

			/// <summary>
			/// ZOX Plan (top view)
			/// </summary>
			public readonly static Plan ZX;

			/// <summary>
			/// XOY Plan (back view)
			/// </summary>
			public readonly static Plan XY;

			/// <summary>
			/// ZOY Plan (right view)
			/// </summary>
			public readonly static Plan ZY;

			/// <summary>
			/// Creates some particuliar plans
			/// </summary>
			static Plan()
			{
				ZX = new Plan(Vector3.zero, Vector3.right, Vector3.forward);
				XY = new Plan(Vector3.zero, Vector3.right, Vector3.up);
				ZY = new Plan(Vector3.zero, Vector3.forward, Vector3.up);
			}
		}
	}
}