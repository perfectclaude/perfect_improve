﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	namespace Maths
	{
		[System.Serializable]
		public class Dimension
		{
			public float width;
			public float height;

			public Rect GetRect(Vector2 position)
			{
				Rect result = new Rect();
				result.x		= position.x;
				result.y		= position.y;
				result.width	= this.width;
				result.height	= this.height;
				return result;
			}

			public Rect GetCenteredRect(Vector2 position)
			{
				Rect result = new Rect();
				result.width = this.width;
				result.height = this.height;
				result.center = position;
				return result;
			}

			public Vector2 GetVector()
			{
				return new Vector2(width, height);
			}			
		}
	}
}