﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	namespace Maths
	{
		/// <summary>
		/// A simple polygon (using transforms)
		/// </summary>
		[System.Serializable]
		public class Polygon
		{
			/// <summary>
			/// Target points
			/// </summary>
			public Transform[] Points;

			/// <summary>
			/// True if :
			/// - 3 points are defined
			/// - No point is null.
			/// </summary>
			/// <returns>True if we can use this polygon</returns>
			public bool ArePointsDefined()
			{
				if (this.Points == null)
					return false;

				if (this.Points.Length < 3)
					return false;

				foreach (var p in this.Points)
				{
					if (p == null)
						return false;
				}

				return true;
			}

			/// <summary>
			/// Draws the polygon at gizmos time (XOZ plan)
			/// </summary>
			/// <param name="yValue">Y value</param>
			/// <param name="c">Color</param>
			public void OnGizmos(float yValue, Color c)
			{
				UGizmos.DrawPolygonXOZ(this, c, yValue);
			}
			/// <summary>
			/// True if the point lies within the polygon (Y component ignored)
			/// </summary>
			/// <param name="p">Point to check</param>
			/// <returns>
			/// True if the point is inside the polygon
			/// False on error
			/// </returns>
			public bool Contains(Vector3 p)
			{
				return Contains(p, false);
			}

			/// <summary>
			/// True if the point lies within the polygon (Y component ignored)
			/// </summary>
			/// <param name="p">Point to check</param>
			/// <param name="errValue">Value to return on error</param>
			/// <returns>True if the point is inside the polygon.</returns>
			public bool Contains(Vector3 p, bool errValue)
			{
				if (!this.ArePointsDefined())
					return errValue;

				return InsidePolygon(GetPolygon(this.Points), new Vector2(p.x, p.z));
			}

			/// <summary>
			/// Create the polygon with transform array
			/// </summary>
			/// <param name="targets">Targets to use</param>
			/// <returns>Real polygon</returns>
			static Vector2[] GetPolygon(Transform[] targets)
			{
				Vector2[] poly = new Vector2[targets.Length];

				for (int i = 0; i < targets.Length; i++)
				{
					poly[i] = new Vector2(
						targets[i].position.x,
						targets[i].position.z
						);
				}

				return poly;
			}

			/// <summary>
			/// Returns true if p is inside polygon
			/// </summary>
			/// <param name="polygon">Polygon points</param>
			/// <param name="p">Point to check</param>
			/// <returns>True if the point is inside the polygon</returns>
			static bool InsidePolygon(Vector2[] polygon, Vector2 p)
			{
				int i;
				float angle = 0;
				Vector2 p1, p2;
				int n = polygon.Length;

				for (i = 0; i < n; i++)
				{
					p1.x = polygon[i].x - p.x;
					p1.y = polygon[i].y - p.y;
					p2.x = polygon[(i + 1) % n].x - p.x;
					p2.y = polygon[(i + 1) % n].y - p.y;
					angle += Angle2D(p1, p2);
				}

				return (Mathf.Abs(angle) >= Mathf.PI);
			}

			/// <summary>
			/// 2 * PI
			/// </summary>
			static readonly float TWOPI = Mathf.PI * 2;

			/// <summary>
			/// Returns the angle AÔB
			/// </summary>
			/// <param name="A">first point</param>
			/// <param name="B">second point</param>
			/// <returns>Radius angle</returns>
			static float Angle2D(Vector2 A, Vector2 B)
			{
				float dtheta, theta1, theta2;

				theta1 = Mathf.Atan2(A.y, A.x);
				theta2 = Mathf.Atan2(B.y, B.x);
				dtheta = theta2 - theta1;
				while (dtheta > Mathf.PI)
					dtheta -= TWOPI;
				while (dtheta < -Mathf.PI)
					dtheta += TWOPI;

				return (dtheta);
			}

		}
	}
}