﻿using UnityEngine;

namespace Utils
{
	namespace Maths
	{
		/// <summary>
		/// A simple interval
		/// </summary>
		[System.Serializable]
		public class Interval
		{
			/// <summary>
			/// Minimum value
			/// </summary>
			public float min;

			/// <summary>
			/// Maximum value
			/// </summary>
			public float max;

			// public bool inversed;

			/// <summary>
			/// Creates an empty interval
			/// </summary>
			public Interval()
			{
				min = 0;
				max = 0;
			}

			/// <summary>
			/// Creates an interval
			/// </summary>
			/// <param name="a">Limit</param>
			/// <param name="b">Limit</param>
			public Interval(float a, float b)
			{
				this.min = Mathf.Min(a, b);
				this.max = Mathf.Max(a, b);
			}

			// public Interval(float a, float b, bool inversed)
			// {
			// 	this.min = Mathf.Min(a, b);
			// 	this.max = Mathf.Max(a, b);
			// 	this.inversed = inversed;
			// }

			/// <summary>
			/// Makes sure that min is the minimum and max the maximum
			/// </summary>
			public void Check()
			{
				if (min > max)
					this.Swap(ref min, ref max);
			}

			/// <summary>
			/// Util function.
			/// Swaps two float values
			/// </summary>
			/// <param name="a">value to swap</param>
			/// <param name="b">value to swap</param>
			private void Swap(ref float a, ref float b)
			{
				float tmp = a;
				a = b;
				b = tmp;
			}

			/// <summary>
			/// Clamps values between min and max
			/// </summary>
			/// <param name="value">value to clamp</param>
			/// <returns>clamped value</returns>
			public float Clamp(float value)
			{
				// if (inversed)
				// 	return Mathf.Clamp(value, max, min);

				return Mathf.Clamp(value, min, max);
			}

			/// <summary>
			/// Get percent value
			/// </summary>
			/// <param name="value">value to convert</param>
			/// <returns>value between 0 and 1</returns>
			public float Ratio(float value)
			{
				// if (inversed)
				// 	return Mathf.InverseLerp(max, min, value);

				return Mathf.InverseLerp(min, max, value);
			}

			/// <summary>
			/// Get value from percent
			/// </summary>
			/// <param name="ratio">percent</param>
			/// <returns>final value</returns>
			public float Lerp(float ratio)
			{
				// if (inversed)
				// 	return Mathf.Lerp(max, min, ratio);

				return Mathf.Lerp(min, max, ratio);
			}

			/// <summary>
			/// Formats this interval into a string
			/// </summary>
			/// <returns>string value</returns>
			public override string ToString()
			{
				return "[" + min + ";" + max + "]";
			}

			/// <summary>
			/// Returns the interval size
			/// </summary>
			/// <returns>interval size</returns>
			public float GetAmplitude()
			{
				return max - min;
			}
		}
	}
}