﻿using UnityEngine;
using System.Collections;

namespace Utils
{
	namespace Maths
	{
		/// <summary>
		/// A simple quad
		/// </summary>
		public struct Quad
		{
			/// <summary>
			/// Quad Point
			/// </summary>
			public Vector3 A;

			/// <summary>
			/// Quad Point
			/// </summary>
			public Vector3 B;

			/// <summary>
			/// Quad Point
			/// </summary>
			public Vector3 C;

			/// <summary>
			/// Quad Point
			/// </summary>
			public Vector3 D;

			/// <summary>
			/// Create a quad from a rect (XOY plan)
			/// </summary>
			/// <param name="r">Rect</param>
			/// <returns>Quad</returns>
			public static Quad FromRect(Rect r)
			{
				Quad q = new Quad();

				q.A = new Vector3(r.xMin, r.yMin, 0);
				q.B = new Vector3(r.xMax, r.yMin, 0);
				q.C = new Vector3(r.xMax, r.yMax, 0);
				q.D = new Vector3(r.xMin, r.yMax, 0);

				return q;
			}
		}
	}
}