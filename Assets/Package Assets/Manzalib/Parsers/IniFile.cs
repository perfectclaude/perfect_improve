﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class IniFile
{
	private Dictionary<string, Section> data;

	public static IniFile Load(string file)
	{
		return new IniFile(file);
	}

	public IniFile()
	{
		this.data = new Dictionary<string, Section>();
	}

	public IniFile(string file)
	{
		this._LoadFile(file);
	}

	private void _LoadFile(string path)
	{
		string txt;
		this.data = new Dictionary<string, Section>();

		using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
		{
			using (StreamReader r = new StreamReader(file))
			{
				txt = r.ReadToEnd();
			}
		}

		if (!string.IsNullOrEmpty(txt))
			this._LoadTxt(txt);
	}

	// récupéré de AICCData.cs :)
	private void _LoadTxt(string raw)
	{
#if VERBOSE
		GUILog.Add("Starts parsing", Color.Lerp(Color.red, Color.yellow, 0.5f));
#endif
		var classesToLower = new List<string>();

		string currentClass = null;
		string text = null;
		bool classNameOk;
		bool alreadyExists;
		string nameToLower;

		string[] lines = raw.Replace("\r", "").Split('\n');

		// Parsage des lignes
		foreach (string line in lines)
		{
			if (!string.IsNullOrEmpty(line))
			{
				string trimedLine = line.Trim();

				// Si la ligne n'est pas un commentaire
				if (trimedLine[0] != ';')
				{
					// Si la ligne indique la création d'une classe
					if (trimedLine[0] == '[')
					{
						// Si une classe de texte était en cours de création
						if (currentClass != null)
						{
							data.Add(currentClass, new Section(currentClass, text.Trim()));
						}

						// On parse le nom de la classe
						currentClass = trimedLine.Substring(1, trimedLine.Length - 2);
						nameToLower = currentClass.ToLower();

						// La classe existait déjà ? On l'ignore.
						classNameOk = Section.IsNameOk(currentClass);
						alreadyExists = classesToLower.Contains(nameToLower);
						if (!classNameOk || alreadyExists)
						{
#if VERBOSE
							GUILog.Add(currentClass + " ignored.", Color.red);
							if(alreadyExists)
								GUILog.Add("\tAlready setted", Color.red);
							if(!classNameOk)
								GUILog.Add("\tWrong name", Color.red);
#endif
							currentClass = null;
						}

						// On fabrique la structure de construction.
						else
						{
							classesToLower.Add(nameToLower);
						}

						// Initialisation des données de la nouvelle classe
						text = string.Empty;
					}

					// Si les données ne sont pas ignorées
					else if (currentClass != null)
					{
						// Concatene les données en mode texte
						text += line + '\n';
					}
				}
			}
		}

		// Si une classe de texte était en cours de création
		if (currentClass != null)
		{
			data.Add(currentClass, new Section(currentClass, text.Trim()));
		}
		
#if VERBOSE
		GUILog.Add("Parsing done", Color.Lerp(Color.red, Color.yellow, 0.5f));
#endif
	}

	public Section this[string index]
	{
		get
		{
			if(!data.ContainsKey(index))
				data.Add(index, new Section(index, ""));

			return data[index];			
		}

		set
		{
			data[index] = value;
		}
	}

	public class Section
	{
		public readonly string							name;
		public readonly string							text;
		public readonly Dictionary<string, Property>	properties;

		public Section(string name, string txt)
		{
			this.name = name;
			this.text = txt;

			this.properties = new Dictionary<string, Property>();

			string[] lines = txt.Split('\n');
			string[] pLineComments;
			string line;
			string[] parts;

			foreach (string fullline in lines)
			{
				pLineComments = fullline.Split(';');
				line = pLineComments[0].Trim();
				if (!string.IsNullOrEmpty(line))
				{
					if (!line.Contains("="))
					{
						this.properties = new Dictionary<string, Property>();
						break;
					}

					parts = line.Split('=');
					if (!this.properties.ContainsKey(parts[0]))
						this.properties.Add(parts[0], new Property(parts[0], parts[1]));
					else
						this.properties[parts[0]].value = parts[1];
				}
			}
		}

		public Property this[string index]
		{
			get
			{
				if (!properties.ContainsKey(index))
					properties.Add(index, new Property(index));
				
				return properties[index];
			}

			set
			{
				if (!properties.ContainsKey(index))
					properties.Add(index, new Property(index));

				properties[index] = value;
			}
		}
		
		public static bool IsNameOk(string name)
		{
			return !name.Contains(".");
		}

		public override string ToString()
		{
			string result = string.Empty;

			foreach (KeyValuePair<string, Property> entry in this.properties)
			{
				if(entry.Value.value != null)
					result += "\r\n\t" + entry.Key + "=\"" + entry.Value.ToString() + "\"";
			}

			return result;
		}
	}

	public class Property
	{
		public readonly string name;

		private string _value;
		public string value
		{
			get
			{
				return _value;
			}
			set
			{
				if (value == null)
					this._value = null;
				else
					this._value = value.Replace("\"", "");
			}
		}

		public Property(string name)
		{
			this.name = name;
		}

		public Property(string name, string value)
		{
			this.name = name;
			this.value = value;
		}

		public string ToPath(string def)
		{
			string result;
			if (value == null)
				result = def;
			else
				result = this.ToString();
			return result.Replace("$STREAMING_ASSETS$", Application.streamingAssetsPath);
		}

		public string ToPath()
		{
			if (value == null)
				throw new Exception("Property not found : " + this.name);

			return ToPath(null);
		}

		public override string ToString()
		{
			if (value == null)
				throw new Exception("Property not found : " + this.name);

			return this.value;
		}

		public float ToFloat()
		{
			if (value == null)
				throw new Exception("Property not found : " + this.name);

			return float.Parse(this.value.Replace(",", ".").Trim());
		}

		public double ToDouble()
		{
			if (value == null)
				throw new Exception("Property not found : " + this.name);

			return double.Parse(this.value.Replace(",", ".").Trim());
		}

		public int ToInt()
		{
			if (value == null)
				throw new Exception("Property not found : " + this.name);

			return int.Parse(this.value.Trim());
		}

		public string ToString(string def)
		{
			if (value == null)
				return def;

			return this.ToString();
		}

		public float ToFloat(float f)
		{
			if (value == null)
				return f;

			return this.ToFloat();
		}

		public double ToDouble(double d)
		{
			if (value == null)
				return d;

			return this.ToDouble();
		}

		public int ToInt(int i)
		{
			if (value == null)
				return i;
			return this.ToInt();
		}
	}

	public override string ToString()
	{
		string result = string.Empty;

		foreach (KeyValuePair<string, Section> entry in this.data)
		{
			result += '[' + entry.Key + "]";
			result += entry.Value.ToString();
			result += "\r\n";
		}

		return result;
	}

	public void Save(string path)
	{
		string toSave = this.ToString();

		this.data = new Dictionary<string, Section>();

		using (FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write))
		{
			using (StreamWriter w = new StreamWriter(file))
			{
				w.Write(toSave);
				w.Flush();
			}
		}
	}
}
