﻿using UnityEngine;
using System.Collections;
using Utils.Network;
using System;

/// <summary>
/// Classe gérant les 'télécommandes' comme l'iPad
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class RemoteController : MulticastManager 
{
	#region Workshop management

	public ScreenAdminAbstract[] Menus;

	private int GetIndexOf(ScreenAdminAbstract menu)
	{
		for (int i = 0; i < Menus.Length; i++)
		{
			if (Menus[i] == menu)
				return i;
		}

		return -1;
	}
			
	#endregion

	#region Events

	public event Action<DeviceManager>			OnDeviceManagerConnected;
	public event Action<DeviceManager>			OnDeviceManagerDisconnected;
	public event Action<bool>					OnRedButtonUpdated;
	public event Action<string, string>			OnSettingUpdated;	
	public event Action<ScreenAdminAbstract>	OnAdminMenuOpened;
	public event Action							OnAdminMenuClosed;

	#endregion

	#region Updates listener

	public void ClickElement(int elementIndex)
	{
		if (elementIndex != -1)
		{
			this.Send(RemoteMessages.MSG_CLICKED, elementIndex);
		}
	}

	public void CloseElement(int elementIndex)
	{
		if (elementIndex != -1)
		{
			this.Send(RemoteMessages.MSG_CLOSED, elementIndex);
		}
	}

	public void CloseMenu()
	{
		this.Send(RemoteMessages.MSG_ADMIN_MENU, -1);
	}

	public void ChangeMenu(ScreenAdminAbstract menu)
	{
		int index = GetIndexOf(menu);
		if (index != -1)
		{
			this.Send(RemoteMessages.MSG_ADMIN_MENU, index);
		}
	}

	public void ChangeSetting(string name, string value)
	{		
		this.Send(RemoteMessages.MSG_CHANGE_SETTING, "[" + name + "|" + value + "]");
	}

	public void UpdateRedButton(bool enabled)
	{
		this.Send(RemoteMessages.MSG_RED_BUTTON, (enabled ? "true" : "false"));
	}	
		
	#endregion

	#region Unity callbacks

	public void OnEnable()
	{
		if (this.JoinBroadcast(RemoteMessages.BROADCAST_PORT))
		{
			MessageBox.Set("En attente de connexion");
			this.StartCoroutine(this.ReceiveCoroutine());
		}
		else
		{
			MessageBox.Set("Impossible de se connecter\nau broadcast");
			Debug.Log("Can't join broadcast");
			this.enabled = false;
		}
	}

	public void OnDisable()
	{
		this.Stop();
		this.Disconnect();
	}
	
	#endregion

	#region Network management
	
	private string Before = "";

	private DeviceManager DataConnection;
	private SocketManager CommandConnection;

	private void Send(string query, object data)
	{
		this.Send(query + '=' + data.ToString());
	}

	private void Send(string query)
	{
		if (this.CommandConnection && this.CommandConnection.IsConnected)
		{
#if VERBOSE
			GUILog.Add("Send : " + query, Color.blue);
#endif
			this.CommandConnection.Send_UTF8_BE(query + "\n");
		}
	}

	private void Disconnect()
	{
		if (this.CommandConnection != null)
		{
			this.Send(RemoteMessages.MSG_DISCONNECTION);
		}

		if (this.CommandConnection != null)
		{
			this.CommandConnection.OnDisconnection -= OnDisconnection; // Evite de se rappeler :)
			if (this.CommandConnection.IsConnected)
				this.CommandConnection.Disconnect();
			
			GameObject.Destroy(this.CommandConnection);
			this.CommandConnection = null;
		}

		if (this.DataConnection != null)
		{
			if (this.DataConnection.IsConnected)
			{
				this.DataConnection.Disconnect();
			}
			
			if(this.OnDeviceManagerDisconnected != null)
				this.OnDeviceManagerDisconnected(this.DataConnection);

			GameObject.Destroy(this.DataConnection);

			this.DataConnection = null;
		}
		MessageBox.Set("Déconnecté\nEn attente d'une connexion");
	}

	private bool ParseConnectionString(string str, out int cmdPort, out int dataPort, out int timeout)
	{
		cmdPort = -1;
		dataPort = -1;
		timeout = -1;

		str = str.Substring(str.IndexOf(RemoteMessages.MSG_WAITING_CONTROLLER));
		if(str.Contains("\n"))
		{
			str = str.Remove(str.IndexOf('\n'));
			str = str.Replace(RemoteMessages.MSG_WAITING_CONTROLLER, "");
							
			string [] parameters = str.Split(' ');
			
			string [] parts;
			string command, variable;
			
			int tmp;

			foreach(string parameter in parameters)
			{
				if(parameter.Contains("="))
				{
					parts = parameter.Split('=');
					command = parts[0].Trim();
					variable = parts[1].Trim();
				}
				else
				{
					command = parameter.Trim();
					variable = null;
				}

				switch(command)
				{
					case "cmd":
						if(int.TryParse(variable, out tmp))						
							cmdPort = tmp;						
						break;
					case "data":
						if(int.TryParse(variable, out tmp))						
							dataPort = tmp;						
						break;
					case "timeout":
						if(int.TryParse(variable, out tmp))
							timeout = tmp;
						break;
				}
			}				
		}

		return (dataPort != -1 && cmdPort != -1);
	}

	protected override void OnReceived(byte[] data, System.Net.IPEndPoint from)
	{
		string str = Binary.BytesToStrUTF8(Binary.FromNetworkBigEndian(data));
		if (str.Contains(RemoteMessages.MSG_WAITING_CONTROLLER))
		{
			int cmdPort, dataPort, timeout;
			if (this.ParseConnectionString(str, out cmdPort, out dataPort, out timeout))
			{
				this.Stop();
				StartCoroutine(ConnectToServer(from, cmdPort, dataPort, timeout));
			}
		}
	}

	private IEnumerator ConnectToServer(System.Net.IPEndPoint server, int cmdPort, int dataPort, int timeout)
	{
		MessageBox.Set("Connexion au serveur :\n" + server.Address.ToString() + " sur le port " + cmdPort);

		string serverName = server.Address.ToString() + " : " + server.Port.ToString();

		this.CommandConnection = this.gameObject.AddComponent<SocketManager>();
		this.CommandConnection.Name = "Command " + serverName;
		this.CommandConnection.OnDisconnection += OnDisconnection;
		yield return this.CommandConnection.StartCoroutine(this.CommandConnection.ConnectCoroutine(server.Address.ToString(), true, cmdPort, 0.25f, 1));

		if (!this.CommandConnection.IsConnected)
		{
			MessageBox.Set("Connexion impossible");
			this.Disconnect();
			yield break;
		}

		this.CommandConnection.StartCoroutine(this.IdleManagement());
		this.CommandConnection.StartCoroutine(this.CommandConnection.ReceiveCoroutine(OnAnswerReceived));

		MessageBox.Set("Connexion au serveur :\n" + server.Address.ToString() + " sur le port " + dataPort);

		this.DataConnection = this.gameObject.AddComponent<DeviceManager>();
		this.DataConnection.ConnectionTimeOut = timeout;
		this.DataConnection.Name = "Data " + serverName;
		this.DataConnection.OnDisconnection += OnDisconnection;
		this.DataConnection.OnTimeOut += OnDisconnection;
		yield return this.DataConnection.StartCoroutine(this.DataConnection.ConnectCoroutine(server.Address.ToString(), true, dataPort, 0.25f, 1));

		if (!this.DataConnection.IsConnected)
		{
			MessageBox.Set("Connexion impossible");
			this.Disconnect();
			yield break;
		}

		MessageBox.Set();

		if (this.OnDeviceManagerConnected != null)
		{
			this.OnDeviceManagerConnected(this.DataConnection);
		}
	}

	private IEnumerator IdleManagement()
	{
		while (this.CommandConnection && this.CommandConnection.IsConnected)
		{
			this.Send(RemoteMessages.MSG_IDLE);
			yield return new WaitForSeconds(15);
		}
	}

	private void OnAnswerReceived(byte [] package)
	{
		string command, variable, name, value;
		bool redButton;
		int integer;

		string data = Before + Binary.BytesToStrUTF8(Binary.FromNetworkBigEndian(package));
		int enter = data.IndexOf('\n');
		while (enter != -1)
		{			
			Before = data.Substring(enter + 1);
			data = data.Remove(enter);
			
#if VERBOSE
			GUILog.Add (data, Color.blue);
#endif
			if (data.Contains("="))
			{
				string[] parts = data.Split('=');
				command = parts[0].Trim();
				variable = parts[1].Trim();
			}
			else
			{
				command = data.Trim();
				variable = null;
			}

			switch (command)
			{
				// Not managed :
				//
				// - MSG_WAITING_CONTROLLER
				// - MSG_DISCONNECTION
				// - MSG_IDLE
				// - MSG_CLICKED
				// - MSG_CLOSED
				// - MSG_CONNECTED
				// 

				case RemoteMessages.MSG_KICKED:
					this.Disconnect();
					break;

				case RemoteMessages.MSG_CHANGE_SETTING:

					variable = variable.Replace("[", "").Replace("]", "");

					integer = variable.IndexOf("|");
					if (integer != -1)
					{
						name = variable.Remove(integer);
						value = variable.Substring(integer + 1);

						if (this.OnSettingUpdated != null)
							this.OnSettingUpdated(name, value);
					}

					break;
										
				case RemoteMessages.MSG_RED_BUTTON:

					redButton = (variable == "true");
					if (redButton || variable == "false")
					{
						if (this.OnRedButtonUpdated != null)
							this.OnRedButtonUpdated(redButton);
					}

					break;

				case RemoteMessages.MSG_ADMIN_MENU:
					if (int.TryParse(variable, out integer))
					{
						if (integer == -1)
						{
							if (this.OnAdminMenuClosed != null)
							{
								this.OnAdminMenuClosed();
							}
						}
						else
						{
							if (this.OnAdminMenuOpened != null)
							{
								this.OnAdminMenuOpened(this.Menus[integer]);
							}
						}
					}
					break;

				default:

					break;
			}

			data = Before;
			enter = data.IndexOf('\n');
		}
		
		if (this.CommandConnection)
			this.CommandConnection.StartCoroutine(this.CommandConnection.ReceiveCoroutine(OnAnswerReceived));
	}

	private void OnDisconnection(SocketManager c)
	{
		this.Disconnect();
		if (this.JoinBroadcast(RemoteMessages.BROADCAST_PORT))
		{
			this.StartCoroutine(this.ReceiveCoroutine());
		}
		else
		{
			MessageBox.Set("Impossible de se connecter\nau broadcast");
			Debug.Log("Can't join broadcast");
			this.enabled = false;
		}
	}

	#endregion
}
