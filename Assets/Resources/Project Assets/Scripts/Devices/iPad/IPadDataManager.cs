﻿
using UnityEngine;
using System.Collections;

/// <summary>
/// Classe principale de l'application iPad
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class IPadDataManager : MonoBehaviour
{
	#region Variables

	private		RemoteController		remoteController;
	public		ScreensManager 			screensManager;
	private		ScreenAtelier			hudAtelier;
	private		SwitchBehavior			RedButton;
	private		GameObject				Pictos;
	private		ButtonBehavior			AdminMenuButton;
	private		InGameSettings			ServerSettings;
	private		ScreenAdminMenu			AdminMenuScreen;
	public		WorkshopElement[]		Workshops;

	#endregion

	#region Initialisation

	protected static T Get<T>() where T : Component
	{
		return GameObject.FindObjectOfType(typeof(T)) as T;
	}

	protected static void Get<T>(out T comp) where T : Component
	{
		comp = Get<T>();
	}

	protected static T Get<T>(string name) where T : Component
	{
		GameObject g = GameObject.Find(name);
		if (g == null)
			return null;
		return g.GetComponent<T>();
	}

	private void Awake()
	{
		this.ServerSettings = new InGameSettings();

		ScreenAdminAbstract.OnAdminChangeScreen	+= OnLocalAdminScreenChanged;
		ScreenAdminAbstract.OnAdminScreenClosed		+= OnLocalAdminMenuClosed;
		ScreenAdminAbstract.OnAdminSettingChanged	+= OnLocalSettingChanged;
		ScreenAdminAbstract.GetSettings				= GetSettings;

		Get(out remoteController);
		if (remoteController)
		{
			remoteController.OnAdminMenuOpened				+= OnServerAdminMenuOpened;
			remoteController.OnAdminMenuClosed				+= OnServerAdminMenuClosed;
			remoteController.OnSettingUpdated				+= OnServerSettingsChanged;
			remoteController.OnRedButtonUpdated				+= OnServerRedButtonUpdated;
			remoteController.OnDeviceManagerConnected		+= OnServerConnected;
			remoteController.OnDeviceManagerDisconnected	+= OnServerDisconnected;
		}

		AdminMenuButton = Get<ButtonBehavior>("AdminButton");
		if (this.AdminMenuButton)
		{
			this.AdminMenuButton.gameObject.SetActive(false);
			this.AdminMenuButton.OnClick += OnLocalOpenAdmin;
		}

		
		if(screensManager)
		{
			this.screensManager.GetS(out hudAtelier);
			this.screensManager.GetS(out AdminMenuScreen);
			
			if(hudAtelier)
			{
				Pictos = hudAtelier.PictosRoot;
				RedButton = hudAtelier.RedButton;
				
				if (RedButton)
				{
					RedButton.gameObject.SetActive(false);
					RedButton.OnSwitch += OnLocalRedButtonClicked;
				}
				
				screensManager.Current = hudAtelier;
			}
		}
		

		foreach (WorkshopElement workshop in this.Workshops)
		{
			workshop.OnMenuClosed	+= OnLocalWorkshopClosed;
			workshop.OnPictoClicked += OnLocalWorkshopClicked;
		}

	}

	#endregion

	#region Local event handlers

	private void OnLocalOpenAdmin(ButtonBehavior obj)
	{
		this.OpenAdmin();
		this.remoteController.ChangeMenu(AdminMenuScreen);
	}

	private void OnLocalAdminScreenChanged(ScreenAdminAbstract screen)
	{
		this.remoteController.ChangeMenu(screen);
		ScreenAdminAbstract.IsReadOnly = false;
	}

	private void OnLocalAdminMenuClosed()
	{		
		this.CloseAdmin();
		this.remoteController.CloseMenu();
	}

	private void OnLocalSettingChanged(string name, string value)
	{
		this.ServerSettings.ChangeSetting(name, value);
		this.remoteController.ChangeSetting(name, value);
		
	}

	private void OnLocalRedButtonClicked(SwitchBehavior btn, bool state)
	{
		this.remoteController.UpdateRedButton(state);
	}

	private void OnLocalWorkshopClicked(WorkshopElement workshop)
	{
		this.remoteController.ClickElement(GetIndexOf(workshop));
		this.OpenWorkshop(workshop);
	}

	private void OnLocalWorkshopClosed(WorkshopElement workshop)
	{
		this.remoteController.CloseElement(GetIndexOf(workshop));
		this.CloseWorkshop(workshop);
	}



	#endregion

	#region Server event handlers

	private void OnServerAdminMenuClosed()
	{
		this.CloseAdmin();
	}

	private void OnServerAdminMenuOpened(ScreenAdminAbstract screen)
	{
		ScreenAdminAbstract.IsReadOnly = (screen.ReadOnlyGameObject);
		this.SwitchAdminScreen(screen);
	}

	private void OnServerSettingsChanged(string name, string value)
	{
		// iPad : Si on recoit la notification de changement de parametre ou de mot de passe, changer le parametre
		this.ServerSettings.ChangeSetting(name, value);
				
		if (screensManager.Current is ScreenAdminAbstract)
		{
			ScreenAdminAbstract adminScreen = screensManager.Current as ScreenAdminAbstract;
			adminScreen.DistantChangeSetting(name, value);
		}
	}
	
	private void OnServerDisconnected(DeviceManager manager)
	{
		if(RedButton)
			RedButton.gameObject.SetActive(false);

		if(Pictos)
			Pictos.SetActive(false);

		if(AdminMenuButton)
			AdminMenuButton.gameObject.SetActive(false);

		this.CloseAdmin();

		if(screensManager)
			screensManager.Current = this.hudAtelier;

		if(ServerSettings != null)
			ServerSettings.Reset();
	}

	private void OnServerConnected(DeviceManager manager)
	{
		RedButton.gameObject.SetActive(true);
		manager.OnDataReceived += OnServerDataReceived;		
		this.AdminMenuButton.gameObject.SetActive(true);
		
	}

	private void OnServerRedButtonUpdated(bool state)
	{
		if (!state)
			MessageBox.Set("Production non planifiée");
		else
			MessageBox.Set();

		Pictos.SetActive(state);
		RedButton.SetState(state);
	}

	private void OnServerDataReceived()
	{
		// Do.. stuff..

		if (this.RedButton.StateSwitch)
		{
			if (screensManager)
			{
				screensManager.UpdateAllScreens();
			}
		}
		else
		{
			MessageBox.Set("Production non planifiée");
		}

	}
	
	#endregion

	#region Global event handlers

	private void OpenAdmin()
	{
		if (!manage_Ecrans.AdminMode)
		{
			SwitchAdminScreen(AdminMenuScreen);
		}
	}
	
	private void SwitchAdminScreen(ScreenAdminAbstract menu)
	{		
		MessageBox.Hide();
		manage_Ecrans.AdminMode = true;
		screensManager.Current = menu;		
	}

	private void CloseAdmin()
	{
		if (manage_Ecrans.AdminMode)
		{
			MessageBox.Show();
			manage_Ecrans.AdminMode = false;
			screensManager.Current = hudAtelier;
		}
	}

	private void OpenWorkshop(WorkshopElement workshop)
	{
#if VERBOSE
		GUILog.Add("OnClicked : " + this.name, Color.black);
#endif

		this.screensManager.Current = workshop.AssociatedScreen;
	}

	private void CloseWorkshop(WorkshopElement workshop)
	{
#if VERBOSE
		GUILog.Add("OnClosed : " + this.name, Color.black);
#endif
		if (this.screensManager.Current == workshop.AssociatedScreen)
		{
			this.screensManager.Current = workshop.ReturnScreen;
		}
	}

	public void CloseAllWorkshops()
	{
		foreach (WorkshopElement e in this.Workshops)
			CloseWorkshop(e);
	}

	#endregion


	private int GetIndexOf(WorkshopElement element)
	{
		for (int i = 0; i < Workshops.Length; i++)
		{
			if (Workshops[i] == element)
				return i;
		}

		return -1;
	}

	private InGameSettings GetSettings()
	{
		return this.ServerSettings;
	}
}
