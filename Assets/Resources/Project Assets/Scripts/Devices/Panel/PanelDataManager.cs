﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Classe principale de l'application Panel
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class PanelDataManager : MonoBehaviour 
{
	private string FilePath;

	private DeviceManager connection;
	private ScreensManager screensManager;
	private ParsedParametersPanel parameters;

	public int ConnectionTimeout;
	private float DataTimeout;
	private bool HasReceivedData;

	void Init()
	{
		this.FilePath = Application.streamingAssetsPath;

		this.screensManager = this.GetComponent<ScreensManager>();

		this.connection = this.gameObject.AddComponent<DeviceManager>();
		connection.OnDataReceived += OnDataReceived;
		connection.OnDisconnection += OnConnectionLost;
	}

	void OnEnable()
	{
		this.Init();
		this.ParseParameters();
		this.Configure();
		this.Connect();
	}

	void Clean()
	{
		if (this.connection)
		{
			GameObject.Destroy(this.connection);
			this.connection = null;
		}
	}

	void OnDisable()
	{
		this.Disconnect();
		this.StopAllCoroutines();
		this.Clean();
	}

	void ParseParameters()
	{
		this.parameters = new ParsedParametersPanel(FilePath + "/Parameters_panel.ini");
	}

	void Configure()
	{
		this.connection.hostIP		= this.parameters.ipServer;
		this.connection.hostPort	= this.parameters.portServer;
		this.DataTimeout			= this.parameters.dataTimeout;
	}
	
	void Connect()
	{
		StartCoroutine(ConnectionCoroutine());
	}

	void Disconnect()
	{
		if (connection && connection.IsConnected)
		{
			connection.Disconnect();
		}
		
		MessageBox.Set ("Déconnecté");
	}

	void OnDataReceived()
	{
		MessageBox.Set();
		this.screensManager.UpdateAllScreens();
		this.HasReceivedData = true;
	}

	void OnConnected()
	{
		MessageBox.Set("En attente de données");
#if VERBOSE
		GUILog.Add("Connected :)", Color.green);
#endif
		this.HasReceivedData = true;
		this.StartCoroutine(CheckData());
	}

	IEnumerator CheckData()
	{
		while (connection != null && connection.IsConnected)
		{
			yield return new WaitForSeconds(this.DataTimeout);
			if (!this.HasReceivedData)
			{
				if (connection != null && connection.IsConnected)
				{
					connection.Disconnect();
					this.WaitToConnect();
				}
			}

			this.HasReceivedData = false;
		}
	}

	IEnumerator ConnectionCoroutine()
	{
		MessageBox.Set("Connexion en cours");
#if VERBOSE
		GUILog.Add("Tries to connect", Color.yellow);
#endif
		yield return connection.StartCoroutine(connection.ConnectCoroutine());

		if (!connection.IsConnected)
		{
#if VERBOSE
			GUILog.Add("Can't connect to host", Color.red);
#endif
			this.WaitToConnect();
		}
		else
		{
			this.OnConnected();
		}	
	}

	void WaitToConnect()
	{
		MessageBox.Set("En attente de connexion");
		StartCoroutine(WaitCoroutine(Connect, this.ConnectionTimeout));
	}

	void OnConnectionLost(SocketManager s)
	{		
		WaitToConnect();
	}

	IEnumerator WaitCoroutine(Action a, int sec)
	{
		if (a != null)
		{
			yield return new WaitForSeconds(sec);
			a();
		}
	}
}
