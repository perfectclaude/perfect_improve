﻿/// <summary>
/// Paramètres se trouvant dans le fichier .ini des Panels
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public struct ParsedParametersPanel
{
	public string ipServer;
	public int portServer;
	public float dataTimeout;

	public ParsedParametersPanel(string path)
	{
		IniFile file = IniFile.Load(path);

		this.ipServer	= file["Serveur"]["AdresseIP"].ToString();
		this.portServer = file["Serveur"]["Port"].ToInt();
		this.dataTimeout = file["Serveur"]["Timeout"].ToFloat();
	}
}
