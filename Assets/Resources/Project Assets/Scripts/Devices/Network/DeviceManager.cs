﻿using UnityEngine;
using System.Collections;
using Utils.Network;
using System;

using B = Utils.Network.Binary;

/// <summary>
/// Classe gérant les appareils qui reçoivent les données du serveur, comme l'iPad ou les Panels
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class DeviceManager : SocketManager
{
	public bool				DebugPackage;
	public event Action		OnDataReceived;

	private byte[] package;

	public string	hostIP;
	public int		hostPort;

	public int packageSize
	{
		get
		{
			if (package == null)
				return 0;

			return package.Length;
		}
	}

	protected override void OnConnection(bool first)
	{
		//Debug.Log ("yup");
		StartCoroutine(this.ReceiveCoroutine(OnReceive));		
	}

	private static int GetInt(byte[] bytes, ref int offset)
	{
		int i = BitConverter.ToInt32(B.FromNetworkBigEndian(B.GetPackagePart(bytes, offset, 4)), 0);
		offset += 4;
		return i;
	}
	
	private void OnManagePackage()
	{
		if (package != null && package.Length >= 8)
		{
			int offset = 0;
				
			if (GetInt(package, ref offset) == 42)   // Header
			{
				int packageSize = GetInt(package, ref offset);

				if (package.Length >= packageSize + 8)
				{
					byte[] fullPackage = B.GetPackagePart(package, offset, packageSize);
					AutomateAndERPInfos.FromBytes(fullPackage);

					if (OnDataReceived != null)
						OnDataReceived();

					B.ReducePackage(ref package, packageSize + 8);
					OnManagePackage();
				}
			}
			else
			{
#if VERBOSE
				GUILog.Add("Corrupted Package", Color.red);
#endif
				this.OnHostDisconnected();
			}
		}
	}

	private void OnReceive(byte[] received)
	{
		if(DebugPackage)
			Debug.Log(B.PackageToString(received));

		if (package == null || package.Length == 0)
			package = received;
		else
			package = Binary.Join(new byte[][] { package, received });

		OnManagePackage();
		
		if(this.IsConnected)
		{
			StartCoroutine(this.ReceiveCoroutine(OnReceive, null));
		}
	}
	
	public IEnumerator ConnectCoroutine()
	{
		return this.ConnectCoroutine(hostIP, true, hostPort);
	}

}
