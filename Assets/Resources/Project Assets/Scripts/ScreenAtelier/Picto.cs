﻿using UnityEngine;
using System.Collections;
using Utils;

/// <summary>
/// Picto d'une machine pouvant être positif/négatif et ouvrant les menus de leur machine associée
/// </summary>
/**
 * @author Sylvain Lafon
 */
public class Picto : MonoBehaviour 
{
	public WorkshopElement workshop;

	public Renderer isOK;
	public Renderer isBad;
	public Camera CustomCamera;

	private ScreensManager screensManager;

	public void Awake()
	{
		screensManager = GameObject.FindObjectOfType(typeof(ScreensManager)) as ScreensManager;
	}

	public void SetState(bool state)
	{
		isOK.enabled = state;
		isBad.enabled = !state;
	}

	private void Start()
	{
		if (this.CustomCamera == null)
			this.CustomCamera = Camera.main;

		this.SetState(true);
	}

	private void Update()
	{
		Vector2 mousePos = UCamera.GetInputPosition(CustomCamera, Input.mousePosition);

		if (this.screensManager.Current is ScreenAtelier && Input.GetMouseButtonUp(0) && IsMouseOn(mousePos))
		{
			this.OnClicked();
		}
#if VERBOSE
		else if(Input.GetMouseButtonUp(0) && IsMouseOn(mousePos))
		{
			GUILog.Add("On clicked while another screen : " + this.name);
		}
#endif
	}

	private bool IsMouseOn(Vector2 inputPos)
	{
		return	UCamera.IsMouseOn(CustomCamera, inputPos, Area.BoundsToRect(isOK.bounds)) || 
				UCamera.IsMouseOn(CustomCamera, inputPos, Area.BoundsToRect(isBad.bounds));
	}

	private void OnClicked()
	{
		Debug.Log("Clicked on " + this.name);
		this.workshop.PictoClicked();
	}

}
