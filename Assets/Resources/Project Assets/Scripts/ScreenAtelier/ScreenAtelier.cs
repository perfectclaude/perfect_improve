﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe gérant l'écran principal (la vue atelier)
/// </summary>
/**
 * @author Sylvain Lafon
 */
public class ScreenAtelier : ScreenAbstract
{
	public SwitchBehavior RedButton;
	public Picto Cirmeca, Robot, StockPF, StockM, Tests;
	public GameObject PictosRoot;
	public GameObject AlerteArretRobot;
	public TextMesh MessageArretRobot;
	
	public override void UpdateValues()
	{		
		Cirmeca.SetState(AutomateAndERPInfos.cirmecaInfos.etatOK);
		Robot.SetState	(AutomateAndERPInfos.robotInfos.etatOK);
		StockPF.SetState(AutomateAndERPInfos.generalInfos.produitsFinisOK);
		StockM.SetState	(AutomateAndERPInfos.generalInfos.composantsOK);
		Tests.SetState	(AutomateAndERPInfos.generalInfos.piecesFuyardesOK);

		AlerteArretRobot.SetActive(AutomateAndERPInfos.robotInfos.popupAffichee);
		if(AutomateAndERPInfos.robotInfos.popupAffichee)
			MessageArretRobot.text = AutomateAndERPInfos.robotInfos.messagePopup;
	}
}
