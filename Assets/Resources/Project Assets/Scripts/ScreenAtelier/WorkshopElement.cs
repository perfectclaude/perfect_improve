﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Classe gérant un atelier
/// </summary>
/**
 * @author Sylvain Lafon
 */
public class WorkshopElement : MonoBehaviour
{
	public	ScreenAbstract AssociatedScreen;
	public ScreenAbstract ReturnScreen;

	public event Action<WorkshopElement> OnPictoClicked;
	public event Action<WorkshopElement> OnMenuClosed;

	private void Awake()
	{
		if (AssociatedScreen && AssociatedScreen.ReturnButton)
		{
			AssociatedScreen.ReturnButton.OnClick += OnReturnButtonClicked;
		}
	}

	private void OpenMenu()
	{
	}

	private void OnReturnButtonClicked(ButtonBehavior o)
	{
		CloseMenuInt();	
	}

	private void CloseMenuInt()
	{
		if (this.OnMenuClosed != null)
			this.OnMenuClosed(this);

		CloseMenu();
	}

	public void CloseMenu()
	{
		if (!AssociatedScreen.ReturnButton)
			this.StopAllCoroutines();		
	}
	

	public void PictoClicked()
	{
		manage_Ecrans.CallFromScreen (ReturnScreen);		// Claude

		if (this.OnPictoClicked != null)
			this.OnPictoClicked(this);

		if (!AssociatedScreen.ReturnButton)		
			this.StartCoroutine(this.WaitToClose());
	}
	
	private IEnumerator WaitToClose()
	{
		yield return null;
		while (!Input.GetMouseButtonUp(0))
			yield return null;

		CloseMenuInt();
	}
}
