﻿using UnityEngine;
using System.Collections;
using Utils;

/// <summary>
/// Popup de message
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class MessageBox : MonoBehaviour 
{
	private bool hidden = false;
	public bool debugLog;
	public TextMesh text;
	//private string etalon;
	public Renderer background;

	private void Awake()
	{
		if (!msgBox)
			msgBox = this;

		//this.etalon = this.text.text;
		this.gameObject.SetActive(false);
	}

	private void _Set(string str)
	{
		if(debugLog)
			Debug.Log(str);

		if (string.IsNullOrEmpty(str))
		{
			this.gameObject.SetActive(false);
			this.text.text = "";
		}
		else
		{
			this.gameObject.SetActive(true && !hidden);
			this.text.text = str;
			//Text.SetText(this.text, str, etalon);
		}
	}

	private static MessageBox msgBox;

	private static void GetInstance()
	{
		msgBox = GameObject.FindObjectOfType(typeof(MessageBox)) as MessageBox;
	}

	public static void Set()
	{
		Set(null);
	}

	public static void Set(string txt)
	{
//		Debug.Log (txt);
		if (!msgBox)
			GetInstance();

		if (msgBox)
			msgBox._Set(txt);
	}

	public static void Hide()
	{
		if (!msgBox)
			GetInstance();

		if (msgBox)
		{
			msgBox.hidden = true;
			msgBox.gameObject.SetActive(false);
		}
	}

	public static void Show()
	{
		if (!msgBox)
			GetInstance();

		if (msgBox)
		{
			msgBox.hidden = false;
			msgBox.gameObject.SetActive(
				!string.IsNullOrEmpty(msgBox.text.text)
			);
		}
	}
}
