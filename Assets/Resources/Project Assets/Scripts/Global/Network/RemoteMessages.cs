﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Constantes communes au serveur et aux télécommandes.
/// Contient notemment et **surtout** les messages échangés
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public static class RemoteMessages 
{
	public const int BROADCAST_PORT					= 325;

	public const string MSG_KICKED					= "Kicked";
	public const string MSG_CONNECTED				= "Connected";
	public const string MSG_WAITING_CONTROLLER		= "Waiting controller";

	public const string MSG_DISCONNECTION			= "Disconnection";
	public const string MSG_IDLE					= "No operation";

	public const string MSG_CLICKED					= "Clicked";
	public const string MSG_CLOSED					= "Closed";

	public const string MSG_RED_BUTTON				= "Red button";
	public const string MSG_ADMIN_MENU				= "Admin menu";
	public const string MSG_CHANGE_SETTING			= "Change setting";
}
