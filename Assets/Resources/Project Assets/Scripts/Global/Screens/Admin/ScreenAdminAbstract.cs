using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Classe parente des écrans de menu (admin)
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public abstract class ScreenAdminAbstract : ScreenAbstract 
{
	public GameObject ReadOnlyGameObject;
	
	public static bool								IsReadOnly = false;
	public static event Action<ScreenAdminAbstract> OnAdminChangeScreen;
	public static event Action						OnAdminScreenClosed;
	public static event Action<string, string>		OnAdminSettingChanged;
	public static Func<InGameSettings>				GetSettings;
	
	protected ScreenAdminMenu MenuScreen;

	protected void OnLocalSettingChanged(string name, string value)
	{
#if DEBUG_MODE
		Debug.Log("Setting changed : '" + name + "' => '" + value + "'");
#endif
		if (OnAdminSettingChanged != null && !IsReadOnly)
		{
			OnAdminSettingChanged(name, value);
		}
	}

//	protected void OnLocalMenuClosed()
	public void OnLocalMenuClosed()
	{
		if (OnAdminScreenClosed != null && !IsReadOnly)
		{
			OnAdminScreenClosed();
		}
	}

	public virtual void DistantChangeSetting(string name, string value)
	{
		// Does nothing
	}

	protected override void Awake()
	{
		base.Awake();

		GetS(out MenuScreen);
	}
	
	protected void BindPass(ButtonBehavior btn, ScreenAdminAbstract scr)
	{
		ScreenAdminPassword passMenu;
		GetS(out passMenu);

		if (passMenu)
		{
			if (btn && scr)
			{
				btn.OnClick += (b) =>
				{
					this.OnLocalSettingChanged("Password/NextScreen", scr.name);
					this.LocalChangeScreen(passMenu);
				};
			}
		}
	}

	protected void LocalChangeScreen(ScreenAdminAbstract scr)
	{
		this.Manager.Current = scr;
		if (OnAdminChangeScreen != null)
		{
			OnAdminChangeScreen(scr);
		}
	}

	public override void OnActivation()
	{
		GUITextField.RemoveFocusToAny();

		if (ReadOnlyGameObject)
		{
			ReadOnlyGameObject.SetActive(IsReadOnly);
		}

		base.OnActivation();
	}

	protected void Bind(ButtonBehavior btn, ScreenAdminAbstract scr)
	{
		if (btn && scr)
		{
			btn.OnClick += (b) =>
			{
				LocalChangeScreen(scr);
			};
		}
	}
}
