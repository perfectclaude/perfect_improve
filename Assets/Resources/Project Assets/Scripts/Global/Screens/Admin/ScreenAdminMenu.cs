﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe gérant l'écran principal du menu
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class ScreenAdminMenu : ScreenAdminAbstract
{
	public ButtonBehavior SeuilsButton;
	public ButtonBehavior MessagesButton;
	public ButtonBehavior CalculsButton;

	protected override void Awake()
	{
		base.Awake();
		
		BindPass(SeuilsButton,		GetS<ScreenAdminSeuils>());
		BindPass(MessagesButton,	GetS<ScreenAdminMessages>());
			Bind(CalculsButton,		GetS<ScreenAdminCalculs>());

		if (this.ReturnButton)
		{
			ReturnButton.OnClick += (o) =>
			{
				this.OnLocalMenuClosed();
			};
		}
	}
	
	public override void OnActivation()
	{
		IsReadOnly = false;
		base.OnActivation();
	}
}
