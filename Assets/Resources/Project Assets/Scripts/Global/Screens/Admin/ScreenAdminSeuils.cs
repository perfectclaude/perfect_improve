using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// Classe gérant la page de paramètrage des seuils
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class ScreenAdminSeuils : ScreenAdminAbstract {

	public float EpsilonXSliders = 0.1f;
	public float EpsilonYSliders = 0.6f;

	public IntelligentSlider[] Sliders;
	private List<IntelligentSlider> Percents;

	private FormManager Form; 
	public ButtonBehavior DefaultButton;
	public ButtonBehavior ValidateButton;
	public TextMesh[] MaximumTexts;
	
	private bool inited;
	private float[] previous;
	private float[] current;

#if DEBUG_MODE
	public bool debug;
#endif

	#region Constants

	public static readonly float[] MAX_VALUES = new float[]
	{
		175,
		175,
		175,
		175,
		175,
		175,
		100,
		10000,
		10000,
		10000,
		10000,
		100
	};

	public static readonly float[] DEFAULT_VALUES = new float[]
	{
		100,
		130,
		95,
		110,
		140,
		105,
		95,
		1000,
		4000,
		3000,
		6000,
		95
	};

	#endregion

	#region Field management

	private void LinkForm()
	{
		this.Form = this.gameObject.AddComponent<FormManager>();
		this.Form.screenWidth = 1024;
		this.Form.screenHeight = 768;

		this.Form.fields = new GUITextField[this.Sliders.Length];
		for (int i = 0; i < this.Sliders.Length; i++)
		{
			this.Form.fields[i] = this.Sliders[i].LinkedValue;
		}
	}

	private void InitFields()
	{		
		IntelligentSlider slider;
		for(int i = 0; i < this.Sliders.Length; i++)
		{
			slider = this.Sliders[i];

			slider.EpsilonX = this.EpsilonXSliders;
			slider.EpsilonY = this.EpsilonYSliders;
			slider.MinValue = 0;
			slider.MaxValue = MAX_VALUES[i];
			slider.OnValueChanged += OnSliderValueChanged;
			slider.LinkedValue.OnReturnPressed += OnFieldReturnPressed;
			slider.LinkedValue.OnFocusChanged += OnFieldFocusChanged;
			
			MaximumTexts[i].text = MAX_VALUES[i].ToString();
		}
	}

	private void EnableFields(bool enable)
	{
		IntelligentSlider slider;
		for (int i = 0; i < this.Sliders.Length; i++)
		{
			slider = this.Sliders[i];
			slider.enabled = enable;
			slider.LinkedValue.enabled = enable;
		}
	}
		
	private void SetPreviousValues()
	{
		InGameSettings p = null;

		if (GetSettings != null)
			p = GetSettings();

		if (p == null)
			p = new InGameSettings();

		this.previous = new float[] 
		{
			p.CadenceStandardCirmeca,			// 0
			p.CadenceStandardMAXCirmeca,		// 1
			p.LimiteControleInferieureCirmeca,	// 2
			p.CadenceStandardRobot,				// 3
			p.CadenceStandardMAXRobot,			// 4
			p.LimiteControleInferieureRobot,	// 5
			p.TauxQualiteStandardRobot * 100,	// 6
			p.MinStockManoeuvre,				// 7
			p.MaxStockManoeuvre,				// 8
			p.MinStockPF,						// 9
			p.MaxStockPF,						// 10
			p.TauxQualiteAirEau * 100			// 11
		};

		this.Percents = new List<IntelligentSlider>();
		this.Percents.Add(this.Sliders[6]);
		this.Percents.Add(this.Sliders[11]);
	}

	private void ChangeSettings(float[] array)
	{
		if (!IsReadOnly)
		{
			for (int i = 0; i < array.Length; i++)
			{
				ChangeSetting(i, array[i]);
			}
		}
	}

	private void ChangeSetting(int i, float val)
	{
		if (current[i] != val)
		{			
			current[i] = Mathf.Clamp(val, 0, MAX_VALUES[i]);
			this.Sliders[i].Value = current[i];						

			if (!IsReadOnly)
			{
				string name = this.Form.fields[i].ControlName;

				if (this.Percents.Contains(this.Sliders[i]))
					this.OnLocalSettingChanged(name, (current[i] * 0.01f).ToString("F3"));
				else
					this.OnLocalSettingChanged(name, ((int)current[i]).ToString());
			}
		}
	}

	private int GetFieldIndex(GUITextField t)
	{
		for (int i = 0; i < this.Form.fields.Length; i++)
		{
			if (this.Form.fields[i] == t)
				return i;
		}

		return -1;
	}

	#endregion

	#region Event handlers

	protected override void Awake()
	{
		base.Awake();

		this.LinkForm();

		if (DefaultButton)
		{
			DefaultButton.OnClick += OnDefaultButtonClicked;
		}

		if (ReturnButton)
		{
			ReturnButton.OnClick += OnReturnButtonClicked;
		}

		if (ValidateButton)
		{
			ValidateButton.OnClick += OnValidateButtonClicked;
		}

#if DEBUG_MODE
		if (debug)
		{
			OnActivation();
		}
#endif
	}

	public override void OnActivation()
	{
		base.OnActivation();

		if (!inited)
		{
			inited = true;
			this.InitFields();
		}

		this.EnableFields(!IsReadOnly);
		this.SetPreviousValues();


		IntelligentSlider slider;
		current = new float[previous.Length];
		for (int i = 0; i < previous.Length; i++)
		{
			current[i] = Mathf.Clamp(previous[i], 0, MAX_VALUES[i]);
			slider = this.Sliders[i];

			slider.Value = current[i];
			if (!slider.inited)
				slider.Reinit();
		}
	}
	
	private void OnSliderValueChanged(IntelligentSlider s, float value)
	{
		if(!IsReadOnly)
			ChangeSetting(GetFieldIndex(s.LinkedValue), value);
	}

	private void OnFieldFocusChanged(GUITextField t, bool focused)
	{
		if (!focused)
		{
			ChangeSetting(GetFieldIndex(t), t.Number);
		}
	}

	private void OnFieldReturnPressed(GUITextField field)
	{
#if !UNITY_IPHONE
		if (field.IsFocused && !IsReadOnly)
		{
			field.RemoveFocus();
			//this.OnMenuValidated();
		}
#endif
	}
		
	private void OnDefaultButtonClicked(ButtonBehavior obj)
	{
		if(!IsReadOnly)
			this.ChangeSettings(DEFAULT_VALUES);
	}

	private void OnReturnButtonClicked(ButtonBehavior obj)
	{
		if (!IsReadOnly)
		{
			this.ChangeSettings(this.previous);
			this.GotoMenu();
		}
	}

	private void OnValidateButtonClicked(ButtonBehavior obj)
	{
		if (!IsReadOnly)
			this.OnMenuValidated();
	}

	private void OnMenuValidated()
	{
		if (!IsReadOnly)
			GotoMenu();
	}

	public override void DistantChangeSetting(string name, string value)
	{
		if (this.Form.NamedFields.ContainsKey(name))
		{
			GUITextField tf = this.Form.NamedFields[name];
			if (tf != null)
			{
				tf.Text = value;
				this.OnFieldFocusChanged(tf, false);
			}
		}
	}

	#endregion

	private void GotoMenu()
	{
		this.LocalChangeScreen(MenuScreen);
	}
}
