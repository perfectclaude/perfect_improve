using UnityEngine;
using System.Collections;

/// <summary>
/// Classe gérant la page d'entrée de mot de passe
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class ScreenAdminPassword : ScreenAdminAbstract
{	
	private ScreenAdminAbstract	NextScreen;
	public ButtonBehavior		ValidateButton;
	public GUITextField			PasswordField;
	
	protected override void Awake()
	{
		base.Awake();
		
		if (ValidateButton)
		{
			ValidateButton.OnClick += OnValidateButtonClicked;
		}

		if (PasswordField)
		{
			PasswordField.OnReturnPressed += OnPasswordFieldReturnPressed;
		}

		Bind(ReturnButton, MenuScreen);
	}

	private void OnPasswordFieldReturnPressed(GUITextField field)
	{
		Validate();
	}

	private void OnValidateButtonClicked(ButtonBehavior btn)
	{
		Validate();
	}

	private void Validate()
	{
		string sha1pass = InGameSettings.SHA1(PasswordField.Text);

		if (PasswordField && GetSettings != null)
		{
			InGameSettings settings = GetSettings();
			if (settings != null)
			{
				this.OnPassValidated();
				/*
				if (!string.IsNullOrEmpty(settings.SHA1Password) &&
					sha1pass == settings.SHA1Password)
				{
					this.OnPassValidated();
				}
				else
				{
					Debug.Log("Mot de passe mauvais ou accès impossible");
					Debug.Log("SHA-1 attendu : " + settings.SHA1Password);
					Debug.Log("SHA-1 tapé : " + sha1pass);
				}
				*/
			}
			PasswordField.Text = "";
		}
	}

	private void OnPassValidated()
	{
		if (GetSettings != null)
		{
			InGameSettings settings = GetSettings();
			if (settings != null)
			{
				this.LocalChangeScreen(
					this.Manager.GetS<ScreenAdminAbstract>(settings.PasswordNextScreen)
				);
			}
		}
	}
	
	public override void OnActivation()
	{
		base.OnActivation();
		PasswordField.Text = "";
		PasswordField.GiveFocus();
	}
}
