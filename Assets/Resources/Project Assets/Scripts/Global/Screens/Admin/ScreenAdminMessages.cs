using System;
using UnityEngine;

/// <summary>
/// Classe gérant le paramètrage des messages à afficher
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class ScreenAdminMessages : ScreenAdminAbstract
{
	public FormManager		Fields;
	public ButtonBehavior	DefaultButton;
	public ButtonBehavior	ValidateButton;
	public TextAsset		DefaultMessagesText;

#if DEBUG_MODE
	public bool debug;
#endif
		
	private string[] previous;
	private string[] current;

	private bool inited;
	private string[] DefaultValues;
	
	#region Constants
	
	const string NORMAL_QUOTE = "\"";
	const string RED_QUOTE = "<b><color=red>" + NORMAL_QUOTE + "</color></b>";
	
	#endregion

	#region Field management

	private string[] GetDefaultValues(TextAsset txt)
	{
		string [] retval = txt.text.Split('\n');
		int len = retval.Length;
		for (int i = 0; i < len; i++)
		{
			retval[i] = retval[i].Replace("\r", "");
		}
		return retval;
	}

	private void InitFields()
	{
		this.DefaultValues = this.GetDefaultValues(DefaultMessagesText);

		if (this.Fields.fields.Length > DefaultValues.Length)
			throw new UnityException("Erreur le nombre de champs ne correspond pas");

		foreach (GUITextField t in this.Fields.fields)
		{
			t.OnReturnPressed += OnFieldReturnPressed;
			t.OnFocusChanged += OnFieldFocusChanged;
			t.FilterNotFocused = this.FilterNotFocused;
		}
	}
			
	private void EnableFields(bool enable)
	{
		foreach (GUITextField tf in this.Fields.fields)
			tf.enabled = enable;
	}
	
	private void SetPreviousValues()
	{
		InGameSettings p = null;

		if(GetSettings != null)
			p = GetSettings();

		if (p == null)
			p = new InGameSettings();

		this.previous = new string[] 
		{
			p.CadenceOK,
			p.CadenceJ_BAD,
			p.CadenceH_BAD,
			p.CadenceBAD,
			p.QualiteOK,
			p.QualiteJ_BAD,
			p.QualiteH_BAD,
			p.QualiteBAD,
			p.AlertSeparator,
			p.StockMangHIGH,
			p.StockMangLOW,
			p.StockMangOK,
			p.StockPFHIGH,
			p.StockPFLOW,
			p.StockPFOK,
			p.RejetAirEauHIGH,
			p.RejetAirEauOK
		};
	}

	private void RefreshFields()
	{
		for (int i = 0; i < this.current.Length; i++)
		{
			this.Fields.fields[i].Text = current[i];
		}
	}

	private string FilterNotFocused(GUITextField t, string wantedValue)
	{
		if (string.IsNullOrEmpty(wantedValue))
			return wantedValue;

		bool richTextAllowed = (wantedValue.IndexOfAny(new char[] { '<', '>' }) == -1 && t.ifNotFocused.richText);
		string quote = (richTextAllowed) ? RED_QUOTE : NORMAL_QUOTE;

		return quote + wantedValue + quote;
	}

	private int GetFieldIndex(GUITextField t)
	{
		for (int i = 0; i < this.Fields.fields.Length; i++)
		{
			if (Fields.fields[i] == t)
				return i;
		}

		return -1;
	}

	private void ChangeSettings(string[] array)
	{
		for (int i = 0; i < array.Length; i++)
		{
			ChangeSetting(i, array[i]);
		}
	}

	private void ChangeSetting(int i, string val)
	{
		if (current[i] != val)
		{
			current[i] = val;
			this.OnLocalSettingChanged("Messages/" + this.Fields.fields[i].ControlName, current[i]);
		}
	}
	
	#endregion

	#region Event handlers

	protected override void Awake()
	{
		base.Awake();
		
		if (DefaultButton)
		{
			DefaultButton.OnClick += OnDefaultButtonClicked;
		}

		if (ReturnButton)
		{
			ReturnButton.OnClick += OnReturnButtonClicked;
		}

		if (ValidateButton)
		{
			ValidateButton.OnClick += OnValidateButtonClicked;
		}

#if DEBUG_MODE
		if (debug)
		{
			OnActivation();
		}
#endif
	}
	
	public override void OnActivation()
	{
		base.OnActivation();

		if (!inited)
		{
			inited = true;
			InitFields();
		}

		this.SetPreviousValues();
		current = new string[previous.Length];
		Array.Copy(previous, current, previous.Length);

		this.EnableFields(!IsReadOnly);
		this.RefreshFields();
	}
	
	private void OnFieldFocusChanged(GUITextField t, bool IsFocused)
	{
		if (!IsFocused)
		{
			ChangeSetting(GetFieldIndex(t), t.Text);
		}
	}

	private void OnFieldReturnPressed(GUITextField field)
	{
#if !UNITY_IPHONE
		if (field.IsFocused)
		{
			field.RemoveFocus();
			//this.OnMenuValidated();
		}
#endif
	}

	private void OnDefaultButtonClicked(ButtonBehavior obj)
	{
		if (!IsReadOnly)
		{
			this.ChangeSettings(DefaultValues);
			this.RefreshFields();
		}
	}	

	private void OnReturnButtonClicked(ButtonBehavior obj)
	{
		if (!IsReadOnly)
		{
			this.ChangeSettings(this.previous);
#if DEBUG_MODE
			if (debug)
			{
				this.RefreshFields();
			}
#endif
			this.LocalChangeScreen(MenuScreen);
		}
	}

	private void OnValidateButtonClicked(ButtonBehavior obj)
	{
		if (!IsReadOnly)
		{
			this.OnMenuValidated();
		}
	}

	private void OnMenuValidated()
	{
		if (!IsReadOnly)
		{
			this.LocalChangeScreen(MenuScreen);
		}
	}
	
	public override void DistantChangeSetting(string name, string value)
	{
		int slashPos = name.IndexOf('/');
		if (slashPos != -1)
		{
			//string category = name.Remove(slashPos);
			string property = name.Substring(slashPos + 1);
			if (this.Fields.NamedFields.ContainsKey(property))
			{
				GUITextField tf = this.Fields.NamedFields[property];
				if (tf != null)
				{
					tf.Text = value;
				}
			}
		}
	}

	#endregion
}
