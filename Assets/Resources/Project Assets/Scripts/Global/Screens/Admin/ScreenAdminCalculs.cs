﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe gérant l'écran d'explications des calculs
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class ScreenAdminCalculs : ScreenAdminAbstract
{
	protected override void Awake()
	{
		base.Awake();
		Bind(ReturnButton, GetS<ScreenAdminMenu>());
	}
	
}
