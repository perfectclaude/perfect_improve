﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe gérant l'écran ERP
/// </summary>
/**
 * @author Aurélien Lévêque <aleveque@manzalab.com>
 */
public class ScreenGeneral : ScreenAbstract
{
	public Chiffre	produitsFinis;
    public Chiffre  produitsFinisAffecte;
	public Bandeau	produitsFinisAlerte;
	public TexteAlerte produitFinisTexte;

    public Chiffre composants;
    public Chiffre composantsAffecte;
	public Bandeau	composantsAlerte;
	public TexteAlerte composantsTexte;

	public Chiffre	piecesFuyards;
	public Bandeau	piecesFuyardsAlerte;
	public TexteAlerte piecesFuyardsTexte;
	
	public TextMesh	heure;
	public Bandeau general;

		
	public override void UpdateValues()
	{
		general.Set(produitsFinis.good && composants.good && piecesFuyards.good);

		produitsFinis.Set(AutomateAndERPInfos.generalInfos.produitsFinis,
						AutomateAndERPInfos.generalInfos.produitsFinisOK);
        produitsFinisAffecte.Set(AutomateAndERPInfos.generalInfos.produitsFinisAffecte,
                        AutomateAndERPInfos.generalInfos.produitsFinisOK);
		composants.Set(AutomateAndERPInfos.generalInfos.composants, 
						AutomateAndERPInfos.generalInfos.composantsOK);
        composantsAffecte.Set(AutomateAndERPInfos.generalInfos.composantsAffecte,
                        AutomateAndERPInfos.generalInfos.composantsOK);
		piecesFuyards.Set(AutomateAndERPInfos.generalInfos.piecesFuyardes, 
						AutomateAndERPInfos.generalInfos.piecesFuyardesOK);
		
		heure.text = GetStringHour(AutomateAndERPInfos.heuresInfos.heureCourante);

		produitsFinisAlerte	.Set(produitsFinis.good);
		composantsAlerte	.Set(composants.good);
		piecesFuyardsAlerte	.Set(piecesFuyards.good);

		produitFinisTexte.Set(produitsFinis.good, AutomateAndERPInfos.generalInfos.produitsFinisAlerte);
		composantsTexte.Set(composants.good, AutomateAndERPInfos.generalInfos.composantsAlerte);
		piecesFuyardsTexte.Set(piecesFuyards.good, AutomateAndERPInfos.generalInfos.piecesFuyardsAlerte);
	}
}
