﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe de base d'un écran
/// </summary>
/**
 * @author Aurélien Lévêque <aleveque@manzalab.com>
 * @author Repris par : Sylvain Lafon <slafon@manzalab.com>
 */
public abstract class ScreenAbstract : MonoBehaviour
{
	public ButtonBehavior QuitButton;
	public ButtonBehavior ReturnButton;

	private ScreensManager _Manager;
	protected ScreensManager Manager { get { return _Manager; } }

	protected Camera ScreensCamera;
	
	public virtual bool AlwaysShown { get { return false; } }
	public virtual void UpdateValues() { }
	public virtual void OnActivation() { }
	public virtual void OnDesactivation() { }

	protected virtual void Awake()
	{
		if (QuitButton)
		{
			QuitButton.OnClick += (o) =>
			{
				Application.Quit();
			};
		}

		Get(out _Manager);
		this.ScreensCamera = Manager.ScreensCamera;
	}
	
	public string GetStringHour(System.DateTime dateTime)
	{
		string time = "";

		if(dateTime.Hour<10)
			time+="0"+dateTime.Hour;
		else
			time+=""+dateTime.Hour;

		time+="H";
		
		if(dateTime.Minute<10)
			time+="0"+dateTime.Minute;
		else
			time+=""+dateTime.Minute;

		return time;
	}

	#region Helpers
	 
	protected T GetS<T>(string name) where T:ScreenAbstract
	{
		return Manager.GetS<T>(name);
	}

	protected T GetS<T>() where T : ScreenAbstract
	{
		return Manager.GetS<T>();
	}

	protected void GetS<T>(out T screen) where T : ScreenAbstract
	{
		screen = GetS<T>();
	}

	/// <summary>
	/// Gets a singleton
	/// </summary>
	/// <typeparam name="T">Singleton's type</typeparam>
	/// <returns>Singleton's reference</returns>
	protected static T Get<T>() where T : Component
	{
		return GameObject.FindObjectOfType(typeof(T)) as T;
	}

	/// <summary>
	/// Puts a singleton into the given reference
	/// </summary>
	/// <typeparam name="T">Singleton's type</typeparam>
	/// <param name="comp">Reference to use</param>
	protected static void Get<T>(out T comp) where T : Component
	{
		comp = Get<T>();
	}

	protected static T Get<T>(string name) where T : Component
	{
		GameObject g = GameObject.Find(name);
		if (g == null)
			return null;
		return g.GetComponent<T>();
	}

	#endregion
}
