﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe gérant l'écran Cirmeca
/// </summary>
/**
 * @author Aurélien Lévêque <aleveque@manzalab.com>
 */
public class ScreenCirmeca : ScreenAbstract {

	public Chiffre						bonnesPieces;
	public ChiffrePourcentageCarre		cadence;
	public ChiffrePourcentageCarre		rendement;
	
	public Chiffre						piecesParHeure;
	
	public ChiffrePourcentageCarre		cadenceDerniereHeure;
	public ChiffrePourcentageCarre		rendementDerniereHeure;
	
	public TextMesh						heureDebut;
	public TextMesh						heureCourante;		
	public TextMesh						heureDerniere;	
	
	public TexteAlerte					texteAlerte;
	public Bandeau						bandeauAlerte;	
	public GameObject					pictoOk;
	public GameObject					pictoNOk;
	
	public override void UpdateValues()
	{		
		bonnesPieces			.Set(AutomateAndERPInfos.cirmecaInfos.bonnePieces, true);
		cadence					.Set(AutomateAndERPInfos.cirmecaInfos.cadence,					
									AutomateAndERPInfos.cirmecaInfos.cadenceRendementOK);		
		rendement				.Set(AutomateAndERPInfos.cirmecaInfos.rendement,				
									AutomateAndERPInfos.cirmecaInfos.cadenceRendementOK);
		piecesParHeure			.Set(AutomateAndERPInfos.cirmecaInfos.piecesParHeure,
									AutomateAndERPInfos.cirmecaInfos.cadenceRendementOK);	
		cadenceDerniereHeure	.Set(AutomateAndERPInfos.cirmecaInfos.cadenceDerniereHeure,		
									AutomateAndERPInfos.cirmecaInfos.cadenceRendementDerniereHeureOK);
		rendementDerniereHeure	.Set(AutomateAndERPInfos.cirmecaInfos.rendementDerniereHeure,	
									AutomateAndERPInfos.cirmecaInfos.cadenceRendementDerniereHeureOK);
		
		heureDebut.text		= GetStringHour(AutomateAndERPInfos.heuresInfos.heureDebut);
		heureCourante.text	= GetStringHour(AutomateAndERPInfos.heuresInfos.heureDerniere);
		heureDerniere.text	= GetStringHour(AutomateAndERPInfos.heuresInfos.heureCourante);
		
		texteAlerte			.Set(	AutomateAndERPInfos.cirmecaInfos.etatOK, 
									AutomateAndERPInfos.cirmecaInfos.texteAlerte);

		bandeauAlerte		.Set(AutomateAndERPInfos.cirmecaInfos.etatOK);
		pictoOk				.SetActive(AutomateAndERPInfos.cirmecaInfos.etatOK);
		pictoNOk			.SetActive(!AutomateAndERPInfos.cirmecaInfos.etatOK);
	}
}
