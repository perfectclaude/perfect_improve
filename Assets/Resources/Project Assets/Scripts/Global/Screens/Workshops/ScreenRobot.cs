﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Classe gérant l'écran Robot
/// </summary>
/**
 * @author Aurélien Lévêque <aleveque@manzalab.com>
 */
public class ScreenRobot : ScreenAbstract {
	
	public Chiffre						bonnesPieces;
	public Chiffre						coupures;
	public ChiffrePourcentageCarre		cadence;
	public ChiffrePourcentageCarre		rendement;
	public ChiffrePourcentageCarre		qualite;
	
	public Chiffre						piecesParHeure;
	public ChiffrePourcentage			rejet;
	public Chiffre						pieceARetraiter;
	
	public ChiffrePourcentageCarre		cadenceDerniereHeure;
	public ChiffrePourcentageCarre		rendementDerniereHeure;
	public ChiffrePourcentageCarre		qualiteDerniereHeure;
	
	public TextMesh						heureDebut;
	public TextMesh						heureCourante;
	public TextMesh						heureDerniere;	
	
	public TexteAlerte					texteAlerte;
	public Bandeau						bandeauAlerte;	
	public GameObject					pictoOk;
	public GameObject					pictoNOk;
	
	public override void UpdateValues()
	{		
		bonnesPieces			.Set(AutomateAndERPInfos.robotInfos.bonnePieces, true);
		coupures				.Set(AutomateAndERPInfos.robotInfos.coupures,
									AutomateAndERPInfos.robotInfos.coupuresOK);
		cadence					.Set(AutomateAndERPInfos.robotInfos.cadence,
									AutomateAndERPInfos.robotInfos.cadenceRendementOK);		
		rendement				.Set(AutomateAndERPInfos.robotInfos.rendement,
									AutomateAndERPInfos.robotInfos.cadenceRendementOK);
		qualite					.Set(AutomateAndERPInfos.robotInfos.qualite,
									AutomateAndERPInfos.robotInfos.qualiteRejetOK);
		piecesParHeure			.Set(AutomateAndERPInfos.robotInfos.piecesParHeure,
									AutomateAndERPInfos.robotInfos.cadenceRendementOK);
		rejet					.Set(AutomateAndERPInfos.robotInfos.rejet,
									AutomateAndERPInfos.robotInfos.qualiteRejetOK);
		pieceARetraiter			.Set(AutomateAndERPInfos.robotInfos.piecesARetraiter,
									AutomateAndERPInfos.robotInfos.qualiteRejetOK);	
		cadenceDerniereHeure	.Set(AutomateAndERPInfos.robotInfos.cadenceDerniereHeure,
									AutomateAndERPInfos.robotInfos.cadenceRendementDerniereHeureOK);
		rendementDerniereHeure	.Set(AutomateAndERPInfos.robotInfos.rendementDerniereHeure,
									AutomateAndERPInfos.robotInfos.cadenceRendementDerniereHeureOK);
		qualiteDerniereHeure	.Set(AutomateAndERPInfos.robotInfos.qualiteDerniereHeure,
									AutomateAndERPInfos.robotInfos.qualiteDerniereHeureOK);
		
		heureDebut.text		= GetStringHour(AutomateAndERPInfos.heuresInfos.heureDebut);
		heureCourante.text	= GetStringHour(AutomateAndERPInfos.heuresInfos.heureDerniere);
		heureDerniere.text	= GetStringHour(AutomateAndERPInfos.heuresInfos.heureCourante);
		
		texteAlerte		.Set(	AutomateAndERPInfos.robotInfos.etatOK,
								AutomateAndERPInfos.robotInfos.texteAlerte);

		bandeauAlerte	.Set(AutomateAndERPInfos.robotInfos.etatOK);
		pictoOk			.SetActive(AutomateAndERPInfos.robotInfos.etatOK);
		pictoNOk		.SetActive(!AutomateAndERPInfos.robotInfos.etatOK);
	}
}
