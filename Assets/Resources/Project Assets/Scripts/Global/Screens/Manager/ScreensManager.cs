﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Classe gérant les différents écrans
/// </summary>
/**
 * @author Aurélien Lévêque <aleveque@gmail.com>
 * @author Retouché par Sylvain Lafon <slafon@manzalab.com>
 */
public class ScreensManager : MonoBehaviour
{
	#region Parameters

	public Camera ScreensCamera;
	public ScreenAbstract[] AutomaticShownScreens;
	public ScreenAbstract[] EveryManagedScreens;
	public float ChangeScreenTime;

	#endregion

	#region Screen management

	private ScreenAbstract current;

	public ScreenAbstract Current
	{
		get
		{
			return current;
		}
		set
		{
			if (current == value)
				return;

			if (ScreensCamera)
				ScreensCamera.enabled = (EveryManagedScreens != null);

			if (EveryManagedScreens != null)
			{
				foreach (ScreenAbstract screen in EveryManagedScreens)
				{
					if (screen != null)
					{
						if (value == screen)
						{
							screen.OnActivation();
							screen.gameObject.SetActive(true);
						}
						else if (screen.gameObject.activeSelf)
						{
							screen.OnDesactivation();
							screen.gameObject.SetActive(false);
						}
					}
				}
			}

			current = value;
		}
	}

	public T GetS<T>(string name) where T : ScreenAbstract
	{
		foreach (ScreenAbstract s in this.EveryManagedScreens)
		{
			if (s is T && s.name == name)
			{
				return s as T;
			}
		}

		return null;
	}

	public void GetS<T>(out T screen) where T : ScreenAbstract
	{
		screen = GetS<T>();
	}

	public T GetS<T>() where T : ScreenAbstract
	{
		foreach (ScreenAbstract s in this.EveryManagedScreens)
		{
			if (s.GetType() == typeof(T))
			{
				return s as T;
			}
		}

		return null;
	}

	#endregion

	#region Colors
	public static Color RGB(int r, int g, int b)
	{
		return new Color(
			Mathf.Clamp01(r / 255f),
			Mathf.Clamp01(g / 255f),
			Mathf.Clamp01(b / 255f)
		);
	}

	public static Color Red
	{
		get
		{
			return RGB(229, 22, 0);
		}
	}

	public static Color Green
	{
		get
		{
			return RGB(135, 178, 0);
		}
	}

	public static Color Orange
	{
		get
		{
			return RGB(255, 163, 0);
		}
	}
	#endregion

	#region Update screens information

	public void UpdateAllScreens()
	{
		foreach (ScreenAbstract screen in AutomaticShownScreens)
		{
			screen.UpdateValues();
		}
	}

	#endregion

	#region Automatic screen show

	private int CurrentScreenIndex;
	private float ChangeScreenTimer;

	private void OnEnable()
	{
		ChangeScreenTimer = 0f;
		CurrentScreenIndex = 0;
		this.ShowCurrent();
	}

	private void NextScreen()
	{
		CurrentScreenIndex++;
		if (CurrentScreenIndex >= AutomaticShownScreens.Length)
			CurrentScreenIndex = 0;

		this.ShowCurrent();
	}

	private void ShowCurrent()
	{
		this.Current = AutomaticShownScreens[CurrentScreenIndex];
	}

	protected virtual void Update()
	{
		if (ChangeScreenTime > 0)
		{
			ChangeScreenTimer += Time.deltaTime;
			while (ChangeScreenTimer > ChangeScreenTime)
			{
				ChangeScreenTimer -= ChangeScreenTime;
				this.NextScreen();
			}
		}
	}

	#endregion
}
