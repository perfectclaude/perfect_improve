﻿using UnityEngine;
using System.Collections;
using System;

using Random = UnityEngine.Random;

/// <summary>
/// Permet de modifier aléatoirement et à la volée les données affichées dans les différents écrans
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 * @author Ce code se trouvait dans une classe d'Aurélien Lévêque <aleveque@manzalab.com>
 */
[System.Obsolete("Ne devrait pas être utilisée en build")]
public class TestScreensManager : ScreensManager
{
#if DEBUG_MODE
	protected override void Update()
	{
		if (Input.GetKeyUp(KeyCode.Space))
			DebugTest();	

		base.Update();
	}

	void DebugTest()
	{
		RobotInfos robotInfos = new RobotInfos();
		robotInfos.bonnePieces = Random.Range(0, 1000);
		robotInfos.coupures = Random.Range(0, 5);
		robotInfos.cadence = Random.Range(0, 100);
		robotInfos.rendement = Random.Range(0, 100);
		robotInfos.qualite = Random.Range(0, 100);
		robotInfos.piecesParHeure = Random.Range(0, 100);
		robotInfos.rejet = Random.Range(0, 10);
		robotInfos.piecesARetraiter = Random.Range(0, 10);
		robotInfos.cadenceDerniereHeure = Random.Range(0, 100);
		robotInfos.rendementDerniereHeure = Random.Range(0, 100);
		robotInfos.qualiteDerniereHeure = Random.Range(0, 100);
		robotInfos.etatOK = Random.Range(0, 100) < 50 ? false : true;
		robotInfos.texteAlerte = "Texte d'alerte!!!";
		SetInfosRobot(robotInfos);

		CirmecaInfos cirmecaInfos = new CirmecaInfos();
		cirmecaInfos.bonnePieces = Random.Range(0, 1000);
		cirmecaInfos.cadence = Random.Range(0, 100);
		cirmecaInfos.rendement = Random.Range(0, 100);
		cirmecaInfos.piecesParHeure = Random.Range(0, 100);
		cirmecaInfos.cadenceDerniereHeure = Random.Range(0, 100);
		cirmecaInfos.rendementDerniereHeure = Random.Range(0, 100);
		cirmecaInfos.etatOK = Random.Range(0, 100) < 50 ? false : true;
		cirmecaInfos.texteAlerte = "Texte d'alerte!!!";
		SetInfosCirmeca(cirmecaInfos);

		GeneralInfos generalInfos = new GeneralInfos();
		generalInfos.produitsFinis = Random.Range(0, 1000);
		generalInfos.composants = Random.Range(0, 1000);
		generalInfos.piecesFuyardes = Random.Range(0, 100);
		SetInfosGeneral(generalInfos);

		SetHeureDebut(DateTime.Today);
		SetHeure(DateTime.Now);

	}

	public void SetInfosRobot(RobotInfos robotInfos)
	{
		AutomateAndERPInfos.robotInfos = robotInfos;
		UpdateAllScreens();
	}

	public void SetInfosCirmeca(CirmecaInfos cirmecaInfos)
	{
		AutomateAndERPInfos.cirmecaInfos = cirmecaInfos;
		UpdateAllScreens();
	}

	public void SetInfosGeneral(GeneralInfos generalInfos)
	{
		AutomateAndERPInfos.generalInfos = generalInfos;
		UpdateAllScreens();
	}

	public void SetHeureDebut(DateTime dateTime)
	{
		AutomateAndERPInfos.heuresInfos.heureDebut = dateTime;
	}

	public void SetHeure(DateTime dateTime)
	{
		AutomateAndERPInfos.heuresInfos.heureCourante = dateTime;
		dateTime = dateTime.AddHours(-1);
		AutomateAndERPInfos.heuresInfos.heureDerniere = dateTime;
	}		
#endif
}
