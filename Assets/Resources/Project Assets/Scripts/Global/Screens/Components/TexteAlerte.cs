﻿using UnityEngine;
using System.Collections;
using Utils;

/// <summary>
/// Un texte d'alerte
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class TexteAlerte : TexteAlertITween
{

}

/// <summary>
/// Texte d'alerte utilisant iTween pour capter l'attention
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public abstract class TexteAlertITween : TexteAlerteBase
{
	float y;
	public Transform target;
	public bool changeColor = false;

	protected override void OnInit()
	{
		y = this.transform.position.y;

		iTween.MoveTo(gameObject, iTween.Hash("position", target.position, "easeType", "easeInBounce", "loopType", "pingPong", "oncomplete", "OnITweenCompleted"));
		if(this.stateOk)
			iTween.Pause(gameObject);
	}

	private void OnITweenCompleted()
	{
		if (Mathf.Abs(this.transform.position.y - y) > 0.001f)
		{
			// En haut.. On laisse tomber.. littéralement.
		}
		else
		{
			// En bas.
			if(this.stateOk)
				iTween.Pause(gameObject);
		}
	}
	
	protected override void OnUpdate()
	{

	}

	protected override void OnSet()
	{
		if (textMesh)
		{
			if(changeColor)
				this.textMesh.color = (this.stateOk) ? ScreensManager.Green : ScreensManager.Red;
			this.textMesh.text = txt;

			if (!this.stateOk)
			{
				iTween.Resume(gameObject);
			}
		}
	}
}

/// <summary>
/// Texte d'alerte utilisant une courbe d'aninamtion pour capter l'attention
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
[System.Obsolete("Cette classe n'a pas été testée depuis longtemps")]
public abstract class TexteAlerteNotif : TexteAlerteBase
{
	public AnimationCurve NotificationCurve;

	private float t;
	private float duration = 1;

	public float minHeight;
	public float maxHeight;
		
	protected override void OnInit()
	{
		t = 0;
	}

	protected override void OnUpdate()
	{
		t += Time.deltaTime;
		float ratio = Mathf.Clamp01(t / duration);

		float y = this.NotificationCurve.Evaluate(ratio);
		y = Mathf.Lerp(minHeight, maxHeight, y);
		
		Vector3 pos = this.transform.localPosition;
		pos.y = y;
		this.transform.localPosition = pos;

		if (t == 1 && !this.stateOk)
		{
			t = 0;
		}
	}

	protected override void OnSet()
	{
		if (textMesh)
		{
			this.textMesh.text = txt;
		}
	}
}

/// <summary>
/// Texte d'alerte qui se zoome et se dézoome pour capter l'attention
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
[System.Obsolete("Cette classe n'a pas été testée depuis longtemps")]
public abstract class TexteAlerteScale : TexteAlerteBase
{
	private Vector3 originalPosition;
	private TextAnchor originalAnchor;

	public float speed = -1;
	public float min = 0.5f;
	public float max = 1.25f;
	
	protected override void OnInit()
	{
		if (textMesh)
		{
			this.originalPosition = this.transform.position;
			this.originalAnchor = this.textMesh.anchor;
		}
	}

	private IEnumerator Replace()
	{
		float oldSpeed = this.speed; 
		float oldScale = this.transform.localScale.x;

		this.transform.localScale = Vector3.one * max;
		this.speed = 0;

		this.textMesh.text = txt;			
		this.transform.position = this.originalPosition;
		this.textMesh.anchor = this.originalAnchor;

		yield return null; // Wait redraw.

		Rect area = Area.BoundsToRect(this.GetComponent<Renderer>().bounds);

		Vector3 newPos = originalPosition;
		newPos.x = area.center.x;
		this.transform.position = newPos;

		this.textMesh.anchor = TextAnchor.MiddleCenter;

		this.transform.localScale = Vector3.one * oldScale;
		this.speed = oldSpeed;
	}

	protected override void OnUpdate()
	{
		if (this.textMesh != null && this.textMesh.text != txt)
		{
			this.StopCoroutine("Replace");
			this.StartCoroutine("Replace");
		}

		float x = this.transform.localScale.x;
		
		if (!stateOk)
		{
			float t = Mathf.InverseLerp(min, max, x);

			t = Mathf.Clamp01(t + speed * Time.deltaTime);

			if (t == 0)
				speed = Mathf.Abs(speed);
			else if(t == 1)
				speed = -Mathf.Abs(speed);

			x = Mathf.Lerp(min, max, t);
		}
		else
		{
			x = Mathf.Lerp(x, 1, Mathf.Abs(speed) * Time.deltaTime);
		}

		this.transform.localScale = Vector3.one * x;
	}

	protected override void OnSet()
	{

	}
}

/// <summary>
/// Classe de base que les textes d'alerte devraient implémenter/// 
/// </summary>
/// <para>Consiste en un texte coloré de manière positive, négative ou bien neutre (si dans un contexte déjà coloré)</para>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
[RequireComponent(typeof(TextMesh))]
public abstract class TexteAlerteBase : MonoBehaviour
{
	protected TextMesh textMesh;
	protected string txt;
	protected bool stateOk;

	public void Set(bool newState, string txt)
	{
		this.stateOk = newState;
		this.txt = txt;

		this.OnSet();
	}

	protected abstract void OnInit();
	protected abstract void OnSet();
	protected abstract void OnUpdate();

	private void Init()
	{
		textMesh = this.GetComponent<TextMesh>();
		this.OnInit();
	}

	private void Update()
	{
		if (textMesh == null)
		{
			this.Init();
		}

		this.OnUpdate();
	}
}
