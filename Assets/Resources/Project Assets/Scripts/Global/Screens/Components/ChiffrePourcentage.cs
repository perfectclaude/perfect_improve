﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Un chiffre avec un pourcentage
/// </summary>
/**
 * @author Aurélien Levêque <aleveque@manzalab.com>
 */
public class ChiffrePourcentage : Chiffre {
	
	public override string suffixe()
	{
		return "%";
	}
}
