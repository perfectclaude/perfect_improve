﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Une donnée numérique colorée positivement ou négativement (rouge/vert)
/// </summary>
/**
 * @author Aurélien Levêque <aleveque@manzalab.com>
 */
public class Chiffre : MonoBehaviour {
	
	/// <summary>
	/// Nombre affiché
	/// </summary>
	public TextMesh	nombreTextMesh;

	/// <summary>
	/// Vrai si l'on doit changer la couleur du chiffre selon son état
	/// </summary>
	public bool changeColor;

	/// <summary>
	/// Retourne vrai si l'état est positif.
	/// </summary>
	public bool good { get; private set; }
		
	/// <summary>
	/// Retourne le suffixe à ajouter au chiffre.
	/// </summary>
	/// <returns>Suffixe à ajouter au chiffre</returns>
	public virtual string suffixe()
	{
		return "";
	}
	
	/// <summary>
	/// Met à jour le chiffre.
	/// </summary>
	/// <param name="nombre">Nouvelle valeur</param>
	/// <param name="good">Nouvel état. Vrai si bon, Faux si mauvais</param>
	public virtual void Set(int nombre, bool good)
	{
		this.good = good;
		this.nombreTextMesh.text = nombre + suffixe();

		if (changeColor)
		{
			ChangeColor(good ? ScreensManager.Green : ScreensManager.Red);
		}
	}
	
	/// <summary>
	/// Change la couleur du chiffre
	/// </summary>
	/// <param name="color">Couleur à utiliser</param>
	public virtual void ChangeColor(Color color)
	{
		if (changeColor)
		{
			nombreTextMesh.color = color;
		}
	}
}
