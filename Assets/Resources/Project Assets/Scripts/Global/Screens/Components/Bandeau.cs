﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Un bandeau coloré en rouge/vert avec un picto positif/négatif
/// </summary>
/**
 * @author Aurélien Levêque <aleveque@manzalab.com>
 */
public class Bandeau : MonoBehaviour {

	/// <summary>
	/// Picto positif
	/// </summary>
	public GameObject pictoOK;

	/// <summary>
	/// Picto négatif
	/// </summary>
	public GameObject pictoNOK;

	/// <summary>
	/// Renderer du bandeau.
	/// </summary>
	/// <remarks>Le matériau utilisé doit avoir la propriété de couleur '_TintColor'.</remarks>
	public Renderer	colorBandeauRenderer;

	/// <summary>
	/// Modifie la couleur du bandeau
	/// </summary>
	/// <param name="color">Couleur à utiliser</param>
	public void ChangeColor(Color color)
	{
		colorBandeauRenderer.material.SetColor("_TintColor", color);
	}
	
	/// <summary>
	/// Règle l'état du bandeau
	/// </summary>
	/// <param name="ok">Vrai si positif. Faux si négatif</param>
	public void Set(bool ok)
	{
		if(ok)
			ChangeColor(ScreensManager.Green);
		else
			ChangeColor(ScreensManager.Red);

		if (pictoOK && pictoNOK)
		{
			pictoOK.SetActive(ok);
			pictoNOK.SetActive(!ok);
		}
	}
}
