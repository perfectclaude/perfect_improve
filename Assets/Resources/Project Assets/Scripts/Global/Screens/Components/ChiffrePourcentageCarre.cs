﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Un chiffre avec un pourcentage dans un carré à taille dynamique coloré de manière positive ou négative (vert/rouge)
/// </summary>
/**
 * @author Aurélien Levêque <aleveque@manzalab.com>
 */
public class ChiffrePourcentageCarre : ChiffrePourcentage {
	
	/// <summary>
	/// Carré comportant le chiffre.
	/// </summary>
	public Renderer	colorCarreRenderer;
	
	/// <summary>
	/// Change la couleur du carré mais pas du chiffre.
	/// </summary>
	/// <param name="color">Couleur du carré à règler</param>
	public override void ChangeColor(Color color)
	{
		colorCarreRenderer.material.SetColor("_TintColor", color);
	}
	
	public override void Set(int nombre, bool good)
	{
		base.Set(nombre, good);
		
		float scale = Mathf.Clamp((float)nombre/100f, 0.44f,1.0f);
		transform.localScale = new Vector3(scale,scale,1f);
	}
}
