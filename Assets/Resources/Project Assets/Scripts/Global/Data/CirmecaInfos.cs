﻿/// <summary>
/// Données affichées relatives à Cirmeca
/// </summary>
/**
 * @author Aurelien Levêque <aleveque@manzalab.com>
 */
public struct CirmecaInfos
{
	/// <summary>
	/// Nombre de pièces produites
	/// </summary>
	public int bonnePieces;

	/// <summary>
	/// Cadence de production de pièces
	/// </summary>
	public int cadence;

	/// <summary>
	/// Rendement de la production
	/// </summary>
	public int rendement;

	/// <summary>
	/// Nombre de pièces réalisées à l'heure
	/// </summary>
	public int piecesParHeure;

	/// <summary>
	/// Cadence ne prenant en compte que la dernière période d'une heure
	/// </summary>
	public int cadenceDerniereHeure;

	/// <summary>
	/// Rendement ne prenant en compte que la dernière période d'une heure
	/// </summary>
	public int rendementDerniereHeure;

	/// <summary>
	/// Vrai si la cadence actuelle est correcte. Faux sinon.
	/// </summary>
	public bool cadenceRendementOK;

	/// <summary>
	/// Vrai si la cadence ne prenant en compte que la dernière période d'une heure est correcte. Faux sinon.
	/// </summary>
	public bool cadenceRendementDerniereHeureOK;

	/// <summary>
	/// Vrai si l'état général de la machine est correct. Faux sinon
	/// </summary>
	public bool etatOK;

	/// <summary>
	/// Texte d'alerte pricipal à afficher
	/// </summary>
	public string texteAlerte;
}