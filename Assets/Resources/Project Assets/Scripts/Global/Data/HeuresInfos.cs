﻿using System;

/// <summary>
/// Données affichées relatives à l'heure
/// </summary>
/**
 * @author Aurelien Levêque <aleveque@manzalab.com>
 */
public struct HeuresInfos
{
	public DateTime heureDebut;
	public DateTime heureCourante;
	public DateTime heureDerniere;
}