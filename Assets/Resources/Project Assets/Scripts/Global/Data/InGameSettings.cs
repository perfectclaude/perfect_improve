﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Security.Cryptography;
using System;
using System.Collections.Generic;

/// <summary>
/// Réglages parsés sur le serveur et modifiables pendant l'execution
/// Envoyés à la télécommande.
/// </summary>
/**
 * @author Sylvain Lafon <slafon@manzalab.com>
 */
public class InGameSettings 
{
	public string	SHA1Password					{ get; protected set; }
	public string	PasswordNextScreen				{ get; protected set; }

	#region Variables
	public int		CadenceStandardCirmeca			{ get; protected set; }
	public int		CadenceStandardMAXCirmeca		{ get; protected set; }
	public int		CadenceStandardRobot			{ get; protected set; }
	public int		CadenceStandardMAXRobot			{ get; protected set; }
	public int		LimiteControleInferieureCirmeca	{ get; protected set; }
	public int		LimiteControleInferieureRobot	{ get; protected set; }
	public float	TauxQualiteStandardRobot		{ get; protected set; }
	public int		MinStockManoeuvre				{ get; protected set; }
	public int		MaxStockManoeuvre				{ get; protected set; }
	public int		MinStockPF						{ get; protected set; }
	public int		MaxStockPF						{ get; protected set; }
	public float	TauxQualiteAirEau				{ get; protected set; }
	public string	CadenceOK						{ get; protected set; }
	public string	CadenceJ_BAD					{ get; protected set; }
	public string	CadenceH_BAD					{ get; protected set; }
	public string	CadenceBAD						{ get; protected set; }
	public string	AlertSeparator					{ get; protected set; }
	public string	QualiteOK						{ get; protected set; }
	public string	QualiteJ_BAD					{ get; protected set; }
	public string	QualiteH_BAD					{ get; protected set; }
	public string	QualiteBAD						{ get; protected set; }
	public string	StockMangHIGH					{ get; protected set; }
	public string	StockMangLOW					{ get; protected set; }
	public string	StockMangOK						{ get; protected set; }
	public string	StockPFHIGH						{ get; protected set; }
	public string	StockPFLOW						{ get; protected set; }
	public string	StockPFOK						{ get; protected set; }
	public string	RejetAirEauHIGH					{ get; protected set; }
	public string	RejetAirEauOK					{ get; protected set; }
	#endregion

	public InGameSettings()
	{
		SHA1Password					= string.Empty;
		PasswordNextScreen				= "Scene_atelier";

		CadenceStandardCirmeca			= (100);
		CadenceStandardMAXCirmeca		= (130);
		LimiteControleInferieureCirmeca = (95);
		CadenceStandardRobot			= (110);
		CadenceStandardMAXRobot			= (140);
		LimiteControleInferieureRobot	= (105);
		TauxQualiteStandardRobot		= (0.95f);
		MinStockManoeuvre				= (1000);
		MaxStockManoeuvre				= (4000);
		MinStockPF						= (3000);
		MaxStockPF						= (6000);
		TauxQualiteAirEau				= (0.95f);
		CadenceOK						= ("");
		CadenceJ_BAD					= ("Cadence journalière insuffisante");
		CadenceH_BAD					= ("Cadence dernière heure insuffisante");
		CadenceBAD						= ("Cadence insuffisante");
		AlertSeparator					= (" et ");
		QualiteOK						= ("");
		QualiteJ_BAD					= ("qualité journalière insuffisante");
		QualiteH_BAD					= ("qualité dernière heure insuffisante");
		QualiteBAD						= ("qualité insuffisante");
		StockMangHIGH					= ("Stock excessif");
		StockMangLOW					= ("Stock insuffisant");
		StockMangOK						= ("");
		StockPFHIGH						= ("Stock excessif");
		StockPFLOW						= ("Stock insuffisant");
		StockPFOK						= ("");
		RejetAirEauHIGH					= ("Taux excessif");
		RejetAirEauOK					= ("");
	}

	protected void LoadVariablesFrom(IniFile file)
	{
		// +------------------------------+-----------------+-------------------------------+---------+------------------------------------+	//
		// |		  VARIABLE			   |	  CLASS		 |			  PROPERTY			  |	 TYPE	|	  DEFAULT VALUE (or Mandatory)	  |	//
		// +------------------------------+-----------------+-------------------------------+---------+------------------------------------+	//

			CadenceStandardCirmeca			= file["Cirmeca"]	["CadenceStandard"]				.ToInt		(100);
			CadenceStandardMAXCirmeca		= file["Cirmeca"]	["CadenceStandardMax"]			.ToInt		(130);
			LimiteControleInferieureCirmeca = file["Cirmeca"]	["LimiteControleInferieure"]	.ToInt		(95);
			CadenceStandardRobot			= file["Robot"]		["CadenceStandard"]				.ToInt		(110);
			CadenceStandardMAXRobot			= file["Robot"]		["CadenceStandardMax"]			.ToInt		(140);
			LimiteControleInferieureRobot	= file["Robot"]		["LimiteControleInferieure"]	.ToInt		(105);
			TauxQualiteStandardRobot		= file["Robot"]		["TauxQualiteStandard"]			.ToFloat	(0.95f);
			MinStockManoeuvre				= file["Stocks"]	["MinimumManoeuvres"]			.ToInt		(1000);
			MaxStockManoeuvre				= file["Stocks"]	["MaximumManoeuvres"]			.ToInt		(4000);
			MinStockPF						= file["Stocks"]	["MinimumProduitsFinis"]		.ToInt		(3000);
			MaxStockPF						= file["Stocks"]	["MaximumProduitsFinis"]		.ToInt		(6000);
			TauxQualiteAirEau				= file["Stocks"]	["TauxQualiteAirEauStandard"]	.ToFloat	(0.95f);
			CadenceOK						= file["Messages"]	["CadencesBonnes"]				.ToString	("");
			CadenceJ_BAD					= file["Messages"]	["CadenceJourneeMauvaise"]		.ToString	("Cadence journalière insuffisante");
			CadenceH_BAD					= file["Messages"]	["CadenceHeureMauvaise"]		.ToString	("Cadence dernière heure insuffisante");
			CadenceBAD						= file["Messages"]	["CadencesMauvaises"]			.ToString	("Cadence insuffisante");
			AlertSeparator					= file["Messages"]	["SeparationMessage"]			.ToString	(" et ");
			QualiteOK						= file["Messages"]	["QualitesBonnes"]				.ToString	("");
			QualiteJ_BAD					= file["Messages"]	["QualiteJourneeMauvaise"]		.ToString	("qualité journalière insuffisante");
			QualiteH_BAD					= file["Messages"]	["QualiteHeureMauvaise"]		.ToString	("qualité dernière heure insuffisante");
			QualiteBAD						= file["Messages"]	["QualitesMauvaises"]			.ToString	("qualité insuffisante");
			StockMangHIGH					= file["Messages"]	["StockManoeuvreExcessif"]		.ToString	("Stock excessif");
			StockMangLOW					= file["Messages"]	["StockManoeuvreInsuffisant"]	.ToString	("Stock insuffisant");
			StockMangOK						= file["Messages"]	["StockManoeuvreCorrect"]		.ToString	("");
			StockPFHIGH						= file["Messages"]	["StockPFExcessif"]				.ToString	("Stock excessif");
			StockPFLOW						= file["Messages"]	["StockPFInsuffisant"]			.ToString	("Stock insuffisant");
			StockPFOK						= file["Messages"]	["StockPFCorrect"]				.ToString	("");
			RejetAirEauHIGH					= file["Messages"]	["TauxQualiteAirEauExcessif"]	.ToString	("Taux excessif");
			RejetAirEauOK					= file["Messages"]	["TauxQualiteAirEauCorrect"]	.ToString	("");
	}
	
	public virtual void ChangeSetting(string name, string value)
	{
		switch (name)
		{
			case "Password/SHA1":
				SHA1Password = value;
				break;

			case "Password/NextScreen":
				PasswordNextScreen = value;
				break;

			case "Cirmeca/CadenceStandard":
				CadenceStandardCirmeca = int.Parse(value);
				break;

			case "Cirmeca/CadenceStandardMax":
				CadenceStandardMAXCirmeca = int.Parse(value);
				break;

			case "Cirmeca/LimiteControleInferieure":
				LimiteControleInferieureCirmeca = int.Parse(value);
				break;

			case "Robot/CadenceStandard":
				CadenceStandardRobot = int.Parse(value);
				break;

			case "Robot/CadenceStandardMax":
				CadenceStandardMAXRobot = int.Parse(value);
				break;

			case "Robot/LimiteControleInferieure":
				LimiteControleInferieureRobot = int.Parse(value);
				break;

			case "Robot/TauxQualiteStandard":
				TauxQualiteStandardRobot = float.Parse(value);
				break;

			case "Stocks/MinimumManoeuvres":
				MinStockManoeuvre = int.Parse(value);
				break;

			case "Stocks/MaximumManoeuvres":
				MaxStockManoeuvre = int.Parse(value);
				break;

			case "Stocks/MinimumProduitsFinis":
				MinStockPF = int.Parse(value);
				break;

			case "Stocks/MaximumProduitsFinis":
				MaxStockPF = int.Parse(value);
				break;

			case "Stocks/TauxQualiteAirEauStandard":
				TauxQualiteAirEau = float.Parse(value);
				break;

			case "Messages/CadencesBonnes":
				CadenceOK = value;
				break;

			case "Messages/CadenceJourneeMauvaise":
				CadenceJ_BAD = value;
				break;

			case "Messages/CadenceHeureMauvaise":
				CadenceH_BAD = value;
				break;

			case "Messages/CadencesMauvaises":
				CadenceBAD = value;
				break;

			case "Messages/SeparationMessage":
				AlertSeparator = value;
				break;

			case "Messages/QualitesBonnes":
				QualiteOK = value;
				break;

			case "Messages/QualiteJourneeMauvaise":
				QualiteJ_BAD = value;
				break;

			case "Messages/QualiteHeureMauvaise":
				QualiteH_BAD = value;
				break;

			case "Messages/QualitesMauvaises":
				QualiteBAD = value;
				break;

			case "Messages/StockManoeuvreExcessif":
				StockMangHIGH = value;
				break;

			case "Messages/StockManoeuvreInsuffisant":
				StockMangLOW = value;
				break;

			case "Messages/StockManoeuvreCorrect":
				StockMangOK = value;
				break;

			case "Messages/StockPFExcessif":
				StockPFHIGH = value;
				break;

			case "Messages/StockPFInsuffisant":
				StockPFLOW = value;
				break;

			case "Messages/StockPFCorrect":
				StockPFOK = value;
				break;

			case "Messages/TauxQualiteAirEauExcessif":
				RejetAirEauHIGH = value;
				break;

			case "Messages/TauxQualiteAirEauCorrect":
				RejetAirEauOK = value;
				break;
		}
	}

	public static string SHA1(string str)
	{
		byte[] buffer = Encoding.UTF8.GetBytes(str);
		SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
		return BitConverter.ToString(cryptoTransformSHA1.ComputeHash(buffer)).Replace("-", "");
	}

	public Dictionary<string, string> ToDictionary()
	{
		Dictionary<string, string> result = new Dictionary<string, string>();

		result.Add("Password/SHA1", SHA1Password);
		result.Add("Cirmeca/CadenceStandard", CadenceStandardCirmeca.ToString());
		result.Add("Cirmeca/CadenceStandardMax", CadenceStandardMAXCirmeca.ToString());
		result.Add("Cirmeca/LimiteControleInferieure", LimiteControleInferieureCirmeca.ToString());
		result.Add("Robot/CadenceStandard", CadenceStandardRobot.ToString());
		result.Add("Robot/CadenceStandardMax", CadenceStandardMAXRobot.ToString());
		result.Add("Robot/LimiteControleInferieure", LimiteControleInferieureRobot.ToString());
		result.Add("Robot/TauxQualiteStandard", TauxQualiteStandardRobot.ToString("F3"));
		result.Add("Stocks/MinimumManoeuvres", MinStockManoeuvre.ToString());
		result.Add("Stocks/MaximumManoeuvres", MaxStockManoeuvre.ToString());
		result.Add("Stocks/MinimumProduitsFinis", MinStockPF.ToString());
		result.Add("Stocks/MaximumProduitsFinis", MaxStockPF.ToString());
		result.Add("Stocks/TauxQualiteAirEauStandard", TauxQualiteAirEau.ToString("F3"));
		result.Add("Messages/CadencesBonnes", CadenceOK);
		result.Add("Messages/CadenceJourneeMauvaise", CadenceJ_BAD);
		result.Add("Messages/CadenceHeureMauvaise", CadenceH_BAD);
		result.Add("Messages/CadencesMauvaises", CadenceBAD);
		result.Add("Messages/SeparationMessage", AlertSeparator);
		result.Add("Messages/QualitesBonnes", QualiteOK);
		result.Add("Messages/QualiteJourneeMauvaise", QualiteJ_BAD);
		result.Add("Messages/QualiteHeureMauvaise", QualiteH_BAD);
		result.Add("Messages/QualitesMauvaises", QualiteBAD);
		result.Add("Messages/StockManoeuvreExcessif", StockMangHIGH);
		result.Add("Messages/StockManoeuvreInsuffisant", StockMangLOW);
		result.Add("Messages/StockManoeuvreCorrect", StockMangOK);
		result.Add("Messages/StockPFExcessif", StockPFHIGH);
		result.Add("Messages/StockPFInsuffisant", StockPFLOW);
		result.Add("Messages/StockPFCorrect", StockPFOK);
		result.Add("Messages/TauxQualiteAirEauExcessif", RejetAirEauHIGH);
		result.Add("Messages/TauxQualiteAirEauCorrect", RejetAirEauOK);
			
		return result;
	}

	public void Reset()
	{
		SHA1Password = string.Empty;
		PasswordNextScreen = "Scene_atelier";
	}
}
