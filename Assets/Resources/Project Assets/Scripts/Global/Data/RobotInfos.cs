﻿using UnityEngine;

/// <summary>
/// Données affichées relatives au Robot
/// </summary>
/**
 * @author Aurelien Levêque <aleveque@manzalab.com>
 */
public struct RobotInfos
{
	public int bonnePieces;
	public int coupures;

	public int cadence;
	public int rendement;
	public int qualite;

	public int piecesParHeure;
	public int rejet;
	public int piecesARetraiter;

	public int cadenceDerniereHeure;
	public int rendementDerniereHeure;
	public int qualiteDerniereHeure;
	
	public bool coupuresOK;
	public bool cadenceRendementOK;
	public bool qualiteRejetOK;
	public bool cadenceRendementDerniereHeureOK;
	public bool qualiteDerniereHeureOK;

	public bool popupAffichee;
	public string messagePopup;

	public bool etatOK;
	public string texteAlerte;

	public override string ToString()
	{
		string str = "Robot infos\n";

		str += "Bonnes pieces"				+ " = " + bonnePieces				+ "\n";
		str += "Coupures"					+ " = " + coupures					+ "\n";
		str += "Cadence"					+ " = " + cadence					+ "\n";
		str += "Rendement"					+ " = " + rendement					+ "\n";
		str += "Qualite"					+ " = " + qualite					+ "\n";
		str += "Pieces/Heure"				+ " = " + piecesParHeure			+ "\n";
		str += "Rejet"						+ " = " + rejet						+ "\n";
		str += "Pieces à retraiter"			+ " = " + piecesARetraiter			+ "\n";
		str += "Cadence derniere heure"		+ " = " + cadenceDerniereHeure		+ "\n";
		str += "Rendement derniere heure"	+ " = " + rendementDerniereHeure	+ "\n";
		str += "Qualite derniere heure"		+ " = " + qualiteDerniereHeure		+ "\n";		
		str += "OK"							+ " = " + etatOK					+ "\n";
		str += "Alerte"						+ " = " + texteAlerte				+ "\n";

		return str;
	}
}