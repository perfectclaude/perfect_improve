﻿/// <summary>
/// Données affichées sur l'écran ERP
/// </summary>
/**
 * @author Aurelien Levêque <aleveque@manzalab.com>
 */
public struct GeneralInfos
{
	public int produitsFinis;
	public int composants;
	public int piecesFuyardes;

    public int produitsFinisAffecte;  
    public int composantsAffecte;     

	public bool produitsFinisOK;
	public bool composantsOK;
	public bool piecesFuyardesOK;

	public string produitsFinisAlerte;
	public string composantsAlerte;
	public string piecesFuyardsAlerte;
}
