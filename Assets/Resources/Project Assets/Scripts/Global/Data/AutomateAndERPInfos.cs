﻿// 
// Cadence réelle calculée = (Nombre de pièces bonnes / Temps en heure décimale)
// Taux de Cadence = (Cadence réelle calculée / Cadence standard)					--> No clamp
// Taux de Rendement = (Cadence réelle calculée / Cadence standard 100% (MAX))		--> No clamp
// Taux de Qualité = (Nombre de pièces bonnes / (Nombre de pièces bonnes + Nombre de pièces mauvaises))
// qualité pas sur cirmeca.
// 

using B = Utils.Network.Binary;
using System.Collections.Generic;
using System;
using System.Text;

/// <summary>
/// Classe regroupant les données affichées sur les différents devices et le serveur.
/// Ce sont ses données qui sont envoyées aux différents devices.
/// </summary>
/**
 * @author Partie affichage : Aurelien Levêque <aleveque@manzalab.com>
 * @author Partie réseau    : Sylvain Lafon <slafon@manzalab.com>
 */
public static class AutomateAndERPInfos 
{
	/// <summary>
	/// Comporte les données relatives à Robot
	/// </summary>
	public static RobotInfos 	robotInfos;

	/// <summary>
	/// Comporte les données relatives à Cirmeca
	/// </summary>
	public static CirmecaInfos	cirmecaInfos;

	/// <summary>
	/// Comporte les données relatives à l'ERP
	/// </summary>
	public static GeneralInfos	generalInfos;

	/// <summary>
	/// Comporte les données relatives à l'heure
	/// </summary>
	public static HeuresInfos	heuresInfos;

	/// <summary>
	/// Ajoute au paquet p une chaîne de caractère à longueur non fixée au préalable
	/// </summary>
	/// <para>Le paquet résultant aura donc une taille variable.</para>
	/// <para>La fonction rajoute au paquet la taille de la chaîne puis la chaine de caractères en UTF-8</para>
	/// <param name="p">Paquet en cours de création</param>
	/// <param name="txt">Chaîne à insérer</param>
	private static void AddVariableText(PackageComposer p, string txt)
	{
		byte[] bTxt;

		if (string.IsNullOrEmpty(txt))
		{
			p.Add((int)0);
		}
		else
		{
			bTxt = Encoding.UTF8.GetBytes(txt);
			p.Add(bTxt.Length);
			p.Add(bTxt);
		}
	}

	#region Binary (big endian) parser

	/// <summary>
	/// Converti la structure entière en un paquet d'octets
	/// </summary>
	/// <returns>Tableau d'octet parsable avec <code>FromBytes</code></returns>
	public static byte[] GetBytes()
	{
		PackageComposer package = new PackageComposer();

		#region General
		package.Add(generalInfos.produitsFinis);
		package.Add(generalInfos.composants);
        package.Add(generalInfos.produitsFinisAffecte);
        package.Add(generalInfos.composantsAffecte);	
		package.Add(generalInfos.piecesFuyardes);
		package.Add(generalInfos.produitsFinisOK,
					generalInfos.composantsOK,
					generalInfos.piecesFuyardesOK);

		AddVariableText(package, generalInfos.produitsFinisAlerte);
		AddVariableText(package, generalInfos.composantsAlerte);
		AddVariableText(package, generalInfos.piecesFuyardsAlerte);

		#endregion

		#region Heure
		package.Add(heuresInfos.heureDebut);
		package.Add(heuresInfos.heureCourante);
		package.Add(heuresInfos.heureDerniere);
		#endregion

		#region Cirmeca
		package.Add(cirmecaInfos.bonnePieces);
		package.Add(cirmecaInfos.cadence);
		package.Add(cirmecaInfos.rendement);
		package.Add(cirmecaInfos.piecesParHeure);
		package.Add(cirmecaInfos.cadenceDerniereHeure);
		package.Add(cirmecaInfos.rendementDerniereHeure);

		package.Add(cirmecaInfos.cadenceRendementOK,
					cirmecaInfos.cadenceRendementDerniereHeureOK,
					cirmecaInfos.etatOK);

		AddVariableText(package, cirmecaInfos.texteAlerte);
		#endregion

		#region Robot
		package.Add(robotInfos.bonnePieces);
		package.Add(robotInfos.coupures);
		package.Add(robotInfos.cadence);
		package.Add(robotInfos.rendement);
		package.Add(robotInfos.qualite);
		package.Add(robotInfos.piecesParHeure);
		package.Add(robotInfos.rejet);
		package.Add(robotInfos.piecesARetraiter);
		package.Add(robotInfos.cadenceDerniereHeure);
		package.Add(robotInfos.rendementDerniereHeure);
		package.Add(robotInfos.qualiteDerniereHeure);

		package.Add(robotInfos.coupuresOK,
					robotInfos.cadenceRendementOK,
					robotInfos.qualiteRejetOK,
					robotInfos.cadenceRendementDerniereHeureOK,
					robotInfos.qualiteDerniereHeureOK,
					robotInfos.etatOK,
					robotInfos.popupAffichee);

		AddVariableText(package, robotInfos.texteAlerte);
		AddVariableText(package, robotInfos.messagePopup);

		#endregion

		package.InsertAt(package.Size, 0);
		package.InsertHeader();

		return package.Package;
	}

	#region Convertion bytes from network to variable

	/// <summary>
	/// Converti un nombre provenant du réseau de paquet d'octets en int
	/// </summary>
	/// <param name="bytes">Paquet entier lu</param>
	/// <param name="offset">Position de l'entier dans le paquet.</param>
	/// <returns>L'entier lu. La position donnée sera la position située après l'entier dans le paquet</returns>
	private static int GetInt(byte[] bytes, ref int offset)
	{
		int i = BitConverter.ToInt32(B.FromNetworkBigEndian(B.GetPackagePart(bytes, offset, 4)), 0);
		offset += 4;
		return i;
	}

	/// <summary>
	/// Converti un nombre provenant du réseau de paquet d'octets en long
	/// </summary>
	/// <param name="bytes">Paquet entier lu</param>
	/// <param name="offset">Position de l'entier dans le paquet.</param>
	/// <returns>L'entier lu. La position donnée sera la position située après l'entier dans le paquet</returns>
	private static long GetLong(byte[] bytes, ref int offset)
	{
		long l = BitConverter.ToInt64(B.FromNetworkBigEndian(B.GetPackagePart(bytes, offset, 8)), 0);
		offset += 8;
		return l;
	}

	/// <summary>
	/// Converti une date provenant du réseau de paquet d'octets en DateTime
	/// </summary>
	/// <param name="bytes">Paquet entier lu</param>
	/// <param name="offset">Position de la date dans le paquet.</param>
	/// <returns>La date lue. La position donnée sera la position située après la date dans le paquet</returns>
	private static DateTime GetDT(byte[] bytes, ref int offset)
	{
		return DateTime.FromBinary(GetLong(bytes, ref offset));
	}

	/// <summary>
	/// Converti une chaîne de caractère (à taille variable) provenant du réseau de paquet d'octets en string
	/// </summary>
	/// <param name="bytes">Paquet entier lu</param>
	/// <param name="offset">Position de la chaine à taille variable dans le paquet.</param>
	/// <returns>La chaine lue. La position donnée sera la position située après la chaine dans le paquet</returns>
	private static string GetString(byte[] bytes, ref int offset)
	{
		int len = GetInt(bytes, ref offset);

		if (len == 0)
			return string.Empty;

		byte[] bStr = B.FromNetworkBigEndian(B.GetPackagePart(bytes, offset, len));
		offset += len;
		
		return Encoding.UTF8.GetString(bStr, 0, len);
	}
	#endregion

	/// <summary>
	/// Lis un paquet d'octets pour mettre à jour la structure
	/// </summary>
	/// <param name="bytes">Le paquet d'octets</param>
	public static void FromBytes(byte[] bytes)
	{
		int offset = 0;
		byte b;

		#region General Infos
		generalInfos.produitsFinis	        = GetInt(bytes, ref offset);
		generalInfos.composants		        = GetInt(bytes, ref offset);
        generalInfos.produitsFinisAffecte   = GetInt(bytes, ref offset);
        generalInfos.composantsAffecte      = GetInt(bytes, ref offset);
        generalInfos.piecesFuyardes         = GetInt(bytes, ref offset);

		b = bytes[offset];
		generalInfos.produitsFinisOK	= (b & 1) != 0;
		generalInfos.composantsOK		= (b & 2) != 0;
		generalInfos.piecesFuyardesOK	= (b & 4) != 0;
		offset++;

		generalInfos.produitsFinisAlerte	= GetString(bytes, ref offset);
		generalInfos.composantsAlerte		= GetString(bytes, ref offset);
		generalInfos.piecesFuyardsAlerte	= GetString(bytes, ref offset);
		#endregion

		#region Heure Infos
		heuresInfos.heureDebut		= GetDT(bytes, ref offset);
		heuresInfos.heureCourante	= GetDT(bytes, ref offset);
		heuresInfos.heureDerniere	= GetDT(bytes, ref offset);
		#endregion

		#region Cirmeca Infos
		cirmecaInfos.bonnePieces			= GetInt(bytes, ref offset);
		cirmecaInfos.cadence				= GetInt(bytes, ref offset);
		cirmecaInfos.rendement				= GetInt(bytes, ref offset);
		cirmecaInfos.piecesParHeure			= GetInt(bytes, ref offset);
		cirmecaInfos.cadenceDerniereHeure	= GetInt(bytes, ref offset);
		cirmecaInfos.rendementDerniereHeure	= GetInt(bytes, ref offset);

		b = bytes[offset];
		cirmecaInfos.cadenceRendementOK					= (b & 1) != 0;
		cirmecaInfos.cadenceRendementDerniereHeureOK	= (b & 2) != 0;
		cirmecaInfos.etatOK								= (b & 4) != 0;
		offset++;

		cirmecaInfos.texteAlerte = GetString(bytes, ref offset);
		#endregion

		#region Robot Infos
		robotInfos.bonnePieces				= GetInt(bytes, ref offset);
		robotInfos.coupures					= GetInt(bytes, ref offset);
		robotInfos.cadence					= GetInt(bytes, ref offset);
		robotInfos.rendement				= GetInt(bytes, ref offset);
		robotInfos.qualite					= GetInt(bytes, ref offset);
		robotInfos.piecesParHeure			= GetInt(bytes, ref offset);
		robotInfos.rejet					= GetInt(bytes, ref offset);
		robotInfos.piecesARetraiter			= GetInt(bytes, ref offset);
		robotInfos.cadenceDerniereHeure		= GetInt(bytes, ref offset);
		robotInfos.rendementDerniereHeure	= GetInt(bytes, ref offset);
		robotInfos.qualiteDerniereHeure		= GetInt(bytes, ref offset);

		b = bytes[offset];
		robotInfos.coupuresOK							= (b & 01) != 0;
		robotInfos.cadenceRendementOK					= (b & 02) != 0;
		robotInfos.qualiteRejetOK						= (b & 04) != 0;
		robotInfos.cadenceRendementDerniereHeureOK		= (b & 08) != 0;
		robotInfos.qualiteDerniereHeureOK				= (b & 16) != 0;
		robotInfos.etatOK								= (b & 32) != 0;
		robotInfos.popupAffichee						= (b & 64) != 0;
		offset++;

		robotInfos.texteAlerte = GetString(bytes, ref offset);
		robotInfos.messagePopup = GetString(bytes, ref offset);
		#endregion
	}
#endregion
}
