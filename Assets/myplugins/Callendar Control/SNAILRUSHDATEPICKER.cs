using UnityEngine;
using System.Collections;
using System; //Unity does not add this by default when creating a new script
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Globalization;


public class SNAILRUSHDATEPICKER : MonoBehaviour 
{
	public static bool DatePicker = false;

	public GUISkin skin;
	public Rect CallendarRectwindow; //= new Rect(0, 0, 574,265);
	private string[] days;

//	private Rect closerect;
    public string monthName;
    private string[,] CustomCallendar;
	public bool AutoCulture;
	public string CallendarCulture;
	public string[] CustomdaysText;
	private int SelectCustomselectedday;
	public int DayNamesChars;
	public bool UpperCase;
	public Texture2D greytext;
	Rect fullscreen;

	public static int selectedmonth;
	public static int selectedday;
	public static int selectedrealday;
	public static int currentmonth=System.DateTime.Now.Month;
	public static int currentyear=System.DateTime.Now.Year; 
	
	GUIStyle mystyle;

	void Start()
	{
		fullscreen = new Rect(-1,-1,Screen.width+1, Screen.height+1);
		CallendarRectwindow.x = (Screen.width - CallendarRectwindow.width)/2;
		CallendarRectwindow.y = (Screen.height - CallendarRectwindow.height)/2;

		mystyle = GUIStyle.none;
		mystyle.alignment = TextAnchor.MiddleCenter;
		mystyle.fontSize = 20;
		mystyle.fontStyle = FontStyle.Bold;
		mystyle.normal.textColor = Color.white;
		mystyle.hover.textColor = Color.grey;

		if (AutoCulture){
			for (int i =1; i<=7;++i)
			{
				Debug.Log (i);
				if (i==7){
					if (UpperCase){
					CustomdaysText[i-1]=CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0].ToString().Substring(0,DayNamesChars).ToUpper();
					}
					else{
					CustomdaysText[i-1]=CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0].ToString().Substring(0,DayNamesChars);
					}
					}
				else{	
					if (UpperCase){
					CustomdaysText[i-1]=CultureInfo.CurrentCulture.DateTimeFormat.DayNames[i].ToString().Substring(0,DayNamesChars).ToUpper();
					}
					else
					{
					CustomdaysText[i-1]=CultureInfo.CurrentCulture.DateTimeFormat.DayNames[i].ToString().Substring(0,DayNamesChars);
					}
				}
			}

    //Culture of the user
    monthName = new DateTime(currentyear, currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CurrentUICulture);
			}
			else
			{
	monthName = new DateTime(currentyear, currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CreateSpecificCulture(CallendarCulture));
		}
	CustomCallendar = CreateCallendar(currentmonth, currentyear);
   	selectedmonth=currentmonth;
	days= new string [42];
	int k =0;
		for (int i = 0; i < 6; ++i)
            {
               for (int j = 0; j < 7; ++j)
                {
			    days[k]= CustomCallendar[i, j];
				if (CustomCallendar[i, j]!=" "){
				if (currentmonth==DateTime.Now.Month && currentyear==DateTime.Now.Year && DateTime.Now.Day==System.Convert.ToInt16(CustomCallendar[i, j])){
					selectedday=k;
				}
				}
				k+=1;
                }
            }
	}

	void Update()
	{
		fullscreen.width = Screen.width+1;
		fullscreen.height = Screen.height+1;
		CallendarRectwindow.x = (Screen.width - CallendarRectwindow.width)/2;
		CallendarRectwindow.y = (Screen.height - CallendarRectwindow.height)/2;
	}

	void OnGUI () 
	{
		if (DatePicker)
		{
			GUI.DrawTexture(fullscreen,greytext);
			GUI.skin=skin;
			CallendarRectwindow = GUI.Window(26, CallendarRectwindow, HISTORYWINDOWDATA,"","datepickerwindow");
			GUI.BringWindowToFront(26);
		}
	}
	
	void HISTORYWINDOWDATA(int windowID){
		GUILayout.BeginHorizontal();		
		//backbutton
		if(GUILayout.Button("","datepickerarrowback",GUILayout.MaxWidth(40))){
		if (currentmonth > 1)
            {
                currentmonth -= 1;
            }
            else
            {
                currentmonth = 12;
                currentyear -= 1;
            }
			
			if (AutoCulture){
            //Culture of the user
            monthName = new DateTime(currentyear, currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CurrentUICulture);
			}
			else
			{
			//Custom Culture
            monthName = new DateTime(currentyear, currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CreateSpecificCulture(CallendarCulture));
			}
			CustomCallendar = CreateCallendar(currentmonth, currentyear);
            selectedmonth=currentmonth;
			selectedday=0;	
			days= new string [42];
			int k =0;
			for (int i = 0; i < 6; ++i)
            {
                for (int j = 0; j < 7; ++j)
                {
//					if (k<3) Debug.Log (CustomCallendar[i, j]);
                	    days[k]= CustomCallendar[i, j];
					if (CustomCallendar[i, j]!=" ")
					{
						if (currentmonth==DateTime.Now.Month && currentyear==DateTime.Now.Year && DateTime.Now.Day==System.Convert.ToInt16(CustomCallendar[i, j]))
						{
							selectedday=k;
//							Debug.Log ("Selected" + k);
						}
					}
					k+=1;
                }
            }
	
		}
		GUILayout.Label(monthName + " ("+currentyear+")","datepickeaarseleection");
		
		//nextbutton
		if(GUILayout.Button("","datepickerarrowfront",GUILayout.MaxWidth(40))){
		if (currentmonth < 12)
            {
                currentmonth += 1;
            }
            else
            {
                currentmonth = 1;
                currentyear += 1;
            }
            
            //Culture of the user
			if (AutoCulture){
           		monthName = new DateTime(currentyear, currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CurrentUICulture);
					}
				else
					{
				//Custom Culture
            	monthName = new DateTime(currentyear, currentmonth, 1).ToString("MMMMMMMMMMMMMMMMM", CultureInfo.CreateSpecificCulture(CallendarCulture));
					}
			CustomCallendar = CreateCallendar(currentmonth, currentyear);
    		selectedmonth=currentmonth;
			selectedday=0;
			days= new string [42];
			int k =0;
			for (int i = 0; i < 6; ++i)
            {
                for (int j = 0; j < 7; ++j)
                {
				//if (k<3) Debug.Log (CustomCallendar[i, j]);
                    days[k]= CustomCallendar[i, j];
				if (CustomCallendar[i, j]!=" "){
				if (currentmonth==DateTime.Now.Month && currentyear==DateTime.Now.Year && DateTime.Now.Day==System.Convert.ToInt16(CustomCallendar[i, j])){
					selectedday=k;
				}
				}
				k+=1;
                }
            }
	
		}
		GUILayout.EndHorizontal();

		GUILayout.Space(1);
		SelectCustomselectedday=-1;
	    SelectCustomselectedday = GUILayout.SelectionGrid(SelectCustomselectedday,CustomdaysText,7,"datepickerdatebuttons");

		GUILayout.Space(0);
		selectedday= GUILayout.SelectionGrid(selectedday,days, CustomCallendar.GetLength(1),"datepickerdatebuttons");

		GUILayout.Space(5);

		GUILayout.BeginHorizontal();
		if(GUILayout.Button("Cancel",mystyle,GUILayout.MaxWidth(CallendarRectwindow.width/2)))
		{
			selectedrealday = 0;
			DatePicker = false;
		}
		if(GUILayout.Button("Select",mystyle,GUILayout.MaxWidth(CallendarRectwindow.width/2)))
		{
			string myday = days[selectedday];
			if (myday != " ")
				int.TryParse(myday,out selectedrealday);				 
			else
				selectedrealday = 1;
			
			DatePicker = false;
		}
		
		GUILayout.EndHorizontal();

		 GUI.DragWindow (new Rect (0,0, 10000, 20));
	}
	

public String[,] CreateCallendar(int month, int year)  
        {
         string[,] customcallendar = new string[6, 7];
         int secondweekfirstday = 0;
		DateTime DateTrick= new DateTime();
		DateTrick=System.Convert.ToDateTime("11/01/" + year);
		DayOfWeek firstday;
		if (DateTrick.Month==1)
		{
		firstday = System.Convert.ToDateTime("01/" + month + "/" + year).DayOfWeek;
		}
		else
		{
		firstday = System.Convert.ToDateTime(month + "/01" + "/" + year).DayOfWeek;	
		}	
		switch (firstday)
            {
                case DayOfWeek.Monday:
                    secondweekfirstday = 8;
                    customcallendar[0, 0] = "1";
                    customcallendar[0, 1] = "2";
                    customcallendar[0, 2] = "3";
                    customcallendar[0, 3] = "4";
                    customcallendar[0, 4] = "5";
                    customcallendar[0, 5] = "6";
                    customcallendar[0, 6] = "7";
                    break;
                case DayOfWeek.Tuesday:
                    secondweekfirstday = 7;
                    customcallendar[0, 0] = " ";
                    customcallendar[0, 1] = "1";
                    customcallendar[0, 2] = "2";
                    customcallendar[0, 3] = "3";
                    customcallendar[0, 4] = "4";
                    customcallendar[0, 5] = "5";
                    customcallendar[0, 6] = "6";
                    break;
                case DayOfWeek.Wednesday:
                    secondweekfirstday = 6;
                    customcallendar[0, 0] = " ";
                    customcallendar[0, 1] = " ";
                    customcallendar[0, 2] = "1";
                    customcallendar[0, 3] = "2";
                    customcallendar[0, 4] = "3";
                    customcallendar[0, 5] = "4";
                    customcallendar[0, 6] = "5";
                    break;
                case DayOfWeek.Thursday:
			        secondweekfirstday = 5;
                    customcallendar[0, 0] = " ";
                    customcallendar[0, 1] = " ";
                    customcallendar[0, 2] = " ";
                    customcallendar[0, 3] = "1";
                    customcallendar[0, 4] = "2";
                    customcallendar[0, 5] = "3";
                    customcallendar[0, 6] = "4";
                    break;
                case DayOfWeek.Friday:
                    secondweekfirstday = 4;
                    customcallendar[0, 0] = " ";
                    customcallendar[0, 1] = " ";
                    customcallendar[0, 2] = " ";
                    customcallendar[0, 3] = " ";
                    customcallendar[0, 4] = "1";
                    customcallendar[0, 5] = "2";
                    customcallendar[0, 6] = "3";
                    break;
                case DayOfWeek.Saturday:
                    secondweekfirstday = 3;
                    customcallendar[0, 0] = " ";
                    customcallendar[0, 1] = " ";
                    customcallendar[0, 2] = " ";
                    customcallendar[0, 3] = " ";
                    customcallendar[0, 4] = " ";
                    customcallendar[0, 5] = "1";
                    customcallendar[0, 6] = "2";
                    break;
                case DayOfWeek.Sunday:
                    secondweekfirstday = 2;
                    customcallendar[0, 0] = " ";
                    customcallendar[0, 1] = " ";
                    customcallendar[0, 2] = " ";
                    customcallendar[0, 3] = " ";
                    customcallendar[0, 4] = " ";
                    customcallendar[0, 5] = " ";
                    customcallendar[0, 6] = "1";
                    break;
            }
            int daysInMonth = System.DateTime.DaysInMonth(currentyear, currentmonth);

            for (int i = 1; i < 6; ++i)
            {

                for (int j = 0; j < 7; ++j)
                {
                    if (secondweekfirstday <= daysInMonth)
                    {
                        customcallendar[i, j] = secondweekfirstday.ToString();
                        secondweekfirstday += 1;
                    }
                    else
                    {
                        customcallendar[i, j] = " ";
                    }

                }// j for
            }// i for
         return customcallendar;
        }
}