using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class NativeToolkitExample : MonoBehaviour {

	public Text console;
	public Texture2D texture;

	string imagePath = "";

	void Start()
	{
		NativeToolkit.StartLocation();
		console.text +=  "\nDevice country: " + NativeToolkit.GetCountryCode ();
	}
	
	void OnEnable ()
	{
		NativeToolkit.OnScreenshotSaved += ScreenshotSaved;	
		NativeToolkit.OnImageSaved += ImageSaved;
		NativeToolkit.OnImagePicked += ImagePicked;
		NativeToolkit.OnCameraShotComplete += CameraShotComplete;
	}

	void OnDisable ()
	{
		NativeToolkit.OnScreenshotSaved -= ScreenshotSaved;	
		NativeToolkit.OnImageSaved -= ImageSaved;
		NativeToolkit.OnImagePicked -= ImagePicked;
		NativeToolkit.OnCameraShotComplete -= CameraShotComplete;
	}

	//=============================================================================
	// Button handlers
	//=============================================================================

	public void OnSaveScreenshotPress()
	{
		NativeToolkit.SaveScreenshot("MyScreenshot", "MyScreenshotFolder", "jpeg");
	}

	public void OnSaveImagePress()
	{
		NativeToolkit.SaveImage(texture, "MyImage", "png");
	}

	public void OnPickImagePress()
	{
		NativeToolkit.PickImage();
	}

	public void OnEmailSharePress()
	{
		NativeToolkit.SendEmail("Hello there", "This is an email sent from my App!", imagePath, "", "", "");
	}

	public void OnCameraPress()
	{
		NativeToolkit.TakeCameraShot();
	}

	public void OnShowDialogPress()
	{
		NativeToolkit.ShowConfirm("Native Toolkit", "This is a pop up dialog!", DialogFinished);
	}

	public void OnShowAlertPress()
	{
		NativeToolkit.ShowAlert("Native Toolkit", "This is a pop up alert!", DialogFinished);
	}

	public void OnLocalNotificationPress()
	{
		NativeToolkit.ShowLocalNotification("Hello there", "This is a local notification!", 0);
	}

	public void OnClearNotificationsPress()
	{
		NativeToolkit.ClearLocalNotifications ();
	}

	public void OnGetLocationPress()
	{
		console.text += "\nLongitude: " + NativeToolkit.GetLongitude ().ToString ();
		console.text += "\nLatitude: " + NativeToolkit.GetLatitude ().ToString ();
	}

	public void OnRateAppPress()
	{
		NativeToolkit.RateApp ("Rate This App", "Please take a moment to rate this App", "Rate Now", "Later", "No, Thanks", "343200656", AppRated);
	}

	//=============================================================================
	// Callbacks
	//=============================================================================

	void ScreenshotSaved(string path)
	{
		console.text += "\n" + "Screenshot saved to: " + path;
	}
	
	void ImageSaved(string path)
	{
		console.text += "\n" + texture.name + " saved to: " + path;
	}

	void ImagePicked(Texture2D texture, string path)
	{
		imagePath = path;
		console.text += "\nImage picked at: " + imagePath;
	}

	void CameraShotComplete(Texture2D texture, string path)
	{
		imagePath = path;
		console.text += "\nCamera shot saved to: " + imagePath;
	}

	void DialogFinished(bool result)
	{
		console.text += "\nDialog returned: " + result;
	}

	void AppRated(string result)
	{
		console.text += "\nRate this app result: " + result;
	}
}