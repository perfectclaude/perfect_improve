{
"Cirmeca_TempsEnMarche"				:"SERVERCIRMECATempsMarche",
"Cirmeca_TempsEnDefaut"				:"SERVERCIRMECATempsDefaut",
"Cirmeca_PiecesBonnes"				:"SERVERCIRMECACompteurPieces",
"Cirmeca_CoupuresBarrieres"			:"SERVERCIRMECACompteurBarriere",
"Robot_TempsEnMarche"				:"SERVERROBOTTempsMarche",
"Robot_TempsEnDefaut"				:"SERVERROBOTTempsDefaut",
"Robot_PiecesBonnes"				:"SERVERROBOTCompteurPiecesBonnes",
"Robot_PiecesMauvaises"				:"SERVERROBOTCompteurPiecesMauvaise",
"Stocks_affecte_ProduitsFinis"		:"SQL:StockPF.query",
"Stocks_affecte_Manoeuvres"			:"SQL:StockMang.query",
"Stocks_non_affecte_ProduitsFinis"	:"SQL:StockAffectePF.query",
"Stocks_non_affecte_Manoeuvres"		:"SQL:StockAffecteMang.query",
"Tests_TauxAirEau"					:"SQL:AirEau.query"
}
